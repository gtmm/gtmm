---
id: api
title: API
sidebar_label: API
---

## `GtmmData`

Main/container object in package.

### Options

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| ff   | f    | f       | f           |

## Lower-level objects

### `FredMetadata`

### `PackLunr`

_Example._

```js
var PackLunr = require("./build/src/PackLunr.js").default;
var C = require("./build/src/constants.js");

var PL = new PackLunr(C.DEFAULT_DATA_PATH, C.DEFAULT_CACHE_PATH);

// Compile all data types
// (Assumes that FredMetadata exists in cache path).
// Takes about 1 minute.
var tic = new Date();
PL.all()
  .then(out => console.log(`Done (${(Date.now() - tic) / 1000}s.)`))
  .catch(err => {
    throw err;
  });
```

### `PackNedb`

### `SearchMetadata`

## Constants

### `DEFAULT_API_KEY`

FRED API Key used to access API. May not exist.

_Types._ `string | null`

_Definition._

```js
const DEFAULT_API_KEY = process.env.FRED_KEY;
```

### `DEFAULT_DATA_PATH`

Default path where long-term data is stored.

_Types._ `string`

### `DEFAULT_CACHE_PATH`

Default path where short-term/temporary data is stored.

_Types._ `string`

### `SERIES_FREQ`

Defines frequencies in FRED data that are applicable to package.

_Types._ `object`

### `FRED_DATA_TYPES`

Defines the data types that are found in FRED metadata.

_Types._ `array`
