---
id: usage
title: Usage
sidebar_label: Usage
---

## Create an Instance

```js
// var GtmmData = require("@gtmm/data").default;
var GtmmData = require("./build/src/index.js").default;

var gd = new GtmmData();
```

## Fetch MetaData

When you first use this package, you'll need to fetch metadata from FRED.

> **Warning.** This can take a long time!

Once you do this once, you'll probably only need to do it once every few months as data series on FRED are updated.

```js
gd.fetchMetadata().then(() => console.log("done."));
```

## Initializing the databases

In order to carry out any queries or get data, you need to connect to each of the databases that they reference.

```js
gd.init().the(() => console.log("done"));
```

You need to ensure that your metadata is in place before making this call (i.e. by calling `gd.fetchMetadata()` at some point in the past). If the method can't find the necessary files it will raise an error.

> **Warning.** This can be a non-trivial blocking call.

## Search Metadata

The `GtmmData` object has an attribute, `search`, that has methods for searching series, categories, releases, sources, and tags.

Here is a basic search over series:

```js
gd.search.series("EMP");
```

### Parameters and output modes

There are three parameters that can be used in these queries:

- `query`. (`string`) Terms to search for. See the lunr.js [docs](https://lunrjs.com/guides/searching.html) for full information about search strings.
- `filter`. (`object|null`) An [`nedb`](https://github.com/louischatriot/nedb#finding-documents) filter for narrowing down search results.
- `limit`. (`number|null`) A limit on the number of results to return. This can help to cut down on search time substantially.

### Further Examples

## Download Data

## Use data

## Check for data updates

```js
var config = require("./build/src/config").default;
config.set("lastUpdated", new Date());

GD.checkForUpdates().then(out => {
  console.log("Updated series:", out);
});
```

> ⚠ **WARNING.** Right now this method is only returning 1000 results for reasons that I can't figure out.
