---
id: databases
title: Databases
sidebar_label: Databases
---

Organization of `@gtmm/data` databases.

## FRED Data

All FRED data is stored in a SQLITE database. It is meant to be compatible with the Python package [DsemmPandas](https://github.com/michaelwooley/DsemmPandas).

## FRED Metadata
