---
id: index
title: @gtmm/data
sidebar_label: @gtmm/data Intro
---

The main point of this package is to make it easy to:

- **Download** data and metadata from FRED;
- **Organize** the data on disk;
- **Search** the metadata; and
- **Retrieve** relevant data and metadata simply.

## API Keys

In order to use this package you will need a free API key from [FRED](https://fred.stlouisfed.org/).

[**Download a key**](https://research.stlouisfed.org/docs/api/api_key.html)

### Default keys

For purposes of testing and ease of use, the package searches for a default API key in `process.env.FRED_KEY`. You can set this variable on a local machine by making an environment variable.

#### Instructions for setting an environment variable

- [Windows](https://www.computerhope.com/issues/ch000549.htm)
- [Mac](https://apple.stackexchange.com/questions/106778/how-do-i-set-environment-variables-on-os-x)

## Search

Searching for data should do two things:

1. Find series, release, category, or tag that matches the query.
1. Return relevant information about the series, release, category, or tag:

- _Series._ What is the full name, category, dates of availability, etc.?
- _Release._ What are the series related to it? What is the full name?
- and so on...

The first step can be accomplished with lunr. However, lunr doesn't return the record of the object that it searches. Instead, it returns the reference to the document that contained the search term. Moreover, it is not possible to

Thus, to do the second step there is a need to reference another source.

## Limitations

In order for this package to handle and deliver results fast, some decisions had to be made about what is and is not important, etc.

### No tables (yet)

FRED has the concept of a "table", which is used in cases where a data release has explicit tables. See, e.g., the ["Employment Situation"](https://fred.stlouisfed.org/release/tables?rid=50) release.

While we can construct these objects with FRED, they are rather more complicated than other objects. Therefore, we have elected to exclude them in this initial version of the package.

> This should be added in future versions.

### No Annual Series

There are more than 300k annual series in FRED. I believe that most of these are non-annual series that have annual analogs (e.g. quarterly GDP growth v. annual GDP growth).

Having this many series becomes problematic when we want to have a lunr index that sits in memory.

Therefore, we decided to exclude these series from the index.

See this blog [post](blog/2018/12/28/series-tokenization-lunr/) for more information on this decision.

What happens if someone tries to download an annual series? This is addressed in [#11](https://gitlab.com/gtmm/gtmm/issues/11).

### Some edges are not downloaded

We found that the number of calls to the API to fetch some types of linkages was prohibitive in terms of time. These are:

1. Related tag edges
1. Tag ↔ Series edges

There are two methods for carrying out these calls in `FredMetadata`. However, they are currently marked as deprecated and a warning will pop up if a user attempts to call them manually.

> Future versions should make it easy to download one set of linkages at a time (piecemeal). This will involve a fred API call followed by an update to the NEDB database.

## Testing

Tests were carried out in jest.

In order to run the tests you should set your API key to be an environment variable (see above). Then, from the `@gtmm/data` directory run:

```bash
yarn test
```

_To do._ Allow tests to be run with lerna from the parent directory.

## FRED Data Organization

Interlinkages between the data are depicted in the figure:

![FRED API Interlinkages](/img/docs/packages/data/fred-api-interlinkages.svg)
