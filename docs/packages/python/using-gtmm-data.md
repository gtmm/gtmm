---
id: using-gtmm-data
title: Using GtmmData
sidebar_label: GtmmData
---

<div>
<style>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
</div>

This tutorial provides a walk-through of the `GtmmData` python package. This package can be used to:

1. Download time series data from different sources (in particular, [FRED](https://fred.stlouisfed.org/)).
1. Transform data
1. Prepare data for use in a model.

## Organization of the package

There is one "abstract" object, `BaseData`, which provides methods for transforming, aggregating, and balancing data.

The `FredData` object is a child of `BaseData`. Besides the `BaseData` methods, it provides capabilities for downloading data from the `FRED` database.

```python
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import GtmmData
```

## Using `FredData`

To download data with `FredData` you'll need a FRED API key, which can be acquired for free from the [FRED API website](https://research.stlouisfed.org/useraccount/login/secure/).

I keep my key in an environment variable:

```python
FRED_KEY = os.environ['FRED_KEY']
```

```python
fd = GtmmData.FredData(apiKey=FRED_KEY)
```

### Downloading data

We can download data from FRED with the `getFredData` method. For full details of this call, see the API docs.

```python
series_to_download = ['A191RL1Q225SBEA', 'CPIAUCSL', 'INDPRO', 'DGS10', 'DEXUSEU']

# Returns the data both as a Frame and stores it in the attribute "data"
df = fd.getFredData(ids=series_to_download, params={}, append=True, getMetadata=True, apiKey=None)
```

Once we do this we have a full set of data and metadata:

```python
fd.data.tail()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A191RL1Q225SBEA</th>
      <th>CPIAUCSL</th>
      <th>INDPRO</th>
      <th>DGS10</th>
      <th>DEXUSEU</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2019-01-29</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.72</td>
      <td>1.1424</td>
    </tr>
    <tr>
      <th>2019-01-30</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.70</td>
      <td>1.1418</td>
    </tr>
    <tr>
      <th>2019-01-31</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.63</td>
      <td>1.1454</td>
    </tr>
    <tr>
      <th>2019-02-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.70</td>
      <td>1.1474</td>
    </tr>
    <tr>
      <th>2019-02-04</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.73</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>

We've also downloaded relevant metadata to go along with the data:

```python
fd.metadata
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fred_freq</th>
      <th>last_updated</th>
      <th>notes</th>
      <th>observation_end</th>
      <th>observation_start</th>
      <th>realtime_end</th>
      <th>realtime_start</th>
      <th>SA</th>
      <th>seasonal_adjustment_short</th>
      <th>title</th>
      <th>units</th>
      <th>freq</th>
      <th>transforms</th>
      <th>aggFunc</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A191RL1Q225SBEA</th>
      <td>Q</td>
      <td>2018-12-21 13:51:02</td>
      <td>BEA Account Code: A191RL\n\n \n \nGross domest...</td>
      <td>2018-07-01</td>
      <td>1947-04-01</td>
      <td>2019-02-05</td>
      <td>2019-02-05</td>
      <td>Seasonally Adjusted Annual Rate</td>
      <td>SAAR</td>
      <td>Real Gross Domestic Product</td>
      <td>% Chg. from Preceding Period</td>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>CPIAUCSL</th>
      <td>M</td>
      <td>2019-01-11 13:51:02</td>
      <td>The Consumer Price Index for All Urban Consume...</td>
      <td>2018-12-01</td>
      <td>1947-01-01</td>
      <td>2019-02-05</td>
      <td>2019-02-05</td>
      <td>Seasonally Adjusted</td>
      <td>SA</td>
      <td>Consumer Price Index for All Urban Consumers: ...</td>
      <td>Index 1982-1984=100</td>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>INDPRO</th>
      <td>M</td>
      <td>2019-01-18 14:31:02</td>
      <td>The Industrial Production Index (INDPRO) is an...</td>
      <td>2018-12-01</td>
      <td>1919-01-01</td>
      <td>2019-02-05</td>
      <td>2019-02-05</td>
      <td>Seasonally Adjusted</td>
      <td>SA</td>
      <td>Industrial Production Index</td>
      <td>Index 2012=100</td>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>DGS10</th>
      <td>D</td>
      <td>2019-02-05 21:21:25</td>
      <td>For further information regarding treasury con...</td>
      <td>2019-02-04</td>
      <td>1962-01-02</td>
      <td>2019-02-05</td>
      <td>2019-02-05</td>
      <td>Not Seasonally Adjusted</td>
      <td>NSA</td>
      <td>10-Year Treasury Constant Maturity Rate</td>
      <td>%</td>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>DEXUSEU</th>
      <td>D</td>
      <td>2019-02-05 21:33:53</td>
      <td>Noon buying rates in New York City for cable t...</td>
      <td>2019-02-01</td>
      <td>1999-01-04</td>
      <td>2019-02-05</td>
      <td>2019-02-05</td>
      <td>Not Seasonally Adjusted</td>
      <td>NSA</td>
      <td>U.S. / Euro Foreign Exchange Rate</td>
      <td>U.S. $ to  1 Euro</td>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
  </tbody>
</table>
</div>

Alternatively, `metadata_small` tells us more to-the-point details:

```python
fd.metadata_small
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>freq</th>
      <th>transforms</th>
      <th>aggFunc</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A191RL1Q225SBEA</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>CPIAUCSL</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>INDPRO</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>DGS10</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>DEXUSEU</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
  </tbody>
</table>
</div>

### Transforms

We can transform the data in any one of th

| Transform | Description                                   | Forward? | Inverse? |
| --------- | --------------------------------------------- | -------- | -------- |
| lin       | Levels (No transformation)                    | ✔        | ✔        |
| chg       | Change                                        | ✔        | ✔        |
| ch1       | Change from Year Ago                          | ❌       | ❌       |
| pch       | Percent Change                                | ❌       | ❌       |
| pc1       | Percent Change from Year Ago                  | ❌       | ❌       |
| pca       | Compounded Annual Rate of Change              | ❌       | ❌       |
| cch       | Continuously Compounded Rate of Change        | ❌       | ❌       |
| cca       | Continuously Compounded Annual Rate of Change | ❌       | ❌       |
| log       | Natural Log                                   | ❌       | ❌       |

<div class='warning'>
    <b>WARNING.</b> (2/5/19) Forward transforms for transforms other than <code>lin</code> and <code>log</code> have not yet been implemented. Trying to do a transform on data of this class will result in a `NotImplementedError`.
</div>

#### Setting the transforms for the data

There are several ways to set the transforms for each variable:

```python
# Make all columns have linear transform
fd.setTrans(None, 'lin')
# One at a time:
fd.setTrans('INDPRO', 'lin')
# Many at once with same transform:
fd.setTrans(['DEXUSEU','DGS10'], 'log')
# Many at once with different transforms:
fd.setTrans({'A191RL1Q225SBEA': 'lin', 'CPIAUCSL': 'log'})
```

This changes the metadata and some internal settings...

```python
fd.metadata_small
```

<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>freq</th>
      <th>transforms</th>
      <th>aggFunc</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A191RL1Q225SBEA</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>CPIAUCSL</th>
      <td>None</td>
      <td>log</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>INDPRO</th>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>DGS10</th>
      <td>None</td>
      <td>log</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>DEXUSEU</th>
      <td>None</td>
      <td>log</td>
      <td>avg</td>
    </tr>
  </tbody>
</table>
</div>

...but they do not change the data (yet):

```python
assert fd.data.equals(df)
```

#### Getting transformed data

To actually transform the data we we call a constructor method like `transformData` or `format`.

For now, let's see how transformData works:

```python
df2 = fd.transformData()
```

This method creates a new dataframe that has the relevant transforms while keeping the data in the object unchanged:

```python
assert ((df2['DEXUSEU'] - fd.data['DEXUSEU'].apply(np.log)) ** 2.).max() < 1.e-9
```

You can also use this method on other datasets (e.g. forecasts) that have the same structure as the underlying data:

```python
# Create some "sample data" that has same form as base data
df_ex = pd.DataFrame(np.exp(np.random.randn(25, 2)), columns=['INDPRO','DEXUSEU'])

# Transform it as would transform base data
df_ex2 = fd.transformData(df_ex)

# Check to ensure that transforms occurred.
assert df_ex2['DEXUSEU'].equals(df_ex['DEXUSEU'].apply(np.log))
```

#### Backwards transforms of data

Suppose that we use the data above to estimate a model and generate some forecasts. If we estimated the model with versions of the base data that transformed, then we might want to get our forecasts back to their original form.

This can be achieved by calling `transformData` with `forward=False`.

```python
df_ex3 = fd.transformData(df_ex2, forward=False)

assert df_ex3.equals(df_ex)
```

<style>
    .warning {
    background-color:rgba(255,0,0,0.25);
    border-left: 3px solid rgba(255,0,0,0.75);
    padding: 10px;
    }
</style>

<div class='warning'>
<b>WARNING.</b> (2/5/19) Inverse transforms for transforms other than `lin` and `log` have not yet been implemented. Trying to do a reverse transform on data of this class will result in a `NotImplementedError`.
</div>

### Aggregating Data to Coarser Frequencies

We can aggregate similarly to the way that we tranform:

```python
# One at a time:
fd.setAgg('A191RL1Q225SBEA', freq='A', aggFunc='avg')
# Many at once with same transform:
fd.setAgg(['DEXUSEU','INDPRO'], 'q', 'eop')
# Many at once with different aggregations:
fd.setAgg({'CPIAUCSL': {'freq':'Q', 'aggFunc': 'SUM'}, 'DGS10': {'freq':'m', 'aggFunc': 'AVG'}})
```

```python
fd.metadata_small
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>freq</th>
      <th>transforms</th>
      <th>aggFunc</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A191RL1Q225SBEA</th>
      <td>A</td>
      <td>lin</td>
      <td>AVG</td>
    </tr>
    <tr>
      <th>CPIAUCSL</th>
      <td>Q</td>
      <td>log</td>
      <td>SUM</td>
    </tr>
    <tr>
      <th>INDPRO</th>
      <td>Q</td>
      <td>lin</td>
      <td>EOP</td>
    </tr>
    <tr>
      <th>DGS10</th>
      <td>M</td>
      <td>log</td>
      <td>AVG</td>
    </tr>
    <tr>
      <th>DEXUSEU</th>
      <td>Q</td>
      <td>log</td>
      <td>EOP</td>
    </tr>
  </tbody>
</table>
</div>

Again, nothing happens until we call the explicit constructor method:

```python
ff = fd.aggregateData(d=None, freq=None, aggFunc=None, balanced=False)
ff.tail()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A191RL1Q225SBEA</th>
      <th>CPIAUCSL</th>
      <th>INDPRO</th>
      <th>DGS10</th>
      <th>DEXUSEU</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2018-10-01</th>
      <td>NaN</td>
      <td>758.436</td>
      <td>109.9487</td>
      <td>3.152273</td>
      <td>1.1456</td>
    </tr>
    <tr>
      <th>2018-11-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3.117000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2018-12-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.832632</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2019-01-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.713810</td>
      <td>1.1474</td>
    </tr>
    <tr>
      <th>2019-02-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.715000</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>

### `format`

The `format` method is the main workhorse of the package. The basic idea is to carry out all transforms and aggregations in one foul swoop.

`format` has the following steps:

1. Apply transforms to the data
1. Aggregate the data according to the frequency and balance conditions.
1. Round the transformed data to significant figures.
1. Remove empty rows
1. Trim the data according to the trim arguments.
1. Check to ensure that there are no gaps in the data.

The ordering of these steps is somewhat important as different results will arise if steps 1 & 2 are switched. It is not necessarily "wrong" to put step 2 before step 1; this is just a choice that was made.

```python
fd2 = fd.format(balanced=True, freq='coarse',trim=None)
fd2.data
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A191RL1Q225SBEA</th>
      <th>CPIAUCSL</th>
      <th>INDPRO</th>
      <th>DGS10</th>
      <th>DEXUSEU</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1999-04-01</th>
      <td>3.1</td>
      <td>15.34</td>
      <td>91.1990</td>
      <td>1.71</td>
      <td>0.0305</td>
    </tr>
    <tr>
      <th>1999-07-01</th>
      <td>5.3</td>
      <td>15.36</td>
      <td>91.7812</td>
      <td>1.77</td>
      <td>0.0623</td>
    </tr>
    <tr>
      <th>1999-10-01</th>
      <td>7.0</td>
      <td>15.38</td>
      <td>94.1555</td>
      <td>1.82</td>
      <td>0.0070</td>
    </tr>
    <tr>
      <th>2000-01-01</th>
      <td>1.5</td>
      <td>15.41</td>
      <td>94.7926</td>
      <td>1.87</td>
      <td>-0.0435</td>
    </tr>
    <tr>
      <th>2000-04-01</th>
      <td>7.5</td>
      <td>15.43</td>
      <td>95.7364</td>
      <td>1.82</td>
      <td>-0.0466</td>
    </tr>
    <tr>
      <th>2000-07-01</th>
      <td>0.5</td>
      <td>15.46</td>
      <td>95.6844</td>
      <td>1.77</td>
      <td>-0.1236</td>
    </tr>
    <tr>
      <th>2000-10-01</th>
      <td>2.5</td>
      <td>15.48</td>
      <td>95.1619</td>
      <td>1.72</td>
      <td>-0.0632</td>
    </tr>
    <tr>
      <th>2001-01-01</th>
      <td>-1.1</td>
      <td>15.51</td>
      <td>93.7253</td>
      <td>1.62</td>
      <td>-0.1285</td>
    </tr>
    <tr>
      <th>2001-04-01</th>
      <td>2.4</td>
      <td>15.53</td>
      <td>92.3317</td>
      <td>1.66</td>
      <td>-0.1656</td>
    </tr>
    <tr>
      <th>2001-07-01</th>
      <td>-1.7</td>
      <td>15.54</td>
      <td>91.3345</td>
      <td>1.61</td>
      <td>-0.0944</td>
    </tr>
    <tr>
      <th>2001-10-01</th>
      <td>1.1</td>
      <td>15.54</td>
      <td>90.5150</td>
      <td>1.56</td>
      <td>-0.1164</td>
    </tr>
    <tr>
      <th>2002-01-01</th>
      <td>3.5</td>
      <td>15.55</td>
      <td>91.8012</td>
      <td>1.62</td>
      <td>-0.1373</td>
    </tr>
    <tr>
      <th>2002-04-01</th>
      <td>2.4</td>
      <td>15.57</td>
      <td>93.4503</td>
      <td>1.63</td>
      <td>-0.0145</td>
    </tr>
    <tr>
      <th>2002-07-01</th>
      <td>1.8</td>
      <td>15.59</td>
      <td>93.3597</td>
      <td>1.45</td>
      <td>-0.0122</td>
    </tr>
    <tr>
      <th>2002-10-01</th>
      <td>0.6</td>
      <td>15.60</td>
      <td>93.1088</td>
      <td>1.39</td>
      <td>0.0474</td>
    </tr>
    <tr>
      <th>2003-01-01</th>
      <td>2.2</td>
      <td>15.63</td>
      <td>93.7350</td>
      <td>1.37</td>
      <td>0.0862</td>
    </tr>
    <tr>
      <th>2003-04-01</th>
      <td>3.5</td>
      <td>15.63</td>
      <td>93.2544</td>
      <td>1.28</td>
      <td>0.1399</td>
    </tr>
    <tr>
      <th>2003-07-01</th>
      <td>7.0</td>
      <td>15.65</td>
      <td>94.0695</td>
      <td>1.44</td>
      <td>0.1527</td>
    </tr>
    <tr>
      <th>2003-10-01</th>
      <td>4.7</td>
      <td>15.66</td>
      <td>94.8627</td>
      <td>1.45</td>
      <td>0.2309</td>
    </tr>
    <tr>
      <th>2004-01-01</th>
      <td>2.2</td>
      <td>15.69</td>
      <td>95.2063</td>
      <td>1.39</td>
      <td>0.2064</td>
    </tr>
    <tr>
      <th>2004-04-01</th>
      <td>3.1</td>
      <td>15.71</td>
      <td>95.6045</td>
      <td>1.52</td>
      <td>0.1971</td>
    </tr>
    <tr>
      <th>2004-07-01</th>
      <td>3.8</td>
      <td>15.73</td>
      <td>96.4943</td>
      <td>1.46</td>
      <td>0.2165</td>
    </tr>
    <tr>
      <th>2004-10-01</th>
      <td>4.1</td>
      <td>15.76</td>
      <td>98.3284</td>
      <td>1.43</td>
      <td>0.3029</td>
    </tr>
    <tr>
      <th>2005-01-01</th>
      <td>4.5</td>
      <td>15.78</td>
      <td>99.3156</td>
      <td>1.46</td>
      <td>0.2600</td>
    </tr>
    <tr>
      <th>2005-04-01</th>
      <td>1.9</td>
      <td>15.80</td>
      <td>99.9838</td>
      <td>1.42</td>
      <td>0.1905</td>
    </tr>
    <tr>
      <th>2005-07-01</th>
      <td>3.6</td>
      <td>15.84</td>
      <td>98.0824</td>
      <td>1.44</td>
      <td>0.1871</td>
    </tr>
    <tr>
      <th>2005-10-01</th>
      <td>2.6</td>
      <td>15.87</td>
      <td>100.9458</td>
      <td>1.50</td>
      <td>0.1691</td>
    </tr>
    <tr>
      <th>2006-01-01</th>
      <td>5.4</td>
      <td>15.89</td>
      <td>101.2742</td>
      <td>1.52</td>
      <td>0.1938</td>
    </tr>
    <tr>
      <th>2006-04-01</th>
      <td>0.9</td>
      <td>15.91</td>
      <td>101.9746</td>
      <td>1.62</td>
      <td>0.2452</td>
    </tr>
    <tr>
      <th>2006-07-01</th>
      <td>0.6</td>
      <td>15.94</td>
      <td>102.1204</td>
      <td>1.59</td>
      <td>0.2380</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2011-04-01</th>
      <td>2.9</td>
      <td>16.24</td>
      <td>96.5889</td>
      <td>1.16</td>
      <td>0.3731</td>
    </tr>
    <tr>
      <th>2011-07-01</th>
      <td>-0.1</td>
      <td>16.26</td>
      <td>97.6132</td>
      <td>0.87</td>
      <td>0.2963</td>
    </tr>
    <tr>
      <th>2011-10-01</th>
      <td>4.7</td>
      <td>16.28</td>
      <td>98.7699</td>
      <td>0.71</td>
      <td>0.2603</td>
    </tr>
    <tr>
      <th>2012-01-01</th>
      <td>3.2</td>
      <td>16.29</td>
      <td>99.1594</td>
      <td>0.71</td>
      <td>0.2877</td>
    </tr>
    <tr>
      <th>2012-04-01</th>
      <td>1.7</td>
      <td>16.30</td>
      <td>100.0478</td>
      <td>0.60</td>
      <td>0.2365</td>
    </tr>
    <tr>
      <th>2012-07-01</th>
      <td>0.5</td>
      <td>16.31</td>
      <td>99.8944</td>
      <td>0.49</td>
      <td>0.2512</td>
    </tr>
    <tr>
      <th>2012-10-01</th>
      <td>0.5</td>
      <td>16.33</td>
      <td>100.9267</td>
      <td>0.53</td>
      <td>0.2766</td>
    </tr>
    <tr>
      <th>2013-01-01</th>
      <td>3.6</td>
      <td>16.34</td>
      <td>101.8186</td>
      <td>0.67</td>
      <td>0.2481</td>
    </tr>
    <tr>
      <th>2013-04-01</th>
      <td>0.5</td>
      <td>16.34</td>
      <td>101.9486</td>
      <td>0.68</td>
      <td>0.2631</td>
    </tr>
    <tr>
      <th>2013-07-01</th>
      <td>3.2</td>
      <td>16.36</td>
      <td>102.6774</td>
      <td>0.99</td>
      <td>0.3027</td>
    </tr>
    <tr>
      <th>2013-10-01</th>
      <td>3.2</td>
      <td>16.37</td>
      <td>103.1747</td>
      <td>1.01</td>
      <td>0.3206</td>
    </tr>
    <tr>
      <th>2014-01-01</th>
      <td>-1.0</td>
      <td>16.39</td>
      <td>104.5893</td>
      <td>1.02</td>
      <td>0.3204</td>
    </tr>
    <tr>
      <th>2014-04-01</th>
      <td>5.1</td>
      <td>16.40</td>
      <td>105.4084</td>
      <td>0.96</td>
      <td>0.3141</td>
    </tr>
    <tr>
      <th>2014-07-01</th>
      <td>4.9</td>
      <td>16.41</td>
      <td>105.7908</td>
      <td>0.92</td>
      <td>0.2333</td>
    </tr>
    <tr>
      <th>2014-10-01</th>
      <td>1.9</td>
      <td>16.40</td>
      <td>106.5032</td>
      <td>0.82</td>
      <td>0.1907</td>
    </tr>
    <tr>
      <th>2015-01-01</th>
      <td>3.3</td>
      <td>16.38</td>
      <td>105.0856</td>
      <td>0.68</td>
      <td>0.0715</td>
    </tr>
    <tr>
      <th>2015-04-01</th>
      <td>3.3</td>
      <td>16.40</td>
      <td>103.6891</td>
      <td>0.77</td>
      <td>0.1092</td>
    </tr>
    <tr>
      <th>2015-07-01</th>
      <td>1.0</td>
      <td>16.41</td>
      <td>103.7281</td>
      <td>0.80</td>
      <td>0.1099</td>
    </tr>
    <tr>
      <th>2015-10-01</th>
      <td>0.4</td>
      <td>16.42</td>
      <td>102.2696</td>
      <td>0.78</td>
      <td>0.0824</td>
    </tr>
    <tr>
      <th>2016-01-01</th>
      <td>1.5</td>
      <td>16.41</td>
      <td>101.5415</td>
      <td>0.65</td>
      <td>0.1302</td>
    </tr>
    <tr>
      <th>2016-04-01</th>
      <td>2.3</td>
      <td>16.44</td>
      <td>101.9476</td>
      <td>0.56</td>
      <td>0.0982</td>
    </tr>
    <tr>
      <th>2016-07-01</th>
      <td>1.9</td>
      <td>16.45</td>
      <td>101.9304</td>
      <td>0.45</td>
      <td>0.1167</td>
    </tr>
    <tr>
      <th>2016-10-01</th>
      <td>1.8</td>
      <td>16.47</td>
      <td>102.7877</td>
      <td>0.75</td>
      <td>0.0537</td>
    </tr>
    <tr>
      <th>2017-01-01</th>
      <td>1.8</td>
      <td>16.49</td>
      <td>102.7236</td>
      <td>0.89</td>
      <td>0.0675</td>
    </tr>
    <tr>
      <th>2017-04-01</th>
      <td>3.0</td>
      <td>16.49</td>
      <td>103.7710</td>
      <td>0.82</td>
      <td>0.1320</td>
    </tr>
    <tr>
      <th>2017-07-01</th>
      <td>2.8</td>
      <td>16.51</td>
      <td>103.1760</td>
      <td>0.81</td>
      <td>0.1666</td>
    </tr>
    <tr>
      <th>2017-10-01</th>
      <td>2.3</td>
      <td>16.53</td>
      <td>105.7698</td>
      <td>0.86</td>
      <td>0.1842</td>
    </tr>
    <tr>
      <th>2018-01-01</th>
      <td>2.2</td>
      <td>16.56</td>
      <td>106.4488</td>
      <td>1.01</td>
      <td>0.2086</td>
    </tr>
    <tr>
      <th>2018-04-01</th>
      <td>4.2</td>
      <td>16.57</td>
      <td>107.4443</td>
      <td>1.07</td>
      <td>0.1550</td>
    </tr>
    <tr>
      <th>2018-07-01</th>
      <td>3.4</td>
      <td>16.58</td>
      <td>108.9362</td>
      <td>1.07</td>
      <td>0.1503</td>
    </tr>
  </tbody>
</table>
<p>78 rows × 5 columns</p>
</div>

`format` returns another `FredData` (or, more generally, `BaseData`) object.

You can use the `data` attribute from the returned object as a balanced dataset at the coarsest frequency should be ready to go for a VAR model.
