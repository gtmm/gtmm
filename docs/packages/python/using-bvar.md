---
id: bvar-usage
title: Using BVAR
sidebar_label: Using BVAR
---

<style>
.todo {
    background-color: #f6f8fa;
    border-left: 2px solid #d1d5da;
    margin: 10px 0px;
    padding:10px;
}
.title {
    font-size: 1.3em;
    font-weight: bold;
}    
.warning {
  background-color:rgba(255,0,0,0.25);
  border-left: 3px solid rgba(255,0,0,0.75);
  padding: 10px;
  margin:10px 0px;
}
</style>

## Top

This tutorial provides a walk-through of the `bvar` python package. This package can be used to estimate and forecast bayesian vector autoregressions.

We'll also demonstrate use cases for the `GtmmData` package.

```python
%reload_ext autoreload
%autoreload 2
%matplotlib inline
%config InlineBackend.figure_format = 'retina'
```

```python
import os
import tempfile
import pickle

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import GtmmData
import bvar
```

```python
sns.set_style('whitegrid')
sns.set_palette('deep')

FRED_KEY = os.environ['FRED_KEY']
FORCE_RELOAD = True
BASE_PATH = os.path.join(tempfile.gettempdir(), 'bvar-usage')
```

## A 3-variable model

We'll start out with a standard three variable model that includes:

- **CPI-U**. Price index
- **Effective Federal Funds Rate**. Nominal interest rate
- **Nonfarm Payrolls**. A proxy/measure of output/fluctuations

We choose these variables to duplicate the version of the three variable specification in Banbura, Giannone, and Reichlin (2012). They choose these variables (in particular, nonfarm payrolls rather than GDP) because of their monthly frequency.

```python
model_name = '3-var'
series = ['CPIAUCSL', 'FEDFUNDS', 'PAYEMS']
```

```python
model_base_path = os.path.join(BASE_PATH, model_name)
data_path = os.path.join(model_base_path, 'data.pkl')
model_path = os.path.join(model_base_path, 'model.pkl')
if not os.path.exists(model_base_path):
    os.makedirs(model_base_path)
```

### Data

```python
if not os.path.exists(data_path) or FORCE_RELOAD:
  fred_data0 = GtmmData.FredData(apiKey=FRED_KEY)
  fred_data0.getFredData(ids=series) # Download data
  fred_data0.setTrans(['CPIAUCSL', 'PAYEMS'], 'log') # Make these variables logs
#   fred_data0.setTrans('FEDFUNDS', 'log') # Try this out
  fred_data = fred_data0.format(balanced=True, freq='coarse',trim=[pd.Timestamp('1959-01-01'), pd.Timestamp('2019-01-01', freq='M')]) # Format it
  fred_data.to_pickle(data_path)
else:
  fred_data = GtmmData.read_pickle(data_path)
```

```python
fig, ax = plt.subplots(1,len(series),figsize=(12,4))
fred_data.data.plot(subplots=True, ax=ax)
```

    array([<matplotlib.axes._subplots.AxesSubplot object at 0x000000000D85C390>,
           <matplotlib.axes._subplots.AxesSubplot object at 0x000000000C49B7B8>,
           <matplotlib.axes._subplots.AxesSubplot object at 0x000000000C3C39B0>],
          dtype=object)

![png](https://i.imgur.com/U9VaTIT.png)

```python
fred_data.metadata
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>fred_freq</th>
      <th>last_updated</th>
      <th>notes</th>
      <th>observation_end</th>
      <th>observation_start</th>
      <th>realtime_end</th>
      <th>realtime_start</th>
      <th>SA</th>
      <th>seasonal_adjustment_short</th>
      <th>title</th>
      <th>units</th>
      <th>freq</th>
      <th>transforms</th>
      <th>aggFunc</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CPIAUCSL</th>
      <td>M</td>
      <td>2019-02-12 16:41:03</td>
      <td>The Consumer Price Index for All Urban Consume...</td>
      <td>2018-12-01</td>
      <td>1947-01-01</td>
      <td>2019-02-12</td>
      <td>2019-02-12</td>
      <td>Seasonally Adjusted</td>
      <td>SA</td>
      <td>Consumer Price Index for All Urban Consumers: ...</td>
      <td>Index 1982-1984=100</td>
      <td>None</td>
      <td>log</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>FEDFUNDS</th>
      <td>M</td>
      <td>2019-02-01 21:21:03</td>
      <td>Averages of daily figures.\n\nThe federal fund...</td>
      <td>2019-01-01</td>
      <td>1954-07-01</td>
      <td>2019-02-12</td>
      <td>2019-02-12</td>
      <td>Not Seasonally Adjusted</td>
      <td>NSA</td>
      <td>Effective Federal Funds Rate</td>
      <td>%</td>
      <td>None</td>
      <td>lin</td>
      <td>avg</td>
    </tr>
    <tr>
      <th>PAYEMS</th>
      <td>M</td>
      <td>2019-02-01 15:10:35</td>
      <td>All Employees: Total Nonfarm, commonly known a...</td>
      <td>2019-01-01</td>
      <td>1939-01-01</td>
      <td>2019-02-12</td>
      <td>2019-02-12</td>
      <td>Seasonally Adjusted</td>
      <td>SA</td>
      <td>All Employees: Total Nonfarm Payrolls</td>
      <td>Thous. of Persons</td>
      <td>None</td>
      <td>log</td>
      <td>avg</td>
    </tr>
  </tbody>
</table>
</div>

### BVAR Usage

The `bvar` can be initiated by calling it, passing the data, lags, prior type, and any relevant hyperparameters at the same time:

#### Priors and hyperparameters

Four possible prior combinations are permitted:

- _Minnesota Prior._ Random walk (MN).
  - `Lambda`. "tightness" parameter. As $\lambda \rightarrow 0$ the terms in the covariance matrix of the coefficients $B$ become smaller and the prior for informative. As $\lambda \rightarrow +\infty$ the prior on the covariance terms becomes less informative.
  - `psi`. The diagonal terms of the scale matrix of the inverse-Wishart distribution. By default the $\psi$ hyperparameter is set to the empirical standard errors from AR(1). This is a common practice in the literature. This functionality can be disabled by setting `graph_opts['psi_ar1'] = False`.
  - `rw`. The diagonal terms on the mean of the coefficient matrix prior. By default, will equal 1 for all variables. Set it equal to zero for stationary series.
- _MN+Sum-of-Coefficients_. A sum-of-coefficients prior (SOC) can be stated in a few different ways. One interpretation is that it implements the idea that the means of lagged variables should be a good predictor of a variable. Another has to do with limiting cointegration between variables. One new
  - `mu`. "Shrinkage" parameter. As $\mu\rightarrow 0$ we force the model to have an "exact differences" form: $\Delta Y*t = C+B_1 \Delta Y*{t-1} + ...$. As $\mu\rightarrow +\infty$ we allow the model in levels to diverge as much as it likes to the model in differences.
- _MN+Dummy Initial Observation_. The DIO prior implements the idea that, when lagged values of $y_i$ are averaged, they should be a good forecast for $y_i$, but without any implication that there are no cross-effects among variables or that the constant term is small.
  - `delta`. As $\delta \rightarrow 0$ then models that are either stationary or that have unit roots without drift are favored.
- _MN+DIO+SOC_ A combination of all three.

The hyperparameters for the hyperpriors (discussed below) are also set to default values.

<div class='todo'>
<div class='title'>To Do</div>
Expand on the above in another document.
</div>

```python
bvs = bvar.core.BVAR(
  data=fred_data.data,
  lags=13,
  prior_spec='DIO_MN_SOC',
  hyperparameters={'Lambda': 0.45, 'mu': 0.214, 'delta': 0.124, 'dof': 14}
)
```

All of the relevant posterior objects for the baseline model are created on construction.

#### Updates

You can easily update the data, lags, prior type, or hyperparameters by calling the `update` method on the `BVAR` instance. Depending on what sort of updates are passed, appropriate parts of the "computational graph" in `bvar_graph` are updated.

```python
# One at a time
bvs.update(lags=40)

# Many at a time
bvs.update(lags=13, T0=45)

# Set T0 < 0 to use entire dataset
bvs.update(T0=-1)
```

#### Backtesting

Backtesting is a set of techniques that generalizes the set of comparisons between observed data and data predicted on a subset of the data.

##### At the mean

The simplest case involves iterative predictions at the mean. In this version we'll compute the model parameters for each period from `T0` at the mean. Of course, we'll only compute these parameters using data from before `T0`. We'll then forecast the model `n_periods` forward and compare these forecasts to the true outcomes.

<div class='todo'>
<div class='title'>To Do</div>
Add info about options.
</div>

```python
bt = bvs.backtest(T0=None, at_mean=True, hier=False, n_periods=12)
```

The returned `BackTest` object contains several methods for summarizing the results.

The `plot_errors` method plots information about the errors at different forecast horizons.

<div class='todo'>
<div class='title'>To Do</div>
Expand on explanation of chart.
</div>

```python
bt.plot_errors(metric='se', cols=None, plot_quantiles=True)
None
```

![png](https://i.imgur.com/WxocGdY.png)

Another way of understanding backtesting results is to find the periods in the data where the model performed better or worse.

It appears that there are two areas where the model performs particularly poorly:

1. The first occurred around the time of the Volcker deflation of the early 80s.
1. The second occurred around the time of the Great Recession of 2007-2009.
   <div class='todo'>
       <div class='title'>To Do: Add date labels</div>
   In future versions of the package more information about the dates will be provided in the plots. Recession shading would also be nice.
   </div>

```python
fig, axes = bt.tsplot_errors()
None
```

![png](https://i.imgur.com/wjnyxI3.png)

##### Alternative backtesting techniques

Backtesting at the mean is simple and fast because it doesn't require any sampling. However, we could also consider a more elaborate technique where we sampled from the posterior. The utility of such a feature isn't clear right now, though, because we can already characterize the posterior pretty thoroughly analytically.

#### Forecasts

Once we are satisfied with the model estimation, we can move on to creating forecasts. Two types are offered:

1. _At the mean._ Make a forecast using the mean coefficient matrix of each variable. This is the simplest and fastest forecast possible.
1. _Conditional._ In this form of forecast we specify that linear combinations of coefficients will follow certain (normal) distributions over the forecast horizon. We then forecast the other variables conditional on these restrictions.
   <div class='todo'>
       <div class='title'>To Do: Add Non-conditional Sampling</div>
   Can also sample from non-conditional posterior distribution.
   </div>

##### At the mean

```python
fam = bvs.forecast(
    n_periods = 12,
    kind = 'at_mean',
    hierarchical = False,
    script = None,
    iters = -1,
    chains = -1
  )
```

A forecast object is returned. It provides two main methods for understanding the output.

First, we can just get the forecasts in a table.

```python
ft = fam.table(cols=None,          # Columns to include. None => get all
          forecast_periods=5, # Number of forecast periods (out of n_periods) to include
          data_periods=-4,    # Number of known data periods to include (default=0)
          as_frame=True,      # If True, returns a pandas dataframe. If False, returns NumPy array
          mark_forecast=True  # If True, adds a variable "forecast" that is =1 if the period is a forecast period.
         )

ft
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CPIAUCSL</th>
      <th>FEDFUNDS</th>
      <th>PAYEMS</th>
      <th>forecast</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>716</th>
      <td>5.529469</td>
      <td>1.950000</td>
      <td>11.915553</td>
      <td>0</td>
    </tr>
    <tr>
      <th>717</th>
      <td>5.532575</td>
      <td>2.190000</td>
      <td>11.917403</td>
      <td>0</td>
    </tr>
    <tr>
      <th>718</th>
      <td>5.532440</td>
      <td>2.200000</td>
      <td>11.918711</td>
      <td>0</td>
    </tr>
    <tr>
      <th>719</th>
      <td>5.532294</td>
      <td>2.270000</td>
      <td>11.920189</td>
      <td>0</td>
    </tr>
    <tr>
      <th>720</th>
      <td>5.533372</td>
      <td>2.286391</td>
      <td>11.921453</td>
      <td>1</td>
    </tr>
    <tr>
      <th>721</th>
      <td>5.535055</td>
      <td>2.263868</td>
      <td>11.922664</td>
      <td>1</td>
    </tr>
    <tr>
      <th>722</th>
      <td>5.536837</td>
      <td>2.269340</td>
      <td>11.923894</td>
      <td>1</td>
    </tr>
    <tr>
      <th>723</th>
      <td>5.538581</td>
      <td>2.255697</td>
      <td>11.925090</td>
      <td>1</td>
    </tr>
    <tr>
      <th>724</th>
      <td>5.540216</td>
      <td>2.213761</td>
      <td>11.926256</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>

There is also a convenience method for plotting the forecasts. The arguments are the same as for the table method.

```python
fig, axes = fam.plot(cols=None, forecast_periods=None, data_periods=-10)
None
```

![png](https://i.imgur.com/jOsHJVx.png)

##### Conditional Forecasts

With a conditional forecast we have to provide a script that specifies a set of equations as well as there means and modes in a normal distribution.

This approach can be helpful when we have data for some variables for a given period but not all periods. In such a case one might set the conditional distribution for that variable in the known data period to be normal with a mean equal to the reported figure and a small variance that helps to account for revisions

<div class='todo'>
    <div class='title'>To Do: Add Syntax Tutorial</div>
Add a link to a separate tutorial outliniing the conditional forecast syntax. See the work already done in "apw_demo.ipynb".
</div>
As an example, at the time of writing (2/7/19) we know the January 2019 nonfarm employment as well as the average effective federal funds rate for January 2019. So we can incorporate these into our predictions. Our forecasts for the first period will follow the specific distributions exactly. However, the forecast for the CPI (`CPIAUCSL`) is unknown. It will be forecast incorporating the information that we already know about the other two variables. Forecasts at future periods will also be influenced by our conditioning in the first period.

In the third conditioning equation we specify that the federal funds rate will remain around 2.38% in February 2019. This is a fairly safe bet because the FOMC is not set to meet in February.

If we like we could add some more speculative conditions as well. For example, if we thought that the federal reserve followed a simple inflation-based Taylor rule of the form $r_t = 0.5 \cdot \pi_t$, then we might specify that `FEDFUNDS[+k] - 0.5 * CPIAUCSL[+k]` $\sim N(0, \sigma^2)$ at each forecast period `k`.

<div class='todo'>
    <div class='title'>To Do: Notes on Lucas Critique</div>
      Add a note on the Lucas Critique and how it pertains to things like the federal funds rate and government spending. In short, it is not smart to "force" a federal funds rate due to the responsiveness of the Fed. One way of approaching this might be to specify a monetary policy rule like that specified above. However, thought should be put into whether this is the way to go.
</div>

```python
conditional_forecast_script = '''
eqn EQ0 = PAYEMS[+1];
eqn EQ1 = FEDFUNDS[+1];
eqn EQ2 = FEDFUNDS[+2];

mean EQ0 = 11.922209936681169; // = log(15074) = Jan. 2019 Nonfarm employment
mean EQ1 = 2.40; // log(2.40) = Jan 2019 Effective Fed funds rate
mean EQ2 = 2.38; // Guessing that the fed will not change rates in February => close to constant FFNDs

std EQ0 = 1e-3;
std EQ1 = 1e-3;
std EQ2 = 1e-2;
'''
```

```python
fc = bvs.forecast(
    n_periods = 12,
    kind = 'apw',
    hierarchical = False,
    script = conditional_forecast_script,
    iters = 2000,
    chains = 4
  )
```

As with the previous forecast, we can use `table` and `plot` methods to summarize the forecast.

The API for this type of forecast is a bit more involved. The API docs provide more details.

The default arguments should provide a fine starting point, though:

```python
fc.table()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="6" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
      <th>forecast</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>...</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.533401</td>
      <td>5.531919</td>
      <td>5.532456</td>
      <td>5.533403</td>
      <td>5.534403</td>
      <td>5.534976</td>
      <td>0.002213</td>
      <td>2.400009</td>
      <td>2.399312</td>
      <td>2.399594</td>
      <td>...</td>
      <td>2.400684</td>
      <td>0.001013</td>
      <td>11.922214</td>
      <td>11.921556</td>
      <td>11.921778</td>
      <td>11.922224</td>
      <td>11.922639</td>
      <td>11.922899</td>
      <td>0.001007</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.535214</td>
      <td>5.532729</td>
      <td>5.533667</td>
      <td>5.535338</td>
      <td>5.536802</td>
      <td>5.537760</td>
      <td>0.003687</td>
      <td>2.380064</td>
      <td>2.373339</td>
      <td>2.375959</td>
      <td>...</td>
      <td>2.386629</td>
      <td>0.010058</td>
      <td>11.923582</td>
      <td>11.922230</td>
      <td>11.922723</td>
      <td>11.923595</td>
      <td>11.924403</td>
      <td>11.924906</td>
      <td>0.001954</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.537097</td>
      <td>5.533739</td>
      <td>5.534877</td>
      <td>5.537240</td>
      <td>5.539326</td>
      <td>5.540548</td>
      <td>0.004988</td>
      <td>2.377618</td>
      <td>2.065884</td>
      <td>2.182804</td>
      <td>...</td>
      <td>2.699305</td>
      <td>0.460798</td>
      <td>11.925098</td>
      <td>11.923125</td>
      <td>11.923931</td>
      <td>11.925098</td>
      <td>11.926396</td>
      <td>11.927063</td>
      <td>0.002891</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.538991</td>
      <td>5.534945</td>
      <td>5.536477</td>
      <td>5.539022</td>
      <td>5.541569</td>
      <td>5.543182</td>
      <td>0.006193</td>
      <td>2.371606</td>
      <td>1.837866</td>
      <td>2.029498</td>
      <td>...</td>
      <td>2.904250</td>
      <td>0.784577</td>
      <td>11.926511</td>
      <td>11.923676</td>
      <td>11.924901</td>
      <td>11.926573</td>
      <td>11.928241</td>
      <td>11.929292</td>
      <td>0.003995</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.540744</td>
      <td>5.535748</td>
      <td>5.537617</td>
      <td>5.540787</td>
      <td>5.543771</td>
      <td>5.545683</td>
      <td>0.007447</td>
      <td>2.342949</td>
      <td>1.645734</td>
      <td>1.882408</td>
      <td>...</td>
      <td>3.046599</td>
      <td>1.011832</td>
      <td>11.927930</td>
      <td>11.924430</td>
      <td>11.925817</td>
      <td>11.927946</td>
      <td>11.930156</td>
      <td>11.931374</td>
      <td>0.005124</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5.542365</td>
      <td>5.536545</td>
      <td>5.538680</td>
      <td>5.542387</td>
      <td>5.545899</td>
      <td>5.548236</td>
      <td>0.008660</td>
      <td>2.310694</td>
      <td>1.521751</td>
      <td>1.789342</td>
      <td>...</td>
      <td>3.095331</td>
      <td>1.162333</td>
      <td>11.929282</td>
      <td>11.924966</td>
      <td>11.926613</td>
      <td>11.929265</td>
      <td>11.931937</td>
      <td>11.933585</td>
      <td>0.006326</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5.543915</td>
      <td>5.537321</td>
      <td>5.539948</td>
      <td>5.543812</td>
      <td>5.548245</td>
      <td>5.550660</td>
      <td>0.009940</td>
      <td>2.297823</td>
      <td>1.450628</td>
      <td>1.700429</td>
      <td>...</td>
      <td>3.194033</td>
      <td>1.285033</td>
      <td>11.930562</td>
      <td>11.925311</td>
      <td>11.927192</td>
      <td>11.930542</td>
      <td>11.933869</td>
      <td>11.935725</td>
      <td>0.007589</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5.545347</td>
      <td>5.537738</td>
      <td>5.540607</td>
      <td>5.545352</td>
      <td>5.550057</td>
      <td>5.553010</td>
      <td>0.011232</td>
      <td>2.293406</td>
      <td>1.330785</td>
      <td>1.655404</td>
      <td>...</td>
      <td>3.281677</td>
      <td>1.414169</td>
      <td>11.931875</td>
      <td>11.925521</td>
      <td>11.928177</td>
      <td>11.931975</td>
      <td>11.935856</td>
      <td>11.937992</td>
      <td>0.008873</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>5.546852</td>
      <td>5.538702</td>
      <td>5.541794</td>
      <td>5.546599</td>
      <td>5.552029</td>
      <td>5.555466</td>
      <td>0.012556</td>
      <td>2.307615</td>
      <td>1.283407</td>
      <td>1.649592</td>
      <td>...</td>
      <td>3.357543</td>
      <td>1.522246</td>
      <td>11.933241</td>
      <td>11.926079</td>
      <td>11.928874</td>
      <td>11.933400</td>
      <td>11.937576</td>
      <td>11.940090</td>
      <td>0.010203</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>5.548430</td>
      <td>5.539130</td>
      <td>5.542581</td>
      <td>5.548399</td>
      <td>5.554189</td>
      <td>5.557517</td>
      <td>0.013926</td>
      <td>2.326923</td>
      <td>1.218425</td>
      <td>1.655505</td>
      <td>...</td>
      <td>3.427453</td>
      <td>1.609375</td>
      <td>11.934598</td>
      <td>11.926617</td>
      <td>11.929629</td>
      <td>11.934477</td>
      <td>11.939990</td>
      <td>11.942428</td>
      <td>0.011535</td>
      <td>1</td>
    </tr>
    <tr>
      <th>10</th>
      <td>5.550024</td>
      <td>5.539873</td>
      <td>5.543623</td>
      <td>5.549736</td>
      <td>5.556006</td>
      <td>5.560221</td>
      <td>0.015344</td>
      <td>2.354099</td>
      <td>1.240345</td>
      <td>1.633733</td>
      <td>...</td>
      <td>3.447825</td>
      <td>1.682082</td>
      <td>11.935991</td>
      <td>11.927274</td>
      <td>11.930383</td>
      <td>11.935806</td>
      <td>11.941634</td>
      <td>11.944865</td>
      <td>0.012852</td>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>5.551789</td>
      <td>5.540778</td>
      <td>5.544824</td>
      <td>5.551610</td>
      <td>5.558510</td>
      <td>5.562888</td>
      <td>0.016849</td>
      <td>2.362986</td>
      <td>1.177161</td>
      <td>1.602695</td>
      <td>...</td>
      <td>3.502457</td>
      <td>1.776331</td>
      <td>11.937375</td>
      <td>11.927911</td>
      <td>11.931146</td>
      <td>11.937101</td>
      <td>11.943470</td>
      <td>11.947129</td>
      <td>0.014194</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
<p>12 rows × 22 columns</p>
</div>

```python
fc.plot()
```

    (<Figure size 864x288 with 3 Axes>,
     array([<matplotlib.axes._subplots.AxesSubplot object at 0x000000000EBC4A58>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x00000000012A87B8>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x00000000014080B8>],
           dtype=object))

![png](https://i.imgur.com/Yy1Rnz9.png)

<div class='todo'>
    <div class='title'>To Do: Inverse Transforms</div>
      Add inverse transform capabilities at top-level object that can be passed down and applied here.
</div>
<div class='warning'>
    <div class='title'>Warning: Odd/bad forecasts</div>
    It appears that something is wrong with this forecast as they tend to explode after some time. 
</div>
<div class='todo'>
    <div class='title'>To Do: Redo this forecast with APW data for comparison</div>
      Use the APW data and forecasts to figure out what is going on.
</div>

#### Hierarchical priors

Setting the hyperparameters of the priors discussed above can oftentimes be quite hard to do. Giannone, Lenza, Primiceri (_RESTAT_, 2015) demonstrate that a principled way to set these hyperparameters is to specify a hierarchical prior (i.e. a prior over the hyperparameters). In their main application they use an empirical bayes approach in which they find the hyperparameter values that maximize the marginal likelihood of the data.

The `BVAR` class contains a method for doing this empirical Bayes estimation.

<div class='warning'>
    <div class='title'>Warning: Full support for empirical Bayes forthcoming</div>
    The <code>BVAR</code> object does not currently support forecasting with the empirical bayes methodology.
</div>

```python
bvs.update(hyperparameters={'lambda': 0.45, 'mu': 0.45, 'delta': 0.45,'dof': 15}, prior_spec=['MN', 'DIO', 'SOC'])
```

```python
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'PSI': {},
      'MU': {},
      'DELTA': {}
  },
    autograd_support = True,
    min_kwargs = {},
    set_hyperparameters = True
  )
```

You can view the optimized

```python
for k, v in eb.get_xstar().items():
  print('{}* = {}'.format(k, v))
```

    LAMBDA* = 0.2835801612360666
    PSI* = [4.54262220e-05 6.24472407e-01 6.82249766e-05]
    MU* = 0.14815661867074525
    DELTA* = 0.43965789651445614

You can also get the optimization output (i.e. from `scipy.optimize.minimize`) with the `res` object:

```python
eb.res
```

          fun: -6350.610661734966
     hess_inv: <6x6 LbfgsInvHessProduct with dtype=float64>
          jac: array([ 0.00739808,  0.00623118,  0.00320117, -0.01081927, -0.00171942,
           -0.00857961])
      message: b'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
         nfev: 10
          nit: 8
       status: 0
      success: True
            x: array([-1.26026044, -9.99942104, -0.47084813, -9.59269983, -1.90948533,
           -0.82175836])

Note that the optimal printed above in `eb.get_xstar()` are going to differ from the values printed in `eb.res` above because the minimization algorithm operates on the log of each parameter by default. The `EmpiricalBayes` object takes care of converting back and forth between the two for you.
