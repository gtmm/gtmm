---
id: index
title: fred-api
sidebar_label: fred-api Intro
---

Javascript wrapper for the St. Louis Federal Reserve Economic Data [Fred API](http://api.stlouisfed.org/).

## Installation

(yarn)

```bash
yarn add fred-api
```

(npm)

```bash
npm install fred-api
```

## Get a FRED API key

Sign up for a Fred API key: [http://api.stlouisfed.org/api_key.html](http://api.stlouisfed.org/api_key.html)

## Usage

### Instantiate a client

```js
var Fred = require("fred-api");

apiKey = process.env.FRED_KEY;
fred = new Fred(apiKey);
```

### Example Call

```bash
fred.getSeries({series_id: 'GNPCA'}, function(error, result) {
    console.log(result)
});

>> Result:

{
    realtime_start: '2015-01-13',
    realtime_end: '2015-01-13',
    seriess: [
        {
            id: 'GNPCA',
            realtime_start: '2015-01-13',
            realtime_end: '2015-01-13',
            title: 'Real Gross National Product',
            observation_start: '1929-01-01',
            observation_end: '2013-01-01',
            frequency: 'Annual',
            frequency_short: 'A',
            units: 'Billions of Chained 2009 Dollars',
            units_short: 'Bil. of Chn. 2009 $',
            seasonal_adjustment: 'Not Seasonally Adjusted',
            seasonal_adjustment_short: 'NSA',
            last_updated: '2014-07-30 10:36:08-05',
            popularity: 25,
            notes: 'BEA Account Code: A001RX1'
        }
    ]
}
```

For a full list of available methods see the [API page](docs/packages/fred-api/api).

### Avoid being throttled

If you make too many requests to FRED in a short period of time your requests will be throttled. FRED's rate limiting is quite generous. However, there may be cases where one runs up against it.

This version of `fred-api` includes handling for cases where requests might be rate limited. Though it cannot cover all possible cases, it can help to avoid some common pitfalls.

In general, one will not need to worry about setting any rate limiting parameters. Let's say that you want to get all tags associated with a large set of categories. Then you can simply do:

```js
const Fred = require('fred-api');

const apiKey = ...;
var fred = new Fred(apiKey);

var category_ids = [...];
var P = category_ids.map(id => fred.getCategoryTags(id));

var Pall = Promise.all(P);
```

#### One Anti-Pattern

Avoid instantiating multiple `Fred` instances. The rate limiting safety guards are based on the number of requests made within a given _instance_, not the total number of requests made by a single API key/IP address.

#### Custom Rate Limiting Parameters

Add these as a second argument:

```js
const Fred = require('fred-api');

const apiKey = ...;
var fred = new Fred(apiKey, {wait: 25*1000, blockSize: 1000, errTol: 100});
```

Here is an explanation of the parameters:

- `wait` (default: 10s/10000ms). Marginal delay between request blocks.
- `blockSize` (default: 3000). Number of requests that can be open at a given time before a marginal delay is added.
- `errTol` (default: 200). Number of request errors to accept and retry before giving up.

More information that can place these parameters in context can be found in [Automatic Rate Limiting](#Automatic-Rate-Limiting).

## Changes in this version of `fred-api`

### Promises, not callbacks

All outputs are returned a promises rather than callbacks. This is a stylistic decision made in order to better integrate with the rest of the package.

### Automatic pagination

There are many cases where all data relevant for a given call is not returned in one call. For example, if we wanted to get all series related to a tag, there might be 2500 series to fetch. However, in one API call we can only retrieve at most 1000 of the series.

This version of `fred-api` takes care of these cases automatically by retrieving all of the elements through multiple API calls.

If you do not wish to get all of the elements, simply pass the `limit` parameter with your call.

### Automatic rate limiting

This version of `fred-api` includes handling for cases where requests might be rate limited. Though it cannot cover all possible cases, it can help to avoid some common pitfalls.

The default rate limit parameters seem to work well but they were only determined heuristically.

Two main strategies are taken to avoid being throttled:

1. _Add automatic delays._ Spacing out the requests can avoid throttling. The problem is that too many long delays takes a lot of time. To overcome this we do the following:
   - Count the number of requests that are open at any given time: when a request is made this ticker (in property `._requests`) increments up one. When the request is complete the count is incremented down one.
   - If we have more than `blockSize` (e.g. 3000) requests open at a given time, we wait a given amount of time before making the request.
   - The waiting time is determined by the number of open requests. The basic idea is to avoid a piling-up of requests after an initial delay:
     - Requests 3001-6000 are delayed for `wait` milliseconds (e.g. 10s/10,000ms).
     - Requests 6001-9000 are delayed for `2 * wait` milliseconds.
1. _Handling for failed requests._ The provision for automatic delays is meant to proactively avoid failed requests. However, sometimes the `blockSize` is too large for a given set of requests or something else happens. Here is the basic protocol for a failed request:
   - Count the number of failed requests since the last successful request. This count is maintained in the property `._requestErrors`
   - When a new request comes in, check to see the number of failed requests recorded:
     - If `_requestErrors > errTol` then an error is thrown.
     - Otherwise, wait the "standard waiting time" (retrieved from `_getWait`) _plus_ a random number of seconds. Once the wait is complete, make the request again. The random seconds of waiting is meant to avoid pile-ons after a standard waiting time.

See [Custom Rate Limiting Parameters](#Custom-Rate-Limiting-Parameters) for more details on the parameters.

### FRED Errors in json mode

One might receive an error like this even if one is in JSON mode:

```xml
    <html>
      <head>
        <title>Oops! Looks like we've encountered an error</title>
      </head>
      <body>
        <div>
              <h2 >Oops! Looks like we've encountered an error 51.202.  So sorry.  Give us a minute to reset and try again.  Thanks!</h2>
        </div>
      </body>
    </html>
```

This would usually generate an uncaught exception when the supposed json output is passed through `JSON.parse`. In `get` and `getAll` this is now wrapped in a `try ... catch` block that raises an error on catch.

## Development

The package is contained in one file: `lib/index.js`. There are two main methods that are called:

- `get` Retrieves API data as a promise.
- `getAll` Does the same thing as `get` except for cases in which there are multiple pages of data to be retrieved (as may be the case when, e.g., one fetches the tags related to a given series).

Each of the API calls listed above provide entry points to these low-level methods.

### Status

### Tests

```js
yarn test
```
