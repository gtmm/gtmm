---
id: index
title: FRED API
sidebar_label: API
---

All of the following methods can be called from an instantiated `fred-api` object:

## categories

    getCategory
    getCategoryChildren
    getCategoryRelated
    getCategorySeries
    getCategoryTags
    getCategoryRelatedTags

## releases

    getReleases
    getReleasesDates
    getRelease
    getReleaseDates
    getReleaseSeries
    getReleaseSources
    getReleaseTags
    getReleaseRelatedTags

## series

    getSeries
    getSeriesCategories
    getSeriesObservations
    getSeriesRelease
    getSeriesSearch
    getSeriesSearchTags
    getSeriesSearchRelatedTags
    getSeriesTags
    getSeriesUpdates
    getSeriesVintageDates

## sources

    getSources
    getSource
    getSourceReleases

## tags

    getTags
    getRelatedTags
    getTagsSeries
