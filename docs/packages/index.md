---
id: index
title: Development
sidebar_label: Home
---

This page is meant for people who wish to contribute to and develop GTMM. Ordinary users should generally have no need to consult this documentation.

GTMM is composed of several more-or-less independent modules.

## [@gtmm/app](app/)

The main GUI application where all of the elements come together.

## [@gtmm/components](components/)

Standalone react components that will enter into the app.

## [@gtmm/data](data/)

Downloads and indexes data from the FRED API. The @gtmm/data package uses `fred-api` to download the data but then takes care of all of the organization of the data that is needed to both make it searchable and easily-retrievable.

## [fred-api](fred-api/)

A wrapper for the FRED API. This is a general purpose tool that is useful for accessing the API data.

## [yapij](https://yapij.gitlab.io) ↗

`yapij` is a standalone library written with GTMM in mind. It facilitates communication between python processes and node-based applications.

There are three main libraries:

### `yapij-py`

Low-level Python side of yapij application. Responsible for listening for commands, executing them, and returning results in the common `yapij` type system.

### `@yapij/js`

Low-level node side of yapij. Responsible for issuing commands, receiving stdout from the python process, and routing output to the right locations.

### `@yapij/re`

High-level react+redux+electron components. These components are written to make it easy to hook `yapij` into a pre-existing application. There are several pre-made components as well as [higher-order components](https://reactjs.org/docs/higher-order-components.html) for making one's own components.

