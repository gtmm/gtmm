/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require("react");

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ""}`;
    const langPart = `${language ? `${language}/` : ""}`;
    return `${baseUrl}${docsPart}${langPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : "") + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
            <h5>Guides</h5>
            <a href={this.docUrl("introduction/getting-started/index.html")}>
              Getting Started
            </a>
            <a href={this.docUrl("tutorials/index.html")}>Tutorials</a>
            <a href={this.docUrl("faq/general/index.html")}>FAQ</a>
          </div>
          <div>
            <h5>Development</h5>
            <a href={this.docUrl("packages/index.html")}>Development Home</a>
            <a href={this.docUrl("packages/app/index.html")}>App</a>
            <a href={this.docUrl("packages/components/index.html")}>
              Components
            </a>
            <a href={this.docUrl("packages/data/index.html")}>Data</a>
            <a href={this.docUrl("packages/fred-api/index.html")}>fred-api</a>
          </div>
          <div>
            <h5>More</h5>
            <a href={`${this.props.config.baseUrl}blog`}>Blog</a>
            <a href={`${this.props.config.baseUrl}about`}>About</a>
            <a href="https://gitlab.com/gtmm">GitLab</a>
          </div>
        </section>

        <section className="copyright">{this.props.config.copyright}</section>
      </footer>
    );
  }
}

module.exports = Footer;

/*
            <h5>Community</h5>
            <a href={this.pageUrl("users.html", this.props.language)}>
              User Showcase
            </a>
            <a
              href="http://stackoverflow.com/questions/tagged/"
              target="_blank"
              rel="noreferrer noopener"
            >
              Stack Overflow
            </a>
            <a href="https://discordapp.com/">Project Chat</a>
            <a
              href="https://twitter.com/"
              target="_blank"
              rel="noreferrer noopener"
            >
              Twitter
            </a>

 */
