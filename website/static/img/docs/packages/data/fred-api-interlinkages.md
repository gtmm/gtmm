```mermaid
graph LR
C[Categories]
R[Releases]
SRC[Sources]
S[Series]
T[Tags]

C --- S
SRC --- R
R --- S
T ---|Top 100 by Popularity Only| S
T --- C
T --- R

T|Related| T

C -->|Children| C
C --> |Parent| C
C -->|Related| C
```
