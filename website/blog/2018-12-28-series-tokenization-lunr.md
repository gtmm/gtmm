---
title: DEV:`lunr` Series tokenization is a headache!
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

There were two basic issues with the initial tokenization of the series for lunr. First, we were running into memory errors. Second, the file sizes are in any case too large to want to import and use for any sustained period of time.

<!--truncate-->

# See if increasing heap memory does it

Start a terminal with `node --max-old-space-size=8192`. Then run:

```js
const PackLunr = require("./build/src/PackLunr.js");

const C = require("./build/src/constants");
const { Filenames } = require("./build/src/utils");

var PL = new PackLunr.default(C.DEFAULT_DATA_PATH, C.DEFAULT_CACHE_PATH);

var idx;
PL.series().then(out => {
  idx = out;

  console.log("done");
});
```

The output is about 197MB! This won't work well for holding it on memory all the time.

# Size management strategies

- Effect of removing `_id` from index (but keeping `title`)
- Effect of **removing `title` from index** (but keeping `_id`)
- **Replacing informative IDs with numeric IDs.** So, instead of "EMPPSF" for the id it would just be 4505 (say).
- Only include a **subset of indices**
- Check for duplicate titles (i.e. Daily, weekly, monthly all share the same title).
- Use only the first 3-4 words of each title
  - Might miss out on a bunch of important things that usually come at the end like industries (e.g. "Total Payroll, Non-supervisory employees: construction").
- Look for

## Results

### Numeric indices are clearly superior

In the case where titles are used saving ~40MB.

| Variables  | id Type | Size     |
| ---------- | ------- | -------- |
| Both       | \_id    | 197MB    |
| Both       | numeric | 146.49MB |
| \_id only  | \_id    | 48.511MB |
| \_id only  | numeric | 39.56MB  |
| title only | \_id    | 142.42MB |
| title only | numeric | 95.154MB |

### Number and popularity of indices

There are 456,517 total series that could be collected.

| Popularity | Count  | # > n  | P(n < N)           |
| ---------- | ------ | ------ | ------------------ |
| 0          | 265183 | 191334 | 0.5808830777386165 |
| 1          | 163250 | 28084  | 0.9384820280515292 |
| 2-49       | 27378  | 706    | 0.9984535077554615 |
| 50-89      | 698    | 8      | 0.9999824760085605 |
| 90-100     | 8      | 0      | 1                  |

The highest popularity (100) is for the daily 10-year bond spread. Most of the popular ones appear to be bond related.

It would certainly be feasible to do all series with popularity > 1. However, it isn't clear what might be missed in the "popularity == 0" category.

Cutting out the popularity = 0 series (with both fields and numeric indices) yields a file size of 55MB.

Including only first four words of popularity = 0 series (and no `_id` for those) yields 88MB.

### Unique Titles

Total Titles: 456517
Number of unique titles: 359807
As a share of all titles: 0.7881568484853795

### By Frequency

| Frequency | # of Series |
| --------- | ----------- |
| M         | 90635       |
| A         | 300292      |
| NA        | 35          |
| Q         | 60055       |
| D         | 1002        |
| BW        | 15          |
| W         | 2049        |
| 5Y        | 541         |
| SA        | 1893        |

Most series are at the annual frequency or greater.

If we only include cases where the frequency is either D(aily), W(eekly), BW (i.e. bi-weekly), M(onthly), or Q(uarterly) then the file size shrinks to 46MB!

# Conclusions

The best way forward is to focus on data at frequencies of less than a year since that is what we will most often be using. When we do this the file size is both reasonable and we don't have issues with memory errors.

It is also preferable to use a straight numeric index for the series rather than use the actual data index from FRED. We need to switch out the handling of the `_id` in `FredMetadata`.
