---
title: Standardizing observations per year
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

In `TsData` I want to standard the number of observations per year based on frequency. The downside is that the resulting year-over-year transforms may not strictly be year-to-year comparisons (e.g. no treatment of leap years). The upside comes in speed gains and standardization of, e.g., compounding rates. This is the same approach taken by [FRED/ALFRED](https://alfred.stlouisfed.org/help#growth_formulas).

We need to expand their discussion in two ways. First, we should distinguish between 5- and 7-day "daily" series. Second, we need to be able to distinguish frequencies by looking at the data alone.

<!--truncate-->

## Our old friends, data frequencies

Recall that the frequencies found in the data are:

| SHORT | LONG                       | N SERIES | N Obs./YEAR |
| ----- | -------------------------- | -------- | ----------- |
| Q     | Quarterly                  | 55338    | 4           |
| Q     | Quarterly, 2nd Month's 1st | 2424     | 4           |
| Q     | Quarterly, Beginning of Pe | 1        | 4           |
| Q     | Quarterly, End of Period   | 1230     | 4           |
| Q     | Quarterly, End of Quarter  | 1229     | 4           |
| M     | Monthly                    | 89407    | 12          |
| M     | Monthly, As of 15th of the | 1        | 12          |
| M     | Monthly, Beginning of Mont | 3        | 12          |
| M     | Monthly, End of Month      | 264      | 12          |
| M     | Monthly, End of Period     | 917      | 12          |
| M     | Monthly, Middle and End of | 1        | 12          |
| M     | Monthly, Middle of Month   | 14       | 12          |
| M     | Monthly, Saturday Nearest  | 10       | 12          |
| BW    | Biweekly, Beg. of Period   | 1        | 26          |
| BW    | Biweekly, Ending Monday    | 1        | 26          |
| BW    | Biweekly, Ending Wednesday | 13       | 26          |
| W     | Weekly                     | 194      | 52          |
| W     | Weekly, As of Monday       | 40       | 52          |
| W     | Weekly, As of Thursday     | 1        | 52          |
| W     | Weekly, As of Wednesday    | 719      | 52          |
| W     | Weekly, Ending Friday      | 115      | 52          |
| W     | Weekly, Ending Monday      | 76       | 52          |
| W     | Weekly, Ending Saturday    | 221      | 52          |
| W     | Weekly, Ending Thursday    | 61       | 52          |
| W     | Weekly, Ending Wednesday   | 622      | 52          |
| D(7)  | Daily, 7-Day               | 178      | 365         |
| D(5)  | Daily, Close               | 135      | 260         |
| D(5)  | Daily                      | 689      | 260         |

We've established previously that the quarterly, monthly, and daily series are all reported in a "harmonized" manner (i.e. the observation for a monthly series is always reported on the first of the month).

The same is not true of the weekly or bi-weekly series.

## Inferring frequency

We can infer the frequency of a series by looking at the amount of time between two adjacent observations.

To calibrate the series we're going to want to look at the average time between

### Fetching the test series

In order to evaluate the distance between observations we run [`./packages/data/scripts/get-sample-data-series.js`](https://gitlab.com/gtmm/gtmm/blob/af169d76734bdac508db7623410c6654ddab5a3d/packages/data/scripts/get-sample-data-series.js) with argument 432 (to set a different seed).

```bash
# from path ./packages/data
node scripts/get-sample-data-series.js 432
```

We want a different seed to get a different set of series from what we'll use for testing purposes.

### Information from test data

Now we want to figure out the "usual" differences in frequencies for each type of frequency:

```js
var sqlite3 = require("sqlite3");
const dbPath = "C:/Users/us57144/.local/share/gtmm_data/fred.data.db";

var db = new sqlite3.Database(dbPath, err => {
  if (err) {
    console.error(err);
  } else {
    console.log("db ready");
  }
});
```

Here are the test series that will be used:

```js
var testSeries = [
  { fs: "BW", f: "Biweekly, Beg. of Period", s: "FLEXSC" },
  { fs: "BW", f: "Biweekly, Ending Monday", s: "DISMULT" },
  { fs: "BW", f: "Biweekly, Ending Wednesday", s: "DISBASE" },
  { fs: "BW", f: "Biweekly, Ending Wednesday", s: "ADJSBASE" },
  { fs: "D5", f: "Daily", s: "THREEFF8" },
  { fs: "D5", f: "Daily", s: "SMRKTD678FRBCLE" },
  { fs: "D7", f: "Daily, 7-Day", s: "RUSRECDM" },
  { fs: "D7", f: "Daily, 7-Day", s: "PRTRECD" },
  { fs: "D5", f: "Daily, Close", s: "VXVCLS" },
  { fs: "D5", f: "Daily, Close", s: "BAMLC2A0C35YSYTW" },
  { fs: "M", f: "Monthly", s: "LTULFHUADTTSTM" },
  { fs: "M", f: "Monthly", s: "MANMNM01INM661S" },
  {
    fs: "M",
    f: "Monthly, As of 15th of the Month",
    s: "M08FAAUS000OHM555NNBR",
  },
  { fs: "M", f: "Monthly, Beginning of Month", s: "G172CRFUN02" },
  { fs: "M", f: "Monthly, Beginning of Month", s: "G172CRJA02" },
  { fs: "M", f: "Monthly, End of Month", s: "M0520AUSM144SNBR" },
  { fs: "M", f: "Monthly, End of Month", s: "M10044USM144NNBR" },
  { fs: "M", f: "Monthly, End of Period", s: "UMXTUO" },
  { fs: "M", f: "Monthly, End of Period", s: "LIINUSGOVONY" },
  { fs: "M", f: "Monthly, Middle and End of the Month", s: "M05F0B46M601NNBR" },
  { fs: "M", f: "Monthly, Middle of Month", s: "M1473BUSM027SNBR" },
  { fs: "M", f: "Monthly, Middle of Month", s: "M0506BUSM436SNBR" },
  {
    fs: "M",
    f: "Monthly, Saturday Nearest Month's End",
    s: "M0584CUSM391NNBR",
  },
  {
    fs: "M",
    f: "Monthly, Saturday Nearest Month's End",
    s: "M0521BUSM391NNBR",
  },
  { fs: "Q", f: "Quarterly", s: "CHLGDPNQDSMEI" },
  { fs: "Q", f: "Quarterly", s: "ULQBBU01GBQ662N" },
  { fs: "Q", f: "Quarterly, 2nd Month's 1st Full Week", s: "ESMXSSNQ" },
  { fs: "Q", f: "Quarterly, 2nd Month's 1st Full Week", s: "EPSMNQ" },
  { fs: "Q", f: "Quarterly, Beginning of Period", s: "Q04049USQ052NNBR" },
  { fs: "Q", f: "Quarterly, End of Period", s: "US13LSTL" },
  { fs: "Q", f: "Quarterly, End of Period", s: "SCLSTL" },
  { fs: "Q", f: "Quarterly, End of Quarter", s: "QRUHAMXDCA" },
  { fs: "Q", f: "Quarterly, End of Quarter", s: "QCONAM770A" },
  { fs: "W", f: "Weekly", s: "LNFACBW027SBOG" },
  { fs: "W", f: "Weekly", s: "GASMIDREFECW" },
  { fs: "W", f: "Weekly, As of Monday", s: "SAVNRNJ" },
  { fs: "W", f: "Weekly, As of Monday", s: "MMRCJD" },
  { fs: "W", f: "Weekly, As of Thursday", s: "WSLB20" },
  { fs: "W", f: "Weekly, As of Wednesday", s: "D1WLTDHBFI" },
  { fs: "W", f: "Weekly, As of Wednesday", s: "WLRRAFOIAL" },
  { fs: "W", f: "Weekly, Ending Friday", s: "WCD6M" },
  { fs: "W", f: "Weekly, Ending Friday", s: "WTP10J17" },
  { fs: "W", f: "Weekly, Ending Monday", s: "GASPRMW" },
  { fs: "W", f: "Weekly, Ending Monday", s: "WSAVCBN" },
  { fs: "W", f: "Weekly, Ending Saturday", s: "MSCEMPLOY" },
  { fs: "W", f: "Weekly, Ending Saturday", s: "IDCCLAIMS" },
  { fs: "W", f: "Weekly, Ending Thursday", s: "MORTMRGN5NC" },
  { fs: "W", f: "Weekly, Ending Thursday", s: "MORTGAGE30NE" },
  { fs: "W", f: "Weekly, Ending Wednesday", s: "SRELCBW027NBOG" },
  { fs: "W", f: "Weekly, Ending Wednesday", s: "OSEDCBW027NBOG" },
];
```

Notice that I differentiate between 5- and 7-day weeks.

Look at the difference between series for one series.

```js
const getSummaryStatsBySeries = () => {
  var out = {};

  db.parallelize(() => {
    testSeries.map(ts => {
      db.all(
        `SELECT * FROM data WHERE series="${ts.s}" ORDER BY series, date;`,
        [],
        (err, rows) => {
          if (err) {
            console.error(err);
          } else {
            rows = rows.map(row => {
              return {
                date: new Date(row.date),
                series: row.series,
                freq: ts.fs,
              };
            });
            var diff = rows.slice(1).map((row, ii) => row.date - rows[ii].date);
            var outi = {
              fs: ts.fs,
              freq: ts.f,
              series: ts.s,
              max: Math.max(...diff),
              min: Math.min(...diff),
              mean: diff.reduce((a, c) => a + c) / diff.length,
              sum: diff.reduce((a, c) => a + c),
              length: diff.length + 1,
            };

            if (Object.keys(out).indexOf(ts.fs) < 0) {
              out[ts.fs] = [];
            }

            out[ts.fs].push(outi);
          }
        }
      );
    });
  });

  return out;
};

var stats = getSummaryStatsBySeries();
```

Next, do summary stats by frequency:

```js
var getSummaryStatsByFreq = () => {
  const fmtStatNum = s =>
    parseInt(s)
      .toString()
      .padEnd(20);

  console.log(
    `| ${"FREQ".padEnd(4)} | ${"MIN".padEnd(20)} | ${"MEAN".padEnd(
      20
    )} | ${"MAX".padEnd(20)}|`
  );
  console.log(
    `| ${"-".repeat(4)} | ${"-".repeat(20)} | ${"-".repeat(20)} | ${"-".repeat(
      20
    )}|`
  );

  var out = {};
  var s2 = Object.keys(stats).map(k => {
    var stat = stats[k].reduce(
      (accum, s) => {
        accum.max = Math.max(accum.max, s.max);
        accum.min = Math.min(accum.min, s.min);
        accum.sum += s.sum;
        accum.length += s.length;
        return accum;
      },
      { fs: stats[k][0].fs, max: 0, min: 1e999, mean: 0, sum: 0, length: 0 }
    );

    stat.mean = stat.sum / stat.length;

    console.log(
      `| ${stat.fs.padEnd(4)} | ${fmtStatNum(stat.min)} | ${fmtStatNum(
        stat.mean
      )} | ${fmtStatNum(stat.max)}|`
    );
    out[k] = stat;
    return stat;
  });

  return out;
};

var statFreq = getSummaryStatsByFreq();
```

| FREQ | MIN        | MEAN       | MAX        |
| ---- | ---------- | ---------- | ---------- |
| Q    | 7776000000 | 7810674310 | 7948800000 |
| M    | 2419200000 | 2619732473 | 2678400000 |
| BW   | 1123200000 | 1207756097 | 1296000000 |
| W    | 432000000  | 604349158  | 777600000  |
| D7   | 86400000   | 86394525   | 86400000   |
| D5   | 86400000   | 120539210  | 259200000  |

Now we want to check to make certain that the series are well-ordered:

```js
const checkWellOrderedFrequencies = () => {
  const checkOrdering = (a, b) =>
    console.log(
      `${a.padEnd(2)} > ${b.padEnd(2)} ? `,
      statFreq[a].min > statFreq[b].max
    );

  console.log("Checking for well-ordered frequencies:");
  checkOrdering("Q", "M");
  checkOrdering("M", "BW");
  checkOrdering("BW", "W");
  checkOrdering("W", "D7");
  checkOrdering("W", "D5");
};

checkWellOrderedFrequencies();
```

_returns_

```
Checking for well-ordered frequencies:
Q > M ?  true
M > BW ?  true
BW > W ?  true
W > D7 ?  true
W > D5 ?  true
```

Good.

#### What should the cut-off points be?

```js
const getMidwayPoints = () => {
  const checkOrdering = (a, b) =>
    console.log(
      `Mean(${a.padEnd(2)}, ${b.padEnd(2)}) ? `,
      (statFreq[a].min + statFreq[b].max) / 2
    );

  console.log("Get the midway points between successive freq:");
  checkOrdering("Q", "M");
  checkOrdering("M", "BW");
  checkOrdering("BW", "W");
  checkOrdering("W", "D7");
  checkOrdering("W", "D5");
};

getMidwayPoints();
```

```
Get the midway points between successive freq:
Mean(Q , M ) ?  5227200000
Mean(M , BW) ?  1857600000
Mean(BW, W ) ?  950400000
Mean(W , D7) ?  259200000
Mean(W , D5) ?  345600000
```

### A function

So then the basic idea will be to look at the difference in time for the first seven observations. (Want to do seven to differentiate between D5 and D7.)

```js
var getObsPerYear = d => {
  // Average difference
  var diff = (d[d.length - 1].date.getTime() - d[0].date.getTime()) / d.length;
  if (diff >= 31535000000) {
    return null;
  } else if (diff > 5227200000) {
    return 4;
  } else if (diff > 1857600000) {
    return 12;
  } else if (diff > 950400000) {
    return 26;
  } else if (diff > 345600000) {
    return 52;
  } else {
    var max = d
      .slice(1, 9)
      .reduce(
        (accum, row, ii) =>
          Math.max(accum, row.date.getTime() - d[ii].date.getTime()),
        -1e99
      );
    if (max > 96400000) {
      return 260;
    } else {
      return 365;
    }
  }
};
```

```js
var expected = { D7: 365, D5: 260, M: 12, BW: 26, W: 52, Q: 4 };

var checkObsPerYear = () => {
  db.serialize(() => {
    testSeries.map(ts => {
      db.all(
        `SELECT * FROM data WHERE series="${ts.s}" ORDER BY series, date;`,
        [],
        (err, rows) => {
          if (err) {
            console.error(err);
          } else {
            var d = rows.map(row => {
              return {
                date: new Date(row.date),
                series: row.series,
                value: row.value,
              };
            });
            try {
              console.log(
                `${ts.fs.padEnd(3)} ${expected[ts.fs]
                  .toString()
                  .padEnd(3)} ${getObsPerYear(d)
                  .toString()
                  .padEnd(3)} ${expected[ts.fs] == getObsPerYear(d)}`
              );
            } catch (e) {
              console.error(e);
            }
          }
        }
      );
    });
  });
};
```

Great ✔
