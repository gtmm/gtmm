---
title: Getting test metadata
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Quick script to get some test metadata for components from our `@gtmm/data`.

<!--truncate-->

```js
//$ cd ../../packages/components
//$ node
var FS = require("fs");
var GtmmData = require("@gtmm/data").default;

const saveMetadataPath = "./src/tools/sampleData/test-metadata-1.json";
const saveDataPath = "./src/tools/sampleData/test-data-1.json";

var projection = {
  id: 1,
  title: 1,
  obsStart: 1,
  obsEnd: 1,
  freq: 1,
  freqShort: 1,
  units: 1,
  sa: 1,
  lastUpdated: 1,
  notes: 1,
  lastDownloaded: 1,
  upToDate: 1,
};

var filters = [
  { upToDate: true },
  { upToDate: false },
  { freqShort: "D" },
  { freqShort: "W" },
  { freqShort: "BW" },
  { freqShort: "M" },
  { freqShort: "Q" },
  { sa: true },
  { sa: false },
];

var collected = {};

const onExec = (err, docs, saveTo) => {
  if (err) {
    console.error(err);
    return;
  }

  var d = docs.reduce((ac, doc) => {
    for (let kk of ["obsStart", "obsEnd", "lastUpdated", "lastDownloaded"]) {
      if (doc[kk]) {
        doc[kk] = new Date(doc[kk]).toLocaleDateString();
      }
    }
    delete doc._id;
    ac[doc.id] = doc;
    return ac;
  }, {});
  collected = Object.assign(collected, d);

  console.log(`Number of series collected: ${Object.keys(collected).length}`);
  FS.writeFileSync(saveTo, JSON.stringify(collected));
};

var GD = new GtmmData();

// prettier-ignore
GD.init().then(() => {
    console.log("Running queries:");
    for (let filter of filters) {
      // prettier-ignore
      GD.search._db.series.find(filter).limit(2).projection(projection).exec((err,docs) => onExec(err,docs,saveMetadataPath));
    }
    return collected;

  // prettier-ignore
  }).then(() => {
    GD.fetch(Object.keys(collected)).then(out => {
      FS.writeFileSync(saveDataPath, JSON.stringify(out));
      console.log("complete");
    });
  });
```

Also do this for well-known series:

```js
const saveMetadataPath2 = "./src/tools/sampleData/test-metadata-2.json";
const saveDataPath2 = "./src/tools/sampleData/test-data-2.json";

const series = ["GDPC1", "UNRATE", "DFF", "DFEDTARU", "USD3MTD156N", "CIVPART"];

collected = {};

// prettier-ignore
GD.search._db.series .find({ id: { $in: series } }) .projection(projection) .exec((err, docs) => onExec(err, docs, saveMetadataPath2));

GD.fetch(Object.keys(collected)).then(out => {
  FS.writeFileSync(saveDataPath2, JSON.stringify(out));
  console.log("complete");
});
```
