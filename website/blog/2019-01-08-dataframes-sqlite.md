---
title: DataFrames in Node?
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

There are a few pandas-like dataframe packages for node. In this part I'm going to check them out and see if they're worth using.

<!--truncate-->

[`data-forge`](https://github.com/data-forge/data-forge-ts) ([docs](https://github.com/data-forge/data-forge-ts/blob/master/docs/guide.md#immutability-and-chained-functions)) is the package that I'll explore. It has some cool and useful features, like [immutability](https://github.com/data-forge/data-forge-ts/blob/master/docs/guide.md#immutability-and-chained-functions).

## Some sample data

Here is some sample data that we might use:

```js
var n = 3,
  T = 10,
  columnNames = ["date", "series", "value"];

var data = [];
for (let ii = 0; ii < n; ii++) {
  var col = (Math.random() + 1).toString(36).substring(7),
    month = 1,
    day = 1;
  for (let tt = 0; tt < T; tt++) {
    // data.push({
    //   date: new Date(1975, month, day),
    //   series: col,
    //   value: Math.random(),
    // });
    data.push([new Date(1975, month, day), col, Math.random()]);
    if (ii % 2) {
      day += 1;
    } else {
      month += 1;
    }
  }
}
```

## Exploring `data-forge`

```js
const dataForge = require("data-forge");
require("data-forge-fs"); // For readFile/writeFile.

var df = new dataForge.DataFrame({ columnNames: columnNames, rows: data });
```

### ❗ Rotating data

`[pivot](https://data-forge.github.io/data-forge-ts/classes/dataframe.html#pivot)` isn't working as expected:

```js
df.pivot(["series"], {
  date: v => v,
  value: v => v,
}).toRows();
```

Returns just a set of series. It seems that this is set up to do some sort of aggregation, not pivoting/reverse-melt.

### Transforming data

Never got here.

### ⚠ Extracting raw arrays

This command returns array-of-arrays:

```js
df.toRows();
```

The behavior here is inconsistent depending on if the input is an array-of-arrays or an array-of-objects.

### ❗ Getting index values

_Note_: calling index values seems to cause a heap memory problem.

```js
df.getIndex().toArray(); // Heap out of memory
df.getIndex().toString(); // Heap out of memory
```

## Conclusion

This is not a good package to use.
