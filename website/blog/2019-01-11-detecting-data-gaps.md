---
title: Detecting Gaps in Time Series
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

The FRED data pipeline is set up and ready to go, for the most part. However, FRED just gives us the data that it has, not the data that we necessarily expect. Maybe a data series is collected on a monthly basis but because of a government shutdown (say) it isn't collected for a given month. We need to detect these gaps in the data before we do anything else with it.

<!--truncate-->

## Where should we deal with this?

The best place to deal with this is probably at the point where the series is downloaded from the internet. Recall that, at that point, the data looks something like [this](https://research.stlouisfed.org/docs/api/fred/series_observations.html):

```json
{
  "realtime_start": "2013-08-14",
  "realtime_end": "2013-08-14",
  "observation_start": "1776-07-04",
  "observation_end": "9999-12-31",
  "units": "lin",
  "output_type": 1,
  "file_type": "json",
  "order_by": "observation_date",
  "sort_order": "asc",
  "count": 84,
  "offset": 0,
  "limit": 100000,
  "observations": [
    {
      "realtime_start": "2013-08-14",
      "realtime_end": "2013-08-14",
      "date": "1929-01-01",
      "value": "1065.9"
    },
    {
      "realtime_start": "2013-08-14",
      "realtime_end": "2013-08-14",
      "date": "1930-01-01",
      "value": "975.5"
    },
    {
      "realtime_start": "2013-08-14",
      "realtime_end": "2013-08-14",
      "date": "1931-01-01",
      "value": "912.1"
    } // ... And so on
  ]
}
```

## What sort of frequencies are we dealing with?

Let's get a full list of frequencies from our database of series to see what we could expect to encounter.

Let's query our database to find each

```js
var GtmmData = require("./build/src/index").default;

var GD = new GtmmData();

// (If not done already)
// GD.fetchMetadata().then(() => console.log("ready"));

GD.init().then(() => console.log("ready")); // Get the DBs
```

Now let's see what we're working with.

```js
var f = {},
  fs = {},
  fex = {};
GD._db.series.find({}, { id: 1, freq: 1, freqShort: 1 }, (err, docs) => {
  docs.map(d => {
    if (Object.keys(f).indexOf(d.freq) < 0) {
      f[d.freq] = 0;
      fex[d.freq] = [];
      fs[d.freq] = d.freqShort;
    }

    fex[d.freq].push(d.id);
    f[d.freq] += 1;
  });

  console.log(
    "\n" +
      `${"FREQ".padEnd(46)} ${"LENGTH".padEnd(10)} ${"EXAMPLES".padEnd(15)}` +
      "\n" +
      "-".repeat(85)
  );
  var k = Object.keys(f).sort();

  k.map(fi => {
    console.log(
      `${fs[fi].padEnd(5)} ${fi.padEnd(40)} ${f[fi]
        .toString()
        .padEnd(10)} ${fex[fi].slice(0, 2)}`
    );
  });
});
```

_returns_

```
FREQ                                           LENGTH     EXAMPLES
-------------------------------------------------------------------------------------
BW    Biweekly, Beg. of Period                 1          FLEXSC
BW    Biweekly, Ending Monday                  1          DISMULT
BW    Biweekly, Ending Wednesday               13         ADJRAM,ADJRES
D     Daily                                    689        DBKAC,DCD1M
D     Daily, 7-Day                             178        DFF,4BIGEURORECD
D     Daily, Close                             135        BAMLC0A0CM,BAMLC0A0CMEY
M     Monthly                                  89407      POP,EXDNUS
M     Monthly, As of 15th of the Month         1          M08FAAUS000OHM555NNBR
M     Monthly, Beginning of Month              3          G172CRFUN02,G172CRHA02
M     Monthly, End of Month                    264        M08313USM173NNBR,M08338USM175NNBR
M     Monthly, End of Period                   917        A11ATI,A11BTI
M     Monthly, Middle and End of the Month     1          M05F0B46M601NNBR
M     Monthly, Middle of Month                 14         M1473BUSM027SNBR,M1475BUSM027SNBR
M     Monthly, Saturday Nearest Month's End    10         M0501AUSM391NNBR,M0501BUSM391NNBR
Q     Quarterly                                55338      CPALTT01KRQ659N,CPALTT01LUQ657N
Q     Quarterly, 2nd Month's 1st Full Week     2424       EI1000T9999XDBNQ,EI100T999XDBNQ
Q     Quarterly, Beginning of Period           1          Q04049USQ052NNBR
Q     Quarterly, End of Period                 1230       MOLLRTL,MOLSTL
Q     Quarterly, End of Quarter                1229       QSGCAM770A,QSGCAMUSDA
W     Weekly                                   194        SMPACBW027NBOG,SMPACBW027SBOG
W     Weekly, As of Monday                     40         CD12NRJD,CD12NRNJ
W     Weekly, As of Thursday                   1          WSLB20
W     Weekly, As of Wednesday                  719        ABS10Y,ABS15
W     Weekly, Ending Friday                    115        LTBOARD,WBA3M
W     Weekly, Ending Monday                    76         CURRENCY,GDBFRW
W     Weekly, Ending Saturday                  221        AKCCLAIMS,AKCEMPLOY
W     Weekly, Ending Thursday                  61         WRMORTG,MORTGAGE15NC
W     Weekly, Ending Wednesday                 622        SCLACBW027SBOG,SREACBW027NBOG
```

Of course, there are also the 5-year, annual, and semiannual series that are not generally imported here.

Besides the usual suspects -- monthly, daily, etc. -- there are also different variations on when in a day or week or month the series is reported.

## (Aside?) Getting data

To move forward we need to get some data.

In doing this, however, I realized that FRED _does_, in fact, report missing data. It does this with a `"."`. I was getting this weird error about a misplaced `.` in my sqlite insert statement and that is what this is!

We can update the data like so:

```js
GD.update(
  Object.keys(fex).reduce((accum, cv) => accum.concat(fex[cv].slice(0, 2)), [])
).then(() => console.log("done downloading."));
```

Now let's see what happens when we query a series that has missing data now:

```js
GD.data.db.all(
  'SELECT * FROM data WHERE series="DCD1M" LIMIT 10;',
  [],
  console.log
);
```

_returns_

```js
> null [ { date: '1965-12-22', series: 'DCD1M', value: 4.76 },
  { date: '1965-12-23', series: 'DCD1M', value: null },
  { date: '1965-12-24', series: 'DCD1M', value: null },
  { date: '1965-12-27', series: 'DCD1M', value: 4.77 },
  { date: '1965-12-28', series: 'DCD1M', value: null },
  { date: '1965-12-29', series: 'DCD1M', value: 4.76 },
  { date: '1965-12-30', series: 'DCD1M', value: null },
  { date: '1965-12-31', series: 'DCD1M', value: null },
  { date: '1966-01-03', series: 'DCD1M', value: 4.77 },
  { date: '1966-01-04', series: 'DCD1M', value: null } ]
```

Nice!

### 7- v. 5-day

Unless they are specifically marked as "Daily, 7-Day", the daily series do not include weekends as "missing" observations.

I am going to ignore these because - for the purposes of computing transforms - it oftentimes makes sense to act as if Monday was the day directly after Friday (i.e. in the case of growth rates).

## Harmonized frequencies

So I was wrong about this. However, the above is not all for nought.

I was thinking that, besides the actual date reported, we'd also want a "harmonized date". For example, series `EI1000T9999XDBNQ` has frequency "Quarterly, 2nd Month's 1st Full Week". We'd like to be able to compare this series easily to other quarterly series like `CPALTT01KRQ659N` by making them have the "same date".

What do the dates of `EI1000T9999XDBNQ` look like?

```js
GD.data.db.all(
  'SELECT * FROM data WHERE series="EI1000T9999XDBNQ" LIMIT 5;',
  [],
  console.log
);

// BAMLC0A0CM;

GD.data.db.all(
  'SELECT * FROM data WHERE series="BAMLC0A0CM" LIMIT 30;',
  [],
  console.log
);
```

_returns_

```js
> null [ { date: '1997-04-01', series: 'EI1000T9999XDBNQ', value: 85 },
  { date: '1997-07-01', series: 'EI1000T9999XDBNQ', value: 55 },
  { date: '1997-10-01', series: 'EI1000T9999XDBNQ', value: 77 },
  { date: '1998-01-01', series: 'EI1000T9999XDBNQ', value: 110 },
  { date: '1998-04-01', series: 'EI1000T9999XDBNQ', value: 91 } ]
```

So actually it looks like this series has the canonical quarterly date format.

Is this true of the other series?

```js
var kk = Object.keys(fex);
kk.sort();
kk.map(k => {
  fex[k].slice(0, 2).map(id =>
    GD.data.db.all(
      `SELECT * FROM data WHERE series="${id}" AND date>= date("2015-01-01") LIMIT 5;`,
      [],
      (e, r) => {
        e
          ? console.error(e)
          : console.log(k, id, "\n\t" + r.map(ri => ri.date).join("\n\t"));
      }
    )
  );
});
```

I'm going to omit the full output but highlight important parts.

### Pre-harmonized series

1. Quarterly
1. Monthly
1. Daily

### Non-harmonized frequencies

#### Weekly

```
Weekly SMPACBW027NBOG
        2015-01-07
        2015-01-14
        2015-01-21
        2015-01-28
        2015-02-04
Weekly, As of Monday CD12NRJD
        2015-01-05
        2015-01-12
        2015-01-19
        2015-01-26
        2015-02-02
```

Notice that the weekly series are reported on the _Wednesday_ of that week. That is, January 7, 2015 was a Wednesday.

#### Biweekly

```
Biweekly, Ending Wednesday ADJRES
        2015-01-07
        2015-01-21
        2015-02-04
        2015-02-18
        2015-03-04
Biweekly, Beg. of Period FLEXSC
        2015-01-08
        2015-01-22
        2015-02-05
        2015-02-19
        2015-03-05
```

## Summary of Findings

1. Missing data is already pointed out by FRED.
1. Some data frequencies already have "pre-harmonized" dates while others do not.

- The "important" frequencies are pre-harmonized.
- Therefore, skip implementing this for now.
