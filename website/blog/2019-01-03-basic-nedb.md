---
title: Basic NEDB
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

We need to make sure that we're understanding how NEDB works. In particular, how and when it is written to file, etc.

<!--truncate-->

Here is a starting point:

```js
var Path = require("path");
var Datastore = require("nedb");

var filename = Path.join(process.env.TEMP, "test-nedb.db");

var db = new Datastore({ filename: filename, autoload: true });
```

Here is some test data:

```js
// prettier-ignore
var collection = [
  {_id: "id1", planet: "Mars", system: "solar", inhabited: false, satellites: ["Phobos", "Deimos"], }, 
  {_id: "id2", planet: "Earth", system: "solar", inhabited: true, humans: { genders: 2, eyes: true }, }, 
  { _id: "id3", planet: "Jupiter", system: "solar", inhabited: false }, 
  {_id: "id4", planet: "Omicron Persei 8", system: "futurama", inhabited: true, humans: { genders: 7 }, }, 
  {_id: "id5", completeData: {planets: [{ name: "Earth", number: 3 }, { name: "Mars", number: 2 }, { name: "Pluton", number: 9 }, ], }, }, 
];
```

Let's add it to the db:

```js
db.insert(collection, (err, newDocs) => {
  if (err) throw err;

  console.log("New Documents added");
  console.log(newDocs);
});
```

Definitely something is there. Here is a screencap of both the db in memory as well as the format of the actual data file:

![Screencap of existing](https://i.imgur.com/csGEogk.png)

**Append: 1/11/19** It looks like I don't understand it as well as I thought...

## Updates

```js
var db = new Datastore({ inMemoryOnly: true });
db.insert(collection, (err, newDocs) => {
  if (err) throw err;

  console.log("New Documents added");
  console.log(newDocs);
});
```

Now let's try to find some keys, modify them, then update them en masse.

```js
var findMofifyUpdate = k => {
  db.find({ system: k }, (err, docs) => {
    var newDocs = docs.map(d => {
      d.planet = "nopper";
      return d;
    });
    console.log(`Key: ${k}, Number found: ${docs.length}`);

    // And now try to update them
    db.update(
      { system: k },
      newDocs,
      { multi: true },
      (err, numAffected, affectedDocuments, upsert) => {
        if (err) {
          console.error(err.errorType);
        } else {
          console.log(`Key ${k} a success`);
        }
      }
    );
  });
};

["solar", "futurama"].map(findMofifyUpdate);
```

So it works for just one document but not multiple documents!
