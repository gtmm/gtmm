---
title: Importing FRED Data to sqlite
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

This post discusses the task of importing FRED data and placing it in a sqlite DB.

<!--truncate-->

Let's start up the object:

```js
var FredData = require("./build/src/FredData.js").default;
var C = require("./build/src/constants.js");

var FD = new FredData(
  C.DEFAULT_DATA_PATH,
  C.DEFAULT_DB_NAME,
  C.DEFAULT_API_KEY,
  C.DEFAULT_RATE_LIMIT_PROPS
);
```

## Connect to the database

Check that we can connect:

```js
FD.connect().then(() => console.log("done"));
```

## Get the data from FRED

```js
var d,
  keys = ["CPIAUCSL", "A191RL1Q225SBEA"];

FD._download(keys).then(out => {
  d = out;
});
```

_returns_

```js
[
  {
    series_id: "CPIAUCSL",
    realtime_start: "2019-01-07",
    realtime_end: "2019-01-07",
    observation_start: "1600-01-01",
    observation_end: "9999-12-31",
    units: "lin",
    output_type: 1,
    file_type: "json",
    order_by: "observation_date",
    sort_order: "asc",
    count: 89,
    offset: 0,
    limit: 100000,
    observations: [
      {
        realtime_start: "2019-01-07",
        realtime_end: "2019-01-07",
        date: "1929-01-01",
        value: "1120.076",
      }, ...
    ],
  },
  {
    series_id: "A191RL1Q225SBEA",
    realtime_start: "2019-01-07",
    realtime_end: "2019-01-07",
    observation_start: "1600-01-01",
    observation_end: "9999-12-31",
    units: "lin",
    output_type: 1,
    file_type: "json",
    order_by: "observation_date",
    sort_order: "asc",
    count: 286,
    offset: 0,
    limit: 100000,
    observations: [
      {
        realtime_start: "2019-01-07",
        realtime_end: "2019-01-07",
        date: "1947-04-01",
        value: "-1.0",
      }, ...
    ],
  },
];
```

## Full download cycle

### Delete series if exist

```js
this.db.run(`DELETE FROM ${FD._td.table}
WHERE series in (${keys.map(k => `"${k}"`).join(",")});`);
```

### Download the data

```js
var d,
  keys = ["CPIAUCSL", "A191RL1Q225SBEA"];

FD._download(keys).then(out => {
  d = out;
});
```

### Transform to something suitable for sqlite

The insert should have the [form](http://www.sqlitetutorial.net/sqlite-insert/):

```sql
INSERT INTO table1 (
 column1,
 column2 ,..)
VALUES
 (
 value1,
 value2 ,...),
 (
 value1,
 value2 ,...),
        ...
 (
 value1,
 value2 ,...);
```

So how to do this?

This is much less intrusive:

```js
var insertString = `INSERT INTO ${FD._td.table} (${FD._td.columns.join(
  ", "
)}) VALUES `;

for (let ii = 0; ii < d.length; ii++) {
  for (let jj = 0; jj < d[ii].observations.length; jj++) {
    insertString += `("${d[ii].observations[jj].date}", "${d[ii].series_id}", ${
      d[ii].observations[jj].value
    }),`;
  }
}
(() => {
  insertString = insertString.slice(0, -1) + ";";

  FD.db.run(insertString, err => {
    if (err) throw err;
  });
})();
```

#### Another approach

Here is one row-by-row approach:

```js
var insertString = `INSERT INTO ${FD._td.table} (${FD._td.columns.join(
  ", "
)}) VALUES(?,?,?)`;

var appendOneRow = (date, series, value) => {
  FD.db.run(insertString, [date, series, value], err => {
    if (err) throw err;
  });
};

FD.db.parallelize(() => {
  d.forEach(series => {
    var seriesId = series.series_id;
    series.observations.forEach(obs =>
      appendOneRow(obs.date, seriesId, obs.value)
    );
  });
});
```

The downside of this approach is that there is noticeable blocking.

### Update the status of the metadata

We also want to update the status of the metadata to

```js
var Datastore = require("nedb");

var { Filenames } = require("./build/src/utils");
var C = require("./build/src/constants.js");

var keys = ["CPIAUCSL", "A191RL1Q225SBEA"];

var s = new Datastore({
  filename: Filenames.nedb("series", C.DEFAULT_DATA_PATH),
  autoload: true,
});

var sample;
s.find({ id: { $in: keys } }, (err, docs) => {
  console.log(err, docs);
  sample = docs;
});
```

From this we see that we need to update the "collected" and "lastUpdated" attributes.

We can do this with the `update` method.

_Note:_ The original timestamp from the FRED metadata had the non-js standard format of `'2018-12-12 07:51:02-06'`. In a new commit I have replaced the `lastUpdated` attribute with `null` in `FredMetadata._appendSeries`.

So then the update will be of the form:

```js
s.update(
  { id: { $in: keys } },
  { $set: { lastUpdated: new Date(), collected: true } },
  { multi: true },
  (err, numAffected, affectedDocuments, upsert) => {
    // These just check things:
    console.log(err, numAffected, affectedDocuments, upsert);
    s.find({ id: { $in: keys } }, (err, docs) => {
      console.log(err, docs);
    });
  }
);
```

Notice how important it is to

### Conclusion

_Place this into the integrated `_download` method._ Need to update the tests.

```js
```

## Updating series

There are two parts to this:

1. Checking for series updates.
2. Getting the specified series updates.

```js
var Datastore = require("nedb");

var { Filenames } = require("./build/src/utils");
var C = require("./build/src/constants.js");

var keys = ["CPIAUCSL", "A191RL1Q225SBEA"];

var s = new Datastore({
  filename: Filenames.nedb("series", C.DEFAULT_DATA_PATH),
  autoload: true,
});

s.find({ collected: 3 }, { id: 1, lastUpdated: 1 }, (err, docs) => {
  if (err) throw err;
  console.log(docs);
});
```
