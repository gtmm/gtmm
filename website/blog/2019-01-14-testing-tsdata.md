---
title: Testing the TsData class
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

`TsData` is a class for handling transforms, aggregations, and shape changes for the data that comes out of our sqlite db. Think of it as a highly-specialized (read: limited) dataframe.

Since I'm a one-man band I usually need to pick my battles when it comes to testing. Sometimes you just need to check to make sure that it works and move on. Othertimes you have some "mission-critical" software that better work no matter what. `TsData` falls into this latter case. In particular, we need to make sure that the data transforms are doing what we expect them to do and that the data is being formatted correctly.

In this post I outline some of the steps taken to make these tests happen.

<!--truncate-->

Basically, the idea is going to be to download a bunch of data, transform it to the expected values/shape "by hand", then run tests against that. This is not a fast process but it is the best way I know to be certain that your tests are doing what you think they're doing and, consequently, that your code actually works.

## Basic data checks

As a starting point, I selected a single series (`ENUC426640110SA`) and computed the relevant transforms in excel. See `./packages/data/tests/sampleData/Q-TsData-basic.ts` and `./packages/data/tests/sampleData/Q-TsData-basic.xlsx`.

This should pick up any howlers.

## Systematic checks

In a more systematic check I'm going to want to carry out checks for multiple series of each type of data frequency as well as special cases.

### Selecting series and getting data in place

In the below snippet we select two random series from each frequency and download them to the database.

(Hacked this together. Should make the main func nicer so that don't have to wait to put into CLI, etc.)

```js
var GtmmData = require("./build/src/index").default;

const defaultSeed = 123;
var seed = 123;

/**
 * Just want a "RNG" that will generate replicable results
 * Know that this is no good for actual applications
 * https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
 * @return {[type]} [description]
 */
const random = () => {
  var x = Math.sin(seed++) * 10000;
  return x - Math.floor(x);
};

const drawRandomUniqueInteger = (pCount, pMin, pMax) => {
  var min = pMin < pMax ? pMin : pMax,
    max = pMax > pMin ? pMax : pMin;
  pCount = Math.min(max - min, pCount);

  var resultArr = [],
    randNumber;
  while (pCount > 0) {
    randNumber = Math.floor(min + random() * (max - min));
    if (resultArr.indexOf(randNumber) == -1) {
      resultArr.push(randNumber);
      pCount--;
    }
  }
  return resultArr;
};

const getAllFrequencies = GD_ => {
  var f = {},
    fs = {},
    fex = {};
  GD_._db.series.find({}, { id: 1, freq: 1, freqShort: 1 }, (err, docs) => {
    var _0 = docs.map(d => {
      if (Object.keys(f).indexOf(d.freq) < 0) {
        f[d.freq] = 0;
        fex[d.freq] = [];
        fs[d.freq] = d.freqShort;
      }

      fex[d.freq].push(d.id);
      f[d.freq] += 1;
    });

    console.log(
      "\n" +
        `${"FREQ".padEnd(46)} ${"LENGTH".padEnd(10)} ${"EXAMPLES".padEnd(15)}` +
        "\n" +
        "-".repeat(85)
    );
    var k = Object.keys(f).sort();

    k.map(fi => {
      console.log(
        `${fs[fi].padEnd(5)} ${fi.padEnd(40)} ${f[fi]
          .toString()
          .padEnd(10)} ${fex[fi].slice(0, 2)}`
      );
    });
  });
  return { freq: f, ex: fex, short: fs };
};

const drawRandomSeries = (f, verbose) => {
  seed = defaultSeed;
  console.log(`Setting seed to ${seed} for replication purposes`);
  var out = Object.keys(f.ex).reduce((accum, k) => {
    accum[k] = drawRandomUniqueInteger(2, 0, f.ex[k].length).map(
      s => f.ex[k][s]
    );
    return accum;
  }, {});

  console.log(
    `| ${"SHORT".padEnd(5)} | ${"FREQUENCY".padEnd(40)} | ${"SERIES".padEnd(
      25
    )} |`
  );
  console.log(`| ${"-".repeat(5)} | ${"-".repeat(40)} | ${"-".repeat(25)} |`);
  var keys = Object.keys(out);
  keys.sort();
  var _1 = keys.map(k => {
    out[k].map(s =>
      console.log(
        `| ${f.short[k].padEnd(5)} | ${k.padEnd(40)} | ${s.padEnd(25)} |`
      )
    );
  });

  return out;
};

const downloadSelectedSeries = s =>
  GD.update(Object.keys(s).reduce((accum, cv) => accum.concat(s[cv]), []))
    .then(td => console.log(`Downloaded ${td.length} series successfully.`))
    .catch(console.error);

/**
 * MAIN (clean this up)
 */

var GD = new GtmmData(),
  freqData,
  selectedSeries;

// (If not done already)
// GD.fetchMetadata().then(() => console.log("ready"));

GD.init().then(() => {
  console.log("GD ready");
  var f_ = getAllFrequencies(GD);
  freqData = f_;
  var s_ = drawRandomSeries(f_, true);
  selectedSeries = s_;
  downloadSelectedSeries(s_);
}); // Get the DBs
```

Here are our selected series:

| SHORT | FREQUENCY                             | SERIES                |
| ----- | ------------------------------------- | --------------------- |
| BW    | Biweekly, Beg. of Period              | FLEXSC                |
| BW    | Biweekly, Ending Monday               | DISMULT               |
| BW    | Biweekly, Ending Wednesday            | DISBASE               |
| BW    | Biweekly, Ending Wednesday            | ADJSBASE              |
| D     | Daily                                 | THREEFF8              |
| D     | Daily                                 | SMRKTD678FRBCLE       |
| D     | Daily, 7-Day                          | RUSRECDM              |
| D     | Daily, 7-Day                          | PRTRECD               |
| D     | Daily, Close                          | VXVCLS                |
| D     | Daily, Close                          | BAMLC2A0C35YSYTW      |
| M     | Monthly                               | LTULFHUADTTSTM        |
| M     | Monthly                               | MANMNM01INM661S       |
| M     | Monthly, As of 15th of the Month      | M08FAAUS000OHM555NNBR |
| M     | Monthly, Beginning of Month           | G172CRFUN02           |
| M     | Monthly, Beginning of Month           | G172CRJA02            |
| M     | Monthly, End of Month                 | M0520AUSM144SNBR      |
| M     | Monthly, End of Month                 | M10044USM144NNBR      |
| M     | Monthly, End of Period                | UMXTUO                |
| M     | Monthly, End of Period                | LIINUSGOVONY          |
| M     | Monthly, Middle and End of the Month  | M05F0B46M601NNBR      |
| M     | Monthly, Middle of Month              | M1473BUSM027SNBR      |
| M     | Monthly, Middle of Month              | M0506BUSM436SNBR      |
| M     | Monthly, Saturday Nearest Month's End | M0584CUSM391NNBR      |
| M     | Monthly, Saturday Nearest Month's End | M0521BUSM391NNBR      |
| Q     | Quarterly                             | CHLGDPNQDSMEI         |
| Q     | Quarterly                             | ULQBBU01GBQ662N       |
| Q     | Quarterly, 2nd Month's 1st Full Week  | ESMXSSNQ              |
| Q     | Quarterly, 2nd Month's 1st Full Week  | EPSMNQ                |
| Q     | Quarterly, Beginning of Period        | Q04049USQ052NNBR      |
| Q     | Quarterly, End of Period              | US13LSTL              |
| Q     | Quarterly, End of Period              | SCLSTL                |
| Q     | Quarterly, End of Quarter             | QRUHAMXDCA            |
| Q     | Quarterly, End of Quarter             | QCONAM770A            |
| W     | Weekly                                | LNFACBW027SBOG        |
| W     | Weekly                                | GASMIDREFECW          |
| W     | Weekly, As of Monday                  | SAVNRNJ               |
| W     | Weekly, As of Monday                  | MMRCJD                |
| W     | Weekly, As of Thursday                | WSLB20                |
| W     | Weekly, As of Wednesday               | D1WLTDHBFI            |
| W     | Weekly, As of Wednesday               | WLRRAFOIAL            |
| W     | Weekly, Ending Friday                 | WCD6M                 |
| W     | Weekly, Ending Friday                 | WTP10J17              |
| W     | Weekly, Ending Monday                 | GASPRMW               |
| W     | Weekly, Ending Monday                 | WSAVCBN               |
| W     | Weekly, Ending Saturday               | MSCEMPLOY             |
| W     | Weekly, Ending Saturday               | IDCCLAIMS             |
| W     | Weekly, Ending Thursday               | MORTMRGN5NC           |
| W     | Weekly, Ending Thursday               | MORTGAGE30NE          |
| W     | Weekly, Ending Wednesday              | SRELCBW027NBOG        |
| W     | Weekly, Ending Wednesday              | OSEDCBW027NBOG        |

## Formatting test data in python

```python
import numpy as np
import pandas as pd
import os
import sqlite3
import json
```

```python
data_package_dir = os.path.abspath("../../packages/data/test/sampleData")
db_file = 'C:/Users/us57144/.local/share/gtmm_data/fred.data.db'

metadata_file = os.path.join(data_package_dir, 'sample-TsData-metadata.json')
agg_sample_data_file = os.path.join(data_package_dir, 'sample-TsData-agg.ts')

with open(metadata_file,'r') as f:
  md = json.load(f)
```

Here is the metadata that we'll use to import the data from the DB:

```python
for k,v in md.items():
  print('{:2s} {:3d} {:2d}'.format(k, v['f'], len(v['s'])))
```

    Q    4  9
    M   12 14
    BW  26  4
    W   52 17
    D5 260  4
    D7 365  2

Make a connection:

```python
conn = sqlite3.connect(db_file)
```

Now get the data:

```python
def get_data(k,d):
  q_single = lambda f, s: ''' (a.series,a.date) IN (
          SELECT b.series,b.date
          FROM data AS b
          WHERE b.series = "{}"
          LIMIT {}
          )
  '''.format(s,2*f)

  q = ('SELECT * FROM data AS a WHERE' +
  '\tOR'.join([q_single(d['f'],s) for s in d['s']]) +
  ' ORDER BY series, date;')

  df = pd.read_sql(q,conn, parse_dates=["date"])
  df['nObsPerYear'] = d['f']
  df['freq'] = k
  return df

df = pd.concat([get_data(k,v) for k,v in md.items()])
```

Now do some transforms to make aggregation easier:

```python
dti = pd.DatetimeIndex(df['date'])
df['month'] = [int(v) for v in dti.month]
df['year'] = [int(v) for v in dti.year]
df['quarter'] = [int(np.floor((d-1)/3) +1) for d in dti.month]
df['value'] = df['value'].apply(lambda n_: np.nan if n_ is None else float(n_))
df = df.sort_values(['freq','series', 'date'])
```

### Aggregation

```python
def apply_agg(
  df_: pd.DataFrame, group_, name_, method_, date_handler_
) -> pd.DataFrame:
  dd = df_.groupby(group_)['value'].apply(method_).to_frame(name_
                                                            ).reset_index()
  dd['date'] = dd.apply(date_handler_, axis=1)
  return df_.merge(
    dd[['series', 'date', name_]], on=['series', 'date'], how='outer'
  ).sort_values(['series', 'date'])


dfa = df.copy()
for group_elements,name_prefix,date_handler in zip([['month'], ['quarter'], []], ['M', 'Q', 'A'], [lambda r_: pd.Timestamp('{:02.0f}-01-{:04.0f}'.format(r_.month,r_.year)), lambda r_: pd.Timestamp(
      '{:02.0f}-01-{:04.0f}'.format(3 * (r_.quarter - 1) + 1, r_.year)
    ), lambda r_: pd.Timestamp(
      '01-01-{:04.0f}'.format(r_.year)
    )]):
  group = ['series', 'year'] + group_elements
  for method, name_suffix in zip(
    [np.sum, np.nanmean, apply_eos_outer(group)], ['SUM', 'AVG', 'EOP']
  ):
    name = '|'.join([name_prefix, name_suffix])
    dfa = apply_agg(dfa, group, name, method, date_handler)
    print(name + ' done')
```

    M|SUM done


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\pandas\core\groupby\groupby.py:2273: RuntimeWarning: Mean of empty slice
      res = f(group)


    M|AVG done
    M|EOP done
    Q|SUM done
    Q|AVG done
    Q|EOP done
    A|SUM done
    A|AVG done
    A|EOP done

Let's check it. Notice that the dates aren't going to come close to matching up exactly:

```python
dfa.loc[dfa['series'] =='BASE'].head(8)
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>date</th>
      <th>series</th>
      <th>value</th>
      <th>nObsPerYear</th>
      <th>freq</th>
      <th>month</th>
      <th>year</th>
      <th>quarter</th>
      <th>M|SUM</th>
      <th>M|AVG</th>
      <th>M|EOP</th>
      <th>Q|SUM</th>
      <th>Q|AVG</th>
      <th>Q|EOP</th>
      <th>A|SUM</th>
      <th>A|AVG</th>
      <th>A|EOP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1060</th>
      <td>1984-01-01</td>
      <td>BASE</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>735.662</td>
      <td>183.915500</td>
      <td>184.657</td>
      <td>4337.65</td>
      <td>188.593478</td>
      <td>193.166</td>
    </tr>
    <tr>
      <th>1061</th>
      <td>1984-02-01</td>
      <td>BASE</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>366.159</td>
      <td>183.0795</td>
      <td>182.956</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1062</th>
      <td>1984-02-15</td>
      <td>BASE</td>
      <td>183.203</td>
      <td>26.0</td>
      <td>BW</td>
      <td>2.0</td>
      <td>1984.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1063</th>
      <td>1984-02-29</td>
      <td>BASE</td>
      <td>182.956</td>
      <td>26.0</td>
      <td>BW</td>
      <td>2.0</td>
      <td>1984.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1064</th>
      <td>1984-03-01</td>
      <td>BASE</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>369.503</td>
      <td>184.7515</td>
      <td>184.657</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1065</th>
      <td>1984-03-14</td>
      <td>BASE</td>
      <td>184.846</td>
      <td>26.0</td>
      <td>BW</td>
      <td>3.0</td>
      <td>1984.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1066</th>
      <td>1984-03-28</td>
      <td>BASE</td>
      <td>184.657</td>
      <td>26.0</td>
      <td>BW</td>
      <td>3.0</td>
      <td>1984.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1067</th>
      <td>1984-04-01</td>
      <td>BASE</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>371.562</td>
      <td>185.7810</td>
      <td>185.814</td>
      <td>1122.571</td>
      <td>187.095167</td>
      <td>189.195</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>

Now we're ready to export to file:

```python
to_export = '''//prettier-ignore
export const sampleAggregationData = {};
'''.format(json.dumps(json.loads(dfa.to_json(None, 'table'))['data']))

with open(agg_sample_data_file,'w') as f:
  f.write(to_export)
```
