---
title: Planning for Data Components
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Beyond @gtmm/data there will be additional library of react components that will be responsible for handling data search, tables, and graphical displays.

These components should be made so that they fit into the larger redux store that will be used in the main app. Moreover, we need to ensure that the data structures handled by these components are conducive to reuse. This is particularly important for the data itself: we don't want to get in a situation where we're carrying out "transposes" every time a render occurs.

<!--truncate-->

There will be a

- [VX](https://vx-demo.now.sh)
- [Plot.ly](https://plot.ly/javascript/react/)
