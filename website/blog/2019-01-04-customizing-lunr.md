---
title: lunr customizations
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

This post focuses on understanding how to customize lunr:

<!--truncate-->

```js
var lunr = require("lunr");

var documents = [
  {
    name: "Lunr",
    text: "Like Solr, but much smaller, and not as bright.",
  },
  {
    name: "React",
    text: "A JavaScript library for building user interfaces.",
  },
  {
    name: "Lodash",
    text:
      "A modern JavaScript utility library delivering modularity, performance & extras.",
  },
];
```

Try to add the token length metadata from docs:

```js
var tokenLengthMetadata = function(builder) {
  // Define a pipeline function that stores the token length as metadata
  var pipelineFunction = function(token) {
    token.metadata["tokenLength"] = token.toString().length;
    return token;
  };

  // Register the pipeline function so the index can be serialised
  lunr.Pipeline.registerFunction(pipelineFunction, "tokenLenghtMetadata");

  // Add the pipeline function to the indexing pipeline
  builder.pipeline.before(lunr.stemmer, pipelineFunction);

  // Whitelist the tokenLength metadata key
  builder.metadataWhitelist.push("tokenLength");
};
```

Now create the index:

```js
var idx = lunr(function() {
  this.ref("name");
  this.field("text");
  this.field("name");
  this.metadataWhitelist = ["position"];
  this.use(tokenLengthMetadata);

  documents.forEach(function(doc) {
    this.add(doc);
  }, this);
});
```

What does it return?

```js
var res = idx.search("javascript");

res[0].matchData.metadata.javascript;
```
