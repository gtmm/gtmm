---
title: DEV:Pre-building lunr indices
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Today, I focus on pre-building an index for [lunr.js](https://lunrjs.com). `lunr` is a lightweight, serverless package for carrying out searches over dataset. We are going to use it to search FRED metadata for a given set of inputs in the `@gtmm/data` package.

<!--truncate-->

# Some Resources

- [`lunr.js` Guide to Pre-building indixes](https://lunrjs.com/guides/index_prebuilding.html) contains a bare-bones overview of pre-building.
- [Lunr with a large index (800,000 items)](https://github.com/olivernn/lunr.js/issues/222). Repository issue that describes one way to implement the pre-building function.
- [Loading large objects from memory](https://www.bennadel.com/blog/3232-parsing-and-serializing-large-objects-using-jsonstream-in-node-js.htm) provides methods for retrieving objects that would overwhelm node's usual heap memory.

# Current state of project

This note applies to the `PackLunr` object in `@gtmm/data`. As of 12/28/18 we have a non-working version of the `sources` method. A reference commit is [8669963d0a5340c34200610ace5382dd27652316](https://gitlab.com/gtmm/gtmm/commit/8669963d0a5340c34200610ace5382dd27652316).

# Desired outcomes

1. Build the lunr index
1. Successfully save the lunr index to disk.
1. Load the index from disk.
1. Carry out a (successful) search on the loaded index.
1. **(Big Picture)** Fill the index without loading the object entirely.

# Experiments

Our experiments will be focused on the FRED sources data, which is relatively small data.

Let's assume that we are in the `@gtmm/data` directory and that we have built any relevant typescript. Also, we have already downloaded the FRED metadata.

## Simple load ✅

First, let's simply load the data, make an index, and carry out a search:

```js
const FS = require("fs");
const lunr = require("lunr");

const C = require("./build/src/constants.js");
const { Filenames } = require("./build/src/utils.js");

// Get the data
var src = JSON.parse(
  FS.readFileSync(Filenames.cache("sources", C.DEFAULT_CACHE_PATH))
);

// Make the index
var idx = lunr(function() {
  this.ref("_id");
  this.field("name");
  this.field("link");
  this.field("notes");

  Object.keys(src).forEach(k => {
    this.add(src[k]);
  });
});
```

Now let's make sure that we can carry out a search. We find:

```bash
> idx.search("world")
[ { ref: '57', score: 4.275, matchData: { metadata: [Object] } },
  { ref: '60', score: 0.207, matchData: { metadata: [Object] } } ]
> JSON.stringify(idx).length
29108
> Object.keys(idx)
[ 'invertedIndex',
  'fieldVectors',
  'tokenSet',
  'fields',
  'pipeline' ]
> Object.keys(src).length
87
```

```js
for (let kk in idx) {
  try {
    console.log(kk, JSON.stringify(idx[kk]).length);
  } catch (e) {
    console.log(kk, "N/A");
  }
}
```

Returns:

```bash
invertedIndex 20558
fieldVectors 7338
tokenSet 108032
fields 23
pipeline 11
search N/A
query N/A
toJSON N/A
```

### Saving and Re-loading

Now let's try to save the thing.

```js
var file = Filenames.lunr("sources", C.DEFAULT_DATA_PATH);

FS.writeFileSync(file, JSON.stringify(idx), {});
```

Can we load from file?

```js
var idx2 = lunr.Index.load(JSON.parse(FS.readFileSync(file, {})));
```

```bash
> idx2.search("world")
[ { ref: '57', score: 4.275, matchData: { metadata: [Object] } },
  { ref: '60', score: 0.207, matchData: { metadata: [Object] } } ]
```

So everything works for this example!

Let's save it for later:

```js
var idx_simple = idx;
```

## Loading each key separately ❗❓

The above works fine for small files. However, our bigger files have a few gigs worth of storage in them. Using `JSON.parse` won't do it.

To take care of this we use the `JSONStream` package.

In this next example we create a promise that resolves the lunr index. We want to see if it returns the proper output or if something is screwed around:

```js
const FS = require("fs");
const lunr = require("lunr");
const JSONStream = require("JSONStream");

const C = require("./build/src/constants.js");
const { Filenames } = require("./build/src/utils.js");

var file = Filenames.lunr("sources", C.DEFAULT_DATA_PATH);

var key = "sources";
var transformStream = JSONStream.parse("*");
var inputStream = FS.createReadStream(
  Filenames.cache(key, C.DEFAULT_CACHE_PATH)
);

var idx,
  ct = 0;

new Promise((resolve, reject) => {
  idx = lunr(function() {
    this.ref("_id");
    this.field("name");
    this.field("link");
    this.field("notes");

    inputStream
      .pipe(transformStream)
      .on("data", data => {
        this.add(data);
        ct += 1;
      })
      .on("end", () => {
        console.log(this);

        console.log("Number of elements processed: ", ct);
        resolve(idx);
      })
      .on("error", e => {
        reject(e);
      });
  });
}).then(idx_ => {
  console.log(Object.keys(idx_));
  console.log("Returned by search: ", idx_.search("world"));
  console.log(JSON.stringify(idx_).length);

  for (let kk in idx) {
    try {
      console.log(kk, JSON.stringify(idx[kk]).length);
    } catch (e) {
      console.log(kk, "N/A");
    }
  }

  // Save it to file:
  FS.writeFileSync(file, JSON.stringify(idx_), {});
});
```

returns

```bash
> Number of elements processed:  87
[ 'invertedIndex',
  'fieldVectors',
  'tokenSet',
  'fields',
  'pipeline' ]
Returned by search:  []
21250
invertedIndex 20558
fieldVectors 2
tokenSet 33
fields 23
pipeline 11
search N/A
query N/A
toJSON N/A
```

#### ❗ Issues ❗

Clearly, we are not getting the correct set of search results.

Moreover, if we try to retrieve the file and run a search we get an error, not just an empty return array:

```js
var idx3 = lunr.Index.load(JSON.parse(FS.readFileSync(file, {})));

idx3.search("world"); // Error!
```

#### Diagnostics

- ✅ We get the correct set of keys.
- ❌ We get the correct set of keys but the approximate size is underweight (~21250 v. ~29108 bytes).
- ✅ We get the correct number of sources that are in the set (87).
- ❌ We see that both the `tokenSet` and `fieldVectors` fields are extremely lightweight.

#### Fix Attempt 1: Examine what happens on return of `lunr`

I am a bit uneasy about returning an object `idx` _within_ the function that creates it. A step is most likely being skipped.

In the lunr docs we see that the `lunr.Builder` object is responsible for carrying out what happens when `lunr(function() {...})` is called. In lines 2656ff. of the source we see that `lunr.Builder.prototype.build` both:

1. Returns a `lunr.Index`, which is what we want; and
1. Makes calls that, among other things, appear to set the `fieldVector` and `tokenSet` attributes.

Let's see what happens if we define a new variable `go` from scratch using only the build prototype:

```js
const FS = require("fs");
const Path = require("path");
const lunr = require("lunr");

var go,
  tempFile = Path.join(process.env.temp, "packed.json");

var g = lunr(function() {
  this.ref("_id");
  this.field("name");
  this.field("link");
  this.field("notes");

  this.add({ _id: "3", name: "fas", link: "f", notes: "f" });

  // `go` is nothing until we call it.
  go = this.build();
});

FS.writeFileSync(tempFile, JSON.stringify(go));

console.log("Some tests:");
console.log("go === g", JSON.stringify(go) === JSON.stringify(g));
console.log("Can search: go.search('fas') = \n\t", go.search("fas"));

var go2 = lunr.Index.load(JSON.parse(FS.readFileSync(tempFile)));
console.log(
  "Can save version search: go2.search('fas') = \n\t",
  go2.search("fas")
);
```

Beautiful!

**So the key is to return the result of `this.build`.**

#### Implementation ✅

```js
const FS = require("fs");
const lunr = require("lunr");
const JSONStream = require("JSONStream");

const C = require("./build/src/constants.js");
const { Filenames } = require("./build/src/utils.js");

var file = Filenames.lunr("sources", C.DEFAULT_DATA_PATH);

var key = "sources";
var transformStream = JSONStream.parse("*");
var inputStream = FS.createReadStream(
  Filenames.cache(key, C.DEFAULT_CACHE_PATH)
);

var idx,
  ct = 0;

new Promise((resolve, reject) => {
  lunr(function() {
    this.ref("_id");
    this.field("name");
    this.field("link");
    this.field("notes");

    inputStream
      .pipe(transformStream)
      .on("data", data => {
        this.add(data);
        ct += 1;
      })
      .on("end", () => {
        console.log("Number of elements processed: ", ct);
        idx = this.build();
        resolve(idx);
      })
      .on("error", e => {
        reject(e);
      });
  });
}).then(idx_ => {
  console.log(Object.keys(idx_));
  console.log("Returned by search: ", idx_.search("world"));
  console.log(JSON.stringify(idx_).length);

  for (let kk in idx) {
    try {
      console.log(kk, JSON.stringify(idx[kk]).length);
    } catch (e) {
      console.log(kk, "N/A");
    }
  }

  // Save it to file:
  FS.writeFileSync(file, JSON.stringify(idx_), {});
});
```

returns

```bash
> Number of elements processed:  87
[ 'invertedIndex',
  'fieldVectors',
  'tokenSet',
  'fields',
  'pipeline' ]
Returned by search:  [ { ref: '57', score: 4.275, matchData: { metadata: [Object] } },
  { ref: '60', score: 0.207, matchData: { metadata: [Object] } } ]
29108
invertedIndex 20558
fieldVectors 7338
tokenSet 107435
fields 23
pipeline 11
search N/A
query N/A
toJSON N/A
```

This looks pretty good. However, it may be worth noting that the `tokenSet` reported here is slightly smaller than that from the simple example (108032).

# Conclusion

We've figured out a solid way to meet all of the goals set out at the beginning. The key was to dig into the lunr source code. For the sources part I will probably just use the simple method that was first discussed. However, for larger cases the second method will certainly be preferable.



