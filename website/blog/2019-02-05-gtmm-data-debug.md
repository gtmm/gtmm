---
title: Going back to GTMM Data
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

This short post deals with some errors that were run into when I dusted off the `GtmmData` python package.

<!--truncate-->

## Test Failures

### Metadata is `None`

```python
import os
import pytest
import numpy as np
import pandas as pd
from urllib.error import HTTPError
from collections import namedtuple

from GtmmData.DB import DB


db_filename = os.path.abspath(os.path.join('tests', 'fixtures', 'data','test_db_empty.db'))

ddb = DB(db_filename=db_filename, fred_api_key=os.environ['FRED_KEY'])
```

Indeed, `ddb.metadata` is `None` here. The reason is that the imported database lacks any of the tables that would make it possible to infer this.

_Solution:_ We should run a script to initiate the tables

```sql
CREATE TABLE IF NOT EXISTS "fred_data" ( "date" TEXT, "year" INTEGER, "quarter" INTEGER, "month" INTEGER, "day" INTEGER, "variable" TEXT, "value" REAL );
```

and

```sql
CREATE TABLE IF NOT EXISTS  "fred_metadata" ( "id" TEXT, "frequency" TEXT, "frequency_short" TEXT, "last_updated" TIMESTAMP, "notes" TEXT, "observation_end" TIMESTAMP, "observation_start" TIMESTAMP, "popularity" INTEGER, "realtime_end" TIMESTAMP, "realtime_start" TIMESTAMP, "seasonal_adjustment" TEXT, "seasonal_adjustment_short" TEXT, "title" TEXT, "units" TEXT, "units_short" TEXT );
```

This seems to work.

### Not finding correct formats

```
___________________ test_DB_format__format_check_fmts[test_db_only1.db] ____________________

get_db_only = 'C:\\Users\\us57144\\Dropbox\\grantThornton\\projects\\macro_model\\gtmm-py\\GtmmData\\tests\\fixtures\\data\\test_db_only1.db'

    @pytest.mark.usefixtures("get_db_only")
    @pytest.mark.skipif(
      'test_DB_format__format_check_fmts' not in TO_TEST,
      reason="Not currently interested."
    )
    def test_DB_format__format_check_fmts(get_db_only):
      ...
      ft = {}
>     assert ddb._format_check_fmts(ft, False) == {
        v: {
          'trans': 'LIN',
          'freq': 'A',
          'agg': 'AVG'
        }
        for v in ddb.metadata.index
      }, 'Empty dict'

tests\test_DB.py:112:
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
GtmmData\DB.py:450: in _format_check_fmts
    lfreq = get_coarse(fmts.keys())
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

series_ids = dict_keys([])

    def get_coarse(series_ids: list) -> str:
      """Find that lowest frequency data frequenecy for a given set of series.

      ....
      else:
        raise RuntimeError(
          'Could not find known frequency to find coarsest frequency. Known frequency values are: {}.\nWas passed:\n{}'
>         .format(['A', 'Q', 'M', 'D'], fv)
        )
E       RuntimeError: Could not find known frequency to find coarsest frequency. Known frequency values are: ['A', 'Q', 'M', 'D'].
E       Was passed:
E       []

GtmmData\DB.py:356: RuntimeError
```

```python
import os
from os import path
import sqlite3
import pytest
import numpy as np
import pandas as pd
from urllib.error import HTTPError
from collections import namedtuple

from GtmmData.DB import DB

BASE_PATH = path.join('tests', 'fixtures', 'data')
FORCE_RELOAD = False

def fredExampleAllSameBaseData(
  ids, filename='fredExampleAllSameBaseData.pkl'
):
  filename = path.join(BASE_PATH, filename)
  if os.path.isfile(filename) and not FORCE_RELOAD:
    d = pd.read_pickle(filename)
  else:
    fd = FredData(apiKey=apiKey)
    fd.getFredData(ids)
    d = fd.data
    d.to_pickle(filename)
  return d


def fredExampleAllSameBaseMetadata(
  ids, filename='fredExampleAllSameBaseMetadata.pkl'
):
  filename = path.join(BASE_PATH, filename)
  if os.path.isfile(filename) and not FORCE_RELOAD:
    d = pd.read_pickle(filename)
  else:
    fd = FredData(apiKey=apiKey)
    d = pd.DataFrame()
    # Request the series
    for key in ids:
      md = fd.fred.series.details(key).set_index('id')
      d = d.append(md)
    d.to_pickle(filename)
  return d

def make_db(db_filename='test_db1.db'):
  filename = path.join(BASE_PATH, db_filename)
  db = sqlite3.connect(filename)
  tables = [
    tbl[0] for tbl in db.execute(
      "SELECT name FROM sqlite_master WHERE type = 'table'"
    ).fetchall()
  ]
  # Drop all tables
  for tbl in tables:
    db.execute('DROP TABLE {};'.format(tbl))
  df = fredExampleAllSameBaseData(['GDPC1', 'GCT1502US', 'UNRATE', 'DAAA'])
  md = fredExampleAllSameBaseMetadata(
    ['GDPC1', 'GCT1502US', 'UNRATE', 'DAAA']
  )
  df = df.reset_index()
  df['year'] = df['date'].apply(lambda ts: ts.year)
  df['month'] = df['date'].apply(lambda ts: ts.month)
  df['day'] = df['date'].apply(lambda ts: ts.day)
  df['quarter'] = df['date'].apply(lambda ts: ((ts.month - 1) // 3) + 1)
  df['date'] = df['date'].apply(
    lambda ts: '{:4d}-{:02d}-{:02d}'.format(ts.year, ts.month, ts.day)
  )
  df = df.melt(id_vars=['date', 'year', 'quarter', 'month', 'day'])
  df.to_sql('fred_data', db, if_exists='replace', index=False)
  md.to_sql('fred_metadata', db, if_exists='replace')
  return df, md, db, filename

def get_db_only(param):
  filename = path.join(BASE_PATH, param)
  # if not os.path.isfile(filename):
  make_db(param)
  return filename

db_filename = get_db_only('test_db_only1.db')

ddb = DB(db_filename=db_filename, fred_api_key=os.environ['FRED_KEY'])


ddb._format_check_fmts({}, False)
```

The issue was that the fixture dict wasn't finding any data where it should have found data. Fixed this by setting `FORCE_RELOAD=True` for one run in `tests/fixtures/BaseDataExamples.py`.
