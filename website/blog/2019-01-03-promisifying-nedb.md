---
title: Promisifying NEDB with Bluebird
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Bluebird's [promisify](http://bluebirdjs.com/docs/api/promise.promisify.html) and [promisifyAll](http://bluebirdjs.com/docs/api/promise.promisifyall.html) features are pretty neat. But it isn't clear how well they handle complex ES6 classes like the NEDB Datastore.

Let's see how it does.

<!--truncate-->

Here's a maybe-naive attempt:

```js
var Promise = require("Bluebird");
var Datastore = Promise.promisifyAll(require("nedb"));
```

So that works!

Let's start up a db and try inserting the sample collection:

```js
var collection = [
  {
    _id: "id1",
    planet: "Mars",
    system: "solar",
    inhabited: false,
    satellites: ["Phobos", "Deimos"],
  },
  {
    _id: "id2",
    planet: "Earth",
    system: "solar",
    inhabited: true,
    humans: { genders: 2, eyes: true },
  },
  { _id: "id3", planet: "Jupiter", system: "solar", inhabited: false },
  {
    _id: "id4",
    planet: "Omicron Persei 8",
    system: "futurama",
    inhabited: true,
    humans: { genders: 7 },
  },
  {
    _id: "id5",
    completeData: {
      planets: [
        { name: "Earth", number: 3 },
        { name: "Mars", number: 2 },
        { name: "Pluton", number: 9 },
      ],
    },
  },
];
```

Then the db is:

```js
var db = new Datastore({ inMemoryOnly: true, autoload: true });

// No complaints
db.insert(collection);

// But this is not recognized!
db.insert(collection).then(newDocs => {
  console.log(newDocs);
});
```

So it does not appear that this works too well.
