---
title: The data storage trifecta
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

We plan to store the data in a sqlite db. We need to fetch the data from storage in _both_ python and node. In each, we then need to put it in a container that can be easily manipulated. The storage shape will be important for making sure that the data can be stored and retrieved in an efficient manner.

<!--truncate-->

This data will be drawn upon to:

1. (node) Populate the data table.
1. (node) Render plots of the data.
1. (python) Compute models.

So we not only need to think about sqlite query performance, we also need to think about any further post-query processing that might need to be done.

## sqlite table shape

First, we need to think about the basic table shape that could be used. Two basic possibilities are "wide" or "long".

### Wide table

A wide table would include one column for each data series:

| date       | series1 | series2 | ...  | seriesN |
| ---------- | ------- | ------- | ---- | ------- |
| "1/1/1900" | 15      | 24      | 43   | 23      |
| "1/2/1900" | 15      | null    | null | 23      |
| ...        | 15      | null    | null | 23      |
| "1/2/2019" | 12      | 12      | 4    | null    |

(Note that sqlite has no notion of date so we have to use strings here [though there is an add-on that I can look up if need be].)

In the [sqlite limits documentation](https://sqlite.org/limits.html), it is noted that there is a maximum column limit of 2000 that can be expanded to ~32,000.

### Long Table

| date       | series  | value |
| ---------- | ------- | ----- |
| "1/1/1900" | series1 | 15    |
| "1/2/1900" | series1 | 15    |
| ...        | series1 | 43    |
| "1/2/2019" | series1 | 12    |
| "1/1/1900" | series2 | 15    |
| "1/2/1900" | series2 | 15    |
| ...        | series2 | 43    |
| "1/2/2019" | series2 | 12    |
| ...        | ...     | ...   |
| "1/1/1900" | seriesN | 15    |
| "1/2/1900" | seriesN | 15    |
| ...        | seriesN | 43    |
| "1/2/2019" | seriesN | 12    |

### Conclusion

In [this Q&A](https://stackoverflow.com/questions/54077773/sqlite-wide-v-long-performance) it was recommended that the "long" format be used.

The main downside is storage space but this should not be too bad.

## Post-query processing

### Models

- Get it into pandas dataframe that is wide.

### Plots

- Probable going to need to have a series.

### Tables

- Going to have array with each entry being a row.
