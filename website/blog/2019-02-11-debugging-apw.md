---
title: Debugging conditional forecasts
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

We need to make sure that we're getting the correct outcomes for the conditional forecasts. Early evidence suggests that we are not due to the fact that we have ballooning uncertainty.

<!--truncate-->

## Setup

```python
%reload_ext autoreload
%autoreload 2
%matplotlib inline
%config InlineBackend.figure_format = 'retina'
```

    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\ipykernel\parentpoller.py:116: UserWarning: Parent poll failed.  If the frontend dies,
                    the kernel may be left running.  Please let us know
                    about your system (bitness, Python, etc.) at
                    ipython-dev@scipy.org
      ipython-dev@scipy.org""")

```python
import os
import tempfile
import pickle

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import GtmmData
import bvar
```

```python
sns.set_style('whitegrid')
sns.set_palette('deep')

FRED_KEY = os.environ['FRED_KEY']
FORCE_RELOAD = True
BASE_PATH = os.path.join(tempfile.gettempdir(), 'bvar-usage')
```

```python
model_name = '3-var'
series = ['CPIAUCSL', 'FEDFUNDS', 'PAYEMS']
model_base_path = os.path.join(BASE_PATH, model_name)
data_path = os.path.join(model_base_path, 'data.pkl')
model_path = os.path.join(model_base_path, 'model.pkl')
if not os.path.exists(model_base_path):
    os.makedirs(model_base_path)

if not os.path.exists(data_path) or FORCE_RELOAD:
  fred_data0 = GtmmData.FredData(apiKey=FRED_KEY)
  fred_data0.getFredData(ids=series) # Download data
  fred_data0.setTrans(['CPIAUCSL', 'PAYEMS'], 'log') # Make these variables logs
#   fred_data0.setTrans('FEDFUNDS', 'log') # Try this out
  fred_data = fred_data0.format(balanced=True, freq='coarse',trim=[pd.Timestamp('1959-01-01'), pd.Timestamp('2019-01-01', freq='M')]) # Format it
  fred_data.to_pickle(data_path)
else:
  fred_data = GtmmData.read_pickle(data_path)
```

```python
bvs = bvar.core.BVAR(
  data=fred_data.data,
  lags=13,
  prior_spec='DIO_MN_SOC',
  hyperparameters={'Lambda': 0.45, 'mu': 0.214, 'delta': 0.124, 'dof': 14}
)
```

## The Issue

Now let's demonstrate the basic issue: the size of the errors balloon for some pretty modest conditions.

```python
conditional_forecast_script = '''
eqn EQ0 = PAYEMS[+1];
eqn EQ1 = FEDFUNDS[+1];
eqn EQ2 = FEDFUNDS[+2];

mean EQ0 = 11.922209936681169; // = log(15074) = Jan. 2019 Nonfarm employment
mean EQ1 = 2.40; // log(2.40) = Jan 2019 Effective Fed funds rate
mean EQ2 = 2.38; // Guessing that the fed will not change rates in February => close to constant FFNDs

std EQ0 = 1e-1;
std EQ1 = 1.1e-1;
std EQ2 = 1.2e-1;
'''
```

```python
fc = bvs.forecast(
    n_periods = 12,
    kind = 'apw',
    hierarchical = False,
    script = conditional_forecast_script,
    iters = 2000,
    chains = 4
  )
```

```python
fc.plot()
```

    (<Figure size 864x288 with 3 Axes>,
     array([<matplotlib.axes._subplots.AxesSubplot object at 0x000000000E69AB38>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x000000000E663940>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x000000000E666470>],
           dtype=object))

![png](https://i.imgur.com/ClnaOn2.png)

```python
fc.table()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="6" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
      <th>forecast</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>...</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.492490</td>
      <td>0.875061</td>
      <td>3.169212</td>
      <td>5.547207</td>
      <td>8.092211</td>
      <td>10.201027</td>
      <td>10.165800</td>
      <td>2.401286</td>
      <td>2.329162</td>
      <td>2.356967</td>
      <td>...</td>
      <td>2.475683</td>
      <td>0.109562</td>
      <td>11.921816</td>
      <td>11.855614</td>
      <td>11.880196</td>
      <td>11.920697</td>
      <td>11.963079</td>
      <td>11.988199</td>
      <td>0.099999</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.394283</td>
      <td>-2.367369</td>
      <td>1.436850</td>
      <td>5.551836</td>
      <td>9.638947</td>
      <td>13.186324</td>
      <td>17.120632</td>
      <td>2.381062</td>
      <td>2.302415</td>
      <td>2.332996</td>
      <td>...</td>
      <td>2.462614</td>
      <td>0.119563</td>
      <td>11.936324</td>
      <td>8.759210</td>
      <td>10.306737</td>
      <td>11.937571</td>
      <td>13.531760</td>
      <td>15.077820</td>
      <td>7.094036</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.408750</td>
      <td>-4.809031</td>
      <td>0.293789</td>
      <td>5.528671</td>
      <td>10.935393</td>
      <td>15.559426</td>
      <td>22.907483</td>
      <td>-7.836124</td>
      <td>-925.035951</td>
      <td>-477.513641</td>
      <td>...</td>
      <td>922.996409</td>
      <td>2065.873189</td>
      <td>11.992213</td>
      <td>6.905851</td>
      <td>9.379202</td>
      <td>11.948189</td>
      <td>14.506437</td>
      <td>16.949973</td>
      <td>11.172436</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.476680</td>
      <td>-7.562915</td>
      <td>-0.990487</td>
      <td>5.460864</td>
      <td>12.300924</td>
      <td>18.198132</td>
      <td>28.246484</td>
      <td>-27.341083</td>
      <td>-1600.672131</td>
      <td>-838.235028</td>
      <td>...</td>
      <td>1593.826334</td>
      <td>3490.605142</td>
      <td>11.850025</td>
      <td>4.584845</td>
      <td>8.108923</td>
      <td>11.948359</td>
      <td>15.576750</td>
      <td>18.838617</td>
      <td>15.672765</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.623194</td>
      <td>-10.131975</td>
      <td>-2.190721</td>
      <td>5.619511</td>
      <td>13.667489</td>
      <td>20.828463</td>
      <td>33.521954</td>
      <td>-25.706204</td>
      <td>-2023.135720</td>
      <td>-1020.310109</td>
      <td>...</td>
      <td>1976.801902</td>
      <td>4495.154388</td>
      <td>11.856579</td>
      <td>2.484105</td>
      <td>7.055508</td>
      <td>11.986611</td>
      <td>16.702210</td>
      <td>21.154200</td>
      <td>20.509156</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5.546416</td>
      <td>-12.252470</td>
      <td>-3.574075</td>
      <td>5.639882</td>
      <td>14.895643</td>
      <td>23.001258</td>
      <td>38.746669</td>
      <td>-13.558671</td>
      <td>-2379.563663</td>
      <td>-1251.878133</td>
      <td>...</td>
      <td>2414.020511</td>
      <td>5208.321245</td>
      <td>11.764425</td>
      <td>0.007495</td>
      <td>5.929463</td>
      <td>11.970629</td>
      <td>17.901548</td>
      <td>23.259075</td>
      <td>25.810779</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5.581483</td>
      <td>-14.460884</td>
      <td>-4.374071</td>
      <td>5.717293</td>
      <td>16.159674</td>
      <td>25.392325</td>
      <td>44.174692</td>
      <td>-11.356173</td>
      <td>-2653.949737</td>
      <td>-1383.703745</td>
      <td>...</td>
      <td>2600.439124</td>
      <td>5762.199086</td>
      <td>11.723146</td>
      <td>-2.477550</td>
      <td>4.442342</td>
      <td>11.961860</td>
      <td>19.104818</td>
      <td>25.857278</td>
      <td>31.204954</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5.613496</td>
      <td>-16.490943</td>
      <td>-5.647691</td>
      <td>5.755443</td>
      <td>17.388834</td>
      <td>27.903984</td>
      <td>49.411796</td>
      <td>7.574173</td>
      <td>-2702.463410</td>
      <td>-1435.091907</td>
      <td>...</td>
      <td>2773.059877</td>
      <td>6234.290004</td>
      <td>11.765973</td>
      <td>-4.880963</td>
      <td>3.186261</td>
      <td>11.971879</td>
      <td>20.195727</td>
      <td>28.234134</td>
      <td>36.761645</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>5.627369</td>
      <td>-19.466820</td>
      <td>-6.615704</td>
      <td>5.741161</td>
      <td>18.380366</td>
      <td>30.401483</td>
      <td>54.863843</td>
      <td>22.337994</td>
      <td>-2894.909616</td>
      <td>-1521.952767</td>
      <td>...</td>
      <td>3076.000751</td>
      <td>6654.200620</td>
      <td>11.740107</td>
      <td>-7.753346</td>
      <td>2.116845</td>
      <td>12.039736</td>
      <td>21.825397</td>
      <td>30.746570</td>
      <td>42.500810</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>5.658766</td>
      <td>-22.205153</td>
      <td>-8.593998</td>
      <td>5.798889</td>
      <td>19.735038</td>
      <td>32.927000</td>
      <td>60.649840</td>
      <td>34.841054</td>
      <td>-2969.289207</td>
      <td>-1550.387197</td>
      <td>...</td>
      <td>3193.057670</td>
      <td>6978.436806</td>
      <td>11.751560</td>
      <td>-10.161366</td>
      <td>0.590241</td>
      <td>12.044420</td>
      <td>23.094756</td>
      <td>33.391937</td>
      <td>48.187327</td>
      <td>1</td>
    </tr>
    <tr>
      <th>10</th>
      <td>5.871107</td>
      <td>-24.400879</td>
      <td>-9.835128</td>
      <td>6.010792</td>
      <td>21.511863</td>
      <td>35.847601</td>
      <td>66.656701</td>
      <td>9.373607</td>
      <td>-3188.769168</td>
      <td>-1716.532710</td>
      <td>...</td>
      <td>3280.751421</td>
      <td>7314.629133</td>
      <td>11.712832</td>
      <td>-13.032004</td>
      <td>-0.421179</td>
      <td>12.014188</td>
      <td>24.035227</td>
      <td>35.921519</td>
      <td>54.186238</td>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>5.913465</td>
      <td>-27.171841</td>
      <td>-10.985810</td>
      <td>5.769348</td>
      <td>22.904657</td>
      <td>39.200440</td>
      <td>73.337927</td>
      <td>-16.119261</td>
      <td>-3455.722574</td>
      <td>-1835.836559</td>
      <td>...</td>
      <td>3462.795648</td>
      <td>7738.012899</td>
      <td>11.785056</td>
      <td>-15.918763</td>
      <td>-1.687638</td>
      <td>12.018545</td>
      <td>25.542641</td>
      <td>38.729644</td>
      <td>59.950624</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
<p>12 rows × 22 columns</p>
</div>

There is clearly something wrong with these forecasts.

The specified distributions are hitting the right moments. They also appear to be close to normal:

```python
fig, axes = plt.subplots(1,3, figsize=(12,4))

for ax, idx,ttl in zip(axes, [1,2,4], ['FEDFUNDS[+1]', 'PAYEMS[+1]', 'FEDFUNDS[+2]']):
  sns.distplot(fc.raw_output[:,idx], ax=ax)
  ax.set_title(ttl)
```

![png](https://i.imgur.com/IvIoPLa.png)

## Checking objects

### Checking the moment matrices

The script parser is rather elaborate. There could be a problem there.

`f` contains the means of each equation. We should see the means of the each equation here in order:

```python
fc.f
```

    array([11.92220994,  2.4       ,  2.38      ])

`Omega_f` contains the covariance matrix of all of the equations. The diagonal elements should be the squares of the `std`s specified in the script. The off-diagonals are computed automatically using a heuristic. These should just not look to crazy or large:

```python
fc.Omega_f
```

    array([[ 0.01      , -0.00297401, -0.00329426],
           [-0.00297401,  0.0121    ,  0.01306796],
           [-0.00329426,  0.01306796,  0.0144    ]])

`Xi` contains the equation matrix that converts the forecast vector into the equations:

```python
fc.Xi
```

    array([[0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
            0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
            0., 0., 0., 0.],
           [0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
            0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
            0., 0., 0., 0.],
           [0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
            0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
            0., 0., 0., 0.]])

All of these objects look to be okay.

### ❌ Inspecting the raw data

Let's plot the raw data to see if there is anything odd going on there. All of the randomness in the sampler is iid across periods so we should basically see a random walk.

```python
rd = fc.raw_output
n_periods = fc.n_periods

fig, axes = plt.subplots(n_periods, len(series), sharex=True, figsize=(4*len(series), 2*n_periods))
axes = axes.flatten()

ff = 1
for ii,(ax,s) in enumerate(zip(axes, np.tile(series,[fc.n_periods]))):

  ax.plot(rd[:,ii].T, alpha=0.25)
  ax.set_title('{}[+{}]'.format(s,ff))

  if ii != 0 and (ii+1) % len(series) == 0:
    ff += 1
```

![png](https://i.imgur.com/d9Z8q2s.png)

This is **not** what was expected!!! We shouldn't see the variance of the draws increase across iterations.

#### Distinguishing between stochastic and non-stochastic components

Below, I replicate a non-jit version of the forecast method. The main difference is that it outputs three different outputs for the overall, non-stochastic, and stochastic parts of the prediction.

```python
from bvar.core.jit_ops import apw_objects as apw
from bvar.core.jit_ops import utils as jit_utils

def apw_nonhier_split(
  y, Xi, f, Omega_f,
  vecB_mean, B_XpXomega_inv, Sigma_scale,
  Sigma_dof, p, n, n_periods, samples
):
  # Make certain that input is C-contiguous

  # Size: [samples, n, n]
  Sigma_draws = jit_utils.random_invwishart(
    Sigma_scale, Sigma_dof, False, samples
  )
  # Size: [samples, n*k]
  B_eps_draws = np.random.randn(samples, n * (n * p + 1))
  # Size: [samples, n*n_periods]
  eps_draws = np.random.randn(samples, n * n_periods)
  M_cast = np.zeros((n * n_periods, n * n_periods))

  out = np.empty((samples, n*n_periods))
  out_stoch = np.empty((samples, n*n_periods))
  out_nonstoch = np.empty((samples, n*n_periods))

  for ii in range(samples):

    # Setup: Posterior
    #   Draw `Sigma` given y [done in bulk above]

    #   Set covariance of B [With Cholesky Transform]
    #   Size: [n*k, n*k], (k = n*p+1)
    B_cov = np.linalg.cholesky(jit_utils.kron(Sigma_draws[ii], B_XpXomega_inv))

    #   Draw B given Covariance/Sigma
    vecB = vecB_mean + np.dot(B_cov, B_eps_draws[ii])

    # Forecast Matrices:
    #   Set `b`
    B = apw.B_pad(vecB=vecB, p=p, n=n, n_periods=n_periods)
    C = jit_utils.vecB_to_C(vecB=vecB, p=p, n=n)

    b = apw.b(
      y=y,
      C=C,
      B=B,
      n=n,
      p=p,
      n_periods=n_periods,
      out=np.zeros((n_periods * n))
    )

    #   Set `M`
    # TODO: There is a way to exclude a cholesky within `M` when drawing the `Sigma`s. So edit `M` to exclude this and update other files that depend on it.
    M = apw.M(
      B=B,
      sigma=Sigma_draws[ii],
      p=p,
      n=n,
      n_periods=n_periods,
      out=M_cast
    )

    # Draw `eps`: The
    #   Preliminary Objects
    D = apw.D(Xi=Xi, M=M)
    Dhat = apw.Dhat(D=D)
    Dstar = apw.Dstar(D=D)
    #   Set E[eps]
    mu_eps = np.dot(Dstar, f) - np.dot(Dstar, np.dot(Xi, b))
    #   Set Cov(eps)
    cov_eps = np.dot(Dstar, np.dot(Omega_f,
                                   Dstar.T)) + np.dot(Dhat.T, Dhat)
    #   Combine
    eps = mu_eps + np.dot(np.linalg.cholesky(cov_eps), eps_draws[ii])

    # Append to output objects
    out[ii] = b + np.dot(M, eps)
    out_stoch[ii] = b
    out_nonstoch[ii] = np.dot(M, eps)

  return out, out_stoch, out_nonstoch
```

```python
graph = bvs.graph

# Set up the inputs to main
call_inputs = {
  'y':
    graph.data[graph.T0 - graph.dims['p']:graph.T0],
  'Xi':
    fc.Xi,
  'f':
    fc.f,
  'Omega_f':
    fc.Omega_f,
  'vecB_mean':
    jit_utils.blockB_to_vecB(graph._post['B_mean']),
  'B_XpXomega_inv':
    np.ascontiguousarray(graph._post['B_XpXomega_inv']),
  'Sigma_scale':
    graph._post['Sigma_scale'],
  'Sigma_dof':
    graph._post['Sigma_dof'],
  'p':
    fc.dims['p'],
  'n':
    fc.dims['n'],
  'n_periods':
    fc.n_periods,
  'samples':
    fc.iters,
}

# # Run main
out, out_stoch, out_non_stoch = apw_nonhier_split(
  **call_inputs
)
```

```python
n_periods = fc.n_periods

fig, axes = plt.subplots(n_periods, len(series), sharex=True, figsize=(4*len(series), 2*n_periods))
axes = axes.flatten()

ff = 1
for ii,(ax,s) in enumerate(zip(axes, np.tile(series,[fc.n_periods]))):

  ax.plot(out_stoch[:,ii].T, alpha=0.25)
  ax.plot(out_non_stoch[:,ii].T, alpha=0.25)
  ax.set_title('{}[+{}]'.format(s,ff))

  if ii != 0 and (ii+1) % len(series) == 0:
    ff += 1
```

![png](https://i.imgur.com/vA28pon.png)

From this it is pretty clear that the issues are driven by the stochastic components.

#### The problem is spotted!

Basically, by recycling the `M_cast` object on each iteration and modifying it inplace we were adding to `M` on each iteration.

```python
M = apw.M(
  B=B,
  sigma=Sigma_draws[ii],
  p=p,
  n=n,
  n_periods=n_periods,
  out=M_cast   # <= This gets added to each iteration! Switch to: np.zeros((n * n_periods, n * n_periods), dtype=FTYPE_)
)
```

## Check to ensure fixed

Let's try this again... (I've now changed out the spot where this occurs)

```python
fc = bvs.forecast(
    n_periods = 12,
    kind = 'apw',
    hierarchical = False,
    script = conditional_forecast_script,
    iters = 2000,
    chains = 4
  )
```

```python
fc.plot()
```

    (<Figure size 864x288 with 3 Axes>,
     array([<matplotlib.axes._subplots.AxesSubplot object at 0x000000000E4094E0>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x000000000D2F1518>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x000000000E8D50B8>],
           dtype=object))

![png](https://i.imgur.com/X8sIwzv.png)

```python
fc.table()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="6" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
      <th>forecast</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>...</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.533173</td>
      <td>5.526604</td>
      <td>5.529408</td>
      <td>5.533231</td>
      <td>5.537042</td>
      <td>5.539732</td>
      <td>0.012705</td>
      <td>2.401620</td>
      <td>2.327736</td>
      <td>2.353171</td>
      <td>...</td>
      <td>2.475970</td>
      <td>0.109902</td>
      <td>11.921388</td>
      <td>11.855022</td>
      <td>11.880555</td>
      <td>11.921605</td>
      <td>11.963199</td>
      <td>11.988387</td>
      <td>0.099682</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.534718</td>
      <td>5.524487</td>
      <td>5.528774</td>
      <td>5.534994</td>
      <td>5.541007</td>
      <td>5.545167</td>
      <td>0.020474</td>
      <td>2.381337</td>
      <td>2.300180</td>
      <td>2.329013</td>
      <td>...</td>
      <td>2.462064</td>
      <td>0.119818</td>
      <td>11.922489</td>
      <td>11.844395</td>
      <td>11.874524</td>
      <td>11.922592</td>
      <td>11.971924</td>
      <td>12.001695</td>
      <td>0.118009</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.536547</td>
      <td>5.520710</td>
      <td>5.527030</td>
      <td>5.536867</td>
      <td>5.546262</td>
      <td>5.552583</td>
      <td>0.030097</td>
      <td>2.366034</td>
      <td>0.908887</td>
      <td>1.540285</td>
      <td>...</td>
      <td>3.790469</td>
      <td>2.617085</td>
      <td>11.923634</td>
      <td>11.823097</td>
      <td>11.861770</td>
      <td>11.924117</td>
      <td>11.986967</td>
      <td>12.025757</td>
      <td>0.151756</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.538336</td>
      <td>5.517833</td>
      <td>5.526188</td>
      <td>5.538832</td>
      <td>5.550884</td>
      <td>5.559501</td>
      <td>0.037799</td>
      <td>2.344480</td>
      <td>-0.798226</td>
      <td>0.481561</td>
      <td>...</td>
      <td>5.426476</td>
      <td>5.419108</td>
      <td>11.924831</td>
      <td>11.805131</td>
      <td>11.850887</td>
      <td>11.925257</td>
      <td>12.000620</td>
      <td>12.047580</td>
      <td>0.182134</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.539993</td>
      <td>5.512204</td>
      <td>5.523566</td>
      <td>5.540188</td>
      <td>5.557264</td>
      <td>5.568397</td>
      <td>0.049613</td>
      <td>2.283845</td>
      <td>-2.623778</td>
      <td>-0.636558</td>
      <td>...</td>
      <td>7.078116</td>
      <td>8.077784</td>
      <td>11.925957</td>
      <td>11.785076</td>
      <td>11.838623</td>
      <td>11.926168</td>
      <td>12.015040</td>
      <td>12.069961</td>
      <td>0.214250</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5.541688</td>
      <td>5.508227</td>
      <td>5.521917</td>
      <td>5.542135</td>
      <td>5.562477</td>
      <td>5.575368</td>
      <td>0.059253</td>
      <td>2.239899</td>
      <td>-3.838576</td>
      <td>-1.362642</td>
      <td>...</td>
      <td>8.192642</td>
      <td>9.844131</td>
      <td>11.927095</td>
      <td>11.766536</td>
      <td>11.828073</td>
      <td>11.927074</td>
      <td>12.028716</td>
      <td>12.090180</td>
      <td>0.243919</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5.543293</td>
      <td>5.505190</td>
      <td>5.520934</td>
      <td>5.543638</td>
      <td>5.566754</td>
      <td>5.582003</td>
      <td>0.067436</td>
      <td>2.217935</td>
      <td>-4.599846</td>
      <td>-1.814368</td>
      <td>...</td>
      <td>8.959856</td>
      <td>11.046254</td>
      <td>11.928237</td>
      <td>11.750143</td>
      <td>11.817598</td>
      <td>11.927849</td>
      <td>12.041178</td>
      <td>12.109273</td>
      <td>0.270739</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5.544864</td>
      <td>5.504021</td>
      <td>5.521073</td>
      <td>5.545470</td>
      <td>5.569614</td>
      <td>5.585691</td>
      <td>0.073069</td>
      <td>2.204713</td>
      <td>-5.990077</td>
      <td>-2.713566</td>
      <td>...</td>
      <td>10.444572</td>
      <td>13.133010</td>
      <td>11.929360</td>
      <td>11.734260</td>
      <td>11.808182</td>
      <td>11.928828</td>
      <td>12.053132</td>
      <td>12.128500</td>
      <td>0.296614</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>5.546326</td>
      <td>5.502312</td>
      <td>5.520922</td>
      <td>5.546866</td>
      <td>5.572887</td>
      <td>5.590428</td>
      <td>0.079233</td>
      <td>2.200300</td>
      <td>-7.514453</td>
      <td>-3.719410</td>
      <td>...</td>
      <td>11.952905</td>
      <td>15.388279</td>
      <td>11.930562</td>
      <td>11.719804</td>
      <td>11.799209</td>
      <td>11.929597</td>
      <td>12.064530</td>
      <td>12.146453</td>
      <td>0.321327</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>5.547685</td>
      <td>5.498237</td>
      <td>5.519005</td>
      <td>5.547992</td>
      <td>5.577478</td>
      <td>5.597326</td>
      <td>0.088710</td>
      <td>2.200745</td>
      <td>-8.940283</td>
      <td>-4.572513</td>
      <td>...</td>
      <td>13.523067</td>
      <td>17.558040</td>
      <td>11.931793</td>
      <td>11.704739</td>
      <td>11.790681</td>
      <td>11.930356</td>
      <td>12.075706</td>
      <td>12.162960</td>
      <td>0.346612</td>
      <td>1</td>
    </tr>
    <tr>
      <th>10</th>
      <td>5.549066</td>
      <td>5.491060</td>
      <td>5.514185</td>
      <td>5.549533</td>
      <td>5.584703</td>
      <td>5.607296</td>
      <td>0.102954</td>
      <td>2.211845</td>
      <td>-10.115462</td>
      <td>-5.337715</td>
      <td>...</td>
      <td>14.656335</td>
      <td>19.340423</td>
      <td>11.933075</td>
      <td>11.692267</td>
      <td>11.783453</td>
      <td>11.931575</td>
      <td>12.086159</td>
      <td>12.178513</td>
      <td>0.368030</td>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>5.550669</td>
      <td>5.482073</td>
      <td>5.509146</td>
      <td>5.550820</td>
      <td>5.593010</td>
      <td>5.618850</td>
      <td>0.119205</td>
      <td>2.231687</td>
      <td>-10.706966</td>
      <td>-5.690462</td>
      <td>...</td>
      <td>15.268597</td>
      <td>20.447136</td>
      <td>11.934345</td>
      <td>11.681418</td>
      <td>11.777082</td>
      <td>11.932647</td>
      <td>12.095014</td>
      <td>12.192745</td>
      <td>0.387298</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
<p>12 rows × 22 columns</p>
</div>

Nice....
