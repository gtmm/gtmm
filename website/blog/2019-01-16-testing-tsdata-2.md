---
title: Testing the TsData class: Aggregation
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

This part continues on a previous post related to testing the `TsData` class. The main focus will be on aggregation of data series after a transform.

<!--truncate-->

## What needs to be tested?

We have three different options for aggregating the data:

1. Monthly ("M")
2. Quarterly ("Q")
3. Annually ("A")

as well as three ways to combine observations from a given aggregation group:

1. Sum
1. Average
1. End-of-period/last observation in the period.

We do _not_ need to test any preliminary transformations because the form of the output of a transformed dataset is identical to the untransformed dataset.

We do need to test across each type of frequency as the aggregation clearly depends on it.

We also need to test what happens when different changes are made to the aggregation and aggregation method after an object has been instantiated.

## Setting up the testing environment and data

Similar to the tests by transformation, we'll set up the test data using an excel spreadsheet ("by hand") then import the data and work from there.

I'll use the same base data as the previous tests.









