---
title: DataFrame transformations
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Discussing some transformations that might be done with

<!--truncate-->

## Setup

```js
function createData(n = 3, T = 10) {
  var data = [];
  for (let ii = 0; ii < n; ii++) {
    var col = (Math.random() + 1).toString(36).substring(7),
      month = 1,
      day = 1;
    for (let tt = 0; tt < T; tt++) {
      data.push({
        date: new Date(1975, month, day),
        series: col,
        value: Math.random(),
      });
      if (ii % 2) {
        day += 1;
      } else {
        month += 1;
      }
    }
  }
  return data;
}

var columnNames = ["date", "series", "value"];
var data = createData();
```

## ✔ Sort orders

We need each of the dataframes to be sorted by date.

```js
function compareDates(a, b) {
  return a.date > b.date;
}
var d2 = data.sort(compareDates);
```
