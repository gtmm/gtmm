---
title: Downsampling time series data
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

When plotting data we don't necessarily want to plot every point, which could lead to big lags.

<!--truncate-->

## Resources

- [Downsampling Time Series for Visual Representation](https://skemman.is/bitstream/1946/15343/3/SS_MSthesis.pdf) Sveinn Steinarsson.
- [JS implementation of 3-bucket](https://bl.ocks.org/FraserChapman/649f1aba28f6bc941d5c)
- [`d3fc-sample` library](https://d3fc.io/api/sample-api.html#largest-triangle-three-buckets)

## Issues to consider

- Resample on every zoom?
- Maybe just cut down on number of tooltips?
