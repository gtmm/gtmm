---
title: FRED Metadata + Google APIs for hosting
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

In the modified version of the into two packages:

1. `@gtmm/data` package does all of the (relatively quick) work of coordinating data and carrying out searches.
1. `@gtmm/fred-metadata-download` splits out the `FredMetadata` class for downloading the metadata files.

This was done because it makes sense to download the FRED metadata as seldom as possible (takes a long time + thousands of API calls) then download the compiled files for use elsewhere.

To do this, though, we're going to need a place to store and retrieve the files. For that we use [Google Drive APIs](https://developers.google.com/drive/).

This post considers some implementation details.

<!--truncate-->

## Drive quickstart

Let's start at the [quickstart](https://developers.google.com/drive/api/v3/quickstart/nodejs?authuser=2).

## Basic Read and Write with Drive

```js
var fileMetadata = {
  name: "photo.jpg",
};
var media = {
  mimeType: "image/jpeg",
  body: fs.createReadStream("files/photo.jpg"),
};
drive.files.create(
  {
    resource: fileMetadata,
    media: media,
    fields: "id",
  },
  function(err, file) {
    if (err) {
      // Handle error
      console.error(err);
    } else {
      console.log("File Id: ", file.id);
    }
  }
);
```
