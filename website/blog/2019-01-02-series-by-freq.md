---
title: DEV:Focusing on series with only particular frequencies.
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

In a previous post I found that it was best to only focus on series that were non-annual. This post does some further exploration of the FRED API in order to understand how best to implement this.

<!--truncate-->

Recall that we got the following "frequency frequency" results:

| Frequency | # of Series |
| --------- | ----------- |
| M         | 90635       |
| A         | 300292      |
| NA        | 35          |
| Q         | 60055       |
| D         | 1002        |
| BW        | 15          |
| W         | 2049        |
| 5Y        | 541         |
| SA        | 1893        |

It still isn't entirely clear what `BW`stands for (bi-weekly??). It also isn't clear why we'd have series that are `SA` or `NA`.

# Example series by frequency

Let's look for example series from each frequency. All of these requests were made via a variation on, e.g,

```bash
curl --request GET \
  --url 'https://api.stlouisfed.org/fred/series?series_id=UMCSENT1&api_key=abcdefghijklmnopqrstuvwxyz123456&file_type=json'
```

(Notice that you need to input your own api key here.)

### 5-Year

```json
{
  "id": "SMPOPNETMGEO",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Net migration for Georgia",
  "observation_start": "1962-01-01",
  "observation_end": "2017-01-01",
  "frequency": "5 Year",
  "frequency_short": "5Y",
  "units": "People",
  "units_short": "People",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2018-09-27 13:31:42-05",
  "popularity": 1,
  "notes": "Net migration is the net total of migrants during the period, that is, the total number of immigrants less the annual number of emigrants, including both citizens and noncitizens. Data are five-year estimates.\n\nWorld Bank Source: United Nations Population Division, World Population Prospects.\n\nSource Indicator: SM.POP.NETM"
}
```

### Annual

```json
{
  "id": "ULQBBU02MXA662N",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Benchmarked Unit Labor Costs - Manufacturing for Mexico (DISCONTINUED)",
  "observation_start": "1970-01-01",
  "observation_end": "2010-01-01",
  "frequency": "Annual",
  "frequency_short": "A",
  "units": "Index 2010=1, Trend",
  "units_short": "Index 2010=1, Trend",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2014-10-06 14:40:45-05",
  "popularity": 1,
  "notes": "[truncated by MW]"
}
```

### Semiannual

```json
{
  "id": "CUUSA212SASS",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Consumer Price Index for All Urban Consumers: Services in Milwaukee-Racine, WI (CMSA) (DISCONTINUED)",
  "observation_start": "1984-01-01",
  "observation_end": "2017-07-01",
  "frequency": "Semiannual",
  "frequency_short": "SA",
  "units": "Index 1982-1984=100",
  "units_short": "Index 1982-1984=100",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2018-01-12 09:14:16-06",
  "popularity": 1,
  "notes": "This series was discontinued as a result of the introduction of a new geographic area sample for the Consumer Price Index. Information on the geographic revision is available at https://www.bls.gov/cpi/additional-resources/geographic-revision-2018.htm."
}
```

### Quarterly

```json
{
  "id": "ULQBBU02LUQ662N",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Benchmarked Unit Labor Costs - Manufacturing for Luxembourg (DISCONTINUED)",
  "observation_start": "1985-01-01",
  "observation_end": "2011-07-01",
  "frequency": "Quarterly",
  "frequency_short": "Q",
  "units": "Index 2010=1, Trend",
  "units_short": "Index 2010=1, Trend",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2013-11-05 13:31:57-06",
  "popularity": 0,
  "notes": "[truncated by MW]"
}
```

### Monthly

```json
{
  "id": "HOHWMN02DEM661N",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Weekly Hours Worked: Manufacturing for Germany (DISCONTINUED)",
  "observation_start": "1957-01-01",
  "observation_end": "2006-10-01",
  "frequency": "Monthly",
  "frequency_short": "M",
  "units": "Index 2005=1",
  "units_short": "Index 2005=1",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2013-08-22 09:01:38-05",
  "popularity": 1,
  "notes": "[truncated by MW]"
}
```

### Biweekly

Notice the difference between "Bi-weekly, Ending Wednesday" and other possibilities.

```json
{
  "id": "INTREQ2",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Interest Rate Paid on Required Reserve Balances (Institutions with 2-Week Maintenance Period) (DISCONTINUED)",
  "observation_start": "2008-10-22",
  "observation_end": "2013-06-26",
  "frequency": "Biweekly, Ending Wednesday",
  "frequency_short": "BW",
  "units": "Percent",
  "units_short": "%",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2013-06-26 15:32:46-05",
  "popularity": 2,
  "notes": "[truncated by MW]"
}
```

### Weekly

```json
{
  "id": "TLSFRIW027SBOG",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "Trading Liabilities, Foreign-Related Institutions (DISCONTINUED)",
  "observation_start": "1996-10-02",
  "observation_end": "2018-01-03",
  "frequency": "Weekly, Ending Wednesday",
  "frequency_short": "W",
  "units": "Billions of U.S. Dollars",
  "units_short": "Bil. of U.S. $",
  "seasonal_adjustment": "Seasonally Adjusted",
  "seasonal_adjustment_short": "SA",
  "last_updated": "2018-01-16 12:21:22-06",
  "popularity": 1,
  "notes": "[truncated by MW]"
}
```

### Daily

```json
{
  "id": "ZAFRECDP",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "OECD based Recession Indicators for South Africa from the Peak through the Period preceding the Trough",
  "observation_start": "1960-02-01",
  "observation_end": "2018-07-31",
  "frequency": "Daily, 7-Day",
  "frequency_short": "D",
  "units": "+1 or 0",
  "units_short": "+1 or 0",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2018-10-09 18:11:22-05",
  "popularity": 1,
  "notes": "[truncated by MW]"
}
```

### Not Applicable

```json
{
  "id": "UMCSENT1",
  "realtime_start": "2019-01-02",
  "realtime_end": "2019-01-02",
  "title": "University of Michigan: Consumer Sentiment (DISCONTINUED)",
  "observation_start": "1952-11-01",
  "observation_end": "1977-11-01",
  "frequency": "Not Applicable",
  "frequency_short": "NA",
  "units": "Index 1966:Q1=100",
  "units_short": "Index 1966:Q1=100",
  "seasonal_adjustment": "Not Seasonally Adjusted",
  "seasonal_adjustment_short": "NSA",
  "last_updated": "2004-01-12 12:08:18-06",
  "popularity": 27,
  "notes": "Please see FRED data series UMCSENT for monthly data beginning in January 1978.\n\nThis data should be cited as follows: \"Surveys of Consumers, University of Michigan, University of Michigan: Consumer Sentiment (DISCONTINUED)© [UMCSENT1], retrieved from FRED, Federal Reserve Bank of St. Louis https://fred.stlouisfed.org/series/UMCSENT1/, (Accessed on date)\"\n\nFor more information about the survey, please see:\nUnited States, and Bureau of Economic Analysis. Handbook of Cyclical Indicators: A Supplement to the Business Conditions Digest. (1977) p. 31, https://fraser.stlouisfed.org/publication/?pid=178\n\nCopyright, 2016, Surveys of Consumers, University of Michigan. Reprinted with permission."
}
```

## Looking into "Not applicable" frequencies further

Examining the [data for `UMCSENT1`](https://fred.stlouisfed.org/series/UMCSENT1) suggests that this frequency is applied to series that are:

1. Non-uniformly spaced (e.g. collect in May one year and August the next); _or_
1. Have a non-standard number of observations per year (e.g. 3).

In the case of `UMCSENT1` both of the above apply. The first few entries have three entries per year:

| Date           | UMCSENT1 |
| -------------- | -------- |
| 1952-11-01     | 86.2     |
| 1953-02-01     | 90.7     |
| 1953-**08**-01 | 80.8     |
| 1953-11-01     | 80.7     |
| 1954-02-01     | 82.0     |
| 1954-**05**-01 | 82.9     |
| 1954-11-01     | 87.0     |
| 1955-02-01     | 95.9     |

However, notice that the second observation of the year was August in 1953 and May in 1954. So it isn't just that the number of observations per year is odd, the distribution of the observations in the year is non-uniform.

Moreover, there were only two observations in 1957 and 1959.

From about 1960 onward there were four observations per year: February, May, August, and November.

It seems to me to be pretty safe to guess that `UMCSENT1` - which was collected by the University of Michigan - was collected irregularly in the first few years of its existence then become more regular as it received better funding.

I don't think it is necessary to dive into this any further. The 35 "NA" series can be safely ignored.

## Summary of findings from examples

1. 👍 "SA" stands for semiannual, _not_ "seasonally adjusted"
1. 😕 "NA" truly does stand for "Not applicable".
1. ❗ Weekly and Biweekly need to be differentiated on when they end: Wednesday, Friday, etc.

# Filtering to the correct series in API calls

When making API calls in `FredMetadata` we want to minimize the number of series that need to be called.

We can fetch series by category and release by passing particular filters. The basic idea is to pass the argument `filter_variable=frequency` and `filter_value=[frequency]`.

In the case of tags we have less control.

## Generality of filter

There are numerous cases where there are many variations on a given frequency. For example, "Daily" v. "Daily, 7-Day".

The request below is an example to demonstrate that a general search like "Daily" will bring in all variations on the theme:

```bash
curl --request GET \
  --url 'https://api.stlouisfed.org/fred/release/series?release_id=441&filter_variable=frequency&filter_value=Daily&api_key=[redacted]&file_type=json&limit=10'
```

This call returns (among others) `CBBTCUSD` which has a frequency "Daily, 7-Day".

It is interesting to note that passing a value "D" (for example) will not yield results. So there doesn't seem to be any initial string matching occurring under the hood.

## Multiple frequencies at once?

It would be nice to be able to specify that we want series with either frequency day _or_ month (for example). This would cut down on API calls.

It does not appear that this is possible. Consider the case of the Consumer Price Index (release id 10), which has both monthly and semiannual releases.

```bash
curl --request GET \
  --url 'https://api.stlouisfed.org/fred/release/series?release_id=10&filter_variable=frequency&filter_value=Monthly&api_key=[redacted]&file_type=json&limit=10'
```

Trying this with `filter_value=Monthly` and `filter_value=Semiannual` both yielded results. However, none of:

- `filter_value=Monthly;Semiannual`
- `filter_value=Monthly|Semiannual`
- `filter_value=Monthly+Semiannual`
- `filter_value=Monthly&Semiannual`
- `filter_value=Monthly&filter_value=Semiannual`

yielded the union of the two calls.

## Dealing with tag -> series connections

Since we can't filter series connected to tags, special arrangements must be made. In short, in `fetchEdgesTagSeries` we need to make certain that the only series added are within the correct subset of frequencies.

# Conclusion: Action Items

What needs to be done?

1. We need to change the structure of the series tag so that it can take the full frequency name.
1. Only request data on series with frequencies `Q`, `M`, `BW`, `W`, and `D`. This will lead to a likely total of 163,756 series.

- Add a constant to the project that sets these out.
