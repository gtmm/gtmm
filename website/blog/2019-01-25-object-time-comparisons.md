---
title: Some time trials on object keys.
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Is it faster to convert date to string for object index then look up strings or use dates?

<!--truncate-->

## Compiling objects

```js
const shortid = require("shortid");
var m = 5,
  nRuns = 5;

const generateData = (m_, N_) => {
  var now = new Date(),
    id,
    date;

  var data = {};

  for (let ii = 0; ii < m_; ii++) {
    id = shortid.generate();
    date = now;
    data[id] = [];
    for (let jj = 0; jj < N_; jj++) {
      data[id].push({ date: date, value: Math.random(), series: id });

      if (Math.random() > 0.75) {
        date = new Date(date - 1e5);
      } else {
        date = new Date(date - 1e6);
      }
    }
  }

  return data;
};

const compareObjectDeconstructWithDate = N_ => {
  var data = generateData(m, N_);

  var avg0, avg1, tic0, toc0, out0, tic1, toc1, out1, col;

  avg0 = 0;
  for (let rr = 0; rr < nRuns; rr++) {
    tic0 = new Date();
    out0 = {};
    for (let ii = 0; ii < m; ii++) {
      col = Object.keys(data)[ii];
      for (let jj = 0; jj < N_; jj++) {
        let djj = data[col][jj].date;
        if (!out0[djj]) {
          out0[djj] = { date: djj };
        }
        out0[djj][col] = data[col][jj].value;
      }
    }
    toc0 = new Date();
    avg0 += toc0 - tic0;
  }
  console.log(`With Dates:   ${avg0 / 5}`);
};

const compareObjectDeconstructWithString = N_ => {
  var data = generateData(m, N_);
  var avg0, avg1, tic0, toc0, out0, tic1, toc1, out1, col;

  avg1 = 0;
  for (let rr = 0; rr < nRuns; rr++) {
    tic1 = new Date();
    out1 = {};
    for (let ii = 0; ii < m; ii++) {
      col = Object.keys(data)[ii];
      for (let jj = 0; jj < N_; jj++) {
        let djj = data[col][jj].date.toString();
        if (!out1[djj]) {
          out1[djj] = { date: data[col][jj].date };
        }
        out1[djj][col] = data[col][jj].value;
      }
    }
    toc1 = new Date();
    avg1 += toc1 - tic1;
  }
  console.log(`With Strings: ${avg1 / nRuns}`);

  return avg1 / nRuns;
};

console.log(
  `Time comparisons out of ${nRuns} runs for data with ${m} variables.`
);
for (let N of [1000, 1e5]) {
  console.log(`With ${N} observations`);
  compareObjectDeconstructWithDate(N);
  compareObjectDeconstructWithString(N);
}
```

_returns_

```
Time comparisons out of 5 runs for data with 5 variables.
With 1000 observations
With Dates:   57.6
With Strings: 17
With 100000 observations
With Dates:   4304.6
With Strings: 1799.4
```

So let's go with the latter!

## Sorting

```js
const shortid = require("shortid");

const generateWiderData = (m_, T_, funkProb) => {
  var data = {},
    now = new Date(),
    di,
    date,
    IDs = Array.apply(null, Array(m_)).map(ii => shortid.generate());

  for (let tt = 0; tt < T_; tt++) {
    // Interesting sorting.
    date =
      Math.random() > funkProb
        ? new Date(now * 1 + tt * 1e5 + 1e5)
        : new Date(now * 1 + tt * 1e5 - 1.23e5);
    di = { date: date };

    for (let ii = 0; ii < m_; ii++) {
      di[IDs[ii]] = Math.random() > 0.5 ? Math.random() : null;
    }
    data[date.toString()] = di;
  }

  return data;
};

const deconstructWithDates = (d, nRuns) => {
  var out,
    tic,
    toc,
    avg = 0;
  for (let rr = 0; rr < nRuns; rr++) {
    tic = new Date();
    out = Object.values(d).sort((a, b) => (a.date > b.date ? 1 : -1));
    toc = new Date();
    avg += toc - tic;
  }
  console.log(`With Dates:  ${avg / nRuns}`);
};

const deconstructWithStrings = (d, nRuns_) => {
  var out,
    tic,
    toc,
    avg = 0;
  for (let rr = 0; rr < nRuns_; rr++) {
    tic = new Date();
    out = Object.values(d).sort(
      (a, b) => (a.date.toString() > b.date.toString() ? 1 : -1)
    );
    toc = new Date();
    avg += toc - tic;
  }

  console.log(`With Strings: ${avg / nRuns_}`);
};

var T = 1000,
  m = 5,
  nRuns = 5;

console.log(
  `Time comparisons out of ${nRuns} runs for data with ${m} variables.`
);
for (let fp of [0.25, 0.1]) {
  console.log(`With disorganization probability ${fp}`);
  for (let T of [1000, 10000]) {
    var dataT = generateWiderData(m, T, fp);
    console.log(`With ${T} observations.`);
    deconstructWithDates(dataT, nRuns);
    deconstructWithStrings(dataT, nRuns);
  }
}
```

But should probably ask if even need to sort first....
