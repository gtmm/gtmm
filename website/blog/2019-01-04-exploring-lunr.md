---
title: Using lunr with FRED data
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

This post considers results returned by `lunr` for our FRED metadata.

Assume the following setup and a node console running from the `@gtmm/data` base directory.

```js
var SearchMetadata = require("./build/src/SearchMetadata.js").default;
var C = require("./build/src/constants.js");

var SM = new SearchMetadata(C.DEFAULT_DATA_PATH, false, "cursor");

SM.loadDatabase();
```

<!--truncate-->

(The last commit before this post was `bb38dfb39614e2e8cab334817524361a89f201f6`. Experiments reported here will probably not work on the most recent versions of the repo.)

## ❗ Not working out of box

### Non-empty searches return empty sets

There seem to be some issues with even basic calls:

```js
SM.series("E").exec((err, docs) => console.log(err, docs)); // null []
SM.releases("E").exec((err, docs) => console.log(err, docs)); // null []
```

However, if we split these up then we certainly get something. Here are the results from a lunr search:

```js
SM._ln.series.search("E")[
  // [returns]
  ({
    ref: "15784",
    score: 13.525,
    matchData: { metadata: [Object] },
  },
  {
    ref: "15787",
    score: 13.525,
    matchData: { metadata: [Object] },
  },
  {
    ref: "15785",
    score: 11.503,
    matchData: { metadata: [Object] },
  },
  {
    ref: "15786",
    score: 11.503,
    matchData: { metadata: [Object] },
  })
];
```

Now the nedb search does something _like_ this:

```js
SM._db.series.find(
  { _id: { $in: ["15784", "15787", "15785", "15786"] } },
  (err, docs) => console.log(err, docs, docs.length)
);
// [returns]
null [...] 4
```

So this looks pretty good.

In fact, a more exact version of the NEDB search was:

```js
SM._db.series.find(
  { $and: [{ _id: { $in: ["15784", "15787", "15785", "15786"] } }, null] },
  (err, docs) => console.log(err, docs, docs.length)
);
// [returns]
> null [] 0
```

So that is the problem there.

What if the `null` was excluded?

```js
SM._db.series.find(
  { $and: [{ _id: { $in: ["15784", "15787", "15785", "15786"] } }] },
  (err, docs) => console.log(err, docs, docs.length)
);
// [returns]
null [...] 4
```

So that was the problem. What if we replace with an empty object?

```js
SM._db.series.find(
  { $and: [{ _id: { $in: ["15784", "15787", "15785", "15786"] } }, {}] },
  (err, docs) => console.log(err, docs, docs.length)
);
// [returns]
null [...] 4
```

A simple fix was added to line 44 of `SearchMetadata.ts`.

### Series searches don't find IDs

This was an issue with `PackLunr` stemming from the change from text to numeric indices for series.

### Too many search results returned

A search like `SM.series('E*')` will return tens of thousands of results. This slows down nedb substantially. (Other aspects are also slowed down but not as much.)

One way to start to fix this is to allow limits on the number of results that are returned. _I've added a `limit` argument that can be appended to any search call._

Another way to cut down on the amount of information that is shuttled between different places is to use NEDB's projections.

## Special Searches

The [lunr docs](https://lunrjs.com/guides/searching.html) have good material on special types of searches.

### Wildcards for preliminary searches

In most "live search" situations where results are returned as the user types, it would probably make sense to use wildcards at the end:

```js
var searches = { "No Wildcard": "title:E", Wildcard: "title:E*" };

for (let k in searches) {
  SM.series(searches[k], null, 10).exec((err, docs) => {
    console.log(`${k} (search='${searches[k]}')`);
    console.log(`Number of documents returned: ${docs.length}`);
    console.log("First 10 titles returned:");
    docs.map((d, ii) => console.log(ii, d.title));
  });
}
```

_returns_

```js
> No Wildcard (search='title:E')
Number of documents returned: 4
First 10 titles returned:
0 'E-Commerce Retail Sales'
1 'E-Commerce Retail Sales as a Percent of Total Sales'
2 'E-Commerce Retail Sales as a Percent of Total Sales'
3 'E-Commerce Retail Sales'
Wildcard (search='title:E*')
Number of documents returned: 10
First 10 titles returned:
0 'Harmonized Index of Consumer Prices: Milk, Cheese, and Eggs for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
1 'Harmonized Index of Consumer Prices: Electricity for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
2 'Harmonized Index of Consumer Prices: Personal Effects, Not Elsewhere Classified for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
3 'Harmonized Index of Consumer Prices: Personal Effects, Not Elsewhere Classified for European Union (EU6-1972, EU9-1980, EU10-1985, EU12-1994, EU15-2004, EU25-2006, EU27-2013, EU28)'
4 'Harmonized Index of Consumer Prices: Energy for European Union (EU6-1972, EU9-1980, EU10-1985, EU12-1994, EU15-2004, EU25-2006, EU27-2013, EU28)'
5 'Real Gross Domestic Product (Euro/ECU series) for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
6 'Real Gross Domestic Product (Euro/ECU series) for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
7 'Gross Domestic Product (Euro/ECU series) for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
8 'Gross Domestic Product (Euro/ECU series) for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
9 'Harmonized Index of Consumer Prices: Education for Euro area (EA11-2000, EA12-2006, EA13-2007, EA15-2008, EA16-2010, EA17-2013, EA18-2014, EA19)'
```

Clearly, the no-wildcard option is focusing on the "E" in "E-Commerce". The wildcard option is picking up on the large number of "E"s in the results, especially with respect to the "EA\*" and "EU\*" that show up at the end. These codes are probably being picked up first because they likely don't appear anywhere else in the document.

In this particular case, it seems to me that the non-wildcard results (i.e. "E-Commerce") are more relevant than the non-wildcard. Other relevant results pertaining to "Earnings" and "Employment" (for example) are lurking in the results but they won't be ranked as high here.

What if our search was two characters?

```js
var searches = { "No Wildcard": "title:Em", Wildcard: "title:Em*" };

for (let k in searches) {
  SM.series(searches[k], null, 10).exec((err, docs) => {
    console.log(`${k} (search='${searches[k]}')`);
    console.log(`Number of documents returned: ${docs.length}`);
    console.log("First 10 titles returned:");
    docs.map((d, ii) => console.log(ii, d.title));
  });
}
```

_returns_

```js
> No Wildcard (search='title:Em')
Number of documents returned: 1
First 10 titles returned:
0 'German Intervention: Bundesbank Purchases in EMS Markets (Millions of DEM) (DISCONTINUED)'
Wildcard (search='title:Em*')
Number of documents returned: 10
First 10 titles returned:
0 'ICE BofAML Europe, the Middle East, and Africa (EMEA) US Emerging Markets Liquid Corporate Plus Sub-Index Effective Yield'
1 'ICE BofAML Europe, the Middle East, and Africa (EMEA) US Emerging Markets Liquid Corporate Plus Sub-Index Option-Adjusted Spread'
2 'ICE BofAML Europe, the Middle East, and Africa (EMEA) Emerging Markets Corporate Plus Sub-Index Effective Yield'
3 'ICE BofAML Europe, the Middle East, and Africa (EMEA) Emerging Markets Corporate Plus Sub-Index Option-Adjusted Spread'
4 'ICE BofAML Europe, the Middle East, and Africa (EMEA) Emerging Markets Corporate Plus Sub-Index Semi-Annual Yield to Worst'
5 'ICE BofAML Europe, the Middle East, and Africa (EMEA) Emerging Markets Corporate Plus Sub-Index Total Return Index Value'
6 'Unemployment Rate in Emanuel County, GA'
7 'Unemployment Rate in Emery County, UT'
8 'Unemployment Rate in Emmons County, ND'
9 'Unemployment Rate in Emporia City, VA'
```

That's a lot better! _So move to wildcards only if the search query is greater than one character._

### Boosting

A boost is a way to make some arguments more relevant than others.

#### Baseline example

The example given in the lunr docs is:

```js
SM.idOnly = true;

var searches = { "With Boost": "EM*^2 UN*", "No Boost": "EM* UN*" };

for (let kk in searches) {
  var wb = SM.series(searches[kk]); // With Boost
  console.log(`${kk} (Search='${searches[kk]}'):` + "\n", wb.slice(0, 5), "\n");
}

SM.idOnly = false;
```

_returns_

```js
With Boost (Search='EM*^2 UN*'):
 [ { _id: '4612', score: 1.5092751368988266 },
  { _id: '25959', score: 1.324570342777168 },
  { _id: '25982', score: 1.324570342777168 },
  { _id: '25981', score: 1.324570342777168 },
  { _id: '25980', score: 1.324570342777168 } ]

No Boost (Search='EM* UN*'):
 [ { _id: '117737', score: 1.571720637814918 },
  { _id: '119324', score: 1.571720637814918 },
  { _id: '19853', score: 1.3349492924202526 },
  { _id: '4612', score: 1.1948263820879303 },
  { _id: '491', score: 1.1777368141199176 } ]
```

These are certainly different.

#### Boosting by field?

What if we boosted according to the field? That is, in a series search the series id might be given priority over the title search:

```js
SM.idOnly = true;

var searches = {
  "With Boost": "id:EM*^2 title:EM*",
  "No Boost": "id:EM* title:EM*",
};

for (let kk in searches) {
  var wb = SM.series(searches[kk]); // With Boost
  console.log(`${kk} (Search='${searches[kk]}'):` + "\n", wb.slice(0, 5), "\n");
}

SM.idOnly = false;
```

_returns_

```js
With Boost (Search='id:EM*^2 title:EM*'):
 [ { _id: '25961', score: 1.626352192652457 },
  { _id: '4612', score: 1.626352192652457 },
  { _id: '25939', score: 1.626352192652457 },
  { _id: '25982', score: 1.626352192652457 },
  { _id: '25981', score: 1.626352192652457 } ]

No Boost (Search='id:EM* title:EM*'):
 [ { _id: '25961', score: 1.626352192652457 },
  { _id: '4612', score: 1.626352192652457 },
  { _id: '25939', score: 1.626352192652457 },
  { _id: '25982', score: 1.626352192652457 },
  { _id: '25981', score: 1.626352192652457 } ]
```

This does nothing. _Moreover,_ requiring matches on both `id` _and_ `title` means that relevant series may be left out. There doesn't appear to be a way to search in `id` _or_ `title` without doing two searches.

Given this, it probably makes the most sense to not use field identifiers for the most part unless, e.g., they are explicitly set.

## ❗ Canceling promises

With Bluebird [promises](http://bluebirdjs.com/docs/api/cancel.html), it should be possible to cancel searches when they are carried out as promises. This could be useful when, e.g., a user is typing in a search in real time.

```js
var Promise = require("bluebird");
SM.returnAs = "promise";

// prettier-ignore
var P = SM.series("EMPLOY*") .then(out => console.log("done")) .catch(e => {throw e; });

P.cancel();
SM.returnAs = "cursor";
```

It doesn't work... Even when the promise configuration is set correctly?

I think that either lunr or nedb is a blocking call, which is preventing the cancellation.

```js
var Promise = require("bluebird");
SM.returnAs = "promise";
SM.idOnly = true;

// prettier-ignore
var P = SM.series("EMPLOY*") .then(out => console.log("done")) .catch(e => {throw e; });

P.cancel();
SM.idOnly = false;
SM.returnAs = "cursor";
```

It appears that `lunr.search` blocks.

## Performance

Start out with ID only:

```js
SM.idOnly = true;
var searches = ["EMPLOY*", "id:EMPLOY*", "EMPLOY", "EMPLOYMENT*"];
var limits = [10, 100, 500, 1000, 5000];

searches.map((s, ii) => {
  console.log(`${"SEARCH".padEnd(14)}` + " LIMIT RET   TIME");
  limits.map((lim, jj) => {
    var tic = new Date();
    var docs = SM.series(s, {}, lim);
    console.log(
      `${('"' + s + '"').padEnd(14)} ${lim
        .toString()
        .padEnd(5)} ${docs.length.toString().padEnd(5)} ${(new Date() - tic) /
        1000}s.`
    );
  });

  var tic = new Date();
  var docs = SM.series(s, {});
  console.log(
    `${('"' + s + '"').padEnd(14)} ${"INF".padEnd(
      5
    )} ${docs.length.toString().padEnd(5)} ${(new Date() - tic) / 1000}s.`
  );
  console.log();
});

SM.idOnly = false;
```

_returns_

```bash
SEARCH         LIMIT RET   TIME
"EMPLOY*"      10    10    0.228s.
"EMPLOY*"      100   100   0.242s.
"EMPLOY*"      500   500   0.662s.
"EMPLOY*"      1000  1000  0.208s.
"EMPLOY*"      5000  5000  0.213s.
"EMPLOY*"      INF   45443 0.217s.

SEARCH         LIMIT RET   TIME
"id:EMPLOY*"   10    10    0.001s.
"id:EMPLOY*"   100   51    0.001s.
"id:EMPLOY*"   500   51    0.001s.
"id:EMPLOY*"   1000  51    0.001s.
"id:EMPLOY*"   5000  51    0s.
"id:EMPLOY*"   INF   51    0s.

SEARCH         LIMIT RET   TIME
"EMPLOY"       10    10    0.019s.
"EMPLOY"       100   100   0.023s.
"EMPLOY"       500   500   0.018s.
"EMPLOY"       1000  1000  0.026s.
"EMPLOY"       5000  5000  0.018s.
"EMPLOY"       INF   5262  0.025s.

SEARCH         LIMIT RET   TIME
"EMPLOYMENT*"  10    0     0s.
"EMPLOYMENT*"  100   0     0s.
"EMPLOYMENT*"  500   0     0s.
"EMPLOYMENT*"  1000  0     0s.
"EMPLOYMENT*"  5000  0     0.001s.
"EMPLOYMENT*"  INF   0     0s.
```

Not surprisingly, it is pretty consistently low across the board.

What if we do a full-on cursor-based approach?

```js
function performanceFull() {
  var searches = ["EMPLOY*", "id:EMPLOY*", "EMPLOY", "EMPLOYMENT*"];
  var limits = [10, 100, 500, 1000, 5000, Infinity];

  function timeItRecur(sIdx, lIdx) {
    var tic = new Date();
    SM.series(searches[sIdx], {}, limits[lIdx]).exec((err, docs) => {
      console.log(
        `${('"' + searches[sIdx] + '"').padEnd(14)} ${limits[lIdx]
          .toString()
          .slice(0, 5)
          .padEnd(5)} ${docs.length.toString().padEnd(5)} ${(new Date() - tic) /
          1000}s.`
      );

      if (lIdx === limits.length - 1) {
        if (sIdx === searches.length - 1) {
          return null;
        } else {
          return timeItRecur(sIdx + 1, 0);
        }
      } else {
        return timeItRecur(sIdx, lIdx + 1);
      }
    });
  }

  console.log(`${"SEARCH".padEnd(14)}` + " LIMIT RET   TIME");
  timeItRecur(0, 0);
}

performanceFull();
```

```bash
SEARCH         LIMIT RET   TIME
"EMPLOY*"      10    10    0.478s.
"EMPLOY*"      100   100   0.497s.
"EMPLOY*"      500   500   0.96s.
"EMPLOY*"      1000  1000  1.6s.
"EMPLOY*"      5000  5000  8.3s.
"EMPLOY*"      Infin 45443 59.046s.
"id:EMPLOY*"   10    10    0.154s.
"id:EMPLOY*"   100   51    0.199s.
"id:EMPLOY*"   500   51    0.194s.
"id:EMPLOY*"   1000  51    0.197s.
"id:EMPLOY*"   5000  51    0.193s.
"id:EMPLOY*"   Infin 51    0.197s.
"EMPLOY"       10    10    0.594s.
"EMPLOY"       100   100   0.297s.
"EMPLOY"       500   500   0.936s.
"EMPLOY"       1000  1000  1.4s.
"EMPLOY"       5000  5000  7.408s.
"EMPLOY"       Infin 5262  7.263s.
"EMPLOYMENT*"  10    0     0.15s.
"EMPLOYMENT*"  100   0     0.167s.
"EMPLOYMENT*"  500   0     0.145s.
"EMPLOYMENT*"  1000  0     0.145s.
"EMPLOYMENT*"  5000  0     0.129s.
"EMPLOYMENT*"  Infin 0     0.147s.
```

So it is definitely the NEDB fetch that is accounting for most of the time.

Let's try something similar to what was done above except this time we'll always limit the cursor to executing on 10 documents:

```js
function performanceFull2() {
  var searches = ["EMPLOY*", "id:EMPLOY*", "EMPLOY", "EMPLOYMENT*"];
  var limits = [10, 100, 500, 1000, 5000, Infinity];

  function timeItRecur(sIdx, lIdx) {
    var tic = new Date();
    // ONLY CHANGE: .limit(10)
    SM.series(searches[sIdx], {}, limits[lIdx])
      .limit(10)
      .exec((err, docs) => {
        console.log(
          `${('"' + searches[sIdx] + '"').padEnd(14)} ${limits[lIdx]
            .toString()
            .slice(0, 5)
            .padEnd(5)} ${docs.length.toString().padEnd(5)} ${(new Date() -
            tic) /
            1000}s.`
        );

        if (lIdx === limits.length - 1) {
          if (sIdx === searches.length - 1) {
            return null;
          } else {
            return timeItRecur(sIdx + 1, 0);
          }
        } else {
          return timeItRecur(sIdx, lIdx + 1);
        }
      });
  }

  console.log(`${"SEARCH".padEnd(14)}` + " LIMIT RET   TIME");
  timeItRecur(0, 0);
}

performanceFull2();
```

_returns_

```bash
SEARCH         LIMIT RET   TIME
"EMPLOY*"      10    10    0.806s.
"EMPLOY*"      100   10    0.357s.
"EMPLOY*"      500   10    0.306s.
"EMPLOY*"      1000  10    0.314s.
"EMPLOY*"      5000  10    0.42s.
"EMPLOY*"      Infin 10    0.433s.
"id:EMPLOY*"   10    10    0.111s.
"id:EMPLOY*"   100   10    0.118s.
"id:EMPLOY*"   500   10    0.129s.
"id:EMPLOY*"   1000  10    0.134s.
"id:EMPLOY*"   5000  10    0.157s.
"id:EMPLOY*"   Infin 10    0.118s.
"EMPLOY"       10    10    0.148s.
"EMPLOY"       100   10    0.217s.
"EMPLOY"       500   10    0.065s.
"EMPLOY"       1000  10    0.076s.
"EMPLOY"       5000  10    0.212s.
"EMPLOY"       Infin 10    0.197s.
"EMPLOYMENT*"  10    0     0.161s.
"EMPLOYMENT*"  100   0     0.603s.
"EMPLOYMENT*"  500   0     0.218s.
"EMPLOYMENT*"  1000  0     0.206s.
"EMPLOYMENT*"  5000  0     0.208s.
"EMPLOYMENT*"  Infin 0     0.227s.
```

It is interesting to note that the time was sometimes lower for _higher_ limits. Maybe this is because it is faster to find when the set of acceptable outputs is greater?

In any case, this is _not_ a good option because it ignores the ranking of the search results.

## Conclusion

**Items to be addressed:**

1. Canceling of promises.
1. Add ability to paginate search results?

**Some Best Practices:**

1. Wildcards are a good way to go. _However,_ consider only using them when the query is, e.g., greater than 2 characters long in order to avoid idiosyncratic cases.
1. Do not use field identifiers for lunr searches unless they are explicitly set.
1. For now, do not try to do "live" updates. Instead, only re-query after a pause in typing.
