---
title: Making Empirical Bayes Optimization More Robust
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

The goal of this post is to suss out some ways to make optimization in the empirical bayes scheme more robust.

<!--truncate-->

```python
%reload_ext autoreload
%autoreload 2
%matplotlib inline
%config InlineBackend.figure_format = 'retina'
```

```python
import os
import tempfile
import pickle

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import GtmmData
import bvar
```

```python
sns.set_style('whitegrid')
sns.set_palette('deep')

FRED_KEY = os.environ['FRED_KEY']
```

```python
def get_forecast_data3v(force_reload=False):
  save_file = os.path.join(tempfile.gettempdir(), 'bvar-usage', 'apw_forecast_out_data_3var.pkl')
  BASE_PATH = os.path.join(tempfile.gettempdir(), 'bvar-usage')
  BASE_FORECAST_PATH = os.path.join(tempfile.gettempdir(), 'bvar-usage', 'base-forecast')

  model_name = '3-var'
  series = ['CPIAUCSL', 'FEDFUNDS', 'PAYEMS']

  model_base_path = os.path.join(BASE_PATH, model_name)
  data_path = os.path.join(model_base_path, 'data.pkl')
  model_path = os.path.join(model_base_path, 'model.pkl')
  if not os.path.exists(model_base_path):
      os.makedirs(model_base_path)

  if not os.path.exists(data_path) or force_reload:
    fred_data0 = GtmmData.FredData(apiKey=FRED_KEY)
    fred_data0.getFredData(ids=series) # Download data
    fred_data0.setTrans(['CPIAUCSL', 'PAYEMS'], 'log') # Make these variables logs
    fred_data = fred_data0.format(balanced=True, freq='coarse',trim=None) # Format it
    fred_data.to_pickle(data_path)
  else:
    fred_data = GtmmData.read_pickle(data_path)

  return fred_data
```

```python
# Get the data
fred_data = get_forecast_data3v(force_reload=False)
# Initiate the BVAR object
bvs = bvar.core.BVAR(
  data=fred_data.data,
  lags=13,
  prior_spec='DIO_MN_SOC',
  hyperparameters={'Lambda': 0.45, 'mu': 0.214, 'delta': 0.124, 'dof': 14}
)
```

## Understanding what is going on...

The initial model that we created was

### By Prior

Let's look at each prior:

```python
def print_eb_info(eb_):
  print('Success? ({}) {}'.format(eb_.res.fun,eb_.res.success))
  if hasattr(eb_.res,'jac'):
    print('Optimization Criteria Value: {}'.format(np.max(np.abs(eb_.res.jac))))
  if eb_.res.success:
    for k,v in eb_.get_xstar().items():
      print('{} = {}'.format(k,v))
```

#### Minnesota

Let's approach this incrementally:

Just optimizing $\lambda$ seems to work fine:

```python
bvs.update(prior_spec='MN', hyperparameters={'LAMBDA': 0.45})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6196.686055481917) True
    Optimization Criteria Value: 9.439582371650879e-06
    LAMBDA = 0.1694558414558298

Optimizing $\lambda$ and $\psi$ with the minimal degrees of freedom necessary also works:

```python
bvs.update(prior_spec='MN', hyperparameters={'LAMBDA': 0.45, 'dof': 14})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
    'PSI': {}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6184.109919714526) True
    Optimization Criteria Value: 4.076060856839092e-06
    LAMBDA = 0.18660211979466082
    PSI = [6.48356888e-05 1.82043557e+00 8.78825372e-05]

**However,** adding any more degrees of freedom will result in an error:

```python
bvs.update(prior_spec='MN', hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
    'PSI': {}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6184.524174806198) False
    Optimization Criteria Value: 0.0010511194505885835


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

```python
bvs.update(prior_spec='MN', hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6197.091504577585) False
    Optimization Criteria Value: 3.558469002194753e-05


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

Now do a smaller `dof` to see what happens:

```python
bvs.update(prior_spec='MN', hyperparameters={'LAMBDA': 0.45, 'dof': 8})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6191.132361776406) True
    Optimization Criteria Value: 1.563842262991555e-06
    LAMBDA = 0.25128495800957135

It appears that this is true even when we just want to optimize $\lambda$!

Findings thus far:

1. We can succesfully optimize the Minnesota prior for all hyperparameters so long as $dof\leq14$.
1. For any $dof>14$ we get divide by zero errors.

Implications going forward:

- All other priors nest the Minnesota prior so this is likely what is affecting those as well.
- The only reason we have such high degrees of freedom (the default is usually $n+2$) is that we have a high number of lags (13).
  - In further priors we make use of the multigamma function $\Gamma_d(a)$.
  - To compute $\Gamma_d(a)$ we need $a > (d-1)/2$ with $a > 0$.

#### Update: `RuntimeWarning: divide by zero encountered in power`

We were getting a lot warnings that read:

```
C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\autograd\numpy\numpy_vjps.py:97: RuntimeWarning: divide by zero encountered in power
  defvjp(anp.sqrt,    lambda ans, x : lambda g: g * 0.5 * x**-0.5)
```

It became apparent that the divide by zero warnings were coming from the backwards pass in autograd. In particular, they were from a `sqrt` term. After some inspection of the marginal likelihood it became clear that it was coming from line 50 of `bvar/core/autograd_ops/ML.py`. We changed:

```python
Omega_chol = np.sqrt(Omega)
```

to:

```python
Omega_chol = np.diag(np.sqrt(np.diag(Omega)))
```

This makes sense because $\Omega$ is a diagonal matrix (that's why we can compute the cholesky so easily). So the backwards pass was choking on the off-diagonal elements of $\Omega$. We've fixed that now.

The other thing to note is that it doesn't make sense to only get the diagonal of $\Omega$ in the `prior_common.Omega` function because we use $\Omega$ all over the place in other parts of the same function.

#### MN-DIO

Interestingly, we _can_ do the estimation with $\lambda$ alone with $dof>14$ with the DIO prior.

```python
bvs.update(prior_spec=['MN','DIO'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6325.639537575419) True
    Optimization Criteria Value: 1.0799919412662895e-06
    LAMBDA = 0.1834884958274614

However, cannot add $\psi$:

```python
bvs.update(prior_spec=['MN','DIO'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'PSI': {},
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6321.731860838286) False
    Optimization Criteria Value: 1.2217577081585773e-05
    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

And we can't do it when $\delta$ is added to the objective either.

```python
bvs.update(prior_spec=['MN','DIO'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6329.571104056096) False
    Optimization Criteria Value: 0.0006469752318784133


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

### Is it coming from the hyperpriors?

Perhaps our non-flat hyperpriors are the issue?

_It appears to be part of the issues but by no means the entire thing..._

```python
bvs.update(prior_spec=['MN'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {'hyperprior': 'diffuse'},
  },
    autograd_support = True,
    min_kwargs = {},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6197.759941802824) True
    Optimization Criteria Value: 0.0020512180321808457
    LAMBDA = 0.16107958211785203

```python
bvs.update(prior_spec=['MN'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {'hyperprior': 'diffuse'},
    'PSI': {'hyperprior': 'diffuse'}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6205.156565975586) False
    Optimization Criteria Value: 2.0482879833692235e-05


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

```python
bvs.update(prior_spec=['MN', 'DIO'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {'hyperprior': 'diffuse'},
#     'PSI': {'hyperprior': 'diffuse'},
      'DELTA': {'hyperprior': 'diffuse'}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6332.284679892626) True
    Optimization Criteria Value: 9.044343399200741e-06
    LAMBDA = 0.16798148044281475
    DELTA = 0.45474880033880516

```python
bvs.update(prior_spec=['MN', 'DIO'], hyperparameters={'LAMBDA': 0.45, 'dof': 15})
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {'hyperprior': 'diffuse'},
    'PSI': {'hyperprior': 'diffuse'},
      'DELTA': {'hyperprior': 'diffuse'}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6369.492012094681) False
    Optimization Criteria Value: 0.00012831677819464605


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

### Investigating the non-flat hyperpriors

I'm going to try modifying the lpdf of the hyperpriors a bit to print out some info about what is being passed in and out:

```python
# Comment out to preserve outputs
bvs.update(prior_spec=['MN', 'DIO'], hyperparameters={'LAMBDA': 0.45, 'dof': 15}, lags=13)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {'hyperprior': 'gamma'},
      'DELTA': {'hyperprior': 'gamma'}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6329.571104056096) False
    Optimization Criteria Value: 0.0006469752318784133


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

    x = Autograd ArrayBox with value 0.45, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.124, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.45, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.124, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.18308334102877596, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.19637147010988285, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.18308334102877596, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.19637147010988285, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.19011051237191837, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 2.381139276902786, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.19011051237191837, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 2.381139276902786, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.18608378452645877, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.576499206250835, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.18608378452645877, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.576499206250835, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.17609412816193576, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5490559367513136, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.17609412816193576, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5490559367513136, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16909455779988436, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5359334192848526, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16909455779988436, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5359334192848526, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16926186255474593, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5379809012298534, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16926186255474593, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5379809012298534, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468397, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468397, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16919743092668657, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.538556549393122, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16919743092668657, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.538556549393122, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920859113301387, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385329388599277, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920859113301387, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385329388599277, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862104000162, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328755921853, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862104000162, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328755921853, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.1692086212646839, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.1692086212646839, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468397, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468397, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468397, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468397, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.1692086212646839, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.1692086212646839, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168736, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = Autograd ArrayBox with value 0.16920862126468394, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = Autograd ArrayBox with value 0.5385328751168735, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16919743092668657, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.538556549393122, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920302600317533, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385447121249084, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.1692058236108018, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.538538793588369, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920722243196085, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385358343444908, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.1692079218468769, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385343547286495, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920827155541907, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385336149222534, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.1692084464099612, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385332450194364, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920853383729997, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385330600681232, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.1692085775509863, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385329675924905, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920859228726276, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385329364181326, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920860677597274, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385329057675022, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948
    x = 0.16920861402032822, loc = 1.6403882032022075, k = 0, theta = 0.3123105625617661
    x = 0.5385328904421877, loc = 2.6180339887498953, k = 0, theta = 0.6180339887498948

The divide by zero error should only be coming up from the hyperpriors here if $x \rightarrow loc$. But that is clearly not the case here.

It is also worth noting that, if you make either of the hyperpriors on either of the variables above `diffuse`, then you'll get a successful optimization.

Thus, we are finding that:

1. The choice of a non-flat hyperprior only affects optimization success by influencing the path of optimization.

## Interaction of lag choice and `dof`

Having a higher number of lags does necessitate a higher number of degrees of freedom in the prior. This may be a key area of interaction.

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': bvs.dims['n']+2}, lags=5)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6322.781387020665) False
    Optimization Criteria Value: 4.414580798994816e-05


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

Let's look more generally at other degrees of freedom:

```python
import warnings
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': 18}, lags=5)

print('lags DOF  SUCCESS?')
with warnings.catch_warnings(record=True):
  for ii in range(5,19):
    bvs.update(hyperparameters={'dof': ii})
    eb = bvs.empirical_bayes(
        hyperparameters={
        'LAMBDA': {},
          'DELTA': {},
          'MU':{},
          'PSI':{}
      },
        autograd_support = True,
        min_kwargs = {'method': 'BFGS'},
        set_hyperparameters = False
      )
    print('{:-4d} {:-4d} {}'.format(bvs.lags, ii, eb.res.success))
```

    lags DOF  SUCCESS?
       5    5 False
       5    6 False
       5    7 False
       5    8 False
       5    9 False
       5   10 False
       5   11 False
       5   12 False
       5   13 False
       5   14 False
       5   15 True
       5   16 False
       5   17 False
       5   18 False

With the exception of `dof=15` we didn't get success.

_Note._ If you try the same thing without optimizing over $\psi$ (i.e. comment out `'PSI': {}` line above), then you'll get success for degrees of freedom = 5 or 6.

So then we can conclude:

1. The error does not primarily seem to be driven by the number of lags or a high lag choice.
1. The error does not always occur with a low number of lags, though.
   - This suggests that there is definitely a role played by path taken by the optimization algorithm
   - That is, it is not just a divide by zero that is going to always arise.
   - Thus, there is a key interaction between the values of the hyperparameters and the `dof` that is playing out.

## Where does `dof` have a critical impact on the marginal likelihood?

- In construction of $\Omega$ (scale matrix of covariance matrix)
  - Enters each terms as $(dof-n-1)\cdot (\lambda/(\ell + 1))^2 / \psi_j$
  - Should not affect anything on the forward or backwards pass so long as $dof > n+1$
- In ML term $0.5 \cdot (T-p+dof) \cdot PebP$
  - Again, should not affect anything...
- In ML in two places as part of a `multigammaln` (i.e. $\Gamma_n(0.5 \cdot dof)$).
  - This seems like the most likely place.

### Effects of `multigammaln`

Let's see how the `multigammaln` terms might effect the ML.

First, plot the terms using parameters similar to what we're using in our model:

```python
from scipy.special import multigammaln

n = 3
x = np.arange(3, 18, 1e-1)
y0 = multigammaln(0.5*x, n)
y1 = multigammaln(0.5 * (1 + x), n)

fig, ax = plt.subplots()

ax.plot(x, y0, x, y1)
```

    [<matplotlib.lines.Line2D at 0xe164da0>,
     <matplotlib.lines.Line2D at 0xe63c518>]

![png](https://i.imgur.com/fy9OlEr.png)

This is a pretty smooth function...Neither the forward nor backwards pass should present much difficulty.

Nonetheless, let's try commenting out the `multigammaln` parts of the

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': bvs.dims['n']+2}, lags=5)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
#       'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6334.994653179569) False
    Optimization Criteria Value: 1.792203416617344e-05


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

    Success? False
    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:153: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.

    ---

    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
    warnings.warn(wm)
    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\autograd\numpy\numpy*vjps.py:97: RuntimeWarning: divide by zero encountered in power
    defvjp(anp.sqrt, lambda ans, x : lambda g: g * 0.5 _ x\*\*-0.5)
    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\autograd\numpy\numpy_vjps.py:97: RuntimeWarning: divide by zero encountered in power
    defvjp(anp.sqrt, lambda ans, x : lambda g: g _ 0.5 _ x\*\*-0.5)
    \_Nowhere_...

### Effects on $\Omega$

Let's just try getting rid of the part of $\Omega$ that depends on $dof$:

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': bvs.dims['n']+2}, lags=5)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6322.781387020665) False
    Optimization Criteria Value: 4.414580798994816e-05


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

    Success? False
    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:153: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.

    ---

    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
    warnings.warn(wm)
    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\autograd\numpy\numpy*vjps.py:97: RuntimeWarning: divide by zero encountered in power
    defvjp(anp.sqrt, lambda ans, x : lambda g: g * 0.5 _ x\*\*-0.5)
    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\autograd\numpy\numpy_vjps.py:97: RuntimeWarning: divide by zero encountered in power
    defvjp(anp.sqrt, lambda ans, x : lambda g: g _ 0.5 \_ x\*\*-0.5)

### Effects on other terms

Let's try selectively removing `dof` from the one remaining term. On line 82 of `core/autograd_ops/ML.py` replace this:

```python
...
    - 0.5 * n * OxxO +
    - 0.5 * (T0 - p + dof_prior) * PebP +
    scsp.multigammaln(0.5 * (T0 - p + dof_prior), n) +
...
```

with this:

```python
...
    - 0.5 * n * OxxO +
    -0.5 * PebP +#- 0.5 * (T0 - p + dof_prior) * PebP +
    scsp.multigammaln(0.5 * (T0 - p + dof_prior), n) +
...
```

Notice that we just changed the coefficient and didn't remove the `PebP` value, which is the log determinant of a rather complex matrix.

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': bvs.dims['n']+2}, lags=5)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS'},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6322.781387020665) False
    Optimization Criteria Value: 4.414580798994816e-05


    c:\users\us57144\dropbox\grantthornton\projects\macro_model\gtmm-py\models\bvar\bvar\core\bvar.py:156: UserWarning: Minimization Unsuccesful: (Status=2) Desired error not necessarily achieved due to precision loss.
    ----------------------------------------------------------------------
    Returning `EmpiricalBayes` object but not setting the hyperparameters in the model.
      warnings.warn(wm)

```python
import warnings
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': 18}, lags=5)
print_special = False

if print_special:
  with warnings.catch_warnings(record=True):
    print('lags DOF  SUCCESS?')
    for lags,rg in zip([5,13], [range(5,19), range(15, 20)]):
      for dof in rg:
        bvs.update(hyperparameters={'dof': dof}, lags=lags)
        eb = bvs.empirical_bayes(
            hyperparameters={
            'LAMBDA': {},
              'DELTA': {},
              'MU':{},
              'PSI':{}
          },
            autograd_support = True,
            min_kwargs = {'method': 'BFGS'},
            set_hyperparameters = False
          )

        print('{:-4d} {:-4d} {}'.format(lags, dof, eb.res.success))
```

    lags DOF SUCCESS?
    5 5 True
    5 6 True
    5 7 True
    5 8 True
    5 9 True
    5 10 True
    5 11 True
    5 12 True
    5 13 True
    5 14 True
    5 15 True
    5 16 True
    5 17 True
    5 18 True
    13 15 False
    13 16 True
    13 17 True
    13 18 True
    13 19 True

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': 15}, lags=13)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6347.880498613274) True
    Optimization Criteria Value: 0.0009326174962449585
    LAMBDA = 0.2787979108637575
    DELTA = 0.44071710340167075
    MU = 0.1484567217591506
    PSI = [4.54917843e-05 6.13320702e-01 6.84742584e-05]

Wow, that's a big and unexpected difference.

Let's try printing out these terms in the ML as they are computed so that we can try to get an idea of what is happening..

```python
# bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'dof': 16}, lags=13)
# eb = bvs.empirical_bayes(
#     hyperparameters={
#     'LAMBDA': {},
#       'DELTA': {},
#       'MU':{},
#       'PSI':{}
#   },
#     autograd_support = True,
#     min_kwargs = {'method': 'BFGS'},
#     set_hyperparameters = False
#   )

# print_eb_info(eb)
```

**Including** the dof from the PebP term we get:

    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 11.2 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.6e-08 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 11.2 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.6e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 11.4 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.03e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 11.4 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.03e-08 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 13.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 3.98e-05 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 13.8 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 3.98e-05 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.1 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 7.46e-09 | False
    ...
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.06e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.06e-08 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.28e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 2.48e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.45e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.43e-09 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 6.02e-09 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 4.97e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.81e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 8.42e-09 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.8 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 8.42e-09 | True
    Success? False

**Excluding** the dof from the PebP term we get:

    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 11.2 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.6e-08 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 11.2 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.6e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.9 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 5.12e-08 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 12.9 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 5.12e-08 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 19.8 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 4.93e-06 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 19.8 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 4.93e-06 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 35.7 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 1.55e-07 | False
    ...
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 35.9 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 2.22e-16 | True
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 35.9 | False
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 2.22e-16 | False
    T0 = 720 | p = 13 | dof = 16 | (_) = 723 | PebP = 35.9 | True
    T0 = 4 | p = 13 | dof = 16 | (_) = 7 | PebP = 2.22e-16 | True
    Success? True
    LAMBDA = 6.479879437755947e-06
    DELTA = 0.04584132666555306
    MU = 0.017137005367478315
    PSI = [1.11616588e-06 1.11598770e-06 1.11611910e-06]

The last entry in each line is `True` if the evaluation is a backwards pass. The cases where `T0=4` refer to cases where the ML is computed for the dummy observations alone. The case with `T0=720` is the case for the main data.

It is interesting to note a few things:

1. There is a big difference in the final value of PebP when `T0=720`.
   - Obviously, the ML is incorrect in the case where we had success.
1. In the second case the ML when `T0=4` only really differs in how close PebP gets to zero.
   - Removing the dummy variable ML does not do much to affect outcomes. (see cell below).
1. In the "included" case there are a lot of forward passes at the end with few backwards passes.

   - In the "excludes" case there are usually two forward passes followed by two backwards passes.
   - Inspection of the parameter values at each iteration shows that the parameter values aren't changing by much at the end of the failed "included" case run. Then again, same is true for successful "excluded" case and just about any successful optimization.

From this, can conclude that the issue is probably not `dof` per se, but rather the fact that the `(T0 - p + dof_prior)` term causes the `PebP` term to have a lot of weight in the marginal likelihood.

    (Output of optimization when remove the dummy variable part of the ML)
    lags DOF SUCCESS?
    5 5 False
    5 6 False
    5 7 False
    5 8 True
    5 9 False
    5 10 True
    5 11 False
    5 12 False
    5 13 False
    5 14 False
    5 15 True
    5 16 False
    5 17 False
    5 18 False
    13 15 False
    13 16 True
    13 17 False
    13 18 False
    13 19 True

## Fine-tuning optimization options

Given everything that we've seen above, it doesn't look like there is any issue with the ML or other inputs to the minimization routine.

So then it comes down to understanding what's happening with minimization.

### BFGS

The default minimization is `BFGS`. Here is a run-down of the important optimization options:

- `gtol=1e-5`. This is the level below which the gradient norm must fall in order to return successfully. Higher `gtol` means a less strenuous criterion for convergence.
- `norm=Inf`. This is the type of norm that is used in the convergence criterion:
  - `Inf`. This is the max-norm (i.e. the max of the absolute values of the gradient) $\max(|\nabla f|)$
  - `-Inf`. This is the min-norm: $\min(|\nabla f|)$
  - `p (int)`. Returns the p-norm: $(\sum (\nabla f)^p) ^ {\frac{1}{p}}$
- `eps (float)`. If you are using a numeric gradient this is the denominator of the difference.
- `maxiter=None`. Number of iterations to try.
- `disp=False`. Whether to display info at the end of minimization.
- `return_all=False`. Returns stuff.

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'MU': 0.45, 'DELTA': 0.45, 'dof': 14}, lags=13)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'BFGS',
                  'options':{'gtol': 1e-04, # 1e-5
                             'norm': np.inf,
                             'eps': 0, #1.4901161193847656e-08,
                             'maxiter': None,
                             'disp': False,
                             'return_all': False}
                 },
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6346.4236740935885) True
    Optimization Criteria Value: 7.1819475486401e-05
    LAMBDA = 0.2804908975534947
    DELTA = 0.4410433690622629
    MU = 0.14862988616822356
    PSI = [4.33550626e-05 5.04262329e-01 6.66708048e-05]

### Nelder-Mead

The Nelder-Mead method is nice because it does not use a gradient:

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'MU': 0.45, 'DELTA': 0.45, 'dof': 14}, lags=13)
ebnm = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'Nelder-Mead'},
    set_hyperparameters = False
  )

print_eb_info(ebnm)
```

    Success? (-6346.423674311365) True
    LAMBDA = 0.2804959270813664
    DELTA = 0.4410651191291515
    MU = 0.1486939630213952
    PSI = [4.33559561e-05 5.04224581e-01 6.66829890e-05]

However, having a gradient makes me want to use it...

### `L-BFGS-B`

The ["L-BFGS" family of algorithms](https://en.wikipedia.org/wiki/Limited-memory_BFGS) implements a limited-memory form of the BFGS algorithm. One of the main differences relative to the regular BFGS algorithm is how it approximates the optimization step $d_k = -H_k \nabla f(x_k)$.

Besides limiting the memory usage of the algorithm, the approximation does some important scaling of the initial hessian matrix to ensure that a unit step of $d_k$ is almost always appropriate. Below, we will see that this is one of the main areas where the regular BFGS algorithm "fails" with our problem:

```python
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'MU': 0.45, 'DELTA': 0.45, 'dof': 14}, lags=13)
eb = bvs.empirical_bayes(
    hyperparameters={
    'LAMBDA': {},
      'DELTA': {},
      'MU':{},
      'PSI':{}
  },
    autograd_support = True,
    min_kwargs = {'method': 'L-BFGS-B', 'options':{'ftol': 1e-10, 'gtol': 1e-10}},
    set_hyperparameters = False
  )

print_eb_info(eb)
```

    Success? (-6346.423674646724) True
    Optimization Criteria Value: 0.00016468466860253272
    LAMBDA = 0.28048876022989916
    DELTA = 0.4410561044280316
    MU = 0.14863014073000716
    PSI = [4.33548457e-05 5.04252192e-01 6.66702586e-05]

```python
# BFGS FVAL v. L-BFGS-B FVAL
-6346.4236740935885 < -6346.423674646724
```

    False

So this works quite well with the one curious caveat that the objective function value is slightly less (i.e. "more optimized") when using the "unconverged" BFGS results.

Let's check to see if we can do this across multiple specifications:

```python
import warnings
bvs.update(prior_spec=['MN','DIO','SOC'], hyperparameters={'LAMBDA': 0.45, 'MU': 0.45, 'DELTA': 0.45, 'dof': 14}, lags=13)
print_special = False

if print_special:
  with warnings.catch_warnings(record=True):
    print('lags DOF  SUCCESS?')
    for lags,rg in zip([5,13], [range(5,19), range(15, 20)]):
      for dof in rg:
        bvs.update(hyperparameters={'dof': dof}, lags=lags)
        eb = bvs.empirical_bayes(
            hyperparameters={
            'LAMBDA': {},
              'DELTA': {},
              'MU':{},
              'PSI':{}
          },
            autograd_support = True,
            min_kwargs = {'method': 'L-BFGS-B'},
            set_hyperparameters = False
          )

        print('{:-4d} {:-4d} {}'.format(lags, dof, eb.res.success))
```

```
lags DOF  SUCCESS?
   5    5 True
   5    6 True
   5    7 True
   5    8 True
   5    9 True
   5   10 True
   5   11 True
   5   12 True
   5   13 True
   5   14 True
   5   15 True
   5   16 True
   5   17 True
   5   18 True
  13   15 True
  13   16 True
  13   17 True
  13   18 True
  13   19 True
```

Although `L-BFGS-B` may not always produce as good of results as `BFGS`, it nonetheless produces results that are nearly identical.

Given that the optimization routine will generally be hidden under several API layers, it makes sense to make `L-BFGS-B` the default optimization method. This is because - in all tests so far - it returns successful results, which will not worry the user.

An alternative to this approach is to keep `BFGS` as the default method but modify the default `gtol` to be something like `1e-4`. The downside of this approach is that it lessens the restrictions on the function when it perhaps ought to be increasing retrictions. Moreover, this loosening of the bounds may not be enough (or too much) for certain datasets.

### Understanding BFGS under the hood

Below I have pasted in some parts of the relevant BFGS code from scipy in order to screw around with it. The biggest change I attempted was to make the function more resiliant to failures to find an appropriate step size. This can be seen in the `_minimize_bfgs` function.

```python
from scipy.optimize import minpack2, OptimizeResult, approx_fprime
from scipy.optimize.linesearch import LineSearchWarning, scalar_search_wolfe1, line_search_wolfe2, line_search_wolfe1
import warnings
numpy = np
_epsilon = np.sqrt(np.finfo(float).eps)

_status_message = {'success': 'Optimization terminated successfully.',
                   'maxfev': 'Maximum number of function evaluations has '
                              'been exceeded.',
                   'maxiter': 'Maximum number of iterations has been '
                              'exceeded.',
                   'pr_loss': 'Desired error not necessarily achieved due '
                              'to precision loss.'}


def vecnorm(x, ord=2):
    if ord == np.inf:
        return numpy.amax(numpy.abs(x))
    elif ord == -np.inf:
        return numpy.amin(numpy.abs(x))
    else:
        return numpy.sum(numpy.abs(x)**ord, axis=0)**(1.0 / ord)

def wrap_function(function, args):
    ncalls = [0]
    if function is None:
        return ncalls, None

    def function_wrapper(*wrapper_args):
        ncalls[0] += 1
        return function(*(wrapper_args + args))

    return ncalls, function_wrapper

class _LineSearchError(RuntimeError):
    pass

def line_search_wolfe1(f, fprime, xk, pk, gfk=None,
                       old_fval=None, old_old_fval=None,
                       args=(), c1=1e-4, c2=0.9, amax=50, amin=1e-8,
                       xtol=1e-14):
    """
    As `scalar_search_wolfe1` but do a line search to direction `pk`
    Parameters
    ----------
    f : callable
        Function `f(x)`
    fprime : callable
        Gradient of `f`
    xk : array_like
        Current point
    pk : array_like
        Search direction
    gfk : array_like, optional
        Gradient of `f` at point `xk`
    old_fval : float, optional
        Value of `f` at point `xk`
    old_old_fval : float, optional
        Value of `f` at point preceding `xk`
    The rest of the parameters are the same as for `scalar_search_wolfe1`.
    Returns
    -------
    stp, f_count, g_count, fval, old_fval
        As in `line_search_wolfe1`
    gval : array
        Gradient of `f` at the final point
    """
    if gfk is None:
        gfk = fprime(xk)

    if isinstance(fprime, tuple):
        eps = fprime[1]
        fprime = fprime[0]
        newargs = (f, eps) + args
        gradient = False
    else:
        newargs = args
        gradient = True

    gval = [gfk]
    gc = [0]
    fc = [0]

    def phi(s):
        fc[0] += 1
        return f(xk + s*pk, *args)

    def derphi(s):
        gval[0] = fprime(xk + s*pk, *newargs)
        if gradient:
            gc[0] += 1
        else:
            fc[0] += len(xk) + 1
        return np.dot(gval[0], pk)

    derphi0 = np.dot(gfk, pk)
#     print(gfk, pk)
#     print(old_fval, old_old_fval, derphi0,'\n','~'*10)
#     derphi0 = min(-1e-8, derphi0)
#     print('new derphi0', derphi0)
    stp, fval, old_fval = scalar_search_wolfe1(
            phi, derphi, old_fval, old_old_fval, derphi0,
            c1=c1, c2=c2, amax=amax, amin=amin, xtol=xtol)

#     print(stp, fval, old_fval)
#     print('-' * 25)

    return stp, fc[0], gc[0], fval, old_fval, gval[0]

def _line_search_wolfe12(f, fprime, xk, pk, gfk, old_fval, old_old_fval,
                         **kwargs):
    """
    Same as line_search_wolfe1, but fall back to line_search_wolfe2 if
    suitable step length is not found, and raise an exception if a
    suitable step length is not found.
    Raises
    ------
    _LineSearchError
        If no suitable step size is found
    """

    extra_condition = kwargs.pop('extra_condition', None)

    ret = line_search_wolfe1(f, fprime, xk, pk, gfk,
                             old_fval, old_old_fval,
                             **kwargs)

    if ret[0] is not None and extra_condition is not None:
        xp1 = xk + ret[0] * pk
        if not extra_condition(ret[0], xp1, ret[3], ret[5]):
            # Reject step if extra_condition fails
            ret = (None,)

    if ret[0] is None:
        # line search failed: try different one.
        with warnings.catch_warnings():
            warnings.simplefilter('ignore', LineSearchWarning)
            kwargs2 = {}
            for key in ('c1', 'c2', 'amax'):
                if key in kwargs:
                    kwargs2[key] = kwargs[key]
            ret = line_search_wolfe2(f, fprime, xk, pk, gfk,
                                     old_fval, old_old_fval,
                                     extra_condition=extra_condition,
                                     **kwargs2)

    if ret[0] is None:
        raise _LineSearchError()

    return ret

def fmin_bfgs(f, x0, fprime=None, args=(), gtol=1e-5, norm=np.inf,
              epsilon=_epsilon, maxiter=None, full_output=0, disp=1,
              retall=0, callback=None):
    opts = {'gtol': gtol,
            'norm': norm,
            'eps': epsilon,
            'disp': disp,
            'maxiter': maxiter,
            'return_all': retall}

    res = _minimize_bfgs(f, x0, args, fprime, callback=callback, **opts)

    if full_output:
      return res
    else:
        if retall:
            return res['x'], res['allvecs']
        else:
            return res['x']


def _minimize_bfgs(fun, x0, args=(), jac=None, callback=None,
                   gtol=1e-5, norm=np.inf, eps=_epsilon, maxiter=None,
                   disp=False, return_all=False,
                   **unknown_options):
    f = fun
    fprime = jac
    epsilon = eps
    retall = return_all

    x0 = np.asarray(x0).flatten()
    if x0.ndim == 0:
        x0.shape = (1,)
    if maxiter is None:
        maxiter = len(x0) * 200
    func_calls, f = wrap_function(f, args)
    if fprime is None:
        grad_calls, myfprime = wrap_function(approx_fprime, (f, epsilon))
    else:
        grad_calls, myfprime = wrap_function(fprime, args)
    gfk = myfprime(x0)
    k = 0
    N = len(x0)
    I = numpy.eye(N, dtype=int)
    Hk = I

    # Sets the initial step guess to dx ~ 1
    old_fval = f(x0)
    old_old_fval = old_fval + np.linalg.norm(gfk) / 2

    xk = x0
    if retall:
        allvecs = [x0]
    warnflag = 0
    gnorm = vecnorm(gfk, ord=norm)
    total_warnings = 0
    while (gnorm > gtol) and (k < maxiter):
        pk = -numpy.dot(Hk, gfk)
        try:
            alpha_k, fc, gc, old_fval, old_old_fval, gfkp1 = \
                     _line_search_wolfe12(f, myfprime, xk, pk, gfk,
                                          old_fval, old_old_fval, amin=1e-100, amax=1e100)
        except _LineSearchError as e:

            if total_warnings < 5:
              total_warnings += 1
              print('(Error)', alpha_k, total_warnings, vecnorm(gfk, ord=norm))
            else:
              # Line search failed to find a better solution.
              warnflag = 2
              break

        xkp1 = xk + alpha_k * pk
        if retall:
            allvecs.append(xkp1)
        sk = xkp1 - xk
        xk = xkp1
        if gfkp1 is None:
            gfkp1 = myfprime(xkp1)

        yk = gfkp1 - gfk
        gfk = gfkp1
        if callback is not None:
            callback(xk)
        k += 1
        gnorm = vecnorm(gfk, ord=norm)
        if (gnorm <= gtol):
            break

        if not numpy.isfinite(old_fval):
            # We correctly found +-Inf as optimal value, or something went
            # wrong.
            warnflag = 2
            break

        try:  # this was handled in numeric, let it remaines for more safety
            rhok = 1.0 / (numpy.dot(yk, sk))
        except ZeroDivisionError:
            rhok = 1000.0
            if disp:
                print("Divide-by-zero encountered: rhok assumed large")
        if np.isinf(rhok):  # this is patch for numpy
            rhok = 1000.0
            if disp:
                print("Divide-by-zero encountered: rhok assumed large")
        A1 = I - sk[:, numpy.newaxis] * yk[numpy.newaxis, :] * rhok
        A2 = I - yk[:, numpy.newaxis] * sk[numpy.newaxis, :] * rhok
        Hk = numpy.dot(A1, numpy.dot(Hk, A2)) + (rhok * sk[:, numpy.newaxis] *
                                                 sk[numpy.newaxis, :])

    fval = old_fval
    if np.isnan(fval):
        # This can happen if the first call to f returned NaN;
        # the loop is then never entered.
        warnflag = 2

    if warnflag == 2:
        msg = _status_message['pr_loss']
    elif k >= maxiter:
        warnflag = 1
        msg = _status_message['maxiter']
    else:
        msg = _status_message['success']

    if disp:
        print("%s%s" % ("Warning: " if warnflag != 0 else "", msg))
        print("         Current function value: %f" % fval)
        print("         Iterations: %d" % k)
        print("         Function evaluations: %d" % func_calls[0])
        print("         Gradient evaluations: %d" % grad_calls[0])

    result = OptimizeResult(fun=fval, jac=gfk, hess_inv=Hk, nfev=func_calls[0],
                            njev=grad_calls[0], status=warnflag,
                            success=(warnflag == 0), message=msg, x=xk,
                            nit=k)
    if retall:
        result['allvecs'] = allvecs
    return result
```

```python
out = fmin_bfgs(f=eb.obj, x0=eb.x_forward(eb.x0), fprime=eb.grad, args=(), gtol=1e-6, norm=np.inf,
              epsilon=0, maxiter=None, full_output=1, disp=1,
              retall=0, callback=None)

vecnorm(out['jac'], np.inf)
```

    (Error) 1.0 1 5.678308050827141e-05
    Divide-by-zero encountered: rhok assumed large


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\ipykernel_launcher.py:247: RuntimeWarning: divide by zero encountered in double_scalars


    (Error) 0.0013231585336425853 2 4.248495027692201e-05
    Divide-by-zero encountered: rhok assumed large


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\ipykernel_launcher.py:247: RuntimeWarning: divide by zero encountered in double_scalars


    (Error) 7.698606698388899e-11 3 5.14780204552423e-05
    Divide-by-zero encountered: rhok assumed large


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\ipykernel_launcher.py:247: RuntimeWarning: divide by zero encountered in double_scalars


    (Error) 0.0093482198085868 4 4.703274857664752e-05
    Divide-by-zero encountered: rhok assumed large


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\ipykernel_launcher.py:247: RuntimeWarning: divide by zero encountered in double_scalars


    (Error) 0.0093482198085868 5 4.703274857664752e-05
    Divide-by-zero encountered: rhok assumed large
    Warning: Desired error not necessarily achieved due to precision loss.
             Current function value: -6346.423675
             Iterations: 23
             Function evaluations: 436
             Gradient evaluations: 340


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\ipykernel_launcher.py:247: RuntimeWarning: divide by zero encountered in double_scalars





    4.703274857664752e-05

```python
out = fmin_bfgs(f=eb.obj, x0=eb.x_forward(eb.x0), fprime=None, args=(), gtol=1e-6, norm=np.inf,
              epsilon=0, maxiter=None, full_output=1, disp=1,
              retall=0, callback=None)

vecnorm(out['jac'], np.inf)
```

    Optimization terminated successfully.
             Current function value: -6333.381824
             Iterations: 0
             Function evaluations: 8
             Gradient evaluations: 1


    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\scipy\optimize\optimize.py:663: RuntimeWarning: invalid value encountered in double_scalars
      grad[k] = (f(*((xk + d,) + args)) - f0) / d[k]
    C:\Users\us57144\AppData\Local\Continuum\anaconda3\lib\site-packages\numpy\core\fromnumeric.py:83: RuntimeWarning: invalid value encountered in reduce
      return ufunc.reduce(obj, axis, dtype, out, **passkwargs)





    nan

The biggest thing to take away from this deep dive into the BFGS algorithm is that the issues are coming up because the linesearch algorithm are having a hard time finding an appropriate step size `alpha_k` after a few iterations.

Slight modifications of the code to allow for a few hangups is not fixing the problem.

## Summary

### Collected Findings

1. The real source of the errors are coming from the minimization routine.
1. In particular, **the computation step size of the `BFGS` algorithm is having a hard time after a few iterations, which is causing the error/warning to be thrown.**
1. In a basic minimization with the default `BFGS` method we generally reach a value of the convergence criteria that is on the scale of 1e-4. By default, the `BFGS` method returns as converged if `gtol<1e-5`. _It simply can't hit that little benchmark!_
1. Using either the `L-BFGS-B` or `Nelder-Mead` minimization methods does tend to produce successful results.

### Modifications to code that arose from this exercise

In order of importance:

1. Made the default method `L-BFGS-B`
   - _Fixes the "convergence issues" that were arising with the `BFGS` method_
1. Changed the computation of $\text{cholesky}(\Omega)$ to account for the zero values.
   - _Fixes the "divide by zero" warnings that were popping up._
1. Made the minimization kwargs merger with default kwargs smoother by using `dict.get()`.
   - _Ensures that no options passed by user to function manually are overwritten._
1. Removed `jac` as a default argument of the optimization objective when `method='Nelder-Mead'`
   - _Removes the harmless warning thrown when a jacobian is supplied with the Nelder-Mead method._

### Usage guidelines

With the new default minimization method (`L-BFGS-B`) the minimization should probably be successful with no additional tinkering.

_In the case of an error:_

- Do not discard the results.
- Investigate the jacobian with `eb.res.jac`. (Where `eb` is the output of the `BVAR.empirical_bayes` method.)
  - All entries should be "pretty close" to zero.
  - "pretty close" almost always means less than unity.
  - To get an idea of what "pretty close" means compute the gradient of your initial input vector to get an idea of how far along it has come towards converging. (`eb.grad(eb.x_forward(eb.x0))`)
- Re-run the minimization with `method='Nelder-Mead'`.
  - Compare the final values of this optimization run with those of the previous one.
  - If they are "pretty close" then you're probably near the desired minimum.
- Try tinkering around with the initialization values.
  - You can reuse the same object but just call the `.optimize` method with new `x0`. (Note: If you had specified for the model hyperparameters to be set in the initial call then this will not occur on subsequent optimizations.)
  - If you have a starting `dict` of new `x0` (call it `x0_dict`), you can call `x0 = eb.x_forward(x0_dict)` to get a 1D NumPy array of values that smashes each of the individual parameters together in the appropriate order.
  - The `.x_forward` method will take care of any logs or other transforms that were previously specified.
