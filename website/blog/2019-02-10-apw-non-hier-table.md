---
title: Forecast statistics for Conditional Forecasts
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

We're headed back into the BVAR python package. This shift in focus was necessaitated by some changes in what was required at what point.

This post deals with the addition of some forecast statistic methods for the conditional forecast routines.

<!--truncate-->

```python
%reload_ext autoreload
%autoreload 2
%matplotlib inline
%config InlineBackend.figure_format = 'retina'
```

```python
import os
import time
import tempfile
import pickle

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import GtmmData
import bvar
```

```python
sns.set_style('whitegrid')
sns.set_palette('deep')

FRED_KEY = os.environ['FRED_KEY']
```

## Getting the data

First, let's get the raw output data from a simple conditional forecast. This will serve as a starting point for our summary methods.

```python
def get_forecast_data(force_reload=False):
  save_file = os.path.join(tempfile.gettempdir(), 'bvar-usage', 'apw_forecast_out_data_3var.pkl')
  BASE_PATH = os.path.join(tempfile.gettempdir(), 'bvar-usage')
  BASE_FORECAST_PATH = os.path.join(tempfile.gettempdir(), 'bvar-usage', 'base-forecast')

  if not os.path.exists(save_file) or force_reload:
    model_name = '3-var'
    series = ['CPIAUCSL', 'FEDFUNDS', 'PAYEMS']

    model_base_path = os.path.join(BASE_PATH, model_name)
    data_path = os.path.join(model_base_path, 'data.pkl')
    model_path = os.path.join(model_base_path, 'model.pkl')
    if not os.path.exists(model_base_path):
        os.makedirs(model_base_path)

    if not os.path.exists(data_path) or force_reload:
      fred_data0 = GtmmData.FredData(apiKey=FRED_KEY)
      fred_data0.getFredData(ids=series) # Download data
      fred_data0.setTrans(['CPIAUCSL', 'PAYEMS'], 'log') # Make these variables logs
      fred_data = fred_data0.format(balanced=True, freq='coarse',trim=None) # Format it
      fred_data.to_pickle(data_path)
    else:
      fred_data = GtmmData.read_pickle(data_path)

    bvs = bvar.core.BVAR(
      data=fred_data.data,
      lags=13,
      prior_spec='DIO_MN_SOC',
      hyperparameters={'Lambda': 0.75, 'mu': 0.214, 'delta': 0.124, 'dof': 13}
    )

    conditional_forecast_script = '''
    eqn EQ0 = PAYEMS[+1];
    eqn EQ1 = FEDFUNDS[+1];
    eqn EQ2 = FEDFUNDS[+2];

    mean EQ0 = 11.922209936681169; // = log(15074) = Jan. 2019 Nonfarm employment
    mean EQ1 = 2.40; // = Jan 2019 Effective Fed funds rate
    mean EQ2 = 2.38; // Guessing that the fed will not change rates in February => close to constant FFNDs

    std EQ0 = 1e-4;
    std EQ1 = 1e-3;
    std EQ2 = 1e-2;
    '''

    fc = bvs.forecast(
        n_periods = 12,
        kind = 'apw',
        hierarchical = False,
        script = conditional_forecast_script,
        iters = 2000,
        chains = 4
      )

    out = {'bvar': bvs,'in': {'graph': bvs.graph, 'script': fc.script, 'n_periods': fc.n_periods, 'data_labels': fc.data_labels, 'iters': fc.iters}, 'raw_output': fc.raw_output}

    with open(save_file, 'wb') as file:
      pickle.dump(out, file)

  else:
    with open(save_file, 'rb') as file:
      out = pickle.load(file)

  return out['bvar'], out['in'], out['raw_output']
```

```python
bvs, forecast_in, raw_output = get_forecast_data(force_reload=False)
```

```python
print('Shape of Output: {}\nIterations: {}\nn_periods: {}\nVariables: {}'.format(raw_output.shape, forecast_in['iters'], forecast_in['n_periods'], forecast_in['graph'].dims['n']))
```

    Shape of Output: (8000, 36)
    Iterations: 8000
    n_periods: 12
    Variables: 3

So the data is organized so that each draw of the forecast is on the first dimension.

On the second dimension we have `12 * 3 = 36` entries. This is because the sampling for all forecast periods at once.

Let's dive into these

#### How are the 36 entries on the second dimension organized?

```python
ro = raw_output[0,:]
print('First draw of forecasts:')
print(ro.reshape((-1,3)))
print('As raw data:')
ro
```

    First draw of forecasts:
    [[ 5.53577855  2.40074589 11.92212885]
     [ 5.53663863  2.38732433 11.92354695]
     [ 5.53956606  2.01845116 11.92336073]
     [ 5.54318133  2.48250623 11.92674961]
     [ 5.54853988  2.9546312  11.92698642]
     [ 5.55655065  3.43253929 11.93348017]
     [ 5.55897808  3.52571498 11.93396423]
     [ 5.56265915  4.29679042 11.93820151]
     [ 5.56892234  4.2134602  11.94194645]
     [ 5.57749572  4.88721265 11.94607636]
     [ 5.58569359  4.63307617 11.9488183 ]
     [ 5.58988806  4.64769547 11.9533195 ]]
    As raw data:





    array([ 5.53577855,  2.40074589, 11.92212885,  5.53663863,  2.38732433,
           11.92354695,  5.53956606,  2.01845116, 11.92336073,  5.54318133,
            2.48250623, 11.92674961,  5.54853988,  2.9546312 , 11.92698642,
            5.55655065,  3.43253929, 11.93348017,  5.55897808,  3.52571498,
           11.93396423,  5.56265915,  4.29679042, 11.93820151,  5.56892234,
            4.2134602 , 11.94194645,  5.57749572,  4.88721265, 11.94607636,
            5.58569359,  4.63307617, 11.9488183 ,  5.58988806,  4.64769547,
           11.9533195 ])

It is clear that the data is organized as:

$$
\underbrace{CPIAUCSL*{+1}, FEDFUNDS*{+1}, PAYEMS*{+1}}*{\text{Forecast Period 1}}\dots\underbrace{CPIAUCSL*{+n}, FEDFUNDS*{+n}, PAYEMS*{+n}}*{\text{Forecast Period } n_periods}
$$

## Goals for the methods

We anticipate creating two main methods:

- `tabletable(self, cols: [list, None] = None, forecast_periods: [int, None] = None, data_periods: [int, None] = 0, as_frame: bool = False, mark_forecast: bool = True, percentiles: [list, np.ndarray, None] = [0.33, 0.66], median:bool=True, mean: bool = True, std: bool = True )`. Will output a pandas dataframe containing a
- `plot()`. Will output a set of plots containing "fan plots" of predictions along with lines for the median and mean.

This means that the main steps will be:

1. Compute relevant summary statistics
1. Reshape the data to an order amenable to the

### Relevant Summary Statistics

Here we can draw upon the relevant numpy methods/functions.

The only thing to check is that we compute along the correct dimensions. Try `axis=0`.

```python
on_axis = 0
s0 = np.mean(raw_output, axis=on_axis).shape[0]
assert s0 == raw_output.shape[1]
print('Shape of output when `axis={}`: {}'.format(on_axis, s0))
```

    Shape of output when `axis=0`: 36

#### Mean, Median, and Standard deviation

```python
mean_ = raw_output.mean(axis=0)
std_ = raw_output.std(axis=0)
```

```python
assert mean_.shape[0] == raw_output.shape[1]
assert std_.shape[0] == raw_output.shape[1]
```

#### Quantiles

Will have arguments in which bands are specified and can include the median or not:

```python
bands_ = [0.33, 0.5]
median_ = True
```

##### Construct the bands to be computed

```python
def get_q_in(bands, median):
  q_in = [0.5 + sgn * (b * 0.5) for b in bands for sgn in [-1, 1]]
  if median: q_in.append(0.5)
  return q_in
```

```python
assert get_q_in([0.33, 0.5], True) == [0.5-0.33*0.5, 0.5+0.33*0.5,0.5-0.5*0.5, 0.5+0.5*0.5, 0.5]
assert get_q_in([0.33, 0.5], False) == [0.5-0.33*0.5, 0.5+0.33*0.5,0.5-0.5*0.5, 0.5+0.5*0.5]
assert get_q_in([0.33], True) == [0.5-0.33*0.5, 0.5+0.33*0.5, 0.5]
assert get_q_in([0.33, 0.5, 0.75], True) == [0.5-0.33*0.5, 0.5+0.33*0.5,0.5-0.5*0.5, 0.5+0.5*0.5,0.5-0.75*0.5, 0.5+0.75*0.5, 0.5]
```

##### Compute the quantiles

```python
q_in = get_q_in(bands_, median_)
q_out = np.quantile(a=raw_output, q=q_in, axis=0)
```

##### Deconstruct the quantiles

For tables we'll want one thing. For plots we'll want another. So this is where the paths split.

### Formatting `table` output

Let's focus first on how we'll deal with the mean and standard deviations:

```python
n = forecast_in['graph'].dims['n']
n_periods = forecast_in['n_periods']
cols = forecast_in['data_labels'].cols
stats = []
data = np.ndarray((n_periods,0))
```

```python
nm = 'Mean'
attr = 'mean'

st = getattr(raw_output, attr)(axis=0)
st = st.reshape((n_periods,n))
stats.append(nm)
data = np.hstack((data, st))
st
```

    array([[  5.70721221,   2.39999033,  11.92221075],
           [  5.67557832,   2.37990108,  11.96587449],
           [  5.63199018, -14.50868911,  12.12897998],
           [  5.68957728, -56.07958272,  12.26641661],
           [  5.79783285, -46.42008304,  12.38040558],
           [  5.73362324, -53.18859906,  12.46557087],
           [  5.61249023, -59.90872291,  12.47028317],
           [  5.36017587, -79.33356175,  12.41577415],
           [  5.24275286, -59.16609078,  12.4192543 ],
           [  5.1271327 , -50.07695708,  12.46742987],
           [  5.0571483 , -32.10243844,  12.39888329],
           [  4.8291882 , -53.66869764,  12.29923084]])

```python
nm = 'SD'
attr = 'std'

st = getattr(raw_output, attr)(axis=0)
st = st.reshape((n_periods,n))
stats.append(nm)
data = np.hstack((data, st))
st
```

    array([[1.01172305e+01, 1.00368167e-03, 9.92876007e-05],
           [1.73035714e+01, 1.00555872e-02, 8.54769088e+00],
           [2.32156313e+01, 2.01147629e+03, 1.30824381e+01],
           [2.83302432e+01, 3.35235516e+03, 1.87308569e+01],
           [3.34583214e+01, 4.28103716e+03, 2.45417029e+01],
           [3.85999408e+01, 4.97732190e+03, 3.05314260e+01],
           [4.35485239e+01, 5.48760891e+03, 3.67346647e+01],
           [4.89206741e+01, 5.94557311e+03, 4.30451659e+01],
           [5.43287635e+01, 6.33154650e+03, 4.93692309e+01],
           [5.97375666e+01, 6.64120471e+03, 5.56463540e+01],
           [6.55321352e+01, 7.02194459e+03, 6.20609618e+01],
           [7.19828695e+01, 7.49598432e+03, 6.84019070e+01]])

```python
idx = pd.MultiIndex.from_product([stats,cols])
idx.tolist()
```

    [('Mean', 'CPIAUCSL'),
     ('Mean', 'FEDFUNDS'),
     ('Mean', 'PAYEMS'),
     ('SD', 'CPIAUCSL'),
     ('SD', 'FEDFUNDS'),
     ('SD', 'PAYEMS')]

```python
df = pd.DataFrame(data, columns=idx).reorder_levels([1, 0], axis=1).sort_index(level=0, axis=1)

df
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="2" halign="left">CPIAUCSL</th>
      <th colspan="2" halign="left">FEDFUNDS</th>
      <th colspan="2" halign="left">PAYEMS</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>SD</th>
      <th>Mean</th>
      <th>SD</th>
      <th>Mean</th>
      <th>SD</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.707212</td>
      <td>10.117230</td>
      <td>2.399990</td>
      <td>0.001004</td>
      <td>11.922211</td>
      <td>0.000099</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.675578</td>
      <td>17.303571</td>
      <td>2.379901</td>
      <td>0.010056</td>
      <td>11.965874</td>
      <td>8.547691</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.631990</td>
      <td>23.215631</td>
      <td>-14.508689</td>
      <td>2011.476294</td>
      <td>12.128980</td>
      <td>13.082438</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.689577</td>
      <td>28.330243</td>
      <td>-56.079583</td>
      <td>3352.355161</td>
      <td>12.266417</td>
      <td>18.730857</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.797833</td>
      <td>33.458321</td>
      <td>-46.420083</td>
      <td>4281.037157</td>
      <td>12.380406</td>
      <td>24.541703</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5.733623</td>
      <td>38.599941</td>
      <td>-53.188599</td>
      <td>4977.321898</td>
      <td>12.465571</td>
      <td>30.531426</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5.612490</td>
      <td>43.548524</td>
      <td>-59.908723</td>
      <td>5487.608913</td>
      <td>12.470283</td>
      <td>36.734665</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5.360176</td>
      <td>48.920674</td>
      <td>-79.333562</td>
      <td>5945.573105</td>
      <td>12.415774</td>
      <td>43.045166</td>
    </tr>
    <tr>
      <th>8</th>
      <td>5.242753</td>
      <td>54.328764</td>
      <td>-59.166091</td>
      <td>6331.546497</td>
      <td>12.419254</td>
      <td>49.369231</td>
    </tr>
    <tr>
      <th>9</th>
      <td>5.127133</td>
      <td>59.737567</td>
      <td>-50.076957</td>
      <td>6641.204709</td>
      <td>12.467430</td>
      <td>55.646354</td>
    </tr>
    <tr>
      <th>10</th>
      <td>5.057148</td>
      <td>65.532135</td>
      <td>-32.102438</td>
      <td>7021.944586</td>
      <td>12.398883</td>
      <td>62.060962</td>
    </tr>
    <tr>
      <th>11</th>
      <td>4.829188</td>
      <td>71.982870</td>
      <td>-53.668698</td>
      <td>7495.984319</td>
      <td>12.299231</td>
      <td>68.401907</td>
    </tr>
  </tbody>
</table>
</div>

Now let's add on the quantiles. The big question is how we should deal with indexing.

```python
q_in = get_q_in(bands_, median_)
q_out = np.quantile(a=raw_output, q=q_in, axis=0)
```

Now sort the outputs according to their percentile:

```python
q_in2 = np.array(q_in)
nq = len(q_in)

idx = q_in2.argsort()
print(idx)
print(q_in2[idx])
```

    [2 0 4 1 3]
    [0.25  0.335 0.5   0.665 0.75 ]

Reshaping so that the output has the form:

$$
\underbrace{CPIAUCSL*{+1}^{q=0.25},\;CPIAUCSL*{+1}^{q=0.33},\;CPIAUCSL*{+1}^{q=0.50}\;CPIAUCSL*{+1}^{q=0.66},\;CPIAUCSL*{+1}^{q=0.75}}*{\text{CPIAUCSL Quantiles, Forecast period 1}}\dots\underbrace{FEDFUNDS*{+1}^{q=0.25},\dots\;FEDFUNDS*{+1}^{q=0.75}}_{\text{FEDFUNDS Quantiles, Forecast period 1}}\\
\dots\\
\dots\\
\underbrace{CPIAUCSL_{+n}^{q=0.25},\;CPIAUCSL*{+n}^{q=0.33},\;CPIAUCSL*{+n}^{q=0.50}\;CPIAUCSL*{+n}^{q=0.66},\;CPIAUCSL*{+n}^{q=0.75}}_{\text{CPIAUCSL Quantiles, Forecast period n}}\dots\underbrace{FEDFUNDS_{+n}^{q=0.25},\dots\;FEDFUNDS*{+n}^{q=0.75}}*{\text{FEDFUNDS Quantiles, Forecast period n}}
$$

```python
q_ = q_out[idx].T.reshape((n_periods, nq*n))
print(q_.shape)
print(q_[:, 0])
```

    (12, 15)
    [  1.17208259  -2.29941553  -5.0232536   -7.40810138  -9.17605245
     -11.90376668 -14.4541255  -17.47721756 -19.53937689 -21.78950252
     -24.33079202 -27.36574052]

```python
q_in.sort()
q_names = ['Q{}'.format(int(100*ii)) for ii in q_in]
idx = pd.MultiIndex.from_product([cols, q_names])
idx.tolist()
```

    [('CPIAUCSL', 'Q25'),
     ('CPIAUCSL', 'Q33'),
     ('CPIAUCSL', 'Q50'),
     ('CPIAUCSL', 'Q66'),
     ('CPIAUCSL', 'Q75'),
     ('FEDFUNDS', 'Q25'),
     ('FEDFUNDS', 'Q33'),
     ('FEDFUNDS', 'Q50'),
     ('FEDFUNDS', 'Q66'),
     ('FEDFUNDS', 'Q75'),
     ('PAYEMS', 'Q25'),
     ('PAYEMS', 'Q33'),
     ('PAYEMS', 'Q50'),
     ('PAYEMS', 'Q66'),
     ('PAYEMS', 'Q75')]

```python
dfq = pd.DataFrame(q_, columns=idx)
dfq.head()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="5" halign="left">CPIAUCSL</th>
      <th colspan="5" halign="left">FEDFUNDS</th>
      <th colspan="5" halign="left">PAYEMS</th>
    </tr>
    <tr>
      <th></th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.172083</td>
      <td>3.255864</td>
      <td>5.558596</td>
      <td>7.928439</td>
      <td>10.067418</td>
      <td>2.399308</td>
      <td>2.399554</td>
      <td>2.399970</td>
      <td>2.400433</td>
      <td>2.400678</td>
      <td>11.922143</td>
      <td>11.922168</td>
      <td>11.922211</td>
      <td>11.922253</td>
      <td>11.922276</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-2.299416</td>
      <td>1.452746</td>
      <td>5.536937</td>
      <td>9.636654</td>
      <td>13.440512</td>
      <td>2.372967</td>
      <td>2.375524</td>
      <td>2.379754</td>
      <td>2.384346</td>
      <td>2.386725</td>
      <td>8.169222</td>
      <td>10.012062</td>
      <td>11.925857</td>
      <td>13.933985</td>
      <td>15.718575</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-5.023254</td>
      <td>-0.107650</td>
      <td>5.528989</td>
      <td>11.066792</td>
      <td>16.249108</td>
      <td>-892.573838</td>
      <td>-463.990003</td>
      <td>0.385163</td>
      <td>455.700275</td>
      <td>878.911284</td>
      <td>6.291760</td>
      <td>8.992019</td>
      <td>11.957307</td>
      <td>15.069844</td>
      <td>17.875194</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-7.408101</td>
      <td>-1.351461</td>
      <td>5.535262</td>
      <td>12.247859</td>
      <td>18.235584</td>
      <td>-1564.614533</td>
      <td>-814.078229</td>
      <td>-20.495660</td>
      <td>767.086656</td>
      <td>1430.779292</td>
      <td>3.856204</td>
      <td>7.887660</td>
      <td>11.999705</td>
      <td>16.458467</td>
      <td>20.629071</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-9.176052</td>
      <td>-2.401746</td>
      <td>5.453769</td>
      <td>13.551090</td>
      <td>21.096778</td>
      <td>-2029.456295</td>
      <td>-1058.992613</td>
      <td>-14.517050</td>
      <td>1016.400988</td>
      <td>1953.515688</td>
      <td>1.423861</td>
      <td>6.388739</td>
      <td>11.996967</td>
      <td>17.937816</td>
      <td>23.208634</td>
    </tr>
  </tbody>
</table>
</div>

```python
df = df.merge(dfq,left_index=True, right_index=True).sort_index(level=0, axis=1)
df.head()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="7" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>...</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.707212</td>
      <td>1.172083</td>
      <td>3.255864</td>
      <td>5.558596</td>
      <td>7.928439</td>
      <td>10.067418</td>
      <td>10.117230</td>
      <td>2.399990</td>
      <td>2.399308</td>
      <td>2.399554</td>
      <td>...</td>
      <td>2.400433</td>
      <td>2.400678</td>
      <td>0.001004</td>
      <td>11.922211</td>
      <td>11.922143</td>
      <td>11.922168</td>
      <td>11.922211</td>
      <td>11.922253</td>
      <td>11.922276</td>
      <td>0.000099</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.675578</td>
      <td>-2.299416</td>
      <td>1.452746</td>
      <td>5.536937</td>
      <td>9.636654</td>
      <td>13.440512</td>
      <td>17.303571</td>
      <td>2.379901</td>
      <td>2.372967</td>
      <td>2.375524</td>
      <td>...</td>
      <td>2.384346</td>
      <td>2.386725</td>
      <td>0.010056</td>
      <td>11.965874</td>
      <td>8.169222</td>
      <td>10.012062</td>
      <td>11.925857</td>
      <td>13.933985</td>
      <td>15.718575</td>
      <td>8.547691</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.631990</td>
      <td>-5.023254</td>
      <td>-0.107650</td>
      <td>5.528989</td>
      <td>11.066792</td>
      <td>16.249108</td>
      <td>23.215631</td>
      <td>-14.508689</td>
      <td>-892.573838</td>
      <td>-463.990003</td>
      <td>...</td>
      <td>455.700275</td>
      <td>878.911284</td>
      <td>2011.476294</td>
      <td>12.128980</td>
      <td>6.291760</td>
      <td>8.992019</td>
      <td>11.957307</td>
      <td>15.069844</td>
      <td>17.875194</td>
      <td>13.082438</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.689577</td>
      <td>-7.408101</td>
      <td>-1.351461</td>
      <td>5.535262</td>
      <td>12.247859</td>
      <td>18.235584</td>
      <td>28.330243</td>
      <td>-56.079583</td>
      <td>-1564.614533</td>
      <td>-814.078229</td>
      <td>...</td>
      <td>767.086656</td>
      <td>1430.779292</td>
      <td>3352.355161</td>
      <td>12.266417</td>
      <td>3.856204</td>
      <td>7.887660</td>
      <td>11.999705</td>
      <td>16.458467</td>
      <td>20.629071</td>
      <td>18.730857</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.797833</td>
      <td>-9.176052</td>
      <td>-2.401746</td>
      <td>5.453769</td>
      <td>13.551090</td>
      <td>21.096778</td>
      <td>33.458321</td>
      <td>-46.420083</td>
      <td>-2029.456295</td>
      <td>-1058.992613</td>
      <td>...</td>
      <td>1016.400988</td>
      <td>1953.515688</td>
      <td>4281.037157</td>
      <td>12.380406</td>
      <td>1.423861</td>
      <td>6.388739</td>
      <td>11.996967</td>
      <td>17.937816</td>
      <td>23.208634</td>
      <td>24.541703</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 21 columns</p>
</div>

##### Adding on non-forecast data

```python
apw = bvar.core.Forecast.ApwNonHier(**forecast_in, auto_run=False)

mark_forecast = True
data_periods = 5
```

```python
ncol = df.columns.shape[0]
dpt = np.tile(apw.data[-data_periods:, ], (1,ncol//n))
```

```python
print(apw.data[-data_periods:,:])
print(dpt.shape)
print(dpt)
```

    [[ 5.52881779  1.91       11.91483091]
     [ 5.52940528  1.95       11.91555322]
     [ 5.53270546  2.19       11.91740342]
     [ 5.53289925  2.2        11.91871052]
     [ 5.5323336   2.27       11.92018896]]
    (5, 21)
    [[ 5.52881779  1.91       11.91483091  5.52881779  1.91       11.91483091
       5.52881779  1.91       11.91483091  5.52881779  1.91       11.91483091
       5.52881779  1.91       11.91483091  5.52881779  1.91       11.91483091
       5.52881779  1.91       11.91483091]
     [ 5.52940528  1.95       11.91555322  5.52940528  1.95       11.91555322
       5.52940528  1.95       11.91555322  5.52940528  1.95       11.91555322
       5.52940528  1.95       11.91555322  5.52940528  1.95       11.91555322
       5.52940528  1.95       11.91555322]
     [ 5.53270546  2.19       11.91740342  5.53270546  2.19       11.91740342
       5.53270546  2.19       11.91740342  5.53270546  2.19       11.91740342
       5.53270546  2.19       11.91740342  5.53270546  2.19       11.91740342
       5.53270546  2.19       11.91740342]
     [ 5.53289925  2.2        11.91871052  5.53289925  2.2        11.91871052
       5.53289925  2.2        11.91871052  5.53289925  2.2        11.91871052
       5.53289925  2.2        11.91871052  5.53289925  2.2        11.91871052
       5.53289925  2.2        11.91871052]
     [ 5.5323336   2.27       11.92018896  5.5323336   2.27       11.92018896
       5.5323336   2.27       11.92018896  5.5323336   2.27       11.92018896
       5.5323336   2.27       11.92018896  5.5323336   2.27       11.92018896
       5.5323336   2.27       11.92018896]]

```python
dfdp = pd.DataFrame(dpt, columns=df.sort_index(level=1, axis=1).columns).sort_index(level=0, axis=1)
if mark_forecast:
  dfdp['forecast'] = False

dfdp
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="6" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
      <th>forecast</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>...</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>1.91</td>
      <td>1.91</td>
      <td>1.91</td>
      <td>...</td>
      <td>1.91</td>
      <td>1.91</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>1.95</td>
      <td>1.95</td>
      <td>1.95</td>
      <td>...</td>
      <td>1.95</td>
      <td>1.95</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>2.19</td>
      <td>2.19</td>
      <td>2.19</td>
      <td>...</td>
      <td>2.19</td>
      <td>2.19</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>2.20</td>
      <td>2.20</td>
      <td>2.20</td>
      <td>...</td>
      <td>2.20</td>
      <td>2.20</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>2.27</td>
      <td>2.27</td>
      <td>2.27</td>
      <td>...</td>
      <td>2.27</td>
      <td>2.27</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 22 columns</p>
</div>

```python
df = dfdp.append(df)
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="7" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>...</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q33</th>
      <th>Q50</th>
      <th>Q66</th>
      <th>Q75</th>
      <th>SD</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>5.528818</td>
      <td>1.910000</td>
      <td>1.910000</td>
      <td>1.910000</td>
      <td>...</td>
      <td>1.910000</td>
      <td>1.910000</td>
      <td>1.910000</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
      <td>11.914831</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>5.529405</td>
      <td>1.950000</td>
      <td>1.950000</td>
      <td>1.950000</td>
      <td>...</td>
      <td>1.950000</td>
      <td>1.950000</td>
      <td>1.950000</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
      <td>11.915553</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>5.532705</td>
      <td>2.190000</td>
      <td>2.190000</td>
      <td>2.190000</td>
      <td>...</td>
      <td>2.190000</td>
      <td>2.190000</td>
      <td>2.190000</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
      <td>11.917403</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>...</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>...</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
    </tr>
    <tr>
      <th>0</th>
      <td>5.707212</td>
      <td>1.172083</td>
      <td>3.255864</td>
      <td>5.558596</td>
      <td>7.928439</td>
      <td>10.067418</td>
      <td>10.117230</td>
      <td>2.399990</td>
      <td>2.399308</td>
      <td>2.399554</td>
      <td>...</td>
      <td>2.400433</td>
      <td>2.400678</td>
      <td>0.001004</td>
      <td>11.922211</td>
      <td>11.922143</td>
      <td>11.922168</td>
      <td>11.922211</td>
      <td>11.922253</td>
      <td>11.922276</td>
      <td>0.000099</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.675578</td>
      <td>-2.299416</td>
      <td>1.452746</td>
      <td>5.536937</td>
      <td>9.636654</td>
      <td>13.440512</td>
      <td>17.303571</td>
      <td>2.379901</td>
      <td>2.372967</td>
      <td>2.375524</td>
      <td>...</td>
      <td>2.384346</td>
      <td>2.386725</td>
      <td>0.010056</td>
      <td>11.965874</td>
      <td>8.169222</td>
      <td>10.012062</td>
      <td>11.925857</td>
      <td>13.933985</td>
      <td>15.718575</td>
      <td>8.547691</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.631990</td>
      <td>-5.023254</td>
      <td>-0.107650</td>
      <td>5.528989</td>
      <td>11.066792</td>
      <td>16.249108</td>
      <td>23.215631</td>
      <td>-14.508689</td>
      <td>-892.573838</td>
      <td>-463.990003</td>
      <td>...</td>
      <td>455.700275</td>
      <td>878.911284</td>
      <td>2011.476294</td>
      <td>12.128980</td>
      <td>6.291760</td>
      <td>8.992019</td>
      <td>11.957307</td>
      <td>15.069844</td>
      <td>17.875194</td>
      <td>13.082438</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.689577</td>
      <td>-7.408101</td>
      <td>-1.351461</td>
      <td>5.535262</td>
      <td>12.247859</td>
      <td>18.235584</td>
      <td>28.330243</td>
      <td>-56.079583</td>
      <td>-1564.614533</td>
      <td>-814.078229</td>
      <td>...</td>
      <td>767.086656</td>
      <td>1430.779292</td>
      <td>3352.355161</td>
      <td>12.266417</td>
      <td>3.856204</td>
      <td>7.887660</td>
      <td>11.999705</td>
      <td>16.458467</td>
      <td>20.629071</td>
      <td>18.730857</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.797833</td>
      <td>-9.176052</td>
      <td>-2.401746</td>
      <td>5.453769</td>
      <td>13.551090</td>
      <td>21.096778</td>
      <td>33.458321</td>
      <td>-46.420083</td>
      <td>-2029.456295</td>
      <td>-1058.992613</td>
      <td>...</td>
      <td>1016.400988</td>
      <td>1953.515688</td>
      <td>4281.037157</td>
      <td>12.380406</td>
      <td>1.423861</td>
      <td>6.388739</td>
      <td>11.996967</td>
      <td>17.937816</td>
      <td>23.208634</td>
      <td>24.541703</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5.733623</td>
      <td>-11.903767</td>
      <td>-3.684431</td>
      <td>5.567625</td>
      <td>14.716477</td>
      <td>23.217721</td>
      <td>38.599941</td>
      <td>-53.188599</td>
      <td>-2324.803750</td>
      <td>-1231.043872</td>
      <td>...</td>
      <td>1129.052660</td>
      <td>2199.907159</td>
      <td>4977.321898</td>
      <td>12.465571</td>
      <td>-1.180717</td>
      <td>4.926950</td>
      <td>11.955524</td>
      <td>19.170440</td>
      <td>25.792445</td>
      <td>30.531426</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5.612490</td>
      <td>-14.454126</td>
      <td>-4.902189</td>
      <td>5.564304</td>
      <td>15.824323</td>
      <td>25.411144</td>
      <td>43.548524</td>
      <td>-59.908723</td>
      <td>-2587.775501</td>
      <td>-1336.282936</td>
      <td>...</td>
      <td>1255.346664</td>
      <td>2343.916026</td>
      <td>5487.608913</td>
      <td>12.470283</td>
      <td>-4.265732</td>
      <td>3.420364</td>
      <td>11.832529</td>
      <td>20.540441</td>
      <td>28.638489</td>
      <td>36.734665</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5.360176</td>
      <td>-17.477218</td>
      <td>-5.830665</td>
      <td>5.553535</td>
      <td>16.669613</td>
      <td>27.517259</td>
      <td>48.920674</td>
      <td>-79.333562</td>
      <td>-2789.683881</td>
      <td>-1452.820458</td>
      <td>...</td>
      <td>1341.349599</td>
      <td>2545.289708</td>
      <td>5945.573105</td>
      <td>12.415774</td>
      <td>-7.125921</td>
      <td>1.690340</td>
      <td>11.855014</td>
      <td>21.833035</td>
      <td>31.687125</td>
      <td>43.045166</td>
    </tr>
    <tr>
      <th>8</th>
      <td>5.242753</td>
      <td>-19.539377</td>
      <td>-7.397520</td>
      <td>5.408567</td>
      <td>17.670460</td>
      <td>29.345825</td>
      <td>54.328764</td>
      <td>-59.166091</td>
      <td>-2915.125041</td>
      <td>-1497.389673</td>
      <td>...</td>
      <td>1410.912662</td>
      <td>2724.453543</td>
      <td>6331.546497</td>
      <td>12.419254</td>
      <td>-10.170956</td>
      <td>0.170148</td>
      <td>11.745662</td>
      <td>23.258270</td>
      <td>34.639018</td>
      <td>49.369231</td>
    </tr>
    <tr>
      <th>9</th>
      <td>5.127133</td>
      <td>-21.789503</td>
      <td>-8.506820</td>
      <td>5.351236</td>
      <td>18.790861</td>
      <td>31.893342</td>
      <td>59.737567</td>
      <td>-50.076957</td>
      <td>-3056.428636</td>
      <td>-1570.765072</td>
      <td>...</td>
      <td>1444.615619</td>
      <td>3009.178347</td>
      <td>6641.204709</td>
      <td>12.467430</td>
      <td>-12.734445</td>
      <td>-1.601373</td>
      <td>11.856743</td>
      <td>24.523358</td>
      <td>37.913466</td>
      <td>55.646354</td>
    </tr>
    <tr>
      <th>10</th>
      <td>5.057148</td>
      <td>-24.330792</td>
      <td>-9.962632</td>
      <td>5.350453</td>
      <td>20.247088</td>
      <td>34.356659</td>
      <td>65.532135</td>
      <td>-32.102438</td>
      <td>-3095.738778</td>
      <td>-1680.360192</td>
      <td>...</td>
      <td>1598.822315</td>
      <td>3170.583072</td>
      <td>7021.944586</td>
      <td>12.398883</td>
      <td>-15.615755</td>
      <td>-2.977579</td>
      <td>11.902155</td>
      <td>26.226897</td>
      <td>40.512028</td>
      <td>62.060962</td>
    </tr>
    <tr>
      <th>11</th>
      <td>4.829188</td>
      <td>-27.365741</td>
      <td>-11.997578</td>
      <td>5.051936</td>
      <td>21.841271</td>
      <td>36.963271</td>
      <td>71.982870</td>
      <td>-53.668698</td>
      <td>-3342.712329</td>
      <td>-1751.054245</td>
      <td>...</td>
      <td>1718.287098</td>
      <td>3382.741609</td>
      <td>7495.984319</td>
      <td>12.299231</td>
      <td>-18.609404</td>
      <td>-4.239403</td>
      <td>11.822811</td>
      <td>27.737804</td>
      <td>43.633840</td>
      <td>68.401907</td>
    </tr>
  </tbody>
</table>
<p>17 rows × 21 columns</p>
</div>

### Creating the `plot` method

Basically, we want to plot

Other things to consider:

- Passing in plot keyword arguments
  - `figsize`
  - `sharex`
  - `sharey`
- Figuring out appropriate number of columns
- Figuring out appropriate colors.

```python
cols = forecast_in['data_labels'].cols
plot_kwds = {}
bands = [0.1, 0.33]
median = True
mean = True
set_title = True
area_alpha = 0.1
annotate = True
data_periods = 10
forecast_periods = None
area_color = sns.palettes.SEABORN_PALETTES['deep'][0]
data_color = sns.palettes.SEABORN_PALETTES['deep'][1]
median_color = sns.palettes.SEABORN_PALETTES['deep'][1]
mean_color = sns.palettes.SEABORN_PALETTES['deep'][2]
col_wrap = 3
```

### Plot the quantiles

Get the quantiles

```python
q_in = get_q_in(bands, median)
q_out = np.quantile(a=raw_output, q=q_in, axis=0)
```

Split out into separate parts:

```python
q_ = [q_out[ii:ii+2] for ii in range(0, 2*len(bands), 2)]
if median:
  med = q_out[-1]

print(len(q_), [qq.shape for qq in q_], med.shape)
```

    2 [(2, 36), (2, 36)] (36,)

Reshape into separate parts:

```python
def reshape_qq(qq):
  return qq.reshape(( 2,n_periods, n)).transpose((0,2,1))

q_ = [reshape_qq(q_out[ii:ii+2]) for ii in range(0, 2*len(bands), 2)]
if median:
  med = q_out[-1].reshape(n_periods, n).T
```

```python
q_[0][0], med
```

    (array([[   5.1097132 ,    4.74977698,    4.38086944,    4.20981301,
                3.97540915,    3.89396301,    3.45618083,    3.13738584,
                2.6655581 ,    2.26376318,    2.00498706,    1.45623779],
            [   2.39984784,    2.37846443,  -91.18489127, -190.43841039,
             -231.88337593, -269.17183834, -260.67927284, -316.23350249,
             -340.7199368 , -376.16232931, -356.41951608, -388.21643114],
            [  11.92219768,   11.53356568,   11.41086351,   11.2012006 ,
               10.96308575,   10.55493791,   10.10469349,    9.65541763,
                9.23002853,    9.01880677,    8.79279093,    8.69927307]]),
     array([[  5.55859631,   5.53693683,   5.52898862,   5.5352621 ,
               5.45376882,   5.56762533,   5.56430445,   5.55353481,
               5.40856667,   5.35123571,   5.35045316,   5.05193626],
            [  2.39996993,   2.37975434,   0.38516342, -20.49566014,
             -14.51704993, -11.06057472,  -1.32471267, -13.89784951,
             -18.78740355, -16.22127349, -18.9385643 , -14.96801991],
            [ 11.92221088,  11.92585666,  11.9573068 ,  11.99970492,
              11.99696677,  11.95552354,  11.83252903,  11.85501357,
              11.7456621 ,  11.85674257,  11.90215544,  11.82281065]]))

#### Get the mean

```python
avg = raw_output.mean(axis=0).reshape((n_periods,n)).T
```

```python
avg[0]
```

    array([5.70721221, 5.67557832, 5.63199018, 5.68957728, 5.79783285,
           5.73362324, 5.61249023, 5.36017587, 5.24275286, 5.1271327 ,
           5.0571483 , 4.8291882 ])

#### Plot it all

```python
import warnings

if data_periods is not None and data_periods > 0:
  dp = apw.data[-data_periods:].T

if forecast_periods is None:
  fp = n_periods+1
elif forecast_periods > n_periods:
  warnings.warn(
          'Asked for {0:} periods but only forecasted {1:} in the future. Returning forecasts for {1:} periods'
          .format(forecast_periods, n_periods)
        )
  fp = n_periods+1
elif forecast_periods < 1:
  warnings.warn(
          'Asked for {0:} but this should be >= 1. Forecasting 1 period.'
          .format(forecast_periods)
        )
  fp = 1
else:
  fp = forecast_periods
```

```python
fig, axes = plt.subplots(1,len(cols), sharex=False, sharey=False, figsize=(12,4))
axes = axes.flatten()

xr = np.arange(0,fp-1,1)
for ii,ax in enumerate(axes):

  for jj in range(len(q_)):
    ax.fill_between(x=xr, y1=q_[jj][0][ii,:fp], y2=q_[jj][1][ii,:fp], alpha=area_alpha, facecolor=area_color)

  if median:
    ax.plot(xr, med[ii], ls=':', color=median_color)
  if mean:
    ax.plot(xr, avg[ii], ls=':', color=mean_color)
  if dp is not None:
    ax.set_xlim((-data_periods, fp))
    ax.plot(np.arange(-data_periods,0), dp[ii], ls='-', color=data_color)
  if set_title:
    ax.set_title(cols[ii])

if annotate:
  ax = axes[-1]
  q_in2 = [q_in[ii:ii+2] for ii in range(0, 2*len(bands), 2)]
  for jj,qq in enumerate(q_in2):
    for ii in range(2):
      ax.annotate('Q{}'.format(int(100*qq[ii])), xy=(xr[-1], q_[jj][ii][-1][-1]), xytext=(xr[-1], q_[jj][ii][-1][-1]))
```

![png](https://i.imgur.com/LoRwCf5.png)

## Testing out the methods

```python
apw = bvar.core.Forecast.ApwNonHier(**forecast_in, auto_run=False)
apw.raw_output = raw_output
```

```python
apw.table(cols=None, data_periods=2, forecast_periods=10, bands=[0.5,0.45])
```

    (2, 21) 21 7 (2, 3)

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="7" halign="left">CPIAUCSL</th>
      <th colspan="6" halign="left">FEDFUNDS</th>
      <th colspan="7" halign="left">PAYEMS</th>
      <th>forecast</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q27</th>
      <th>Q50</th>
      <th>Q72</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q27</th>
      <th>...</th>
      <th>Q75</th>
      <th>SD</th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q27</th>
      <th>Q50</th>
      <th>Q72</th>
      <th>Q75</th>
      <th>SD</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>5.532899</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>...</td>
      <td>2.200000</td>
      <td>2.200000</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>11.918711</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>5.532334</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>...</td>
      <td>2.270000</td>
      <td>2.270000</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>11.920189</td>
      <td>False</td>
    </tr>
    <tr>
      <th>0</th>
      <td>5.707212</td>
      <td>1.172083</td>
      <td>1.820161</td>
      <td>5.558596</td>
      <td>9.374341</td>
      <td>10.067418</td>
      <td>10.117230</td>
      <td>2.399990</td>
      <td>2.399308</td>
      <td>2.399384</td>
      <td>...</td>
      <td>2.400678</td>
      <td>0.001004</td>
      <td>11.922211</td>
      <td>11.922143</td>
      <td>11.922151</td>
      <td>11.922211</td>
      <td>11.922268</td>
      <td>11.922276</td>
      <td>0.000099</td>
      <td>True</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5.675578</td>
      <td>-2.299416</td>
      <td>-0.996630</td>
      <td>5.536937</td>
      <td>12.165938</td>
      <td>13.440512</td>
      <td>17.303571</td>
      <td>2.379901</td>
      <td>2.372967</td>
      <td>2.373805</td>
      <td>...</td>
      <td>2.386725</td>
      <td>0.010056</td>
      <td>11.965874</td>
      <td>8.169222</td>
      <td>8.797250</td>
      <td>11.925857</td>
      <td>15.095541</td>
      <td>15.718575</td>
      <td>8.547691</td>
      <td>True</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5.631990</td>
      <td>-5.023254</td>
      <td>-3.363592</td>
      <td>5.528989</td>
      <td>14.735267</td>
      <td>16.249108</td>
      <td>23.215631</td>
      <td>-14.508689</td>
      <td>-892.573838</td>
      <td>-745.142456</td>
      <td>...</td>
      <td>878.911284</td>
      <td>2011.476294</td>
      <td>12.128980</td>
      <td>6.291760</td>
      <td>7.275517</td>
      <td>11.957307</td>
      <td>16.929321</td>
      <td>17.875194</td>
      <td>13.082438</td>
      <td>True</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.689577</td>
      <td>-7.408101</td>
      <td>-5.257462</td>
      <td>5.535262</td>
      <td>16.450543</td>
      <td>18.235584</td>
      <td>28.330243</td>
      <td>-56.079583</td>
      <td>-1564.614533</td>
      <td>-1326.535506</td>
      <td>...</td>
      <td>1430.779292</td>
      <td>3352.355161</td>
      <td>12.266417</td>
      <td>3.856204</td>
      <td>5.207960</td>
      <td>11.999705</td>
      <td>19.187021</td>
      <td>20.629071</td>
      <td>18.730857</td>
      <td>True</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5.797833</td>
      <td>-9.176052</td>
      <td>-6.762024</td>
      <td>5.453769</td>
      <td>18.703658</td>
      <td>21.096778</td>
      <td>33.458321</td>
      <td>-46.420083</td>
      <td>-2029.456295</td>
      <td>-1690.783469</td>
      <td>...</td>
      <td>1953.515688</td>
      <td>4281.037157</td>
      <td>12.380406</td>
      <td>1.423861</td>
      <td>3.051905</td>
      <td>11.996967</td>
      <td>21.370389</td>
      <td>23.208634</td>
      <td>24.541703</td>
      <td>True</td>
    </tr>
    <tr>
      <th>5</th>
      <td>5.733623</td>
      <td>-11.903767</td>
      <td>-9.154052</td>
      <td>5.567625</td>
      <td>20.169185</td>
      <td>23.217721</td>
      <td>38.599941</td>
      <td>-53.188599</td>
      <td>-2324.803750</td>
      <td>-1926.215822</td>
      <td>...</td>
      <td>2199.907159</td>
      <td>4977.321898</td>
      <td>12.465571</td>
      <td>-1.180717</td>
      <td>0.652790</td>
      <td>11.955524</td>
      <td>23.616410</td>
      <td>25.792445</td>
      <td>30.531426</td>
      <td>True</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5.612490</td>
      <td>-14.454126</td>
      <td>-11.339495</td>
      <td>5.564304</td>
      <td>22.089155</td>
      <td>25.411144</td>
      <td>43.548524</td>
      <td>-59.908723</td>
      <td>-2587.775501</td>
      <td>-2197.875587</td>
      <td>...</td>
      <td>2343.916026</td>
      <td>5487.608913</td>
      <td>12.470283</td>
      <td>-4.265732</td>
      <td>-1.839271</td>
      <td>11.832529</td>
      <td>25.880526</td>
      <td>28.638489</td>
      <td>36.734665</td>
      <td>True</td>
    </tr>
    <tr>
      <th>7</th>
      <td>5.360176</td>
      <td>-17.477218</td>
      <td>-13.556761</td>
      <td>5.553535</td>
      <td>23.893229</td>
      <td>27.517259</td>
      <td>48.920674</td>
      <td>-79.333562</td>
      <td>-2789.683881</td>
      <td>-2360.669968</td>
      <td>...</td>
      <td>2545.289708</td>
      <td>5945.573105</td>
      <td>12.415774</td>
      <td>-7.125921</td>
      <td>-4.270124</td>
      <td>11.855014</td>
      <td>28.375590</td>
      <td>31.687125</td>
      <td>43.045166</td>
      <td>True</td>
    </tr>
    <tr>
      <th>8</th>
      <td>5.242753</td>
      <td>-19.539377</td>
      <td>-15.554052</td>
      <td>5.408567</td>
      <td>25.742955</td>
      <td>29.345825</td>
      <td>54.328764</td>
      <td>-59.166091</td>
      <td>-2915.125041</td>
      <td>-2427.564806</td>
      <td>...</td>
      <td>2724.453543</td>
      <td>6331.546497</td>
      <td>12.419254</td>
      <td>-10.170956</td>
      <td>-6.827457</td>
      <td>11.745662</td>
      <td>31.041344</td>
      <td>34.639018</td>
      <td>49.369231</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9</th>
      <td>5.127133</td>
      <td>-21.789503</td>
      <td>-17.052993</td>
      <td>5.351236</td>
      <td>27.205881</td>
      <td>31.893342</td>
      <td>59.737567</td>
      <td>-50.076957</td>
      <td>-3056.428636</td>
      <td>-2567.727690</td>
      <td>...</td>
      <td>3009.178347</td>
      <td>6641.204709</td>
      <td>12.467430</td>
      <td>-12.734445</td>
      <td>-9.017099</td>
      <td>11.856743</td>
      <td>33.629562</td>
      <td>37.913466</td>
      <td>55.646354</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
<p>12 rows × 22 columns</p>
</div>

```python
apw.table(cols=['FEDFUNDS'], data_periods=None, forecast_periods=10, bands=[0.5])
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead tr th {
        text-align: left;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="5" halign="left">FEDFUNDS</th>
      <th>forecast</th>
    </tr>
    <tr>
      <th></th>
      <th>Mean</th>
      <th>Q25</th>
      <th>Q50</th>
      <th>Q75</th>
      <th>SD</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2.399990</td>
      <td>2.399308</td>
      <td>2.399970</td>
      <td>2.400678</td>
      <td>0.001004</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2.379901</td>
      <td>2.372967</td>
      <td>2.379754</td>
      <td>2.386725</td>
      <td>0.010056</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-14.508689</td>
      <td>-892.573838</td>
      <td>0.385163</td>
      <td>878.911284</td>
      <td>2011.476294</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-56.079583</td>
      <td>-1564.614533</td>
      <td>-20.495660</td>
      <td>1430.779292</td>
      <td>3352.355161</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-46.420083</td>
      <td>-2029.456295</td>
      <td>-14.517050</td>
      <td>1953.515688</td>
      <td>4281.037157</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>-53.188599</td>
      <td>-2324.803750</td>
      <td>-11.060575</td>
      <td>2199.907159</td>
      <td>4977.321898</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>-59.908723</td>
      <td>-2587.775501</td>
      <td>-1.324713</td>
      <td>2343.916026</td>
      <td>5487.608913</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>-79.333562</td>
      <td>-2789.683881</td>
      <td>-13.897850</td>
      <td>2545.289708</td>
      <td>5945.573105</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>-59.166091</td>
      <td>-2915.125041</td>
      <td>-18.787404</td>
      <td>2724.453543</td>
      <td>6331.546497</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>-50.076957</td>
      <td>-3056.428636</td>
      <td>-16.221273</td>
      <td>3009.178347</td>
      <td>6641.204709</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>

```python
apw.plot(cols=['FEDFUNDS', 'CPIAUCSL'], data_periods=10)
```

    (<Figure size 864x288 with 3 Axes>,
     array([<matplotlib.axes._subplots.AxesSubplot object at 0x0000000012CBFA20>,
            <matplotlib.axes._subplots.AxesSubplot object at 0x0000000012DF53C8>,
            None], dtype=object))

![png](https://i.imgur.com/R8apuvV.png)
