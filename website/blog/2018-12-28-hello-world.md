---
title: Hello, World!
author: Michael Wooley
authorURL: michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

If you're reading this then you've found the homepage of the GTMM project. In this post I'll describe what I want to do with this blog. For more general information about the project, check out the "[**About**](/about)" page.

<!--truncate-->

#### Uses of the blog

The basic idea is to use this space as a place for idiosyncratic-but-useful information that doesn't fit well into the documentation or tutorials.

I anticipate splitting the posts into a few broad categories:

1. **Project updates** containing news about developments and milestones in the project.
1. **Development Posts** that describe experiments that come up in the development of the framework. These will serve as notebooks for me when I'm trying to work something out and need to run some quick scripts with notes.
1. **Usage Posts** will provide simple use cases of different elements of the framework.

I'll make an effort to differentiate posts that have to do with each of these cases.

#### Contributing to the blog

If you'd like to contribute to the blog please feel free to reach out to me (Michael Wooley) at [michael.wooley@us.gt.com](mailto:michael.wooley@us.gt.com). Don't feel like you need to have anything ready when you get in touch!

I think that's it for now.










