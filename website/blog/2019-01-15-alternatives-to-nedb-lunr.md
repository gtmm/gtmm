---
title: Alternatives to NEDB/lunr
author: Michael Wooley
authorURL: https://michaelwooley.github.io
authorImageURL: https://media.licdn.com/dms/image/C5603AQGRUJ4ufuTthA/profile-displayphoto-shrink_200_200/0?e=1551312000&v=beta&t=yN4l8WNXDqQ98Oby9uKloMK35vAfifMPgnLqUOtB-E8
---

Some notes on FTS3/4/5 to be filled in later:

https://www.sqlite.org/fts3.html
https://github.com/mapbox/node-sqlite3/issues/104
https://www.sqlite.org/compile.html
