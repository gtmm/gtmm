/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require("react");

const CompLibrary = require("../../core/CompLibrary.js");

const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

const Dormant = () => (
  <Container background="highlight" padding={["bottom", "top"]}>
    <h2>😴 GTMM is now dormant</h2>
    <p>
      I (Michael Wooley) no longer work for Grant Thornton as of February 22,
      2019. There will be no further updates to this site or the project after
      that time.
    </p>
    <p>I do not know what will become of the project at this time.</p>
    <p>
      Please contact me at{" "}
      <a href="mailto:wm.wooley@gmail.com">wm.wooley@gmail.com</a> with any
      questions.
    </p>{" "}
  </Container>
);

function About(props) {
  const { config: siteConfig, language = "" } = props;
  const { baseUrl, docsUrl } = siteConfig;
  const docsPart = `${docsUrl ? `${docsUrl}/` : ""}`;
  const langPart = `${language ? `${language}/` : ""}`;
  const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;
  const imgUrl = img => `${baseUrl}img/${img}`;

  const supportLinks = [
    {
      content:
        "Please do not hesitate to contact [Michael Wooley](michaelwooley.github.io), the primary maintainer:\n- E: [michael.wooley@us.gt.com](mailto:michael.wooley@us.gt.com) | [wm.wooley@gmail.com](mailto:wm.wooley@gmail.com)\n- W: [michaelwooley.github.io](michaelwooley.github.io)\n- O: [LinkedIn](https://www.linkedin.com/in/michael-wooley/)",
      title: "Contact",
    },
    {
      content: "[Learn more about GT.](https://www.grantthornton.com/)",
      title: "[Grant Thornton](https://www.grantthornton.com/)",
    },
    {
      content: "➡ Check out the [blog](blog).",
      title: "Stay up to date",
    },
    {
      content:
        "Try reading the [FAQ](docs/faq/general) and [Getting Started](docs/introduction/getting-started) pages first. If that doesn't do it shoot an email to [michael.wooley@us.gt.com](mailto:michael.wooley@us.gt.com)",
      title: "Get Help",
    },
  ];

  return (
    <div>
      <Dormant />
      <div className="docMainWrapper wrapper">
        <Container className="mainContainer documentContainer postContainer">
          <div className="post">
            <header className="postHeader">
              <h1>About</h1>
            </header>

            <CompLibrary.MarkdownBlock>
              This project is maintained by [Michael
              Wooley](michaelwooley.github.io) at [Grant
              Thornton's](https://www.grantthornton.com/) [Economics
              Group](https://www.grantthornton.com/dianeswonk) (Chicago).
            </CompLibrary.MarkdownBlock>
            <GridBlock contents={supportLinks} layout="twoColumn" />
          </div>
        </Container>
      </div>
    </div>
  );
}

module.exports = About;
