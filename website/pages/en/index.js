/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require("react");

const CompLibrary = require("../../core/CompLibrary.js");

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const { siteConfig, language = "" } = this.props;
    const { baseUrl, docsUrl } = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ""}`;
    const langPart = `${language ? `${language}/` : ""}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <h2 className="projectTitle">
        {siteConfig.title}
        <small>{siteConfig.tagline}</small>
      </h2>
    );

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
          <PromoSection>
            <Button href="docs/getting-started">Get Started</Button>
            <Button href="docs/packages">Underlying Package Docs</Button>
            <Button href="/blog">Development Notebooks</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const { config: siteConfig, language = "" } = this.props;
    const { baseUrl } = siteConfig;

    const UnderConstruction = () => (
      <Container padding={["bottom", "top"]} background={"light"}>
        <h2>⚠ Under construction.</h2>
        <div>
          The website, app, and underlying software are incomplete may change.
        </div>
      </Container>
    );

    const Dormant = () => (
      <Container background="highlight" padding={["bottom", "top"]}>
        <h2>😴 GTMM is now dormant</h2>
        <p>
          I (Michael Wooley) no longer work for Grant Thornton as of February
          22, 2019. There will be no further updates to this site or the project
          after that time.
        </p>
        <p>I do not know what will become of the project at this time.</p>
        <p>
          Please contact me at{" "}
          <a href="mailto:wm.wooley@gmail.com">wm.wooley@gmail.com</a> with any
          questions.
        </p>{" "}
      </Container>
    );

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Dormant />
          <UnderConstruction />
        </div>
      </div>
    );
  }
}

module.exports = Index;
