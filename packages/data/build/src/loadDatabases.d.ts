import * as Promise from "bluebird";
import * as Datastore from "nedb";
import * as lunr from "lunr";
export interface INedbStores {
    series: Datastore;
    categories: Datastore;
    sources: Datastore;
    releases: Datastore;
    tags: Datastore;
}
export interface ILunrIndices {
    series: lunr.Index;
    categories: lunr.Index;
    sources: lunr.Index;
    releases: lunr.Index;
    tags: lunr.Index;
}
export interface IDatabaseObject {
    nedb: INedbStores;
    lunr: ILunrIndices;
}
/**
 * Go back to the pre-serialized source and load it to file.
 */
export declare const loadDatabases: (basePath: string) => Promise<IDatabaseObject>;
