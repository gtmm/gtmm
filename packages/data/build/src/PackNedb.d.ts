import * as Promise from "bluebird";
import FredMetadata from "./FredMetadata";
import { IStringTMap } from "./types";
export default class PackNedb {
    basePath: string;
    cachePath: string;
    constructor(basePath: string, cachePath: string);
    series(obj?: IStringTMap<any> | null): Promise<any>;
    releases(obj?: IStringTMap<any> | null): Promise<any>;
    categories(obj?: IStringTMap<any> | null): Promise<any>;
    sources(obj?: IStringTMap<any> | null): Promise<any>;
    tags(obj?: IStringTMap<any> | null): Promise<any>;
    all(fmd?: FredMetadata | null): Promise<any>;
    _fetchInMemory(key: string, obj?: IStringTMap<any> | null): Promise<any>;
}
