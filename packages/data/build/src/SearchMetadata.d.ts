import { IEmptyObject, IAnyTMap } from "./types";
import { IDatabaseObject, ILunrIndices, INedbStores } from "./loadDatabases";
declare type IReturnAs = "cursor" | "promise";
/**
 * Search the metadata for a given query.
 */
export default class SearchMetadata {
    basePath: string;
    idOnly: boolean;
    returnAs: IReturnAs;
    _db: INedbStores;
    _ln: ILunrIndices;
    constructor(basePath: string, idOnly: boolean, returnAs: IReturnAs, db: IDatabaseObject | null);
    _genericSearch(key: string, query: string, filter: IAnyTMap<any> | IEmptyObject | null, limit: number): any;
    series(query: string, filter?: IAnyTMap<any> | null, limit?: number): any;
    tags(query: string, filter?: IAnyTMap<any> | null, limit?: number): any;
    releases(query: string, filter?: IAnyTMap<any> | null, limit?: number): any;
    categories(query: string, filter?: IAnyTMap<any> | null, limit?: number): any;
    sources(query: string, filter?: IAnyTMap<any> | null, limit?: number): any;
}
export {};
