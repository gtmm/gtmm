import * as Promise from "bluebird";
import FredMetadata from "./FredMetadata";
import { IStringTMap } from "./types";
export default class PackLunr {
    basePath: string;
    cachePath: string;
    constructor(basePath: string, cachePath: string);
    _fetchOutMemory(key: string, fields: string[]): Promise<any>;
    _fetchInMemory(key: string, fields: string[], obj?: IStringTMap<any> | null): Promise<any>;
    series(obj?: IStringTMap<any> | null): Promise<any>;
    tags(obj?: IStringTMap<any> | null): Promise<any>;
    releases(obj?: IStringTMap<any> | null): Promise<any>;
    categories(obj?: IStringTMap<any> | null): Promise<any>;
    sources(obj?: IStringTMap<any> | null): Promise<any>;
    all(fmd?: FredMetadata | null): Promise<any>;
}
