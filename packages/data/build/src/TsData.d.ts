import { IStringTMap, ITransforms } from "./types";
declare type ICols = string | string[] | null;
interface ITsDataObsInput {
    series: string;
    date: string;
    value: number;
}
export declare type ITsDataInput = ITsDataObsInput[];
interface ITsDataObs {
    series: string;
    date: Date;
    value: number;
}
declare type ITsData = ITsDataObs[];
declare type IAgg = "ANY" | "M" | "Q" | "A" | "any" | "m" | "q" | "a";
declare type IAggMethod = "avg" | "sum" | "eop" | "AVG" | "SUM" | "EOP";
declare type IDatesAs = "string" | "date";
export declare type IDataShape = "long" | "wide";
interface IWideObs {
    date: Date | string;
    [keys: string]: null | number | Date | string;
}
export declare type IWideData = IWideObs[];
interface IWideObject {
    [dates: string]: IWideObs;
}
interface ILongObs {
    date: Date;
    value: number;
    series: string;
}
declare type ILongVar = ILongObs[];
export declare type ILongData = IStringTMap<ILongVar>;
/**
 *
 * [List of data transforms and abbreviations.](https://research.stlouisfed.org/docs/api/fred/series_observations.html#units)
 * [Growth formulas used.](https://alfred.stlouisfed.org/help#growth_formulas)
 */
export declare class TsData {
    /**
     * Default values returned by
     * @type {[type]}
     */
    v: ILongData | IWideData;
    _w: IWideObject;
    df: ILongData;
    dfw: ILongData;
    cols: string[];
    trans: IStringTMap<ITransforms>;
    timestamps: IStringTMap<number>;
    md: IStringTMap<{
        nObsPerYear: number;
        freq: string;
        bds: {
            x: Date[];
            y: number[];
        };
    }>;
    /**
     * If true, will check to ensure that columns passed into transform methods are valid.
     * @type {boolean}
     */
    strict: boolean;
    /**
     * For wide, return the date parameter as either a date or a string.
     * @type {IDatesAs}
     */
    datesAs: IDatesAs;
    _agg: IAgg;
    _aggMethod: IAggMethod;
    _dShape: IDataShape;
    constructor(df: ITsDataInput, dataShape?: IDataShape, agg?: IAgg, aggMethod?: IAggMethod, strict?: boolean, datesAs?: IDatesAs);
    _init(df: ITsDataInput): void;
    readonly raw: ITsData;
    readonly values: ILongData | IWideData;
    readonly long: ILongData;
    readonly wide: IWideData;
    reset(): void;
    agg: IAgg;
    aggMethod: IAggMethod;
    dataShape: IDataShape;
    /**
     * Alias for changeFromYearAgo
     * @param  {string|string[]} cols [description]
     * @return {ILongData}            [description]
     */
    ch1(cols?: ICols): ILongData | IWideData;
    /**
     * [pctDiffYearOverYear description]
     * @param  {string|string[]} cols    Columns to be made year-over-year.
     * @return {ILongData}               [description]
     */
    changeFromYearAgo(cols?: ICols): ILongData | IWideData;
    _oneChangeFromYearAgo(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    /**
     * Alias for percentChangeFromYearAgo
     * @param  {string|string[]} cols [description]
     * @return {ILongData}            [description]
     */
    pc1(cols?: ICols): ILongData | IWideData;
    /**
     * [pctDiffYearOverYear description]
     * @param  {string|string[]} cols    Columns to be made year-over-year.
     * @return {ILongData}               [description]
     */
    percentChangeFromYearAgo(cols?: ICols): ILongData | IWideData;
    _onePercentChangeFromYearAgo(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    log(cols?: ICols): ILongData | IWideData;
    _oneLog(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    lin(cols?: ICols): ILongData | IWideData;
    levels(cols?: ICols): ILongData | IWideData;
    _oneLevels(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    chg(cols?: ICols): ILongData | IWideData;
    change(cols?: ICols): ILongData | IWideData;
    _oneChange(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    pch(cols?: ICols): ILongData | IWideData;
    percentChange(cols?: ICols): ILongData | IWideData;
    _onePercentChange(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    cch(cols?: ICols): ILongData | IWideData;
    continuouslyCompoundedRateOfChange(cols?: ICols): ILongData | IWideData;
    _oneContinuouslyCompoundedRateOfChange(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    cca(cols?: ICols): ILongData | IWideData;
    continuouslyCompoundedAnnualRateOfChange(cols?: ICols): ILongData | IWideData;
    _oneContinuouslyCompoundedAnnualRateOfChange(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    pca(cols?: ICols): ILongData | IWideData;
    compoundedAnnualRateOfChange(cols?: ICols): ILongData | IWideData;
    _oneCompoundedAnnualRateOfChange(col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    /**
     * Get the number of observations in a year.
     *
     * @todo: Rewrite this so that we can estimate number of observations per year even when have less than year of data, etc.
     * @param  {string} col [description]
     * @return {number}     [description]
     */
    _getNObsPerYear(col: string): (string | number)[];
    _transformGeneral(cols: string | string[] | null | undefined, transformer: (col: string) => {
        out: ILongVar;
        bds: [number, number];
    }, trans: ITransforms): ILongData | IWideData;
    _updateAllSeries(): IStringTMap<ILongObs[]> | IWideObs[];
    _updateAllSeriesLookup(trans: ITransforms, col: string): {
        out: ILongVar;
        bds: [number, number];
    };
    _checkCols(cols?: ICols): string[];
    _aggregateGeneral(col: string, d: ILongVar): ILongVar;
    _aggregateMonthly(col: string, d: ILongVar): ILongVar;
    _aggregateQuarterly(col: string, d: ILongVar): ILongVar;
    _aggregateAnnually(col: string, d: ILongVar): ILongVar;
    _aggregatePeriodFactory(col: string, d: ILongVar, exempt: string[], dateFormatter: (d: Date) => string): ILongVar;
    _aggregateMethodGeneral(col: string, d: IStringTMap<number[]>): ILongVar;
    _aggregateMethodSum(d: number[]): number;
    _aggregateMethodAvg(d: number[]): number;
    _aggregateMethodEOP(d: number[]): number;
    _make(cols: string[]): ILongData | IWideData;
    _makeLong(): ILongData;
    _makeWide(): IWideData;
    _updateWide(cols: string[]): IWideObject;
}
export {};
