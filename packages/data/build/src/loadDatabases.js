"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FS = require("fs");
const Promise = require("bluebird");
const Datastore = require("nedb");
const lunr = require("lunr");
const utils_1 = require("./utils");
const constants_1 = require("./constants");
/**
 * Go back to the pre-serialized source and load it to file.
 */
exports.loadDatabases = (basePath) => new Promise((resolve, reject) => {
    var areReady = 0, needReady = constants_1.FRED_DATA_TYPES.length * 2;
    var db = {}, ln = {};
    const tick = () => {
        areReady += 1;
        if (areReady >= needReady) {
            resolve({ nedb: db, lunr: ln });
        }
    };
    utils_1.checkAllFilesExist(basePath, reject);
    constants_1.FRED_DATA_TYPES.map(dt => {
        db[dt] = new Datastore({
            filename: utils_1.Filenames.nedb(dt, basePath),
            autoload: false,
        });
        db[dt].loadDatabase(err => {
            if (err)
                reject(err);
            tick();
        });
    });
    constants_1.FRED_DATA_TYPES.map(dt => {
        var d = FS.readFileSync(utils_1.Filenames.lunr(dt, basePath), {}).toString();
        ln[dt] = lunr.Index.load(JSON.parse(d));
        tick();
    });
});
//# sourceMappingURL=loadDatabases.js.map