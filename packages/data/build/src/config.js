"use strict";
/**
 * Configuration files for package.
 *
 * On windows will save to somewhere like
 * C:\Users\us57144\.config\configstore
 */
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigStore = require("configstore");
const constants_1 = require("./constants");
const Config = new ConfigStore(constants_1.PKG_NAME, {
    lastUpdated: new Date(2018, 6, 1),
    downloadedSeries: [],
});
exports.default = Config;
//# sourceMappingURL=config.js.map