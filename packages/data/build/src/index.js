"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const C = require("./constants");
const utils_1 = require("./utils");
const FredMetadata_1 = require("./FredMetadata");
const PackLunr_1 = require("./PackLunr");
const PackNedb_1 = require("./PackNedb");
const SearchMetadata_1 = require("./SearchMetadata");
const FredData_1 = require("./FredData");
var TsData_1 = require("./TsData");
exports.TsData = TsData_1.TsData;
const loadDatabases_1 = require("./loadDatabases");
class GtmmData {
    constructor(apiKey = C.DEFAULT_API_KEY, dbName = C.DEFAULT_DB_NAME, basePath = C.DEFAULT_DATA_PATH, cachePath = C.DEFAULT_CACHE_PATH, rateLimitProps = {}, searchReturnIdOnly = false, searchReturnAs = "cursor") {
        this.apiKey = apiKey || C.DEFAULT_API_KEY;
        this.dbName = dbName || C.DEFAULT_DB_NAME;
        this.basePath = basePath || C.DEFAULT_DATA_PATH;
        this.cachePath = cachePath || C.DEFAULT_CACHE_PATH;
        this.rateLimitProps = rateLimitProps || {};
        this.options = {
            search: {
                idOnly: searchReturnIdOnly,
                returnAs: searchReturnAs,
            },
        };
    }
    init(force = false) {
        return new Promise((resolve, reject) => {
            utils_1.checkAllFilesExist(this.basePath, reject);
            loadDatabases_1.loadDatabases(this.basePath).then((out) => {
                this._ln = out.lunr;
                this._db = out.nedb;
                this.search = new SearchMetadata_1.default(this.basePath, this.options.search.idOnly, this.options.search.returnAs, out);
                this.data = new FredData_1.default(this.basePath, this.dbName, this.apiKey, this.rateLimitProps, this._db.series);
                this.data
                    .connect()
                    .then(() => {
                    resolve((this.ready = true));
                })
                    .catch(err => reject(err));
            });
        });
    }
    _checkLoad() {
        if (!this.ready) {
            throw new Error("Object is not ready. Call .init()");
        }
    }
    /**
     * Fetch FRED metadata from API and file into lunr and nedb databases.
     */
    fetchMetadata(force = false) {
        var FMD = new FredMetadata_1.default(this.apiKey, this.cachePath, this.rateLimitProps);
        var PL = new PackLunr_1.default(this.basePath, this.cachePath);
        var PN = new PackNedb_1.default(this.basePath, this.cachePath);
        // @hack: Added delay in-between calls to ensure that files fully written to file before start to read them. Longer-term solution is to allow a FredMetadata object to be passed to each of the other "alls" so that can read directly from there.
        return new Promise((resolve, reject) => FMD.all(force)
            .then(out => {
            PL.all(FMD)
                .then(() => {
                PN.all(FMD)
                    .then(() => resolve(true))
                    .catch(err => reject(err));
            })
                .catch(err => reject(err));
        })
            .catch(err => reject(err)));
    }
    /**
     * Fetch a FRED series.
     */
    fetch(ids, ensureDownloaded = false) {
        this._checkLoad();
        return this.data.fetch(ids, ensureDownloaded);
    }
    /**
     * Wrapper for FredData.update().
     */
    update(which = null, force = false) {
        this._checkLoad();
        return this.data.update(which, force);
    }
    /**
     * Wrapper around FredData.checkForUpdates()
     */
    checkForUpdates() {
        this._checkLoad();
        return this.data.checkForUpdates();
    }
    /**
     * Wrapper around FredData.close();
     */
    close() {
        return this.data.close();
    }
}
exports.default = GtmmData;
//# sourceMappingURL=index.js.map