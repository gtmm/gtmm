import * as Promise from "bluebird";
import * as Fred from "@gtmm/fred-api";
import { ICategories, ISources, IReleases, ITags, ISeriess, IRelease, ISource, ISeries, ITag, ICategory } from "./FredMetadata.types";
import { IStringTMap, IRateLimitProps, IEmptyObject } from "./types";
export default class FredMetadata {
    fred: Fred;
    series: ISeriess;
    tags: ITags;
    sources: ISources;
    releases: IReleases;
    categories: ICategories;
    fetched: IStringTMap<boolean>;
    cachePath: string | null;
    _seriesCount: number;
    constructor(apiKey?: string, cachePath?: string | null, rateLimitProps?: IRateLimitProps | IEmptyObject);
    readonly apiKey: string;
    all(force?: boolean): Promise<any>;
    getCachedFiles(): void;
    clearCachedFiles(): void;
    _cacheFile(key: string, obj: IStringTMap<any>): void;
    /**
     * Get the sources and associated releases for each.
     */
    fetchSources(): Promise<any>;
    fetchReleases(): Promise<any>;
    fetchCategories(): Promise<any>;
    fetchTags(): any;
    /**
     * This is a "special" edge fetcher due to the fact that it will also
     * take care of the fetching of the edges.
     *
     * Additionally, per the article on series tokenization in blog, will
     * only include those with frequency in "M", "Q", "D", "BW", "W".
     * @return {[type]} [description]
     */
    fetchSeries(): any;
    fetchEdgesReleaseTag(): Promise<any>;
    fetchEdgesCategoryTag(): Promise<any>;
    fetchEdgesReleaseSource(): Promise<any>;
    fetchEdgeCategoryRelated(): Promise<any>;
    /**
     * Get all related tags in a series.
     *
     * @deprecated This method takes to long to run. Instead, newer versions will make it possible to import these linkages one at a time as they are called.
     */
    fetchEdgesTagsRelated(): Promise<any>;
    /**
     * Fetch the series that are associated with a given tag.
     * @deprecated This method takes to long to run. Instead, newer versions will make it possible to import these linkages one at a time as they are called.
     */
    fetchEdgesTagSeries(): any;
    _appendSeries(d: IStringTMap<any>, fromType: string | null, fromTypeId: number | string | null, inplace?: boolean): ISeries | null;
    _appendTag(d: IStringTMap<any>, inplace?: boolean): ITag;
    _appendSource(d: IStringTMap<any>, inplace?: boolean): ISource;
    /**
     * Appends releases to release catalog even if they already exist.
     * @type {[type]}
     */
    _appendRelease(d: IStringTMap<any>, inplace?: boolean): IRelease;
    _appendCategory(d: IStringTMap<any>, children: IStringTMap<any>[] | null, inplace?: boolean): ICategory;
}
