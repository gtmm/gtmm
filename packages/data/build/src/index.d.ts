import * as Promise from "bluebird";
import { IRateLimitProps, IEmptyObject } from "./types";
import SearchMetadata from "./SearchMetadata";
import FredData from "./FredData";
export { TsData, ITsDataInput, ILongData, IWideData } from "./TsData";
import { ILunrIndices, INedbStores } from "./loadDatabases";
export default class GtmmData {
    apiKey: string;
    basePath: string;
    dbName: string;
    cachePath: string;
    rateLimitProps: IRateLimitProps | IEmptyObject;
    options: {
        search: {
            idOnly: boolean;
            returnAs: "cursor" | "promise";
        };
    };
    search: SearchMetadata;
    data: FredData;
    /**
     * Object Containing the lunr indices for carrying out searches.
     * (Internal)
     * @type {[type]}
     */
    _ln: ILunrIndices;
    /**
     * Object containing the NEDB datastores for carrying fetching
     * metadata. (Internal.)
     * @type {[type]}
     */
    _db: INedbStores;
    /**
     * [ready description]
     * @type {[type]}
     */
    ready: boolean;
    constructor(apiKey?: string, dbName?: string, basePath?: string, cachePath?: string, rateLimitProps?: IRateLimitProps | IEmptyObject, searchReturnIdOnly?: boolean, searchReturnAs?: "cursor" | "promise");
    init(force?: boolean): Promise<any>;
    _checkLoad(): void;
    /**
     * Fetch FRED metadata from API and file into lunr and nedb databases.
     */
    fetchMetadata(force?: boolean): Promise<any>;
    /**
     * Fetch a FRED series.
     */
    fetch(ids: string, ensureDownloaded?: boolean): Promise<any>;
    /**
     * Wrapper for FredData.update().
     */
    update(which?: null | string[] | string, force?: boolean): Promise<any>;
    /**
     * Wrapper around FredData.checkForUpdates()
     */
    checkForUpdates(): Promise<any>;
    /**
     * Wrapper around FredData.close();
     */
    close(): void;
}
