import { IFilenameCacheFunc, IFilenameBaseFunc } from "./utils.types";
import { IStringTMap } from "./types";
/**
 * A utility for creating standardized filenames for the files used
 * internally by the package.
 * @type {[type]}
 */
export declare const Filenames: IStringTMap<IFilenameBaseFunc | IFilenameCacheFunc>;
/**
 * Convert a javascript date to a date that is suitable for FRED to
 *  process. See
 *  https://research.stlouisfed.org/docs/api/fred/series_updates.html#start_time
 *  for more information.
 * @param  {[type]} string [description]
 * @return {[type]}        [description]
 */
export declare const dateToFredDate: (d: Date) => string;
/**
 * Check that all files necessary to query FRED metadata are present.
 * @param  basePath Base path to file.
 * @param  asBoolean If `true`, will only return T/F. If `false` (default), will raise informative error message if not all files are found.
 * @return         If `true`, good to go. Not otherwise.
 */
export declare const checkAllFilesExist: (basePath: string, reject?: ((arg: any) => void) | undefined) => boolean | Error;
