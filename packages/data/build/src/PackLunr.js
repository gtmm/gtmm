"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FS = require("fs");
const Promise = require("bluebird");
const lunr = require("lunr");
const JSONStream = require("JSONStream");
const utils_1 = require("./utils");
class PackLunr {
    constructor(basePath, cachePath) {
        this.basePath = basePath;
        this.cachePath = cachePath;
    }
    _fetchOutMemory(key, fields) {
        return new Promise((resolve, reject) => {
            var outFile = utils_1.Filenames.lunr(key, this.basePath), cacheFile = utils_1.Filenames.cache(key, this.cachePath);
            if (!FS.existsSync(cacheFile)) {
                var ei = new Error(`The cache file: ${cacheFile} does not exist. Ensure that the FredMetadata command has been called.`);
                reject(ei);
            }
            else {
                var transformStream = JSONStream.parse("*");
                var inputStream = FS.createReadStream(cacheFile);
                lunr(function () {
                    this.ref("_id");
                    for (let ii = 0; ii < fields.length; ii++) {
                        this.field(fields[ii]);
                    }
                    inputStream
                        .pipe(transformStream)
                        .on("data", data => {
                        this.add(data);
                    })
                        .on("end", () => {
                        var idx = this.build();
                        FS.writeFile(outFile, JSON.stringify(idx.toJSON()), {}, (err) => {
                            if (err) {
                                reject(err);
                                return;
                            }
                            resolve(idx);
                        });
                    })
                        .on("error", e => {
                        reject(e);
                    });
                });
            }
        });
    }
    _fetchInMemory(key, fields, obj = null) {
        const fetchSrc = (k, reject) => {
            var cacheFile = utils_1.Filenames.cache(key, this.cachePath);
            if (!FS.existsSync(cacheFile)) {
                var ei = new Error(`The cache file: ${cacheFile} does not exist. Ensure that the FredMetadata command has been called.`);
                reject(ei);
            }
            return JSON.parse(FS.readFileSync(cacheFile).toString());
        };
        // MAIN FUNCTION
        return new Promise((resolve, reject) => {
            var idx, src;
            src = obj ? obj : fetchSrc(key, reject);
            idx = lunr(function () {
                this.ref("_id");
                for (let ii = 0; ii < fields.length; ii++) {
                    this.field(fields[ii]);
                }
                Object.keys(src).forEach(k => this.add(src[k]));
            });
            FS.writeFile(utils_1.Filenames.lunr(key, this.basePath), JSON.stringify(idx), {}, (err) => {
                if (err)
                    reject(err);
                resolve(idx);
            });
        });
    }
    series(obj = null) {
        var key = "series";
        var fields = ["title", "id"];
        if (obj) {
            return this._fetchInMemory(key, fields, obj);
        }
        else {
            return this._fetchOutMemory(key, fields);
        }
    }
    tags(obj = null) {
        var key = "tags";
        var fields = ["name", "groupId", "notes"];
        return this._fetchInMemory(key, fields, obj);
    }
    releases(obj = null) {
        var key = "releases";
        var fields = ["name", "link", "notes"];
        return this._fetchInMemory(key, fields, obj);
    }
    categories(obj = null) {
        var key = "categories";
        var fields = ["name", "notes"];
        return this._fetchInMemory(key, fields, obj);
    }
    sources(obj = null) {
        var key = "sources";
        var fields = ["name", "link", "notes"];
        return this._fetchInMemory(key, fields, obj);
    }
    all(fmd = null) {
        if (fmd) {
            if (!Object.keys(fmd.fetched).every(f => fmd.fetched[f])) {
                throw new Error("Not all elements in FredMetadata object present. Fetch them before proceeding further.");
            }
            else {
                return new Promise((resolve, reject) => {
                    Promise.all([
                        this.sources(fmd.sources),
                        this.categories(fmd.categories),
                        this.tags(fmd.tags),
                        this.releases(fmd.releases),
                    ])
                        .then(() => {
                        this.series(fmd.series)
                            .then(() => {
                            this.tags(fmd.tags)
                                .then(() => resolve(true))
                                .catch(reject);
                        })
                            .catch(reject);
                    })
                        .catch(reject);
                });
            }
        }
        else {
            return Promise.all([
                this.sources(),
                this.categories(),
                this.tags(),
                this.releases(),
                this.series(),
            ]);
        }
    }
}
exports.default = PackLunr;
//# sourceMappingURL=PackLunr.js.map