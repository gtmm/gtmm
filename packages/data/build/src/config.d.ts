/**
 * Configuration files for package.
 *
 * On windows will save to somewhere like
 * C:\Users\us57144\.config\configstore
 */
import * as ConfigStore from "configstore";
declare const Config: ConfigStore;
export default Config;
