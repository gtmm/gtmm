"use strict";
/**
 * Get a default API key. Useful for testing purposes.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const FS = require("fs");
const Path = require("path");
const xdgBasedir = require("xdg-basedir");
const PKG_NAME = "@gtmm-data";
exports.PKG_NAME = PKG_NAME;
const DEFAULT_API_KEY = process.env.FRED_KEY || "NOT FOUND";
exports.DEFAULT_API_KEY = DEFAULT_API_KEY;
const DEFAULT_DATA_PATH = Path.join(xdgBasedir.data, "gtmm_data");
exports.DEFAULT_DATA_PATH = DEFAULT_DATA_PATH;
const DEFAULT_CACHE_PATH = Path.join(process.env.temp || xdgBasedir.data, "gtmm_cache_data");
exports.DEFAULT_CACHE_PATH = DEFAULT_CACHE_PATH;
const DEFAULT_DB_NAME = "fred.data.db";
exports.DEFAULT_DB_NAME = DEFAULT_DB_NAME;
if (!FS.existsSync(DEFAULT_DATA_PATH)) {
    FS.mkdirSync(DEFAULT_DATA_PATH, { recursive: true });
    FS.mkdirSync(Path.join(DEFAULT_DATA_PATH, "nedb"), { recursive: true });
}
if (!FS.existsSync(Path.join(DEFAULT_DATA_PATH, "lunr"))) {
    FS.mkdirSync(Path.join(DEFAULT_DATA_PATH, "lunr"), { recursive: true });
}
if (!FS.existsSync(Path.join(DEFAULT_DATA_PATH, "nedb"))) {
    FS.mkdirSync(Path.join(DEFAULT_DATA_PATH, "nedb"), { recursive: true });
}
// Per
const SERIES_FREQ = {
    short: ["Q", "M", "BW", "W", "D"],
    starts: ["Quarterly", "Monthly", "Biweekly", "Weekly", "Daily"],
};
exports.SERIES_FREQ = SERIES_FREQ;
const FRED_DATA_TYPES = ["series", "sources", "tags", "releases", "categories"];
exports.FRED_DATA_TYPES = FRED_DATA_TYPES;
const DEFAULT_RATE_LIMIT_PROPS = {
    wait: 10 * 1000,
    blockSize: 1000,
    errTol: 200,
};
exports.DEFAULT_RATE_LIMIT_PROPS = DEFAULT_RATE_LIMIT_PROPS;
//# sourceMappingURL=constants.js.map