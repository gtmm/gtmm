/**
 * Get a default API key. Useful for testing purposes.
 */
declare const PKG_NAME = "@gtmm-data";
declare const DEFAULT_API_KEY: string;
declare const DEFAULT_DATA_PATH: string;
declare const DEFAULT_CACHE_PATH: string;
declare const DEFAULT_DB_NAME = "fred.data.db";
declare const SERIES_FREQ: {
    short: string[];
    starts: string[];
};
declare const FRED_DATA_TYPES: string[];
declare const DEFAULT_RATE_LIMIT_PROPS: {
    wait: number;
    blockSize: number;
    errTol: number;
};
export { PKG_NAME, DEFAULT_API_KEY, DEFAULT_DATA_PATH, DEFAULT_CACHE_PATH, SERIES_FREQ, FRED_DATA_TYPES, DEFAULT_RATE_LIMIT_PROPS, DEFAULT_DB_NAME, };
