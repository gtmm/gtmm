"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
const loadDatabases_1 = require("./loadDatabases");
/**
 * Search the metadata for a given query.
 */
class SearchMetadata {
    constructor(basePath, idOnly, returnAs, db) {
        this.basePath = basePath;
        this.idOnly = idOnly;
        this.returnAs = returnAs;
        if (db) {
            this._ln = db.lunr;
            this._db = db.nedb;
        }
        else {
            loadDatabases_1.loadDatabases(this.basePath)
                .then(out => {
                this._ln = out.lunr;
                this._db = out.nedb;
            })
                .catch(err => {
                throw err;
            });
        }
    }
    _genericSearch(key, query, filter, limit) {
        filter = filter || {};
        // Carry out the lunr search.
        var res;
        if (this.returnAs === "promise") {
            return new Promise((resolve, reject) => {
                res = this._ln[key].search(query).slice(0, limit);
                if (!filter && this.idOnly) {
                    resolve(res.map(ri => {
                        return { _id: ri.ref, score: ri.score };
                    }));
                }
                else {
                    var matches = res.map(ri => ri.ref);
                    this._db[key].find({ $and: [{ _id: { $in: matches } }, filter] }, (err, docs) => {
                        if (err)
                            reject(err);
                        if (this.idOnly) {
                            resolve(docs.map(d => {
                                return {
                                    _id: d._id,
                                    score: res[matches.indexOf(d._id)].score,
                                };
                            }));
                        }
                        else {
                            resolve(docs.map(d => {
                                d.score = res[matches.indexOf(d._id)].score;
                                return d;
                            }));
                        }
                    });
                }
            });
        }
        else {
            res = this._ln[key].search(query).slice(0, limit);
            if (this.idOnly) {
                return res.map(ri => {
                    return { _id: ri.ref, score: ri.score };
                });
            }
            else {
                var matches = res.map(ri => ri.ref);
                return this._db[key].find({
                    $and: [{ _id: { $in: matches } }, filter],
                });
            }
        }
    }
    series(query, filter = null, limit = Infinity) {
        return this._genericSearch("series", query, filter, limit);
    }
    tags(query, filter = null, limit = Infinity) {
        return this._genericSearch("series", query, filter, limit);
    }
    releases(query, filter = null, limit = Infinity) {
        return this._genericSearch("releases", query, filter, limit);
    }
    categories(query, filter = null, limit = Infinity) {
        return this._genericSearch("categories", query, filter, limit);
    }
    sources(query, filter = null, limit = Infinity) {
        return this._genericSearch("sources", query, filter, limit);
    }
}
exports.default = SearchMetadata;
//# sourceMappingURL=SearchMetadata.js.map