import * as sqlite3 from "sqlite3";
import * as Promise from "bluebird";
import * as Datastore from "nedb";
import { IStringTMap, IRateLimitProps, IEmptyObject } from "./types";
/**
 * Handles downloads, updates, and local fetches of FRED data.
 */
export default class FredData {
    dbPath: string;
    db: sqlite3.Database | null;
    ready: boolean;
    /**
     * FRED API Object.
     * @type {[type]}
     */
    _f: any;
    /**
     * Information about table schema
     * @type {Object}
     */
    _td: {
        table: string;
        columns: string[];
    };
    /**
     * Series datastore
     * @type {[type]}
     */
    _s: Datastore;
    constructor(basePath: string, dbName: string, apiKey: string, rateLimitProps: IEmptyObject | IRateLimitProps | undefined, series: Datastore);
    /**
     * [fetchSeries description]
     */
    fetch(ids: string[] | string, ensureDownloaded?: boolean): Promise<any>;
    /**
     * Update the data in the database.
     *
     * @params which  If `null`, all collected series are updated. If a string or array of strings, specified series IDs are updated.
     * @params force  If `false`, only the series that have not been downloaded since the last update are downloaded. If `true` all series passed in `which` downloaded.
     */
    update(which?: null | string[] | string, force?: boolean): Promise<any>;
    /**
     * Get a list of series that have been updated.
     * (Requires internet connection and valid API key.)
     *
     * @param strict (default:true) If true, will formally check the relationship between `lastUpdated` and `lastDownloaded` for each series that is updated. This may help to resolve any inconsistencies in updates.
     */
    checkForUpdates(strict?: boolean): Promise<any>;
    /**
     * Download data from FRED API. If a DB connection exists append it
     * to the DB.
     * @type {[type]}
     */
    _download(ids: string[] | string, params?: IStringTMap<string>, append?: boolean): Promise<any>;
    /**
     * Download one series from FRED API. Attach the series_id to the output. Usually called internally by `_download`.
     * @type {[type]}
     */
    _downloadOne(params: IStringTMap<string>): Promise<any>;
    /**
     * Connect to local SQLITE database
     */
    connect(): Promise<any>;
    /**
     * Close the sqlite database connection
     */
    close(): void;
    /**
     * Clear the DB table completely and start again.
     * @return {[type]} [description]
     */
    resetDb(): Promise<any>;
    _notConnectedError(): Promise<any>;
}
