"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FS = require("fs");
const Promise = require("bluebird");
const Datastore = require("nedb");
const utils_1 = require("./utils");
class PackNedb {
    constructor(basePath, cachePath) {
        this.basePath = basePath;
        this.cachePath = cachePath;
    }
    series(obj = null) {
        return this._fetchInMemory("series", obj);
    }
    releases(obj = null) {
        return this._fetchInMemory("releases", obj);
    }
    categories(obj = null) {
        return this._fetchInMemory("categories", obj);
    }
    sources(obj = null) {
        return this._fetchInMemory("sources", obj);
    }
    tags(obj = null) {
        return this._fetchInMemory("tags", obj);
    }
    all(fmd = null) {
        if (fmd) {
            if (!Object.keys(fmd.fetched).every(f => fmd.fetched[f])) {
                throw new Error("Not all elements in FredMetadata object present. Fetch them before proceeding further.");
            }
            else {
                return Promise.all([
                    this.series(fmd.series),
                    this.releases(fmd.releases),
                    this.sources(fmd.sources),
                    this.tags(fmd.tags),
                    this.categories(fmd.categories),
                ]);
            }
        }
        else {
            return Promise.all([
                this.series(),
                this.releases(),
                this.sources(),
                this.tags(),
                this.categories(),
            ]);
        }
    }
    _fetchInMemory(key, obj = null) {
        return new Promise((resolve, reject) => {
            // Make contingencies for if there is some sort of error in the write.
            var file = utils_1.Filenames.nedb(key, this.basePath), dFile = utils_1.Filenames.nedb(key + "-deprecated", this.basePath), cacheFile = utils_1.Filenames.cache(key, this.cachePath);
            var preExisting = FS.existsSync(file);
            if (!FS.existsSync(cacheFile)) {
                var ei = new Error(`The cache file: ${cacheFile} does not exist. Ensure that the FredMetadata command has been called.`);
                reject(ei);
            }
            if (preExisting) {
                FS.renameSync(file, dFile);
            }
            var src = obj
                ? obj
                : JSON.parse(FS.readFileSync(cacheFile).toString());
            try {
                // Get the database
                var db = new Datastore({ filename: file, autoload: true });
                // Pack everything to a long array.
                var toPack = Object.keys(src).map(k => src[k]);
                // insert into db
                db.insert(toPack, err => {
                    if (err) {
                        throw err;
                    }
                    // Remove the old db
                    if (preExisting) {
                        FS.unlink(dFile, err => {
                            if (err) {
                                throw err;
                            }
                            resolve(true);
                        });
                    }
                    else {
                        resolve(true);
                    }
                });
            }
            catch (e) {
                // If there is a failure then undo the renaming.
                if (preExisting) {
                    FS.renameSync(dFile, file);
                }
                reject(e);
            }
        });
    }
}
exports.default = PackNedb;
//# sourceMappingURL=PackNedb.js.map