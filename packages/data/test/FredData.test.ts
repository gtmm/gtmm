import * as FS from "fs";
import * as Path from "path";

import * as Datastore from "nedb";

import { loadDatabases } from "../src/loadDatabases";
import Config from "../src/config";
import FredData from "../src/FredData";
import * as C from "../src/constants";
import { IStringTMap } from "../src/types";
import { SERIES_FREQ } from "../src/constants";

jest.setTimeout(100000);

describe("FredData: Connect and close db", () => {
  var FD;

  beforeAll(() => {
    return loadDatabases(C.DEFAULT_DATA_PATH).then(db => {
      FD = new FredData(
        C.DEFAULT_DATA_PATH,
        C.DEFAULT_DB_NAME,
        C.DEFAULT_API_KEY,
        C.DEFAULT_RATE_LIMIT_PROPS,
        db.nedb.series
      );
    });
  });

  it("can connect", done => {
    FD.connect().then(() => {
      expect(FD.db).not.toBeNull();
      expect(FD.ready).toBeTruthy();
      done();
    });
  });

  it("can close", done => {
    FD.connect().then(() => {
      FD.close();

      setTimeout(() => {
        expect(FD.db).toBeNull();
        expect(FD.ready).toBeFalsy();
        done();
      }, 1000);
    });
  });

  afterAll(() => {
    try {
      FD.close();
    } catch (e) {
      console.error(e);
    }
  });
});

describe("FredData: assume connection exists", () => {
  var FD;
  beforeAll(done => {
    return loadDatabases(C.DEFAULT_DATA_PATH).then(db => {
      FD = new FredData(
        C.DEFAULT_DATA_PATH,
        C.DEFAULT_DB_NAME,
        C.DEFAULT_API_KEY,
        C.DEFAULT_RATE_LIMIT_PROPS,
        db.nedb.series
      );
      return FD.connect().then(() => {
        FD.resetDb().then(() => done());
      });
    });
  });

  afterAll(() => {
    FD.close();
  });

  describe("download series", () => {
    var outputKeys = expect.arrayContaining([
      "series_id",
      "realtime_start",
      "realtime_end",
      "observation_start",
      "observation_end",
      "units",
      "output_type",
      "file_type",
      "order_by",
      "sort_order",
      "count",
      "offset",
      "limit",
      "observations",
    ]);

    it("can download one", () => {
      var key = "GNPCA";

      expect.assertions(3);

      return FD._downloadOne({ series_id: key }).then(out => {
        expect(Object.keys(out)).toEqual(outputKeys);
        expect(out.observations.length).toBeGreaterThan(0);
        expect(out.series_id).toEqual(key);
      });
    });

    it("can download multiple - no params", () => {
      var keys = ["GNPCA", "A191RL1Q225SBEA"];

      expect.assertions(1 + 3 * keys.length);

      return FD._download(keys).then(out => {
        expect(out.length).toEqual(keys.length);

        for (let ii = 0; ii < keys.length; ii++) {
          expect(Object.keys(out[ii])).toEqual(outputKeys);
          expect(out[ii].observations.length).toBeGreaterThan(0);
          expect(out[ii].series_id).toEqual(keys[ii]);
        }
      });
    });

    it("can download multiple - w/ params", () => {
      var keys = ["GNPCA", "A191RL1Q225SBEA"];
      var params = { limit: "10", sort_order: "desc" };

      var nAssert = 1 + (3 + Object.keys(params).length) * keys.length;
      expect.assertions(nAssert);

      return FD._download(keys, params).then(out => {
        expect(out.length).toEqual(keys.length);

        for (let ii = 0; ii < keys.length; ii++) {
          expect(Object.keys(out[ii])).toEqual(outputKeys);
          expect(out[ii].observations.length).toBeGreaterThan(0);
          expect(out[ii].series_id).toEqual(keys[ii]);

          for (let kk in params) {
            expect(out[ii][kk].toString()).toEqual(params[kk]);
          }
        }
      });
    });
  });

  it("download data and write to db", done => {
    var keys = ["GNPCA", "A191RL1Q225SBEA"];

    return FD._download(keys)
      .then(out => {
        expect(out.length).toEqual(keys.length);

        FD.db.serialize(() => {
          var totCount = 0;

          keys.forEach((k, ii) => {
            FD.db.all(
              `SELECT * FROM ${FD._td.table} WHERE series="${
                out[ii].series_id
              }" ORDER BY date;`,
              [],
              (err, rows) => {
                if (err) throw err;
                expect(rows.length).toEqual(out[ii].observations.length);
              }
            );
            totCount += out[ii].observations.length;
          });

          FD.db.all(
            `SELECT * FROM ${FD._td.table} ORDER BY series, date;`,
            [],
            (err, rows) => {
              if (err) throw err;
              expect(rows.length).toEqual(totCount);
            }
          );
          done();
        });
      })
      .catch(err => {
        console.error(err);
        done();
      });
  });

  describe("can check for updates", () => {
    it("w/ strict", done => {
      expect.assertions(2);
      var hourAgo = new Date();
      hourAgo.setDate(hourAgo.getDate() - 1);

      Config.set("lastUpdated", hourAgo);
      FD.checkForUpdates()
        .then(updates => {
          expect(updates).not.toBeNull();
          expect(updates.limit).toEqual(updates.count);
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });

    it("non-strict", done => {
      var hourAgo = new Date();
      hourAgo.setDate(hourAgo.getDate() - 1);

      Config.set("lastUpdated", hourAgo);

      expect.assertions(2);
      FD.checkForUpdates(false)
        .then(updates => {
          expect(updates).not.toBeNull();
          expect(updates.limit).toEqual(updates.count);
          done();
        })
        .catch(err => {
          console.log(err);
          done();
        });
    });
  });

  describe("update data", () => {
    var keys = ["GNPCA", "A191RL1Q225SBEA"];
    beforeEach(() => {
      Config.set("lastUpdated", new Date());
    });

    describe("without previous data", () => {
      beforeAll(done => {
        return FD.resetDb().then(() => done());
      });

      describe("no arguments", () => {
        it("force=false", done => {
          return FD.update(null, false).then(out => {
            console.log(out);
            expect(out).toBe(expect.arrayContaining([]));
            FD.db.all(
              "SELECT series,count(*) FROM data GROUP BY series;",
              [],
              (err, rows) => {
                if (err) throw err;
                expect(rows.length).toBe(0);
                done();
              }
            );
          });
        });

        it("force=true", done => {
          return FD.update(null, true)
            .then(out => {
              console.log(out);
              expect(out).toBe(expect.arrayContaining([]));
              FD.db.all(
                "SELECT series,count(*) FROM data GROUP BY series;",
                [],
                (err, rows) => {
                  if (err) throw err;
                  expect(rows.length).toBe(0);
                  done();
                }
              );
            })
            .catch(err => {
              console.log(err);
              done();
            });
        });
      });

      describe("single series", () => {
        beforeEach(done => {
          return FD.resetDb().then(() => done());
        });

        it("force=false", done => {
          return FD.update(keys[0], false).then(out => {
            console.log(out);
            expect(out).toBe(expect.arrayContaining([keys[0]]));

            FD.db.all(
              "SELECT series,count(*) FROM data GROUP BY series;",
              [],
              (err, rows) => {
                if (err) throw err;

                expect(rows[0].series).toBe(keys[0]);
                expect(rows[0].count).toBeGreaterThan(0);
                done();
              }
            );
            // expect();
          });
        });

        it("force=true", done => {
          return FD.update(keys[0], true).then(out => {
            console.log(out);
            expect(out).toBe(expect.arrayContaining([]));
            done();
          });
        });
      });
    });
  });
});
