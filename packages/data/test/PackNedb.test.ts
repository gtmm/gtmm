import * as FS from "fs";
import * as Path from "path";

import * as Datastore from "nedb";
import * as JSONStream from "JSONStream";

import FredMetadata from "../src/FredMetadata";
import PackNedb from "../src/PackNedb";
import * as C from "../src/constants";
import { Filenames } from "../src/utils";

import CountAssertions from "./tools/CountAssertions";
import { getCacheNotFoundError, deleteFolderRecursive } from "./tools";

jest.setTimeout(1000000);

describe("PackNedb", () => {
  describe("should work", () => {
    describe("in file", () => {
      var PN: PackNedb;

      const checkBasic = (
        key: string,
        testName: string,
        done: () => void,
        testKey: string = "name"
      ): void => {
        var filename = Filenames.nedb(key, PN.basePath);

        var CA: CountAssertions = new CountAssertions(5, done);

        return PN[key]()
          .then(out => {
            expect(out).toBeTruthy();
            CA.tick();
            expect(FS.existsSync(filename)).toBeTruthy();
            CA.tick();

            var db: Datastore = new Datastore({
              filename: filename,
              autoload: true,
            });

            // Compare the counts
            var cacheCount: number = 0;
            var transformStream = JSONStream.parse("*");
            var inputStream = FS.createReadStream(
              Filenames.cache(key, PN.cachePath)
            );

            inputStream
              .pipe(transformStream)
              .on("data", data => {
                cacheCount += 1;
              })
              .on("end", () => {
                db.count({}, (err, count) => {
                  if (err) throw err;
                  expect(count).toBeGreaterThan(0);
                  CA.tick();
                  expect(count).toEqual(cacheCount);
                  CA.tick();
                });
              })
              .on("error", err => {
                throw err;
              });

            // Check for particular series
            db.count({ [testKey]: testName }, (err, count) => {
              if (err) throw err;
              expect(count).toBeGreaterThan(0);
              CA.tick();
            });
          })
          .catch(err => {
            throw err;
          });
      };

      beforeAll(() => {
        PN = new PackNedb(C.DEFAULT_DATA_PATH, C.DEFAULT_CACHE_PATH);
      });

      it("sources", done => {
        var key: string = "sources",
          testName: string = "Dow Jones & Company";

        return checkBasic(key, testName, done);
      });

      it("releases", done => {
        var key: string = "releases",
          testName: string =
            "Advance Monthly Sales for Retail and Food Services";
        return checkBasic(key, testName, done);
      });

      it("tags", done => {
        var key: string = "tags",
          testName: string = "usa";
        return checkBasic(key, testName, done);
      });

      it("categories", done => {
        var key: string = "categories",
          testName: string = "Productivity & Costs";
        return checkBasic(key, testName, done);
      });

      it("series", done => {
        var key: string = "series",
          testName: string = "CPIAUCSL";
        return checkBasic(key, testName, done, "id");
      });
    });

    describe("from pre-existing FredMetadata", () => {
      var FMD: FredMetadata,
        PN: PackNedb,
        dataPath: string = Path.join(C.DEFAULT_DATA_PATH, "pre-existing");

      var PN: PackNedb;

      const checkBasic = (
        key: string,
        testName: string,
        done: () => void,
        testKey: string = "name"
      ): void => {
        var filename = Filenames.nedb(key, PN.basePath);

        var CA: CountAssertions = new CountAssertions(5, done);

        return PN[key](FMD[key])
          .then(out => {
            expect(out).toBeTruthy();
            CA.tick();
            expect(FS.existsSync(filename)).toBeTruthy();
            CA.tick();

            var db: Datastore = new Datastore({
              filename: filename,
              autoload: true,
            });

            // Compare the counts
            var cacheCount: number = 0;
            var transformStream = JSONStream.parse("*");
            var inputStream = FS.createReadStream(
              Filenames.cache(key, PN.cachePath)
            );

            inputStream
              .pipe(transformStream)
              .on("data", data => {
                cacheCount += 1;
              })
              .on("end", () => {
                db.count({}, (err, count) => {
                  if (err) throw err;
                  expect(count).toBeGreaterThan(0);
                  CA.tick();
                  expect(count).toEqual(cacheCount);
                  CA.tick();
                });
              })
              .on("error", err => {
                throw err;
              });

            // Check for particular series
            db.count({ [testKey]: testName }, (err, count) => {
              if (err) throw err;
              expect(count).toBeGreaterThan(0);
              CA.tick();
            });
          })
          .catch(err => {
            throw err;
          });
      };

      beforeAll(() => {
        deleteFolderRecursive(dataPath);
        FS.mkdirSync(Path.join(dataPath, "nedb"), { recursive: true });

        PN = new PackNedb(dataPath, C.DEFAULT_CACHE_PATH);
        FMD = new FredMetadata(C.DEFAULT_API_KEY, C.DEFAULT_CACHE_PATH, {});
        return FMD.all(false);
      });

      afterAll(() => {
        deleteFolderRecursive(dataPath);
      });

      it("sources", done => {
        var key: string = "sources",
          testName: string = "Dow Jones & Company";

        return checkBasic(key, testName, done);
      });

      it("releases", done => {
        var key: string = "releases",
          testName: string =
            "Advance Monthly Sales for Retail and Food Services";
        return checkBasic(key, testName, done);
      });

      it("tags", done => {
        var key: string = "tags",
          testName: string = "usa";
        return checkBasic(key, testName, done);
      });

      it("categories", done => {
        var key: string = "categories",
          testName: string = "Productivity & Costs";
        return checkBasic(key, testName, done);
      });

      it("series", done => {
        var key: string = "series",
          testName: string = "CPIAUCSL";
        return checkBasic(key, testName, done, "id");
      });
    });
  });

  describe("pack all", () => {
    describe("from file", () => {
      var PN: PackNedb;
      beforeAll(() => {
        PN = new PackNedb(C.DEFAULT_DATA_PATH, C.DEFAULT_CACHE_PATH);
      });
      it("all", done => {
        PN.all()
          .then(out => {
            expect(out.every(outi => outi === true)).toBeTruthy();
            expect(out.length).toBe(5);
            done();
          })
          .catch(err => {
            throw err;
          });
      });
    });

    describe("from pre-existing FredMetadata", () => {
      var FMD: FredMetadata,
        PN: PackNedb,
        dataPath: string = Path.join(C.DEFAULT_DATA_PATH, "pre-existing");

      var PN: PackNedb;

      beforeAll(() => {
        deleteFolderRecursive(dataPath);
        FS.mkdirSync(Path.join(dataPath, "nedb"), { recursive: true });

        PN = new PackNedb(dataPath, C.DEFAULT_CACHE_PATH);
        FMD = new FredMetadata(C.DEFAULT_API_KEY, C.DEFAULT_CACHE_PATH, {});
        return FMD.all(false);
      });

      afterAll(() => {
        deleteFolderRecursive(dataPath);
      });

      it("all", done => {
        PN.all(FMD)
          .then(out => {
            expect(out.every(outi => outi === true)).toBeTruthy();
            expect(out.length).toBe(5);
            done();
          })
          .catch(err => {
            throw err;
          });
      });
    });
  });

  describe("should fail", () => {
    var PN: PackNedb,
      cachePath: string = Path.parse(C.DEFAULT_CACHE_PATH).dir;

    beforeAll(() => {
      PN = new PackNedb(C.DEFAULT_DATA_PATH, cachePath);
    });

    it("tags/_fetchInMemory", () => {
      var key: string = "tags";

      expect.assertions(1);
      return expect(PN[key]()).rejects.toEqual(
        getCacheNotFoundError(key, cachePath)
      );
    });
  });
});
