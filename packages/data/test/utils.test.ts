import { checkAllFilesExist, dateToFredDate } from "../src/utils";

describe("utils", () => {
  describe("dateToFredDate", () => {
    it("can do: Fri, 02 Feb 1996 03:04:05 GMT", () => {
      var date = new Date(Date.UTC(96, 1, 2, 3, 4, 5)),
        exp = "199602020304";

      expect(dateToFredDate(date)).toEqual(exp);
    });

    it("can do: Sun, 31 Dec 1899 00:00:00 GMT", () => {
      var date = new Date(Date.UTC(0, 0, 0, 0, 0, 0)),
        exp = "189912310000";

      expect(dateToFredDate(date)).toEqual(exp);
    });

    it("can do: Mon, 21 Oct 1996 12:45:05 GMT", () => {
      var date = new Date(Date.UTC(96, 9, 21, 12, 45, 5)),
        exp = "199610211245";

      expect(dateToFredDate(date)).toEqual(exp);
    });
  });
});
