import * as FS from "fs";
import * as Path from "path";

import GtmmData from "../src/index";
import * as C from "../src/constants";

jest.setTimeout(10000000);

const catchPromise = (err: Error, done): void => {
  console.error(err);
  expect(true).toBeFalsy();
  done();
};

describe("GtmmData: Main object", () => {
  var GD;
  beforeAll(() => {
    GD = new GtmmData();
  });

  it("can fetch metadata", done => {
    expect.assertions(1);
    return GD.fetchMetadata()
      .then(out => {
        expect(out).toBeTruthy();
        done();
      })
      .catch(err => catchPromise(err, done));
  });
});
