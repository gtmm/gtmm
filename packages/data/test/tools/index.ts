import * as FS from "fs";

import { Filenames } from "../../src/utils";

export const getCacheNotFoundError = (key: string, cachePath: string): Error =>
  new Error(
    `The cache file: ${Filenames.cache(
      key,
      cachePath
    )} does not exist. Ensure that the FredMetadata command has been called.`
  );

export const deleteFolderRecursive = path => {
  if (FS.existsSync(path)) {
    FS.readdirSync(path).forEach(function(file, index) {
      var curPath = path + "/" + file;
      if (FS.lstatSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath);
      } else {
        // delete file
        FS.unlinkSync(curPath);
      }
    });
    FS.rmdirSync(path);
  }
};
