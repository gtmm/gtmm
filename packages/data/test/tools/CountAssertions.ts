export default class CountAssertions {
  total: number;
  received: number;
  done: () => void;

  constructor(total: number, done: () => void) {
    this.total = total;
    this.done = done;

    this.received = 0;
  }

  tick() {
    this.received += 1;
    if (this.received >= this.total) {
      this.done();
    }
  }
}
