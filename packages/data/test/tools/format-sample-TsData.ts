import { data as _rawFreqData } from "../sampleData/sample-TsData-freq";
import { sampleAggregationData as _rawAggData } from "../sampleData/sample-TsData-agg";

const formatSampleFreqData = d => {
   var keys = [],
      md = {},
      data = [],
      out = {
         lin: {},
         chg: {},
         ch1: {},
         pch: {},
         pc1: {},
         pca: {},
         cch: {},
         cca: {},
         log: {},
      };

   d.map(di => {
      let s = di.series;
      if (keys.indexOf(s) < 0) {
         keys.push(s);
         Object.keys(out).map(k => {
            out[k][s] = [];
         });
         md[s] = { nObsPerYear: di.nObsPerYear, freq: di.freq };
      }

      var date = new Date(di.date + " 10:00:00 GMT");

      Object.keys(out).map(k => {
         if (di[k] != null) {
            out[k][s].push({ date: date, series: di.series, value: di[k] });
         }
      });
      data.push({ date: di.date, series: di.series, value: di.value });
   });

   return { ...out, ...{ data: data, md: md, series: keys } };
};

const aggCats = [["M|AVG", "M", "AVG"]];

const formatSampleAggData = d => {
   var keys = [],
      md = {},
      data = [],
      out = {
         M: { AVG: {}, SUM: {}, EOP: {} },
         Q: { AVG: {}, SUM: {}, EOP: {} },
         A: { AVG: {}, SUM: {}, EOP: {} },
         ANY: { AVG: {} },
      };

   var aggCats = Object.keys(out).reduce(
      (accum0, k0) =>
         accum0.concat(
            Object.keys(out[k0]).reduce((accum1, k1) => {
               accum1.push([k0, k1, [k0, k1].join("|")]);
               return accum1;
            }, [])
         ),
      []
   );

   d.map(di => {
      let s = di.series;
      if (keys.indexOf(s) < 0) {
         keys.push(s);
         aggCats.map(k => {
            out[k[0]][k[1]][s] = [];
         });
         md[s] = { nObsPerYear: di.nObsPerYear, freq: di.freq };
      }

      var date = new Date(di.date + " 10:00:00 GMT");

      aggCats.map(k => {
         if (di[k[2]] != null) {
            out[k[0]][k[1]][s].push({
               date: date,
               series: di.series,
               value: di[k[2]],
            });
         }
      });
      data.push({ date: di.date, series: di.series, value: di.value });
   });

   return { ...out, ...{ data: data, md: md, series: keys } };
};

export const sampleDataFreq = formatSampleFreqData(_rawFreqData);
export const sampleDataAgg = formatSampleAggData(_rawAggData);
