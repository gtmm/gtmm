import * as FS from "fs";
import * as Path from "path";

import * as lunr from "lunr";

import SearchMetadata from "../src/SearchMetadata";
import * as C from "../src/constants";
import { Filenames } from "../src/utils";

jest.setTimeout(1000000);

describe("SearchMetadata", () => {
  it("can load SearchMetadata", done => {
    var SM = new SearchMetadata(C.DEFAULT_DATA_PATH, false, "promise", null);

    expect(typeof SM._ln).toEqual("object");
    expect(typeof SM._db).toEqual("object");

    expect(Object.keys(SM._ln)).toEqual(
      expect.arrayContaining(C.FRED_DATA_TYPES)
    );

    expect(Object.keys(SM._db)).toEqual(
      expect.arrayContaining(C.FRED_DATA_TYPES)
    );

    done();
  });

  describe("promise mode", () => {
    describe("idOnly", () => {
      var SM: SearchMetadata;

      beforeAll(() => {
        SM = new SearchMetadata(C.DEFAULT_DATA_PATH, true, "promise", null);
      });

      it("can search series", () => {
        var out = SM.series("EMP");

        expect(out).toEqual(expect.arrayContaining([]));
      });
    });
  });

  describe("cursor mode", () => {
    null;
  });
});
