import * as FS from "fs";
import * as Path from "path";

import FredMetadata from "../src/FredMetadata";
import * as C from "../src/constants";
import { IStringTMap } from "../src/types";
import { SERIES_FREQ } from "../src/constants";

var testFredMetadata;

jest.setTimeout(10000000);

describe("FredMetadata: Get fred metadata from API", () => {
  var sTestFredMetadata;
  beforeAll(() => {
    testFredMetadata = new FredMetadata();
  });

  it("Has a valid API Key", () => {
    expect(typeof testFredMetadata.apiKey).toBe("string");
    expect(testFredMetadata.fred.apiKey.length).toBe(32);
  });

  describe("Appending to outputs", () => {
    describe("Releases", () => {
      var full = {
        id: 9,
        realtime_start: "2013-08-13",
        realtime_end: "2013-08-13",
        name: "Advance Monthly Sales for Retail and Food Services",
        press_release: true,
        link: "http://www.census.gov/retail/",
        notes: "asfas",
      };

      var partial = {
        id: 9,
        realtime_start: "2013-08-13",
        realtime_end: "2013-08-13",
        name: "Advance Monthly Sales for Retail and Food Services",
        press_release: true,
        link: "http://www.census.gov/retail/",
      };

      beforeEach(() => {
        testFredMetadata.releases = {};
      });

      it("can append full", () => {
        testFredMetadata._appendRelease(full);

        expect(Object.keys(testFredMetadata.releases).length).toBe(1);
        expect(Object.keys(testFredMetadata.releases)["0"]).toBe("9");
      });

      it("Can append partial", () => {
        testFredMetadata._appendRelease(partial);

        expect(Object.keys(testFredMetadata.releases).length).toBe(1);
        expect(Object.keys(testFredMetadata.releases)[0]).toBe("9");
        expect(testFredMetadata.releases["9"].notes).toBeUndefined();
      });

      it("Append multiple", () => {
        let partial2 = { ...partial, ...{ id: 13 } };

        testFredMetadata._appendRelease(partial);
        testFredMetadata._appendRelease(partial2);

        expect(Object.keys(testFredMetadata.releases).length).toBe(2);
        expect(Object.keys(testFredMetadata.releases)).toEqual(
          expect.arrayContaining(["9", "13"])
        );
      });

      afterAll(() => {
        testFredMetadata.releases = {};
      });
    });

    describe("Sources", () => {
      var full = {
        id: 13,
        realtime_start: "2013-08-14",
        realtime_end: "2013-08-14",
        name: "Institute for Supply Management",
        link: "http://www.ism.ws/",
        notes: "asfsa",
      };

      var partial = {
        id: 13,
        realtime_start: "2013-08-14",
        realtime_end: "2013-08-14",
        name: "Institute for Supply Management",
        link: "http://www.ism.ws/",
      };

      beforeEach(() => {
        testFredMetadata.sources = {};
      });

      it("can append full", () => {
        testFredMetadata._appendSource(full);

        expect(Object.keys(testFredMetadata.sources).length).toBe(1);
        expect(Object.keys(testFredMetadata.sources)[0]).toBe("13");
      });

      it("Can append partial", () => {
        testFredMetadata._appendSource(partial);

        expect(Object.keys(testFredMetadata.sources).length).toBe(1);
        expect(Object.keys(testFredMetadata.sources)[0]).toBe("13");
        expect(testFredMetadata.sources["13"].notes).toBeUndefined();
      });

      it("Append multiple", () => {
        let partial2 = { ...partial, ...{ id: 9 } };

        testFredMetadata._appendSource(partial);
        testFredMetadata._appendSource(partial2);

        expect(Object.keys(testFredMetadata.sources).length).toBe(2);
        expect(Object.keys(testFredMetadata.sources)).toEqual(
          expect.arrayContaining(["13", "9"])
        );
      });

      afterAll(() => {
        testFredMetadata.sources = {};
      });
    });

    describe("Series", () => {
      const full = {
        id: "GNPCA",
        realtime_start: "2013-08-14",
        realtime_end: "2013-08-14",
        title: "Real Gross National Product",
        observation_start: "1929-01-01",
        observation_end: "2012-01-01",
        frequency: "Daily",
        frequency_short: "D",
        units: "Billions of Chained 2009 Dollars",
        units_short: "Bil. of Chn. 2009 $",
        seasonal_adjustment: "Not Seasonally Adjusted",
        seasonal_adjustment_short: "NSA",
        last_updated: "2013-07-31 09:26:16-05",
        popularity: 39,
        notes: "BEA Account Code: A001RX1",
      };

      const partial = {
        id: "GNPCA",
        title: "Real Gross National Product",
        observation_start: "1929-01-01",
        observation_end: "2012-01-01",
        frequency: "Daily",
        frequency_short: "D",
        units: "Billions of Chained 2009 Dollars",
        units_short: "Bil. of Chn. 2009 $",
        seasonal_adjustment: "Not Seasonally Adjusted",
        seasonal_adjustment_short: "NSA",
        last_updated: "2013-07-31 09:26:16-05",
        popularity: 39,
      };

      beforeEach(() => {
        testFredMetadata.series = {};
      });

      it("Can append new, complete series", () => {
        testFredMetadata._appendSeries(full, null, null);

        expect(Object.keys(testFredMetadata.series).length).toBe(1);
        expect(Object.keys(testFredMetadata.series)[0]).toBe("GNPCA");

        let g = testFredMetadata.series.GNPCA.edges;
        expect(g.categories.length).toBe(0);
        expect(g.releases.length).toBe(0);
        expect(g.tags.length).toBe(0);
      });

      it("Can append new, partial series", () => {
        testFredMetadata._appendSeries(partial, null, null);

        expect(Object.keys(testFredMetadata.series).length).toBe(1);
        expect(Object.keys(testFredMetadata.series)[0]).toBe("GNPCA");

        let g = testFredMetadata.series.GNPCA.edges;
        expect(g.categories.length).toBe(0);
        expect(g.releases.length).toBe(0);
        expect(g.tags.length).toBe(0);

        expect(testFredMetadata.series.GNPCA.notes).toBeUndefined();
      });

      it("Can add category", () => {
        testFredMetadata._appendSeries(full, null, null);
        testFredMetadata._appendSeries({ id: full.id }, "categories", 7);

        let g = testFredMetadata.series.GNPCA.edges;

        expect(g.categories.length).toBe(1);
        expect(g.categories[0]).toBe("7");

        expect(g.releases.length).toBe(0);
        expect(g.tags.length).toBe(0);
      });

      it("Can add tag", () => {
        testFredMetadata._appendSeries(full, null, null);
        testFredMetadata._appendSeries({ id: full.id }, "tags", "tag");

        let g = testFredMetadata.series.GNPCA.edges;

        expect(g.tags.length).toBe(1);
        expect(g.tags[0]).toBe("tag");

        expect(g.releases.length).toBe(0);
        expect(g.categories.length).toBe(0);
      });

      it("Can add releases", () => {
        testFredMetadata._appendSeries(full, null, null);
        testFredMetadata._appendSeries({ id: full.id }, "releases", 8);

        let g = testFredMetadata.series.GNPCA.edges;

        expect(g.releases.length).toBe(1);
        expect(g.releases[0]).toBe("8");

        expect(g.tags.length).toBe(0);
        expect(g.categories.length).toBe(0);
      });

      afterAll(() => {
        testFredMetadata.series = {};
      });
    });

    describe("Tags", () => {
      var full = {
        name: "oecd",
        group_id: "src",
        notes: "Organisation for Economic Co-operation and Development",
        created: "2012-02-27 10:18:19-06",
        popularity: 76,
        series_count: 61488,
      };

      var partial = {
        name: "oecd",
        group_id: "src",
        created: "2012-02-27 10:18:19-06",
        popularity: 76,
        series_count: 61488,
      };

      beforeEach(() => {
        testFredMetadata.tags = {};
      });

      it("can append full", () => {
        testFredMetadata._appendTag(full);

        expect(Object.keys(testFredMetadata.tags).length).toBe(1);
        expect(Object.keys(testFredMetadata.tags)[0]).toBe("oecd");
      });

      it("Can append partial", () => {
        testFredMetadata._appendTag(partial);

        expect(Object.keys(testFredMetadata.tags).length).toBe(1);
        expect(Object.keys(testFredMetadata.tags)[0]).toBe("oecd");
        expect(testFredMetadata.tags["oecd"].notes).toBeUndefined();
      });

      it("Append multiple", () => {
        let partial2 = { ...partial, ...{ name: "test" } };

        testFredMetadata._appendTag(partial);
        testFredMetadata._appendTag(partial2);

        expect(Object.keys(testFredMetadata.tags).length).toBe(2);
        expect(Object.keys(testFredMetadata.tags)).toEqual(
          expect.arrayContaining(["oecd", "test"])
        );
      });

      afterAll(() => {
        testFredMetadata.tags = {};
      });
    });
  });

  describe("Can fetch data", () => {
    it("Can fetch sources", () => {
      expect.assertions(4);

      return testFredMetadata
        .fetchSources()
        .then(res => {
          expect(typeof res).toBe("object");
          expect(res.sources.length).toBeGreaterThan(0);
          expect(Object.keys(testFredMetadata.sources).length).toBeGreaterThan(
            0
          );
          expect(testFredMetadata.fetched.sources).toBeTruthy();
        })
        .catch(err => {
          throw err;
        });
    });

    it("Can fetch releases", () => {
      expect.assertions(4);

      return testFredMetadata
        .fetchReleases()
        .then(res => {
          expect(typeof res).toBe("object");
          expect(res.releases.length).toBeGreaterThan(0);
          expect(Object.keys(testFredMetadata.releases).length).toBeGreaterThan(
            0
          );
          expect(testFredMetadata.fetched.releases).toBeTruthy();
        })
        .catch(err => {
          throw err;
        });
    });

    it("Can fetch categories", () => {
      expect.assertions(2);

      return testFredMetadata
        .fetchCategories()
        .then(res => {
          expect(
            Object.keys(testFredMetadata.categories).length
          ).toBeGreaterThan(0);

          expect(testFredMetadata.fetched.categories).toBeTruthy();
        })
        .catch(err => {
          throw err;
        });
    });

    it("Can fetch tags", () => {
      expect.assertions(3);

      return testFredMetadata
        .fetchTags()
        .then(res => {
          expect(typeof res).toBe("object");
          expect(res.tags.length).toBeGreaterThan(0);
          expect(Object.keys(testFredMetadata.tags).length).toBeGreaterThan(0);
        })
        .catch(err => {
          throw err;
        });
    });
    /*
var FredMetadata = require("./build/src/FredMetadata.js").default;
var s = new FredMetadata();

var r = s.releases;
var c = s.categories;
Object.keys(r).map(k => {
  r[k].edges.sources = [];
  r[k].edges.series = [];
  r[k].edges.tags = [];
});

Object.keys(c).map(k => {
  c[k].edges.related = [];
  c[k].edges.series = [];
  c[k].edges.tags = [];
});

s.releases = r;
s.categories = c;
var out;
s.fetchSeries().then(res => { res = out; console.log("done"); }); 
*/
    describe("Separate series environment", () => {
      var sTestFredMetadata;
      beforeAll(() => {
        sTestFredMetadata = new FredMetadata();

        var r = sTestFredMetadata.releases;
        var c = sTestFredMetadata.categories;
        Object.keys(r).map(k => {
          r[k].edges.sources = [];
          r[k].edges.series = [];
          r[k].edges.tags = [];
        });

        Object.keys(c).map(k => {
          c[k].edges.related = [];
          c[k].edges.series = [];
          c[k].edges.tags = [];
        });

        sTestFredMetadata.releases = r;
        sTestFredMetadata.categories = c;
      });

      it("can fetch series", () => {
        // expect.assertions(12);
        return sTestFredMetadata
          .fetchSeries()
          .then(res => {
            // Has series as a property
            expect(
              Object.keys(sTestFredMetadata.series).length
            ).toBeGreaterThan(0);
            // Recognized as being fetched
            expect(sTestFredMetadata.fetched.series).toBeTruthy();

            // Release 51 has a correct element
            // Get example element
            var ts = sTestFredMetadata.series["BOMTVLM133S"];
            // Release should have index of `ts`: Will find the index.
            expect(
              sTestFredMetadata.releases["51"].edges.series.indexOf(ts._id)
            ).toBeGreaterThan(-1);
            // `ts` should have release in its index.
            expect(ts.edges.releases.indexOf("51")).toBeGreaterThan(-1);

            // Category 125 has correct elements
            var ts = sTestFredMetadata.series["BOPBCA"];
            // Category should have index of `ts`
            expect(
              sTestFredMetadata.categories["125"].edges.series.indexOf(ts._id)
            ).toBeGreaterThan(-1);
            // `ts` should have category in index
            expect(ts.edges.categories.indexOf("125")).toBeGreaterThan(-1);
            // Should only have series with frequencies specified previously.
            expect(
              Object.keys(sTestFredMetadata.series).some(
                s =>
                  SERIES_FREQ.short.indexOf(
                    sTestFredMetadata.series[s].freqShort
                  ) < 0
              )
            ).toBeFalsy();

            // Should have each of the expected frequencies.
            for (let ii = 0; ii < SERIES_FREQ.short.length; ii++) {
              expect(
                Object.keys(sTestFredMetadata.series).some(
                  s =>
                    sTestFredMetadata.series[s].freqShort ===
                    SERIES_FREQ.short[ii]
                )
              ).toBeTruthy();
            }
          })
          .catch(err => {
            throw err;
          });
      });
    });
  });

  describe("Collecting edges", () => {
    var eTestFredMetadata;
    beforeAll(() => {
      eTestFredMetadata = new FredMetadata(
        C.DEFAULT_API_KEY,
        C.DEFAULT_CACHE_PATH,
        {} // { blockSize: 750, errTol: 2e6 }
      );
    });

    it("releases -> tags", () => {
      expect.assertions(4);
      return eTestFredMetadata
        .fetchEdgesReleaseTag()
        .then(res => {
          let r = expect.arrayContaining(["86"]);
          let t = expect.arrayContaining(["commercial paper", "frb", "usa"]);

          expect(eTestFredMetadata.releases["86"].edges.tags).toEqual(t);

          expect(
            eTestFredMetadata.tags["commercial paper"].edges.releases
          ).toEqual(r);
          expect(eTestFredMetadata.tags["frb"].edges.releases).toEqual(r);
          expect(eTestFredMetadata.tags["usa"].edges.releases).toEqual(r);
        })
        .catch(err => {
          throw err;
        });
    });

    it("categories -> tags", () => {
      expect.assertions(4);
      return eTestFredMetadata
        .fetchEdgesCategoryTag()
        .then(res => {
          let r = expect.arrayContaining(["125"]);
          let t = expect.arrayContaining(["usa", "bea", "nation"]);

          expect(eTestFredMetadata.categories["125"].edges.tags).toEqual(t);
          expect(eTestFredMetadata.tags["usa"].edges.categories).toEqual(r);
          expect(eTestFredMetadata.tags["bea"].edges.categories).toEqual(r);
          expect(eTestFredMetadata.tags["nation"].edges.categories).toEqual(r);
        })
        .catch(err => {
          throw new Error(
            "Throwing an error in REQUESTSS!! here!" + err.message
          );
        });
    });

    it("releases -> sources", () => {
      expect.assertions(3);
      return eTestFredMetadata.fetchEdgesReleaseSource().then(res => {
        let r = expect.arrayContaining(["51"]);
        let s0 = ["18", "19"];
        let s = expect.arrayContaining(s0);

        expect(eTestFredMetadata.releases["51"].edges.sources).toEqual(s);

        for (let ii in s0) {
          expect(eTestFredMetadata.sources[s0[ii]].edges.releases).toEqual(r);
        }
      });
    });

    it("categories -> related", () => {
      expect.assertions(1);
      return eTestFredMetadata
        .fetchEdgeCategoryRelated()
        .then(res => {
          let r = expect.arrayContaining([
            "149",
            "150",
            "151",
            "152",
            "153",
            "154",
            "193",
          ]);

          expect(eTestFredMetadata.categories["32073"].edges.related).toEqual(
            r
          );
        })
        .catch(err => {
          throw err;
        });
    });

    console.log(
      "\x1b[41m%s\x1b[0m",
      "NOTE: SKIPPING TESTS OF fetchEdgesTagSeries() and fetchEdgesTagsRelated() AS THEY TAKE > 12hrs. THEY ARE ALSO NOW DEPRECATED"
    );
    xit("tags -> related", () => {
      expect.assertions(2);
      return eTestFredMetadata.fetchEdgesTagsRelated().then(() => {
        var r0 = expect.arrayContaining([
          "nation",
          "nsa",
          "interest",
          "banks",
          "employer",
        ]);

        expect(
          eTestFredMetadata.tags.edges.related["monetary aggregates"]
        ).toEqual(r0);

        var r1 = expect.arrayContaining([
          "25 years +",
          "real",
          "banks",
          "price",
        ]);

        expect(eTestFredMetadata.tags.edges.related["weekly"]).toEqual(r1);
      });
    });

    xit("series <-> tags", done => {
      expect.assertions(8);
      return eTestFredMetadata
        .fetchEdgesTagSeries()
        .then(res => {
          expect(eTestFredMetadata.series["STLFSI"].edges.tags).toEqual(
            expect.arrayContaining(["usa", "nsa", "nation"])
          );
          expect(eTestFredMetadata.tags["slovenia"].edges.series).toEqual(
            expect.arrayContaining([
              "00XALCSIM086NEST",
              "00XAP0SIM086NEST",
              "00XAPFSIM086NEST",
            ])
          );

          // Should only have series with frequencies specified previously.
          expect(
            eTestFredMetadata.series.some(
              s => SERIES_FREQ.short.indexOf(s.freqShort) < 0
            )
          ).toBeFalsy();

          // Should have each of the expected frequencies.
          for (let ii = 0; ii < SERIES_FREQ.short.length; ii++) {
            expect(
              eTestFredMetadata.series.some(
                s => s.freqShort === SERIES_FREQ.short[ii]
              )
            ).toBeTruthy();
          }

          done();
        })
        .catch(err => {
          throw err;
        });
    });
  });

  describe("Fetch all metadata", () => {
    var FMD,
      cacheDir = Path.join(C.DEFAULT_CACHE_PATH, "fetch-all");
    beforeAll(() => {
      if (FS.existsSync(cacheDir)) {
        FS.rmdirSync(cacheDir);
      }

      FS.mkdirSync(cacheDir, { recursive: true });
      FMD = new FredMetadata(C.DEFAULT_API_KEY, cacheDir, {});
    });

    afterAll(() => {
      FS.rmdirSync(cacheDir);
    });

    it("can fetch all", done => {
      expect.assertions(1);
      FMD.all().then(out => {
        expect(out).toBeTruthy();
        done();
      });
    });
  });
});
