import * as FS from "fs";
import * as Path from "path";

import * as lunr from "lunr";

import FredMetadata from "../src/FredMetadata";
import PackLunr from "../src/PackLunr";
import * as C from "../src/constants";
import { Filenames } from "../src/utils";

import { getCacheNotFoundError, deleteFolderRecursive } from "./tools";

jest.setTimeout(1000000);

describe("PackLunr: Make metadata searchable.", () => {
  describe("should work", () => {
    describe("from file", () => {
      var PL: PackLunr;
      beforeAll(() => {
        PL = new PackLunr(C.DEFAULT_DATA_PATH, C.DEFAULT_CACHE_PATH);
      });

      it("pack sources", () => {
        expect.assertions(2);

        return PL.sources().then(ii => {
          var idx, searchResults;
          var file: string = Filenames.lunr("sources", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("bureau");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack releases", () => {
        expect.assertions(2);

        return PL.releases().then(() => {
          var idx, searchResults;
          var file: string = Filenames.lunr("releases", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("Equifax");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack categories", () => {
        expect.assertions(2);

        return PL.categories().then(() => {
          var idx, searchResults;
          var file: string = Filenames.lunr("categories", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("Miami");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack tags", () => {
        expect.assertions(2);

        return PL.tags().then(() => {
          var idx, searchResults;
          var file: string = Filenames.lunr("tags", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("coffee");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack series", () => {
        expect.assertions(2);

        return PL.series().then(() => {
          var idx, searchResults;
          var file: string = Filenames.lunr("series", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("unemployment");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });
    });

    describe("from pre-existing FredMetadata", () => {
      var FMD: FredMetadata,
        PL: PackLunr,
        dataPath: string = Path.join(C.DEFAULT_DATA_PATH, "pre-existing");
      beforeAll(() => {
        deleteFolderRecursive(dataPath);
        FS.mkdirSync(Path.join(dataPath, "lunr"), { recursive: true });

        PL = new PackLunr(dataPath, C.DEFAULT_CACHE_PATH);
        FMD = new FredMetadata(C.DEFAULT_API_KEY, C.DEFAULT_CACHE_PATH, {});
        return FMD.all(false);
      });

      afterAll(() => {
        deleteFolderRecursive(dataPath);
      });

      it("pack sources", () => {
        expect.assertions(2);

        return PL.sources(FMD.sources).then(ii => {
          var idx, searchResults;
          var file: string = Filenames.lunr("sources", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("bureau");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack releases", () => {
        expect.assertions(2);

        return PL.releases(FMD.releases).then(() => {
          var idx, searchResults;
          var file: string = Filenames.lunr("releases", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("Equifax");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack categories", () => {
        expect.assertions(2);

        return PL.categories(FMD.categories).then(() => {
          var idx, searchResults;
          var file: string = Filenames.lunr("categories", PL.basePath);
          var srcs = JSON.parse(FS.readFileSync(file).toString());

          idx = lunr.Index.load(srcs);
          searchResults = idx.search("Miami");

          expect(FS.existsSync(file)).toBeTruthy();
          expect(searchResults.length).toBeGreaterThan(0);
        });
      });

      it("pack tags", () => {
        expect.assertions(1);

        return PL.tags(FMD.tags).then(() => {
          expect(
            FS.existsSync(Filenames.lunr("tags", PL.basePath))
          ).toBeTruthy();
        });
      });

      it("pack series", () => {
        expect.assertions(1);
        return PL.series(FMD.series).then(() => {
          expect(
            FS.existsSync(Filenames.lunr("series", PL.basePath))
          ).toBeTruthy();
        });
      });
    });
  });

  describe("pack all", () => {
    describe("from file", () => {
      var PL: PackLunr;
      beforeAll(() => {
        PL = new PackLunr(C.DEFAULT_DATA_PATH, C.DEFAULT_CACHE_PATH);
      });

      it("pack all", () => {
        expect.assertions(1);

        return PL.all().then(out => {
          expect(out.length).toEqual(5);
        });
      });
    });

    describe("from pre-existing data sources", () => {
      var FMD: FredMetadata,
        PL: PackLunr,
        dataPath: string = Path.join(C.DEFAULT_DATA_PATH, "pre-existing");
      beforeAll(() => {
        deleteFolderRecursive(dataPath);
        FS.mkdirSync(Path.join(dataPath, "lunr"), { recursive: true });

        PL = new PackLunr(dataPath, C.DEFAULT_CACHE_PATH);
        FMD = new FredMetadata(C.DEFAULT_API_KEY, C.DEFAULT_CACHE_PATH, {});
        return FMD.all(false);
      });

      afterAll(() => {
        deleteFolderRecursive(dataPath);
      });

      console.log(
        "\x1b[41m%s\x1b[0m",
        "NOTE: SKIPPING TESTS OF PACK ALL WITH pre-existing DATA SOURCE AS THIS LEADS TO MEMORY ERROR WHEN RUN WITH OTHER TESTS. CAN BE RUN SEPARATELY TO VERIFY CORRECT."
      );
      xit("pack all", () => {
        expect.assertions(1);

        return PL.all(FMD).then(out => {
          expect(out).toBeTruthy();
        });
      });
    });
  });

  describe("should fail", () => {
    var PL: PackLunr,
      cachePath = Path.parse(C.DEFAULT_CACHE_PATH).dir;

    beforeAll(() => {
      PL = new PackLunr(C.DEFAULT_DATA_PATH, cachePath);
    });

    it("tags/_fetchInMemory", () => {
      var key: string = "tags";

      expect.assertions(1);
      return expect(PL[key]()).rejects.toEqual(
        getCacheNotFoundError(key, cachePath)
      );
    });

    it("series/_fetchOutMemory", () => {
      var key: string = "series";

      expect.assertions(1);
      return expect(PL[key]()).rejects.toEqual(
        getCacheNotFoundError(key, cachePath)
      );
    });
  });
});
