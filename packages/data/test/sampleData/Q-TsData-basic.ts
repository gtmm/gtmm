export const series = "ENUC426640110SA";

export const data = [
  { date: "1-1-1990", series: "ENUC426640110SA", value: 557.4 },
  { date: "4-1-1990", series: "ENUC426640110SA", value: 571.6 },
  { date: "7-1-1990", series: "ENUC426640110SA", value: 584.7 },
  { date: "10-1-1990", series: "ENUC426640110SA", value: 591.8 },
  { date: "1-1-1991", series: "ENUC426640110SA", value: 598.2 },
  { date: "4-1-1991", series: "ENUC426640110SA", value: 613.4 },
  { date: "7-1-1991", series: "ENUC426640110SA", value: 629.5 },
  { date: "10-1-1991", series: "ENUC426640110SA", value: 620 },
  { date: "1-1-1992", series: "ENUC426640110SA", value: 618.9 },
  { date: "4-1-1992", series: "ENUC426640110SA", value: 630.9 },
  { date: "7-1-1992", series: "ENUC426640110SA", value: 623.9 },
  { date: "10-1-1992", series: "ENUC426640110SA", value: 666.8 },
  { date: "1-1-1993", series: "ENUC426640110SA", value: 701.3 },
];

export const ch1 = [
  { date: new Date("1-1-1991"), series: "ENUC426640110SA", value: 40.8 },
  { date: new Date("4-1-1991"), series: "ENUC426640110SA", value: 41.8 },
  { date: new Date("7-1-1991"), series: "ENUC426640110SA", value: 44.8 },
  { date: new Date("10-1-1991"), series: "ENUC426640110SA", value: 28.2 },
  { date: new Date("1-1-1992"), series: "ENUC426640110SA", value: 20.7 },
  { date: new Date("4-1-1992"), series: "ENUC426640110SA", value: 17.5 },
  { date: new Date("7-1-1992"), series: "ENUC426640110SA", value: -5.6 },
  { date: new Date("10-1-1992"), series: "ENUC426640110SA", value: 46.8 },
  { date: new Date("1-1-1993"), series: "ENUC426640110SA", value: 82.4 },
];

export const lin = [
  { date: new Date("1-1-1990"), series: "ENUC426640110SA", value: 557.4 },
  { date: new Date("4-1-1990"), series: "ENUC426640110SA", value: 571.6 },
  { date: new Date("7-1-1990"), series: "ENUC426640110SA", value: 584.7 },
  { date: new Date("10-1-1990"), series: "ENUC426640110SA", value: 591.8 },
  { date: new Date("1-1-1991"), series: "ENUC426640110SA", value: 598.2 },
  { date: new Date("4-1-1991"), series: "ENUC426640110SA", value: 613.4 },
  { date: new Date("7-1-1991"), series: "ENUC426640110SA", value: 629.5 },
  { date: new Date("10-1-1991"), series: "ENUC426640110SA", value: 620.0 },
  { date: new Date("1-1-1992"), series: "ENUC426640110SA", value: 618.9 },
  { date: new Date("4-1-1992"), series: "ENUC426640110SA", value: 630.9 },
  { date: new Date("7-1-1992"), series: "ENUC426640110SA", value: 623.9 },
  { date: new Date("10-1-1992"), series: "ENUC426640110SA", value: 666.8 },
  { date: new Date("1-1-1993"), series: "ENUC426640110SA", value: 701.3 },
];

export const chg = [
  { date: new Date("4-1-1990"), series: "ENUC426640110SA", value: 14.2 },
  { date: new Date("7-1-1990"), series: "ENUC426640110SA", value: 13.1 },
  { date: new Date("10-1-1990"), series: "ENUC426640110SA", value: 7.1 },
  { date: new Date("1-1-1991"), series: "ENUC426640110SA", value: 6.4 },
  { date: new Date("4-1-1991"), series: "ENUC426640110SA", value: 15.2 },
  { date: new Date("7-1-1991"), series: "ENUC426640110SA", value: 16.1 },
  { date: new Date("10-1-1991"), series: "ENUC426640110SA", value: -9.5 },
  { date: new Date("1-1-1992"), series: "ENUC426640110SA", value: -1.1 },
  { date: new Date("4-1-1992"), series: "ENUC426640110SA", value: 12.0 },
  { date: new Date("7-1-1992"), series: "ENUC426640110SA", value: -7.0 },
  { date: new Date("10-1-1992"), series: "ENUC426640110SA", value: 42.9 },
  { date: new Date("1-1-1993"), series: "ENUC426640110SA", value: 34.5 },
];

export const pch = [
  {
    date: new Date("4-1-1990"),
    series: "ENUC426640110SA",
    value: 2.547542160029,
  },
  {
    date: new Date("7-1-1990"),
    series: "ENUC426640110SA",
    value: 2.291812456263,
  },
  {
    date: new Date("10-1-1990"),
    series: "ENUC426640110SA",
    value: 1.214297930563,
  },
  {
    date: new Date("1-1-1991"),
    series: "ENUC426640110SA",
    value: 1.081446434606,
  },
  {
    date: new Date("4-1-1991"),
    series: "ENUC426640110SA",
    value: 2.540956201939,
  },
  {
    date: new Date("7-1-1991"),
    series: "ENUC426640110SA",
    value: 2.624714704923,
  },
  {
    date: new Date("10-1-1991"),
    series: "ENUC426640110SA",
    value: -1.509134233519,
  },
  {
    date: new Date("1-1-1992"),
    series: "ENUC426640110SA",
    value: -0.177419354839,
  },
  {
    date: new Date("4-1-1992"),
    series: "ENUC426640110SA",
    value: 1.938923897237,
  },
  {
    date: new Date("7-1-1992"),
    series: "ENUC426640110SA",
    value: -1.109526073863,
  },
  {
    date: new Date("10-1-1992"),
    series: "ENUC426640110SA",
    value: 6.876101939413,
  },
  {
    date: new Date("1-1-1993"),
    series: "ENUC426640110SA",
    value: 5.173965206959,
  },
];

export const pc1 = [
  {
    date: new Date("1-1-1991"),
    series: "ENUC426640110SA",
    value: 7.319698600646,
  },
  {
    date: new Date("4-1-1991"),
    series: "ENUC426640110SA",
    value: 7.312806158153,
  },
  {
    date: new Date("7-1-1991"),
    series: "ENUC426640110SA",
    value: 7.662048913973,
  },
  {
    date: new Date("10-1-1991"),
    series: "ENUC426640110SA",
    value: 4.765123352484,
  },
  {
    date: new Date("1-1-1992"),
    series: "ENUC426640110SA",
    value: 3.46038114343,
  },
  {
    date: new Date("4-1-1992"),
    series: "ENUC426640110SA",
    value: 2.852950766221,
  },
  {
    date: new Date("7-1-1992"),
    series: "ENUC426640110SA",
    value: -0.8895949166,
  },
  {
    date: new Date("10-1-1992"),
    series: "ENUC426640110SA",
    value: 7.548387096774,
  },
  {
    date: new Date("1-1-1993"),
    series: "ENUC426640110SA",
    value: 13.313944094361,
  },
];

export const pca = [
  {
    date: new Date("4-1-1990"),
    series: "ENUC426640110SA",
    value: 10.586222413221,
  },
  {
    date: new Date("7-1-1990"),
    series: "ENUC426640110SA",
    value: 9.487236683157,
  },
  {
    date: new Date("10-1-1990"),
    series: "ENUC426640110SA",
    value: 4.946381266682,
  },
  {
    date: new Date("1-1-1991"),
    series: "ENUC426640110SA",
    value: 4.396464601731,
  },
  {
    date: new Date("4-1-1991"),
    series: "ENUC426640110SA",
    value: 10.557816230048,
  },
  {
    date: new Date("7-1-1991"),
    series: "ENUC426640110SA",
    value: 10.919486714175,
  },
  {
    date: new Date("10-1-1991"),
    series: "ENUC426640110SA",
    value: -5.901257391962,
  },
  {
    date: new Date("1-1-1992"),
    series: "ENUC426640110SA",
    value: -0.707790994612,
  },
  {
    date: new Date("4-1-1992"),
    series: "ENUC426640110SA",
    value: 7.984190971259,
  },
  {
    date: new Date("7-1-1992"),
    series: "ENUC426640110SA",
    value: -4.364786245446,
  },
  {
    date: new Date("10-1-1992"),
    series: "ENUC426640110SA",
    value: 30.473532881877,
  },
  {
    date: new Date("1-1-1993"),
    series: "ENUC426640110SA",
    value: 22.358175059518,
  },
];

export const cch = [
  {
    date: new Date("4-1-1990"),
    series: "ENUC426640110SA",
    value: 2.515633100786,
  },
  {
    date: new Date("7-1-1990"),
    series: "ENUC426640110SA",
    value: 2.265944912591,
  },
  {
    date: new Date("10-1-1990"),
    series: "ENUC426640110SA",
    value: 1.206984478448,
  },
  {
    date: new Date("1-1-1991"),
    series: "ENUC426640110SA",
    value: 1.075640622974,
  },
  {
    date: new Date("4-1-1991"),
    series: "ENUC426640110SA",
    value: 2.50921054843,
  },
  {
    date: new Date("7-1-1991"),
    series: "ENUC426640110SA",
    value: 2.590860180425,
  },
  {
    date: new Date("10-1-1991"),
    series: "ENUC426640110SA",
    value: -1.520637544526,
  },
  {
    date: new Date("1-1-1992"),
    series: "ENUC426640110SA",
    value: -0.177576929382,
  },
  {
    date: new Date("4-1-1992"),
    series: "ENUC426640110SA",
    value: 1.920366263145,
  },
  {
    date: new Date("7-1-1992"),
    series: "ENUC426640110SA",
    value: -1.115727226002,
  },
  {
    date: new Date("10-1-1992"),
    series: "ENUC426640110SA",
    value: 6.65000517599,
  },
  {
    date: new Date("1-1-1993"),
    series: "ENUC426640110SA",
    value: 5.044560466623,
  },
];

export const cca = [
  {
    date: new Date("4-1-1990"),
    series: "ENUC426640110SA",
    value: 10.062532403146,
  },
  {
    date: new Date("7-1-1990"),
    series: "ENUC426640110SA",
    value: 9.063779650365,
  },
  {
    date: new Date("10-1-1990"),
    series: "ENUC426640110SA",
    value: 4.827937913794,
  },
  {
    date: new Date("1-1-1991"),
    series: "ENUC426640110SA",
    value: 4.302562491895,
  },
  {
    date: new Date("4-1-1991"),
    series: "ENUC426640110SA",
    value: 10.036842193721,
  },
  {
    date: new Date("7-1-1991"),
    series: "ENUC426640110SA",
    value: 10.363440721701,
  },
  {
    date: new Date("10-1-1991"),
    series: "ENUC426640110SA",
    value: -6.082550178106,
  },
  {
    date: new Date("1-1-1992"),
    series: "ENUC426640110SA",
    value: -0.710307717529,
  },
  {
    date: new Date("4-1-1992"),
    series: "ENUC426640110SA",
    value: 7.681465052579,
  },
  {
    date: new Date("7-1-1992"),
    series: "ENUC426640110SA",
    value: -4.462908904009,
  },
  {
    date: new Date("10-1-1992"),
    series: "ENUC426640110SA",
    value: 26.600020703959,
  },
  {
    date: new Date("1-1-1993"),
    series: "ENUC426640110SA",
    value: 20.17824186649,
  },
];

export const log = [
  {
    date: new Date("1/1/1990"),
    series: "ENUC426640110SA",
    value: 6.323283115048,
  },
  {
    date: new Date("4/1/1990"),
    series: "ENUC426640110SA",
    value: 6.348439446056,
  },
  {
    date: new Date("7/1/1990"),
    series: "ENUC426640110SA",
    value: 6.371098895182,
  },
  {
    date: new Date("10/1/1990"),
    series: "ENUC426640110SA",
    value: 6.383168739966,
  },
  {
    date: new Date("1/1/1991"),
    series: "ENUC426640110SA",
    value: 6.393925146196,
  },
  {
    date: new Date("4/1/1991"),
    series: "ENUC426640110SA",
    value: 6.41901725168,
  },
  {
    date: new Date("7/1/1991"),
    series: "ENUC426640110SA",
    value: 6.444925853484,
  },
  {
    date: new Date("10/1/1991"),
    series: "ENUC426640110SA",
    value: 6.429719478039,
  },
  {
    date: new Date("1/1/1992"),
    series: "ENUC426640110SA",
    value: 6.427943708745,
  },
  {
    date: new Date("4/1/1992"),
    series: "ENUC426640110SA",
    value: 6.447147371377,
  },
  {
    date: new Date("7/1/1992"),
    series: "ENUC426640110SA",
    value: 6.435990099117,
  },
  {
    date: new Date("10/1/1992"),
    series: "ENUC426640110SA",
    value: 6.502490150877,
  },
  {
    date: new Date("1/1/1993"),
    series: "ENUC426640110SA",
    value: 6.552935755543,
  },
];
