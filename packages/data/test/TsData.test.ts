import * as FS from "fs";
import * as Path from "path";

import { TsData, IDataShape } from "../src/TsData";
import * as B from "./sampleData/Q-TsData-basic";
import { sampleDataWide } from "./sampleData/sample-TsData-wide";
import { sampleDataFreq, sampleDataAgg } from "./tools/format-sample-TsData";

const aggregationCategories = [
  ["M", "AVG"],
  ["M", "SUM"],
  ["M", "EOP"],
  ["Q", "AVG"],
  ["Q", "SUM"],
  ["Q", "EOP"],
  ["A", "AVG"],
  ["A", "SUM"],
  ["A", "EOP"],
  ["m", "avg"],
  ["q", "sum"],
  ["a", "eop"],
  ["any", "AVG"],
  ["ANY", "avg"],
];

describe("TsData: Basic (quarterly) data", () => {
  it("can initialize", () => {
    var TS = new TsData(B.data, "long", "any", "avg", true);
  });

  describe("Assume initialized", () => {
    var TS;

    beforeAll(() => {
      TS = new TsData(B.data, "long", "any", "avg", true);
    });

    it("can compile df", () => {
      expect(Object.keys(TS.df)).toContain(B.series);
    });

    it("has correct cols", () => {
      expect(TS.cols).toContain(B.series);
    });

    it("will throw error on bad column", () => {
      expect(() => TS.pca(["sfa"])).toThrowError(/Column name/);
    });

    test.each`
      tsMethod                                      | bKey
      ${"lin"}                                      | ${"lin"}
      ${"levels"}                                   | ${"lin"}
      ${"log"}                                      | ${"log"}
      ${"chg"}                                      | ${"chg"}
      ${"change"}                                   | ${"chg"}
      ${"pch"}                                      | ${"pch"}
      ${"percentChange"}                            | ${"pch"}
      ${"cch"}                                      | ${"cch"}
      ${"continuouslyCompoundedRateOfChange"}       | ${"cch"}
      ${"cca"}                                      | ${"cca"}
      ${"continuouslyCompoundedAnnualRateOfChange"} | ${"cca"}
      ${"pca"}                                      | ${"pca"}
      ${"compoundedAnnualRateOfChange"}             | ${"pca"}
      ${"changeFromYearAgo"}                        | ${"ch1"}
      ${"ch1"}                                      | ${"ch1"}
      ${"percentChangeFromYearAgo"}                 | ${"pc1"}
      ${"pc1"}                                      | ${"pc1"}
    `("can transform $tsMethod ($bKey)", ({ tsMethod, bKey }) => {
      var arg;
      if (bKey === "lin") {
        arg = [B.series];
      } else if (bKey === "pca") {
        arg = B.series;
      }
      var out = TS[tsMethod](arg),
        exp = B[bKey];

      // console.log(bKey, out);

      var v = out[B.series];
      expect(Object.keys(TS.dfw)).toContain(B.series);
      expect(Object.keys(TS.v)).toContain(B.series);

      try {
        for (let ii = 0; ii < v.length; ii++) {
          var diff: number = (v[ii].value - exp[ii].value) / exp[ii].value;
          expect(Math.abs(diff)).toBeLessThan(1e-6);
        }
      } catch (e) {
        console.log(bKey, out);
        throw e;
      }
    });
  });
});

describe.each([true, false])("TsData: All", strict => {
  var TS, ms;

  beforeAll(() => {
    ms = expect.arrayContaining(sampleDataFreq.series);
    TS = new TsData(sampleDataFreq.data, "long", "any", "AVG", strict);
  });

  it("can compile df", () => {
    expect(Object.keys(TS.df)).toEqual(ms);
  });

  it("has correct cols", () => {
    expect(TS.cols).toEqual(ms);
  });

  it("can reset", () => {
    var v = JSON.stringify(TS.v);
    TS.pc1(TS.cols.slice(0, 4));
    var dfw = JSON.stringify(TS.dfw);

    TS.reset();
    expect(JSON.stringify(TS.dfw)).not.toEqual(dfw);
    expect(JSON.stringify(TS.v)).toEqual(v);
  });

  it("can get values (I)", () => {
    var v = TS.v;
    expect(v).not.toBeNull();
  });
  it("can get values (II)", () => {
    var v = TS.values;
    expect(v).not.toBeNull();
  });

  it("can get raw", () => {
    var v = TS.raw;
    expect(v).not.toBeNull();
  });

  it("can set metadata correctly", () => {
    Object.keys(TS.md).map(k => {
      expect(TS.md[k].nObsPerYear).toEqual(sampleDataFreq.md[k].nObsPerYear);
      expect(TS.md[k].freq).toEqual(sampleDataFreq.md[k].freq);
    });
  });

  test.each`
    tsMethod                                      | bKey
    ${"levels"}                                   | ${"lin"}
    ${"log"}                                      | ${"log"}
    ${"change"}                                   | ${"chg"}
    ${"percentChange"}                            | ${"pch"}
    ${"continuouslyCompoundedRateOfChange"}       | ${"cch"}
    ${"continuouslyCompoundedAnnualRateOfChange"} | ${"cca"}
    ${"compoundedAnnualRateOfChange"}             | ${"pca"}
    ${"changeFromYearAgo"}                        | ${"ch1"}
    ${"percentChangeFromYearAgo"}                 | ${"pc1"}
  `("can transform $tsMethod ($bKey)", ({ tsMethod, bKey }) => {
    var out = TS[tsMethod](sampleDataFreq.series),
      exp = sampleDataFreq[bKey];

    expect(Object.keys(TS.dfw)).toEqual(ms);
    expect(Object.keys(out)).toEqual(ms);

    for (let kk = 0; kk < sampleDataFreq.series.length; kk++) {
      var lbl = sampleDataFreq.series[kk];
      var v = out[lbl],
        ee = exp[lbl],
        tol = bKey === "pca" ? 1e-4 : 1e-6;
      try {
        expect(v.length).toEqual(ee.length);
        for (let ii = 0; ii < v.length; ii++) {
          expect(v[ii].date).toEqual(ee[ii].date);
          var diff: number =
            ee[ii].value !== 0
              ? (v[ii].value - ee[ii].value) / ee[ii].value
              : v[ii].value - ee[ii].value;
          expect(Math.abs(diff)).toBeLessThan(tol);
        }
      } catch (e) {
        console.log(bKey, out[lbl], exp[lbl]);
        throw e;
      }
    }
  });
});

describe("TsData: aggregation of series", () => {
  describe("can compile non-standard aggregation on init", () => {
    test.each(aggregationCategories)("(%s, %s)", (agg, aggMethod) => {
      var TS = new TsData(sampleDataAgg.data, "long", agg, aggMethod, false);

      var rec = TS.v,
        exp = sampleDataAgg[agg.toUpperCase()][aggMethod.toUpperCase()];

      for (let kk = 0; kk < sampleDataAgg.series.length; kk++) {
        var lbl = sampleDataAgg.series[kk];
        var v = rec[lbl],
          ee = exp[lbl],
          tol = 1e-6;
        try {
          expect(v.length).toEqual(ee.length);
          for (let ii = 0; ii < v.length; ii++) {
            expect(v[ii].date).toEqual(ee[ii].date);
            var diff: number =
              ee[ii].value !== 0
                ? (v[ii].value - ee[ii].value) / ee[ii].value
                : v[ii].value - ee[ii].value;
            expect(Math.abs(diff)).toBeLessThan(tol);
          }
        } catch (e) {
          console.log("\x1b[31m%s\x1b[0m", `(${agg}, ${aggMethod})`);
          console.log("\x1b[31m%s\x1b[0m", "RECEIVED: \n", rec[lbl]);
          console.log("\x1b[32m%s\x1b[0m", "EXPECTED: \n", exp[lbl]);

          throw e;
        }
      }
    });
  });

  describe("can change aggregation method", () => {
    test.each(aggregationCategories)("(%s, %s)", (agg, aggMethod) => {
      // Init in one configuration
      var TS = new TsData(
        sampleDataAgg.data,
        "long",
        agg,
        aggMethod.toLowerCase() === "avg" ? "sum" : "avg",
        false
      );

      // Now change to a different aggregation method
      TS.aggMethod = aggMethod;

      var rec = TS.v,
        exp = sampleDataAgg[agg.toUpperCase()][aggMethod.toUpperCase()];

      expect(Object.keys(rec)).toEqual(
        expect.arrayContaining(Object.keys(exp))
      );

      for (let kk = 0; kk < sampleDataAgg.series.length; kk++) {
        var lbl = sampleDataAgg.series[kk];
        var v = rec[lbl],
          ee = exp[lbl],
          tol = 1e-6;
        try {
          expect(v.length).toEqual(ee.length);
          for (let ii = 0; ii < v.length; ii++) {
            expect(v[ii].date).toEqual(ee[ii].date);
            var diff: number =
              ee[ii].value !== 0
                ? (v[ii].value - ee[ii].value) / ee[ii].value
                : v[ii].value - ee[ii].value;
            expect(Math.abs(diff)).toBeLessThan(tol);
          }
        } catch (e) {
          console.log("\x1b[31m%s\x1b[0m", `(${agg}, ${aggMethod})`);
          console.log("\x1b[31m%s\x1b[0m", "RECEIVED: \n", rec[lbl]);
          console.log("\x1b[32m%s\x1b[0m", "EXPECTED: \n", exp[lbl]);

          throw e;
        }
      }
    });
  });

  describe("can change aggregation", () => {
    test.each(aggregationCategories)("(%s, %s)", (agg, aggMethod) => {
      // Init in one configuration
      var TS = new TsData(
        sampleDataAgg.data,
        "long",
        agg.toLowerCase() === "any" ? "M" : "any",
        aggMethod,
        false
      );

      // Now change to a different aggregation method
      TS.agg = agg;

      var rec = TS.v,
        exp = sampleDataAgg[agg.toUpperCase()][aggMethod.toUpperCase()];

      expect(Object.keys(rec)).toEqual(
        expect.arrayContaining(Object.keys(exp))
      );

      for (let kk = 0; kk < sampleDataAgg.series.length; kk++) {
        var lbl = sampleDataAgg.series[kk];
        var v = rec[lbl],
          ee = exp[lbl],
          tol = 1e-6;
        // try {
        expect(v.length).toEqual(ee.length);
        for (let ii = 0; ii < v.length; ii++) {
          expect(v[ii].date).toEqual(ee[ii].date);
          var diff: number =
            ee[ii].value !== 0
              ? (v[ii].value - ee[ii].value) / ee[ii].value
              : v[ii].value - ee[ii].value;
          expect(Math.abs(diff)).toBeLessThan(tol);
        }
        // } catch (e) {
        //   console.log("\x1b[31m%s\x1b[0m", `(${agg}, ${aggMethod})`);
        //   console.log("\x1b[31m%s\x1b[0m", "RECEIVED: \n", rec[lbl]);
        //   console.log("\x1b[32m%s\x1b[0m", "EXPECTED: \n", exp[lbl]);
        //   throw e;
        // }
      }
    });
  });
});

describe("TsData: Wide format", () => {
  describe("can compile wide on init", () => {
    test.each(aggregationCategories)("(%s, %s)", (agg, aggMethod) => {
      var TS = new TsData(sampleDataWide.data, "wide", agg, aggMethod, false);

      var rec = TS.v,
        exp =
          sampleDataWide.exp[
            [agg.toUpperCase(), aggMethod.toUpperCase()].join("|")
          ];

      try {
        expect(rec.length).toEqual(exp.length);
      } catch (e) {
        console.log(agg, aggMethod);
        // @ts-ignore
        console.log(rec.slice(-10));
        console.log(exp.slice(-10));
        throw e;
      }

      for (let ii in exp) {
        // try {
        expect(Object.keys(rec[ii])).toEqual(
          expect.arrayContaining(Object.keys(exp[ii]))
        );

        for (let kk in exp[ii]) {
          if (kk === "date") {
            expect(new Date(exp[ii][kk] + " 10:00:00 GMT")).toEqual(
              rec[ii][kk]
            );
          } else {
            var diff: number =
              exp[ii][kk] !== 0
                ? (exp[ii][kk] - rec[ii][kk]) / exp[ii][kk]
                : exp[ii][kk] - rec[ii][kk];
            expect(Math.abs(diff)).toBeLessThan(1e-6);
          }
        }
        // } catch (e) {
        //   // console.log("\x1b[31m%s\x1b[0m", `(${agg}, ${aggMethod})`);
        //   // console.log("\x1b[31m%s\x1b[0m", "RECEIVED: \n", rec[ii]);
        //   // console.log(
        //   //   "\x1b[32m%s\x1b[0m",
        //   //   "EXPECTED: \n",
        //   //   exp[ii],
        //   //   new Date(exp[ii].date + " 10:00:00 GMT")
        //   // );
        //   throw e;
        // }
      }
    });
  });

  describe("can switch between long and wide", () => {
    it("long => wide", () => {
      var firstShape: IDataShape = "long",
        secondShape: IDataShape = "wide";
      var TS = new TsData(sampleDataWide.data, firstShape, "any", "AVG", false);

      TS.dataShape = secondShape;

      var rec = TS.v,
        exp = sampleDataWide.exp["ANY|AVG"];

      try {
        expect(rec.length).toEqual(exp.length);
      } catch (e) {
        // @ts-ignore
        console.log(rec.slice(0, 10));
        console.log(exp.slice(0, 10));
        throw e;
      }

      for (let ii in exp) {
        try {
          expect(Object.keys(rec[ii])).toEqual(
            expect.arrayContaining(Object.keys(exp[ii]))
          );

          for (let kk in exp[ii]) {
            if (kk === "date") {
              expect(new Date(exp[ii][kk] + " 10:00:00 GMT")).toEqual(
                rec[ii][kk]
              );
            } else {
              var diff: number =
                exp[ii][kk] !== 0
                  ? (exp[ii][kk] - rec[ii][kk]) / exp[ii][kk]
                  : exp[ii][kk] - rec[ii][kk];
              expect(Math.abs(diff)).toBeLessThan(1e-6);
            }
          }
        } catch (e) {
          console.log(
            "\x1b[31m%s\x1b[0m",
            "RECEIVED: \n",
            // @ts-ignore
            rec.slice(ii - 5, ii + 5),
            "\n",
            "\x1b[32m%s",
            "EXPECTED: \n",
            // @ts-ignore
            exp.slice(ii - 5, ii + 5)
          );
          throw e;
        }
      }
    });

    it("wide => long", () => {
      var firstShape: IDataShape = "wide",
        secondShape: IDataShape = "long";
      var TS = new TsData(sampleDataWide.data, firstShape, "any", "AVG", false);

      TS.dataShape = secondShape;

      var rec = TS.v,
        exp = sampleDataAgg["ANY"]["AVG"];

      expect(Object.keys(rec)).toEqual(
        expect.arrayContaining(Object.keys(exp))
      );

      for (let kk = 0; kk < sampleDataAgg.series.length; kk++) {
        var lbl = sampleDataAgg.series[kk];
        var v = rec[lbl],
          ee = exp[lbl],
          tol = 1e-6;

        try {
          expect(v.length).toEqual(ee.length);
          for (let ii = 0; ii < v.length; ii++) {
            var diff: number =
              ee[ii].value !== 0
                ? (v[ii].value - ee[ii].value) / ee[ii].value
                : v[ii].value - ee[ii].value;
            expect(Math.abs(diff)).toBeLessThan(tol);
          }
        } catch (e) {
          console.log(
            "\x1b[31m%s\x1b[0m",
            "RECEIVED: \n",
            rec[lbl].slice(0, 10)
          );
          console.log(
            "\x1b[32m%s\x1b[0m",
            "EXPECTED: \n",
            exp[lbl].slice(0, 10)
          );

          throw e;
        }
      }
    });
  });
});

/*
shortFreq, freq                                    , series
Q        , "Quarterly"                             , ENUC426640110SA
Q        , "Quarterly"                             , G7CPGRLE01IXOBQ
Q        , "Quarterly, 2nd Month's 1st Full Week"  , ERRPRXSLNQ
Q        , "Quarterly, 2nd Month's 1st Full Week"  , EQLLNQ
Q        , "Quarterly, End of Quarter"             , QSGCAM770A
Q        , "Quarterly, End of Quarter"             , QNLNAMUSDA
Q        , "Quarterly, Beginning of Period"        , Q04049USQ052NNBR
Q        , "Quarterly, End of Period"              , DALLLFROBEP
Q        , "Quarterly, End of Period"              , LLRNPT25

M        , "Monthly"                               , CP0530CZM086NEST
M        , "Monthly"                               , LTUCPHP0702IXOBM
M        , "Monthly, Middle of Month"              , M1480AUSM144SNBR
M        , "Monthly, Middle of Month"              , M0583BUSM436SNBR
M        , "Monthly, Saturday Nearest Month's End" , M0501DUSM391NNBR
M        , "Monthly, Saturday Nearest Month's End" , M0501BUSM391NNBR
M        , "Monthly, Middle and End of the Month"  , M05F0B46M601NNBR
M        , "Monthly, End of Month"                 , I4247IM144NCEN
M        , "Monthly, End of Month"                 , M09088USM156NNBR
M        , "Monthly, As of 15th of the Month"      , M08FAAUS000OHM555NNBR
M        , "Monthly, Beginning of Month"           , G172CRJA02
M        , "Monthly, Beginning of Month"           , G172CRFUN02
M        , "Monthly, End of Period"                , BUSINV
M        , "Monthly, End of Period"                , BORROWRCM

BW       , "Biweekly, Ending Wednesday"            , BASENS
BW       , "Biweekly, Ending Wednesday"            , BASE
BW       , "Biweekly, Ending Monday"               , DISMULT
BW       , "Biweekly, Beg. of Period"              , FLEXSC

W        , "Weekly, Ending Wednesday"              , TASFRIW027SBOG
W        , "Weekly, Ending Wednesday"              , OLRSCBW027NBOG
W        , "Weekly"                                , GASPRMCOVECW
W        , "Weekly"                                , GASDESLSGCW
W        , "Weekly, Ending Friday"                 , WTP5A11
W        , "Weekly, Ending Friday"                 , WGS5YR
W        , "Weekly, Ending Thursday"               , MORTMRGN5NE
W        , "Weekly, Ending Thursday"               , MORTGAGE30W
W        , "Weekly, As of Thursday"                , WSLB20
W        , "Weekly, As of Wednesday"               , D12WCPCA
W        , "Weekly, As of Wednesday"               , D5WLFOL
W        , "Weekly, Ending Monday"                 , WTCNS
W        , "Weekly, Ending Monday"                 , GASMIDMWW
W        , "Weekly, As of Monday"                  , CD1NRNJ
W        , "Weekly, As of Monday"                  , CD6RCJD
W        , "Weekly, Ending Saturday"               , WAINSUREDUR
W        , "Weekly, Ending Saturday"               , VAINSUREDUR

D        , "Daily, 7-Day"                          , NDLRECDM
D        , "Daily, 7-Day"                          , BRARECD

D        , "Daily"                                 , THREEFF3
D        , "Daily"                                 , NZD12MD156N
D        , "Daily, Close"                          , BAMLC0A0CMEY
D        , "Daily, Close"                          , BAMLEMXOCOLCRPIUSSYTW


 */
