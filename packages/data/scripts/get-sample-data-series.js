var GtmmData = require("../build/src/index").default;

const defaultSeed = parseInt(process.argv[2]) || 123;
var seed = defaultSeed;

/**
 * Just want a "RNG" that will generate replicable results
 * Know that this is no good for actual applications
 * https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
 * @return {[type]} [description]
 */
const random = () => {
  var x = Math.sin(seed++) * 10000;
  return x - Math.floor(x);
};

const drawRandomUniqueInteger = (pCount, pMin, pMax) => {
  var min = pMin < pMax ? pMin : pMax,
    max = pMax > pMin ? pMax : pMin;
  pCount = Math.min(max - min, pCount);

  var resultArr = [],
    randNumber;
  while (pCount > 0) {
    randNumber = Math.floor(min + random() * (max - min));
    if (resultArr.indexOf(randNumber) == -1) {
      resultArr.push(randNumber);
      pCount--;
    }
  }
  return resultArr;
};

const getAllFrequencies = GD_ =>
  new Promise((resolve, reject) => {
    var f = {},
      fs = {},
      fex = {};
    GD_._db.series.find({}, { id: 1, freq: 1, freqShort: 1 }, (err, docs) => {
      if (err) reject(err);

      var _0 = docs.map(d => {
        if (Object.keys(f).indexOf(d.freq) < 0) {
          f[d.freq] = 0;
          fex[d.freq] = [];
          fs[d.freq] = d.freqShort;
        }

        fex[d.freq].push(d.id);
        f[d.freq] += 1;
      });

      console.log(
        "\n" +
          `${"FREQ".padEnd(46)} ${"LENGTH".padEnd(10)} ${"EXAMPLES".padEnd(
            15
          )}` +
          "\n" +
          "-".repeat(85)
      );
      var k = Object.keys(f).sort();

      var _0 = k.map(fi => {
        console.log(
          `${fs[fi].padEnd(5)} ${fi.padEnd(40)} ${f[fi]
            .toString()
            .padEnd(10)} ${fex[fi].slice(0, 2)}`
        );
      });
      resolve({ freq: f, ex: fex, short: fs });
    });
  });

const drawRandomSeries = (f, verbose) => {
  seed = defaultSeed;
  console.log(`Setting seed to ${seed} for replication purposes`);
  var out = Object.keys(f.ex).reduce((accum, k) => {
    accum[k] = drawRandomUniqueInteger(2, 0, f.ex[k].length).map(
      s => f.ex[k][s]
    );
    return accum;
  }, {});

  if (verbose) {
    console.log(
      `| ${"SHORT".padEnd(5)} | ${"FREQUENCY".padEnd(40)} | ${"SERIES".padEnd(
        25
      )} |`
    );
    console.log(`| ${"-".repeat(5)} | ${"-".repeat(40)} | ${"-".repeat(25)} |`);
    var keys = Object.keys(out);
    keys.sort();
    var _0 = keys.map(k => {
      out[k].map(s =>
        console.log(
          `| ${f.short[k].padEnd(5)} | ${k.padEnd(40)} | ${s.padEnd(25)} |`
        )
      );
    });
  }

  return out;
};

const downloadSelectedSeries = (GD_, s) =>
  GD_.update(Object.keys(s).reduce((accum, cv) => accum.concat(s[cv]), []))
    .then(td => console.log(`Downloaded ${td.length} series successfully.`))
    .catch(console.error);

/**
 * MAIN (clean this up)
 */
(async () => {
  var GD = new GtmmData(),
    freqData,
    selectedSeries;

  console.log("starting...", defaultSeed);
  // (If not done already)
  // GD.fetchMetadata().then(() => console.log("ready"));
  var _0 = await GD.init();
  console.log("GD ready");
  var f_ = await getAllFrequencies(GD);
  var s_ = drawRandomSeries(f_, true);
  downloadSelectedSeries(GD, s_);
})();
