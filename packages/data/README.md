# Data v. Metadata

# Search

Searching for data should do two things:

1. Find series, release, category, or tag that matches the query.
1. Return relevant information about the series, release, category, or tag:

- _Series._ What is the full name, category, dates of availability, etc.?
- _Release._ What are the series related to it? What is the full name?
- and so on...

The first step can be accomplished with lunr. However, lunr doesn't return the record of the object that it searches. Instead, it returns the reference to the document that contained the search term. Moreover, it is not possible to

Thus, to do the second step there is a need to reference another source.

# FRED Data Organization

Interlinkages between the data are depicted in the figure:

```mermaid
graph LR
C[Categories]
R[Releases]
SRC[Sources]
S[Series]
T[Tags]

C --- S
SRC --- R
R --- S
T ---|Top 100 by Popularity Only| S
T --- C
T --- R

T -->|Related| T
C -->|Children| C
C --> |Parent| C
C -->|Related| C
```

# lunr.js

```js
const lunr = require("lunr");
const shortid = require("shortid");

var baseDocs = [
  {
    name: "Lunr",
    text: "Like Solr, but much smaller, and not as bright.",
  },
  {
    name: "React",
    text: "A JavaScript library for building user interfaces.",
  },
  {
    name: "Lodash",
    text:
      "A modern JavaScript utility library delivering modularity, performance & extras.",
  },
];

var idx = lunr(function() {
  this.ref("name");

  this.field("name"); // As field and ref to search it.
  this.field("text");

  documents.forEach(function(doc) {
    this.add(doc);
  }, this);
});

console.log(idx.search("bright"));
```

# `nedb`

# Project Organization

## Typescript

Trying to use typescript. Adopting google's [`gts`](https://github.com/google/ts-style) configuration.

Typescript references:

- [Book (Basarat)](https://basarat.gitbooks.io/typescript)
- [Enforcing the type of the indexed members of a Typescript object?](https://stackoverflow.com/questions/13315131/enforcing-the-type-of-the-indexed-members-of-a-typescript-object)
- [How to type function callback?](https://stackoverflow.com/questions/29689966/typescript-how-to-define-type-for-a-function-callback-as-any-function-type-no)
