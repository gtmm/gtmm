/**
 * Get a default API key. Useful for testing purposes.
 */

import * as FS from "fs";
import * as Path from "path";
import * as xdgBasedir from "xdg-basedir";

const PKG_NAME = "@gtmm-data";

const DEFAULT_API_KEY: string = process.env.FRED_KEY || "NOT FOUND";
const DEFAULT_DATA_PATH = Path.join(xdgBasedir.data, "gtmm_data");
const DEFAULT_CACHE_PATH = Path.join(
  process.env.temp || xdgBasedir.data,
  "gtmm_cache_data"
);
const DEFAULT_DB_NAME = "fred.data.db";

if (!FS.existsSync(DEFAULT_DATA_PATH)) {
  FS.mkdirSync(DEFAULT_DATA_PATH, { recursive: true });

  FS.mkdirSync(Path.join(DEFAULT_DATA_PATH, "nedb"), { recursive: true });
}

if (!FS.existsSync(Path.join(DEFAULT_DATA_PATH, "lunr"))) {
  FS.mkdirSync(Path.join(DEFAULT_DATA_PATH, "lunr"), { recursive: true });
}

if (!FS.existsSync(Path.join(DEFAULT_DATA_PATH, "nedb"))) {
  FS.mkdirSync(Path.join(DEFAULT_DATA_PATH, "nedb"), { recursive: true });
}

// Per
const SERIES_FREQ = {
  short: ["Q", "M", "BW", "W", "D"],
  starts: ["Quarterly", "Monthly", "Biweekly", "Weekly", "Daily"],
};

const FRED_DATA_TYPES = ["series", "sources", "tags", "releases", "categories"];

const DEFAULT_RATE_LIMIT_PROPS = {
  wait: 10 * 1000,
  blockSize: 1000,
  errTol: 200,
};

export {
  PKG_NAME,
  DEFAULT_API_KEY,
  DEFAULT_DATA_PATH,
  DEFAULT_CACHE_PATH,
  SERIES_FREQ,
  FRED_DATA_TYPES,
  DEFAULT_RATE_LIMIT_PROPS,
  DEFAULT_DB_NAME,
};
