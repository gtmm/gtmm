import * as Path from "path";
import * as FS from "fs";

import * as Promise from "bluebird";
import * as Datastore from "nedb";
import * as JSONStream from "JSONStream";

import FredMetadata from "./FredMetadata";
import { Filenames } from "./utils";

import {
  ICategories,
  ISources,
  IReleases,
  ITags,
  ISeriess,
  IRelease,
  ISource,
  ISeries,
  ITag,
  ICategory,
} from "./FredMetadata.types";

import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
} from "./types";

export default class PackNedb {
  basePath: string;
  cachePath: string;

  constructor(basePath: string, cachePath: string) {
    this.basePath = basePath;
    this.cachePath = cachePath;
  }

  series(obj: IStringTMap<any> | null = null): Promise<any> {
    return this._fetchInMemory("series", obj);
  }

  releases(obj: IStringTMap<any> | null = null): Promise<any> {
    return this._fetchInMemory("releases", obj);
  }

  categories(obj: IStringTMap<any> | null = null): Promise<any> {
    return this._fetchInMemory("categories", obj);
  }

  sources(obj: IStringTMap<any> | null = null): Promise<any> {
    return this._fetchInMemory("sources", obj);
  }

  tags(obj: IStringTMap<any> | null = null): Promise<any> {
    return this._fetchInMemory("tags", obj);
  }

  all(fmd: FredMetadata | null = null): Promise<any> {
    if (fmd) {
      if (!Object.keys(fmd.fetched).every(f => fmd.fetched[f])) {
        throw new Error(
          "Not all elements in FredMetadata object present. Fetch them before proceeding further."
        );
      } else {
        return Promise.all([
          this.series(fmd.series),
          this.releases(fmd.releases),
          this.sources(fmd.sources),
          this.tags(fmd.tags),
          this.categories(fmd.categories),
        ]);
      }
    } else {
      return Promise.all([
        this.series(),
        this.releases(),
        this.sources(),
        this.tags(),
        this.categories(),
      ]);
    }
  }

  _fetchInMemory(
    key: string,
    obj: IStringTMap<any> | null = null
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      // Make contingencies for if there is some sort of error in the write.
      var file: string = Filenames.nedb(key, this.basePath),
        dFile: string = Filenames.nedb(key + "-deprecated", this.basePath),
        cacheFile = Filenames.cache(key, this.cachePath);
      var preExisting: boolean = FS.existsSync(file);

      if (!FS.existsSync(cacheFile)) {
        var ei: Error = new Error(
          `The cache file: ${cacheFile} does not exist. Ensure that the FredMetadata command has been called.`
        );
        reject(ei);
      }

      if (preExisting) {
        FS.renameSync(file, dFile);
      }

      var src: IStringTMap<any> = obj
        ? obj
        : JSON.parse(FS.readFileSync(cacheFile).toString());

      try {
        // Get the database
        var db: Datastore = new Datastore({ filename: file, autoload: true });

        // Pack everything to a long array.
        var toPack: IStringTMap<any>[] = Object.keys(src).map(k => src[k]);

        // insert into db
        db.insert(toPack, err => {
          if (err) {
            throw err;
          }

          // Remove the old db
          if (preExisting) {
            FS.unlink(dFile, err => {
              if (err) {
                throw err;
              }
              resolve(true);
            });
          } else {
            resolve(true);
          }
        });
      } catch (e) {
        // If there is a failure then undo the renaming.
        if (preExisting) {
          FS.renameSync(dFile, file);
        }
        reject(e);
      }
    });
  }
}
