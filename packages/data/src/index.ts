import * as Promise from "bluebird";

import * as C from "./constants";
import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
} from "./types";
import { checkAllFilesExist } from "./utils";

import FredMetadata from "./FredMetadata";
import PackLunr from "./PackLunr";
import PackNedb from "./PackNedb";
import SearchMetadata from "./SearchMetadata";
import FredData from "./FredData";
export { TsData, ITsDataInput, ILongData, IWideData } from "./TsData";

import {
  IDatabaseObject,
  ILunrIndices,
  INedbStores,
  loadDatabases,
} from "./loadDatabases";

export default class GtmmData {
  apiKey: string;
  basePath: string;
  dbName: string;
  cachePath: string;
  rateLimitProps: IRateLimitProps | IEmptyObject;
  options: {
    search: {
      idOnly: boolean;
      returnAs: "cursor" | "promise";
    };
  };

  search: SearchMetadata;
  data: FredData;

  /**
   * Object Containing the lunr indices for carrying out searches.
   * (Internal)
   * @type {[type]}
   */
  _ln: ILunrIndices;
  /**
   * Object containing the NEDB datastores for carrying fetching
   * metadata. (Internal.)
   * @type {[type]}
   */
  _db: INedbStores;

  /**
   * [ready description]
   * @type {[type]}
   */
  ready: boolean;

  constructor(
    apiKey: string = C.DEFAULT_API_KEY,
    dbName: string = C.DEFAULT_DB_NAME,
    basePath: string = C.DEFAULT_DATA_PATH,
    cachePath: string = C.DEFAULT_CACHE_PATH,
    rateLimitProps: IRateLimitProps | IEmptyObject = {},
    searchReturnIdOnly: boolean = false,
    searchReturnAs: "cursor" | "promise" = "cursor"
  ) {
    this.apiKey = apiKey || C.DEFAULT_API_KEY;
    this.dbName = dbName || C.DEFAULT_DB_NAME;
    this.basePath = basePath || C.DEFAULT_DATA_PATH;
    this.cachePath = cachePath || C.DEFAULT_CACHE_PATH;
    this.rateLimitProps = rateLimitProps || {};
    this.options = {
      search: {
        idOnly: searchReturnIdOnly,
        returnAs: searchReturnAs,
      },
    };
  }

  init(force: boolean = false): Promise<any> {
    return new Promise((resolve, reject) => {
      checkAllFilesExist(this.basePath, reject);

      loadDatabases(this.basePath).then((out: IDatabaseObject) => {
        this._ln = out.lunr;
        this._db = out.nedb;
        this.search = new SearchMetadata(
          this.basePath,
          this.options.search.idOnly,
          this.options.search.returnAs,
          out
        );

        this.data = new FredData(
          this.basePath,
          this.dbName,
          this.apiKey,
          this.rateLimitProps,
          this._db.series
        );

        this.data
          .connect()
          .then(() => {
            resolve((this.ready = true));
          })
          .catch(err => reject(err));
      });
    });
  }

  _checkLoad(): void {
    if (!this.ready) {
      throw new Error("Object is not ready. Call .init()");
    }
  }

  /**
   * Fetch FRED metadata from API and file into lunr and nedb databases.
   */
  fetchMetadata(force: boolean = false): Promise<any> {
    var FMD = new FredMetadata(
      this.apiKey,
      this.cachePath,
      this.rateLimitProps
    );
    var PL = new PackLunr(this.basePath, this.cachePath);
    var PN = new PackNedb(this.basePath, this.cachePath);

    // @hack: Added delay in-between calls to ensure that files fully written to file before start to read them. Longer-term solution is to allow a FredMetadata object to be passed to each of the other "alls" so that can read directly from there.
    return new Promise((resolve, reject) =>
      FMD.all(force)
        .then(out => {
          PL.all(FMD)
            .then(() => {
              PN.all(FMD)
                .then(() => resolve(true))
                .catch(err => reject(err));
            })
            .catch(err => reject(err));
        })
        .catch(err => reject(err))
    );
  }

  /**
   * Fetch a FRED series.
   */
  fetch(ids: string, ensureDownloaded: boolean = false): Promise<any> {
    this._checkLoad();
    return this.data.fetch(ids, ensureDownloaded);
  }

  /**
   * Wrapper for FredData.update().
   */
  update(
    which: null | string[] | string = null,
    force: boolean = false
  ): Promise<any> {
    this._checkLoad();
    return this.data.update(which, force);
  }

  /**
   * Wrapper around FredData.checkForUpdates()
   */
  checkForUpdates(): Promise<any> {
    this._checkLoad();
    return this.data.checkForUpdates();
  }

  /**
   * Wrapper around FredData.close();
   */
  close(): void {
    return this.data.close();
  }
}
