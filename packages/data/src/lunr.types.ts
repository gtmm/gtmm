export interface ILunrSeries {
  title: string;
  _id: string;
}

export interface ILunrTag {
  name: string;
  groupId: string;
  notes: string;
}

export interface ILunrRelease {
  name: string;
  link: string;
  notes: string;
}

export interface ILunrSource {
  name: string;
  link: string;
  notes: string;
}

export interface ILunrCategory {
  name: string;
  notes: string;
}
