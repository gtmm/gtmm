export type IStringOrNumber = number | string;

export interface IStringTMap<T> {
  [key: string]: T;
}

export interface INumberTMap<T> {
  [key: number]: T;
}

export interface IAnyTMap<T> {
  key: T;
}

export interface IEmptyObject {}

export interface IRateLimitProps {
  wait: number;
  blockSize: number;
  errTol: number;
}

export type IRejectFunc = () => any;

export type ITransforms =
  | "lin"
  | "chg"
  | "ch1"
  | "pch"
  | "pc1"
  | "cch"
  | "cca"
  | "pca"
  | "log";
