export interface IFilenameBaseFunc {
  (key: string, basePath: string): string;
}

export interface IFilenameCacheFunc {
  (key: string, cachePath: string): string;
}
