import * as sqlite3 from "sqlite3";
import * as Path from "path";
import * as Promise from "bluebird";
import * as ConfigStore from "configstore";

import * as Datastore from "nedb";
import * as Fred from "@gtmm/fred-api";
import Config from "./config";

import { Filenames, dateToFredDate } from "./utils";
import * as C from "./constants";
import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
} from "./types";

/**
 * Handles downloads, updates, and local fetches of FRED data.
 */
export default class FredData {
  dbPath: string;
  db: sqlite3.Database | null;
  ready: boolean;

  /**
   * FRED API Object.
   * @type {[type]}
   */
  _f = Fred;
  /**
   * Information about table schema
   * @type {Object}
   */
  _td: {
    table: string;
    columns: string[];
  };
  /**
   * Series datastore
   * @type {[type]}
   */
  _s: Datastore;

  constructor(
    basePath: string,
    dbName: string,
    apiKey: string,
    rateLimitProps: IRateLimitProps | IEmptyObject = {},
    series: Datastore
  ) {
    this.dbPath = Path.join(basePath, dbName);

    this._f = new Fred(apiKey, rateLimitProps);
    this._s = series;

    this._td = { table: "data", columns: ["date", "series", "value"] };
    this.ready = false;
  }

  /**
   * [fetchSeries description]
   */
  fetch(
    ids: string[] | string,
    ensureDownloaded: boolean = false
  ): Promise<any> {
    /**
     * Checks to see if all of the series IDs have previously been collected.
     * @param  id_ Series IDs to be checked.
     * @return Promise
     */
    const checkAndDownloadIds = (id_: string[]): Promise<any> => {
      return new Promise((resolve, reject) => {
        this._s.find(
          { id: { $in: id_ }, collected: false },
          { id: 1 },
          (err, docs) => {
            if (err) reject(err);
            if (docs.length === 0) {
              resolve(true);
            } else {
              this.checkForUpdates(false)
                .then(() => {
                  var id_: string[] = docs.map(d => d.id.toString());
                  this._download(id_, {}, true)
                    .then(() => resolve(true))
                    .catch(err => reject(err));
                })
                .catch(err => reject(err));
            }
          }
        );
      });
    };

    // MAIN FUNCTION
    if (!this.db) return this._notConnectedError();

    var IDs: string[] = typeof ids === "object" ? ids : [ids];

    return new Promise((resolve, reject) => {
      var P: Promise<any> = ensureDownloaded
        ? checkAndDownloadIds(IDs)
        : Promise.resolve(true);

      P.then(() => {
        // @ts-ignore Known to not be null
        this.db.all(
          `SELECT * FROM ${this._td.table} WHERE series IN (${IDs.map(
            id => `"${id}"`
          ).join(",")});`,
          (err, rows) => {
            if (err) reject(err);
            resolve(rows);
          }
        );
      }).catch(err => reject(err));
    });
  }

  /**
   * Update the data in the database.
   *
   * @params which  If `null`, all collected series are updated. If a string or array of strings, specified series IDs are updated.
   * @params force  If `false`, only the series that have not been downloaded since the last update are downloaded. If `true` all series passed in `which` downloaded.
   */
  update(
    which: null | string[] | string = null,
    force: boolean = false
  ): Promise<any> {
    if (!this.db) {
      return this._notConnectedError();
    }
    var now = new Date();
    return new Promise((resolve, reject) => {
      this.checkForUpdates(false)
        .then(updates => {
          var query;

          // Set up the query for the database
          if (!which) {
            query = { collected: true };
          } else {
            which = typeof which === "object" ? which : [which];
            query = { id: { $in: which } };
          }
          if (!force) {
            query.upToDate = false;
          }

          // Find the documents in the database to see which need to be updated.
          this._s.find(query, { id: 1 }, (err, docs) => {
            if (err) reject(err);
            // Get the IDs to be downloaded.
            var toDownload: any[] = docs.map(s => s.id);

            // Download them.
            if (toDownload.length > 0) {
              this._download(toDownload, {}, true)
                .then(() => {
                  resolve(toDownload);
                })
                .catch(err => reject(err));
            } else {
              resolve(toDownload);
            }
          });
        })
        .catch(err => reject(err));
    });
  }

  /**
   * Get a list of series that have been updated.
   * (Requires internet connection and valid API key.)
   *
   * @param strict (default:true) If true, will formally check the relationship between `lastUpdated` and `lastDownloaded` for each series that is updated. This may help to resolve any inconsistencies in updates.
   */
  checkForUpdates(strict: boolean = true): Promise<any> {
    var lastUpdated = dateToFredDate(new Date(Config.get("lastUpdated"))),
      now = new Date();

    const strictUpdate = (
      updates: IStringTMap<any>,
      updatedSeries: string[],
      reject: (arg: any) => void,
      resolve: (arg: any) => void
    ): void => {
      this._s.find({ id: { $in: updatedSeries } }, (err, docs) => {
        if (err) reject(err);
        var isFalse = docs
          .map(d => {
            d.lastUpdated = new Date(
              updates.seriess[updatedSeries.indexOf(d.id)].last_updated
            );

            if (d.lastDownloaded && d.lastUpdated < d.lastDownloaded) {
              return null;
            } else {
              return d.id;
            }
          })
          .filter(d => d);
        this._s.update(
          { id: { $in: isFalse } },
          { $set: { upToDate: false } },
          { multi: true },
          (err, numAffected, affectedDocuments, upsert) => {
            if (err) reject(err);
            resolve(updates);
          }
        );
      });
    };

    // MAIN
    if (!this.db) {
      return this._notConnectedError();
    }

    return new Promise((resolve, reject) => {
      this._f
        .getSeriesUpdates({
          start_time: lastUpdated,
          end_time: dateToFredDate(new Date()),
        })
        .then(updates => {
          Config.set("lastUpdated", now);
          var updatedSeries: string[] = updates.seriess.map(s => s.id);

          if (updatedSeries.length === 0) {
            resolve(updates.seriess);
          }

          /**
           * NOTE: Why it is not always neccesary to
           * check lastUpdated < lastDownloaded?
           *
           * So long as the `_download` method is always preceded
           * by a call to this method, then it is never necessary
           * to have `strict=true`. This is the case in the `update`
           * and `fetch` methods.
           */
          if (strict) {
            strictUpdate(updates, updatedSeries, reject, resolve);
          } else {
            this._s.update(
              { id: { $in: updatedSeries } },
              { $set: { upToDate: false } },
              { multi: true },
              (err, numAffected, affectedDocuments, upsert) => {
                if (err) reject(err);
                resolve(updates);
              }
            );
          }
        })

        .catch(err => reject(err));
    });
  }

  /**
   * Download data from FRED API. If a DB connection exists append it
   * to the DB.
   * @type {[type]}
   */
  _download(
    ids: string[] | string,
    params: IStringTMap<string> = {},
    append: boolean = true
  ): Promise<any> {
    const deletePreExisting = keys => {
      // @ts-ignore Known to not be null
      return this.db.run(`DELETE FROM ${this._td.table}
WHERE series in (${keys.map(k => `"${k}"`).join(",")});`);
    };

    var insertStart: string = `INSERT INTO ${
      this._td.table
    } (${this._td.columns.join(", ")}) VALUES `;

    const appendNewDataToFrame = (s: string): void => {
      var q: string = insertStart + s.slice(0, -1) + ";";

      // @ts-ignore Known to not be null
      this.db.serialize(() => {
        // @ts-ignore Known to not be null
        this.db.run(q, err => {
          if (err) throw err;
        });
      });
    };

    const appendNewData = d => {
      var insertString: string = "",
        tick: number = 0;

      for (let ii = 0; ii < d.length; ii++) {
        for (let jj = 0; jj < d[ii].observations.length; jj++) {
          insertString += `("${d[ii].observations[jj].date}", "${
            d[ii].series_id
          }", ${
            d[ii].observations[jj].value === "."
              ? "NULL"
              : d[ii].observations[jj].value
          }),`;
        }
      }
      (() => appendNewDataToFrame(insertString))();
    };

    // MAIN FUNCTION
    if (!this.db) {
      return this._notConnectedError();
    }

    var IDs: string[] = typeof ids === "object" ? ids : [ids],
      now = new Date();

    var P: Promise<any>[] = IDs.map(id => {
      var p = { ...{ series_id: id }, ...params };
      return this._downloadOne(p);
    });
    return Promise.all(P).then(d => {
      if (append) {
        // @ts-ignore Known to not be null
        this.db.serialize(() => {
          // Delete the series if it exists
          deletePreExisting(IDs);
          // Append the series.
          appendNewData(d);
        });
      }
      // Update the series metadata.
      this._s.update(
        { id: { $in: IDs } },
        { $set: { lastDownloaded: now, collected: true, upToDate: true } },
        { multi: true },
        (err, numAffected, affectedDocuments, upsert) => {
          if (err) throw err;
        }
      );
      return d;
    });
  }

  /**
   * Download one series from FRED API. Attach the series_id to the output. Usually called internally by `_download`.
   * @type {[type]}
   */
  _downloadOne(params: IStringTMap<string>): Promise<any> {
    return this._f.getSeriesObservations(params).then(out => {
      out.series_id = params.series_id;
      return out;
    });
  }

  /**
   * Connect to local SQLITE database
   */
  connect(): Promise<any> {
    const P = new Promise((resolve, reject) => {
      if (this.ready && this.db) resolve(true);
      // @ts-ignore Known to not be null
      this.db = new sqlite3.Database(this.dbPath, err => {
        if (err) {
          reject(err);
        }

        resolve(true);
      });
    });

    return P.then(out => {
      // @ts-ignore Known to not be null
      this.db.run(
        `CREATE TABLE if not exists ${
          this._td.table
        } (date TEXT, series TEXT, value REAL)`
      );

      this.ready = true;
      return true;
    });
  }

  /**
   * Close the sqlite database connection
   */
  close(): void {
    if (this.ready) {
      // @ts-ignore Known to not be null
      this.db.close(err => {
        if (err) {
          throw err;
        }
        this.db = null;
        this.ready = false;
      });
    }
  }

  /**
   * Clear the DB table completely and start again.
   * @return {[type]} [description]
   */
  resetDb(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.ready) {
        // @ts-ignore Known to not be null
        this.db.serialize(() => {
          // @ts-ignore Known to not be null
          this.db
            .run(`DROP TABLE IF EXISTS ${this._td.table};`)
            .run(
              `CREATE TABLE if not exists ${
                this._td.table
              } (date TEXT, series TEXT, value REAL)`
            );
        });
        Config.set("lastUpdated", new Date(2018, 6, 1));

        // TODO: Mark the data as uncollected in `this._s`
        // Update the series metadata.
        this._s.update(
          {},
          { $set: { lastDownloaded: null, collected: false, upToDate: false } },
          { multi: true },
          (err, numAffected, affectedDocuments, upsert) => {
            if (err) reject(err);
            resolve(true);
          }
        );
      } else {
        reject(Error("No database connection. Cannot reset the DB."));
      }
    });
  }

  _notConnectedError(): Promise<any> {
    return Promise.reject(new Error("Database is not connected."));
  }
}
