import * as FS from "fs";
import * as Path from "path";

import { IFilenameCacheFunc, IFilenameBaseFunc } from "./utils.types";
import { IStringTMap } from "./types";
import * as C from "./constants";

/**
 * A utility for creating standardized filenames for the files used
 * internally by the package.
 * @type {[type]}
 */
export const Filenames: IStringTMap<IFilenameBaseFunc | IFilenameCacheFunc> = {
  cache: (key, cachePath) =>
    Path.join(cachePath, `${key}.FredMetadata.cache.json`),
  lunr: (key, basePath) => Path.join(basePath, "lunr", `${key}.json`),
  nedb: (key, basePath) => Path.join(basePath, "nedb", `${key}.db`),
};

/**
 * Convert a javascript date to a date that is suitable for FRED to
 *  process. See
 *  https://research.stlouisfed.org/docs/api/fred/series_updates.html#start_time
 *  for more information.
 * @param  {[type]} string [description]
 * @return {[type]}        [description]
 */
export const dateToFredDate = (d: Date): string =>
  `${d.getFullYear()}${(d.getUTCMonth() + 1).toString().padStart(2, "0")}${d
    .getUTCDate()
    .toString()
    .padStart(2, "0")}${d
    .getUTCHours()
    .toString()
    .padStart(2, "0")}${d
    .getUTCMinutes()
    .toString()
    .padStart(2, "0")}`;

/**
 * Check that all files necessary to query FRED metadata are present.
 * @param  basePath Base path to file.
 * @param  asBoolean If `true`, will only return T/F. If `false` (default), will raise informative error message if not all files are found.
 * @return         If `true`, good to go. Not otherwise.
 */
export const checkAllFilesExist = (
  basePath: string,
  reject?: (arg: any) => void
): boolean | Error => {
  var allExist: boolean =
    C.FRED_DATA_TYPES.map(dt => {
      return FS.existsSync(Filenames.nedb(dt, basePath));
    }).every(k => k === true) &&
    C.FRED_DATA_TYPES.map(dt => {
      return FS.existsSync(Filenames.lunr(dt, basePath));
    }).every(k => k === true);

  if (!allExist) {
    var s =
      `Could not find necessary files in ${basePath}.` +
      "\n\n" +
      `PKG  ${"DTYPE".padEnd(15)} EXISTS?` +
      "\n";
    for (let ii = 0; ii < C.FRED_DATA_TYPES.length; ii++) {
      s +=
        `lunr ${C.FRED_DATA_TYPES[ii].padEnd(15)} ${FS.existsSync(
          Filenames.lunr(C.FRED_DATA_TYPES[ii], basePath)
        )}` + "\n";
      s +=
        `nedb ${C.FRED_DATA_TYPES[ii].padEnd(15)} ${FS.existsSync(
          Filenames.nedb(C.FRED_DATA_TYPES[ii], basePath)
        )}` + "\n";
    }
    s +=
      "\nCall `GtmmData.fetchAll()` to load this data in full. (WARNING: This may take a long time!)";

    var err: Error = new Error(s);

    if (!reject) {
      throw new Error(s);
    } else {
      reject(err);
    }
  }

  return allExist;
};
