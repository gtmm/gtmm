import * as FS from "fs";
import * as Path from "path";
import * as Promise from "bluebird";

import * as Datastore from "nedb";
import * as lunr from "lunr";

import { Filenames, checkAllFilesExist } from "./utils";
import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
  IAnyTMap,
} from "./types";
import { FRED_DATA_TYPES } from "./constants";

export interface INedbStores {
  series: Datastore;
  categories: Datastore;
  sources: Datastore;
  releases: Datastore;
  tags: Datastore;
}

export interface ILunrIndices {
  series: lunr.Index;
  categories: lunr.Index;
  sources: lunr.Index;
  releases: lunr.Index;
  tags: lunr.Index;
}

export interface IDatabaseObject {
  nedb: INedbStores;
  lunr: ILunrIndices;
}

/**
 * Go back to the pre-serialized source and load it to file.
 */
export const loadDatabases = (basePath: string): Promise<IDatabaseObject> =>
  new Promise((resolve, reject) => {
    var areReady = 0,
      needReady = FRED_DATA_TYPES.length * 2;
    var db: INedbStores = <INedbStores>{},
      ln: ILunrIndices = <ILunrIndices>{};

    const tick = () => {
      areReady += 1;
      if (areReady >= needReady) {
        resolve({ nedb: db, lunr: ln });
      }
    };

    checkAllFilesExist(basePath, reject);

    FRED_DATA_TYPES.map(dt => {
      db[dt] = new Datastore({
        filename: Filenames.nedb(dt, basePath),
        autoload: false,
      });
      db[dt].loadDatabase(err => {
        if (err) reject(err);
        tick();
      });
    });

    FRED_DATA_TYPES.map(dt => {
      var d: string = FS.readFileSync(
        Filenames.lunr(dt, basePath),
        {}
      ).toString();
      ln[dt] = lunr.Index.load(JSON.parse(d));
      tick();
    });
  });
