import { IStringTMap, INumberTMap } from "./types";

export interface ICategory {
  _id: string;
  name: string;
  notes?: string;

  family: {
    parentId?: string;
    children: string[];
  };

  edges: {
    related: string[];
    series: string[];
    tags: string[];
  };
}

export interface ISource {
  _id: string;
  name: string;
  link: string;
  notes?: string;

  edges: {
    releases: string[];
  };
}

export interface IRelease {
  _id: string;
  name: string;
  pressRelease: boolean;
  link: string;
  notes?: string;

  edges: {
    sources: string[];
    series: string[];
    tags: string[];
  };
}

export interface ITag {
  _id: string;
  name: string;
  groupId: string;
  notes?: string;
  seriesCount: number;
  popularity: number;

  edges: {
    series: string[];
    releases: string[];
    categories: string[];
    relatedTags: string[];
  };
}

export interface ISeries {
  _id: string;
  id: string;
  title: string;
  obsStart: Date;
  obsEnd: Date;
  freq: string;
  freqShort: string;
  units: string; // Use units_short
  unitsShort: string;
  sa: boolean; // Seasonal adjustment
  lastUpdated: Date;
  popularity: number;
  notes?: string;
  collected: boolean;
  lastDownloaded: Date | null;
  upToDate: boolean;

  edges: {
    categories: string[];
    releases: string[];
    tags: string[];
  };
}

export interface ICategories extends INumberTMap<ICategory> {}
export interface ISources extends INumberTMap<ISource> {}
export interface IReleases extends INumberTMap<IRelease> {}
export interface ITags extends INumberTMap<ITag> {}
export interface ISeriess extends INumberTMap<ISeries> {}
