import * as Promise from "bluebird";
import * as Path from "path";
import * as FS from "fs";
import * as JSONStream from "JSONStream";

import * as Fred from "@gtmm/fred-api";

import {
  DEFAULT_API_KEY,
  DEFAULT_DATA_PATH,
  DEFAULT_CACHE_PATH,
  SERIES_FREQ,
} from "./constants";
import {
  ICategories,
  ISources,
  IReleases,
  ITags,
  ISeriess,
  IRelease,
  ISource,
  ISeries,
  ITag,
  ICategory,
} from "./FredMetadata.types";
import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
} from "./types";

export default class FredMetadata {
  fred: Fred;
  series: ISeriess;
  tags: ITags;
  sources: ISources;
  releases: IReleases;
  categories: ICategories;
  fetched: IStringTMap<boolean>;
  cachePath: string | null;
  _seriesCount: number;

  constructor(
    apiKey: string = DEFAULT_API_KEY,
    cachePath: string | null = DEFAULT_CACHE_PATH,
    rateLimitProps: IRateLimitProps | IEmptyObject = {}
  ) {
    this.fred = new Fred(apiKey, rateLimitProps);

    this.series = {};
    this.tags = {};
    this.sources = {};
    this.releases = {};
    this.categories = {};

    this.cachePath = cachePath;

    this._seriesCount = 0;

    this.fetched = {
      sources: false,
      categories: false,
      releases: false,
      series: false,
      tags: false,
    };

    this.getCachedFiles();
  }

  get apiKey(): string {
    return this.fred.apiKey;
  }

  all(force: boolean = true): Promise<any> {
    if (!force && Object.keys(this.fetched).every(f => this.fetched[f])) {
      return Promise.resolve();
    }
    return new Promise((resolve, reject) =>
      Promise.all([
        this.fetchSources(),
        this.fetchReleases(),
        this.fetchCategories(),
        this.fetchTags(),
      ]).then(out => {
        this.fetchSeries()
          .then(() =>
            Promise.all([
              this.fetchEdgesReleaseTag(),
              this.fetchEdgesCategoryTag(),
              this.fetchEdgesReleaseSource(),
              this.fetchEdgeCategoryRelated(),
              //this.fetchEdgesTagsRelated(),
              //this.fetchEdgesTagSeries(),
            ])
              .then(() => resolve(true))
              .catch(err => reject(err))
          )
          .catch(err => reject(err));
      })
    );
  }

  getCachedFiles(): void {
    if (!this.cachePath) {
      return;
    } else if (!FS.existsSync(this.cachePath)) {
      FS.mkdirSync(this.cachePath, { recursive: true });
      return;
    }

    for (let k in this.fetched) {
      try {
        var out: IStringTMap<any> = JSON.parse(
          FS.readFileSync(
            Path.join(this.cachePath, `${k}.FredMetadata.cache.json`)
          ).toString()
        );
        this[k] = out;
        this.fetched[k] = true;
      } catch (e) {
        null;
      }
    }
  }

  clearCachedFiles(): void {
    if (this.cachePath) {
      FS.rmdirSync(this.cachePath);
    }
  }

  _cacheFile(key: string, obj: IStringTMap<any>): void {
    this.fetched[key] = true;
    this[key] = obj;

    if (!this.cachePath) {
      return;
    }

    var file: string = Path.join(
      this.cachePath,
      `${key}.FredMetadata.cache.json`
    );

    // https://www.bennadel.com/blog/3232-parsing-and-serializing-large-objects-using-jsonstream-in-node-js.htm
    var transformStream = JSONStream.stringifyObject();
    var outputStream = FS.createWriteStream(file);
    transformStream.pipe(outputStream);

    // Want to use the series ID that is a number when storing.
    if (key === "series") {
      Object.keys(obj).map((k, ii) =>
        transformStream.write([obj[k]._id, obj[k]])
      );
    } else {
      Object.keys(obj).map(k => transformStream.write([k, obj[k]]));
    }

    transformStream.end();
    return;
  }

  /**
   * Get the sources and associated releases for each.
   */
  fetchSources(): Promise<any> {
    this.sources = {};

    return this.fred.getSources({}).then(res => {
      res.sources.map(r => {
        this._appendSource(r);
      });
      this._cacheFile("sources", this.sources);

      return res;
    });
  }

  fetchReleases(): Promise<any> {
    this.releases = {};

    return this.fred.getReleases({}).then(res => {
      res.releases.map(r => {
        this._appendRelease(r);
      });

      this._cacheFile("releases", this.releases);
      return res;
    });
  }

  fetchCategories(): Promise<any> {
    const fetchCategory: (cat: IStringTMap<any>) => Promise<any> = cat => {
      return this.fred
        .getCategoryChildren({ category_id: cat.id })
        .then(res => {
          if (res.categories.length > 0) {
            this._appendCategory(cat, res.categories);
            var P = res.categories.map(c => fetchCategory(c));
            return Promise.all(P);
          } else {
            return true;
          }
        });
    };

    this.categories = {};

    // Append the root to the tree
    var rootCategory: IStringTMap<any> = { id: 0, name: "Categories" };
    return fetchCategory(rootCategory).then(res => {
      this._cacheFile("categories", this.categories);
      return res;
    });
  }

  fetchTags() {
    this.tags = {};

    return this.fred.getTags({}).then(res => {
      res.tags.map(r => {
        this._appendTag(r);
      });

      this._cacheFile("tags", this.tags);
      return res;
    });
  }

  /**
   * This is a "special" edge fetcher due to the fact that it will also
   * take care of the fetching of the edges.
   *
   * Additionally, per the article on series tokenization in blog, will
   * only include those with frequency in "M", "Q", "D", "BW", "W".
   * @return {[type]} [description]
   */
  fetchSeries() {
    if (!this.fetched.releases) {
      return this.fetchReleases().then(r => this.fetchSeries());
    } else if (!this.fetched.categories) {
      return this.fetchCategories().then(r => this.fetchSeries());
    }

    // Reset the series just in case
    this.series = {};

    this.fred._rl.errTol = 10 * 200;

    // Get the series:
    // // Via releases -> series linkages
    var rid: string[] = Object.keys(this.releases);
    var Pr: Promise<any>[] = rid.map(
      (id: string, ii: number): Promise<any> => {
        return this.fred
          .getReleaseSeries({
            release_id: parseInt(id),
          })
          .then(res => {
            // Create and assign indices to series
            var arr: string[] = res.seriess
              .map(r => {
                var s: ISeries | null = this._appendSeries(r, "releases", id);
                if (s) {
                  return s._id;
                } else {
                  return;
                }
              })
              .filter(r => r != null);

            // Append
            this.releases[id].edges.series = arr;
            return;
          });
      }
    );

    // Via categories -> series linkages
    var cid: string[] = Object.keys(this.categories);
    var Pc: Promise<any>[] = cid.map(
      (id: string): Promise<any> => {
        return this.fred
          .getCategorySeries({
            category_id: parseInt(id),
          })
          .then(res => {
            var arr: string[] = res.seriess
              .map(r => {
                var s: ISeries | null = this._appendSeries(r, "categories", id);
                if (s) {
                  return s._id;
                } else {
                  return;
                }
              })
              .filter(r => r != null);
            this.categories[id].edges.series = arr;

            return;
          });
      }
    );

    var P: Promise<any>[] = Pr.concat(Pc);

    return Promise.all(P).then(res => {
      this._cacheFile("series", this.series);
      this._cacheFile("categories", this.categories);
      this._cacheFile("releases", this.releases);
      this.fred._rl.errTol = 200;
      return res;
    });
  }

  fetchEdgesReleaseTag(): Promise<any> {
    const update: (id: string) => Promise<any> = id => {
      return this.fred
        .getReleaseTags({ release_id: parseInt(id) })
        .then(res => {
          var arr: string[] = res.tags.map(t => {
            var idt: string = t.name;
            try {
              tags[idt].edges.releases.push(id);
            } catch (e) {
              var to: ITag = this._appendTag(t, false);
              tags[idt] = to;
              tags[idt].edges.releases.push(id);
            }

            return idt;
          });
          releases[id].edges.tags = arr;
        });
    };

    if (!this.fetched.tags) {
      return this.fetchTags().then(r => this.fetchEdgesReleaseTag());
    } else if (!this.fetched.releases) {
      return this.fetchReleases().then(r => this.fetchEdgesReleaseTag());
    }

    var tags: ITags = Object.assign({}, this.tags);
    var releases: IReleases = Object.assign({}, this.releases);

    Object.keys(tags).map(key => {
      tags[key].edges.releases = [];
    });

    var P: Promise<any>[] = Object.keys(releases).map(r => update(r));

    return Promise.all(P).then(out => {
      var t2: ITags = Object.assign({}, this.tags);
      var r2: IReleases = Object.assign({}, this.releases);

      Object.keys(tags).map(key => {
        try {
          t2[key].edges.releases = tags[key].edges.releases;
        } catch (e) {
          t2[key] = tags[key];
        }
      });

      Object.keys(releases).map(key => {
        try {
          r2[key].edges.tags = releases[key].edges.tags;
        } catch (e) {
          r2[key] = releases[key];
        }
      });

      this._cacheFile("tags", t2);
      this._cacheFile("releases", r2);

      return out;
    });
  }

  fetchEdgesCategoryTag(): Promise<any> {
    const update: (id: string) => Promise<any> = id => {
      return this.fred
        .getCategoryTags({
          category_id: parseInt(id),
          limit: 200,
          order_by: "popularity",
        })
        .then(res => {
          var arr: string[] = res.tags.map(t => {
            var idt: string = t.name;

            try {
              tags[idt].edges.categories.push(id);
            } catch (e) {
              var to: ITag = this._appendTag(t, false);
              tags[idt] = to;
              tags[idt].edges.categories.push(id);
            }
            return idt;
          });
          categories[id].edges.tags = arr;
        });
    };

    if (!this.fetched.tags) {
      return this.fetchTags().then(r => this.fetchEdgesCategoryTag());
    } else if (!this.fetched.categories) {
      return this.fetchCategories().then(r => this.fetchEdgesCategoryTag());
    }

    var tags: ITags = Object.assign({}, this.tags);
    var categories: ICategories = Object.assign({}, this.categories);

    Object.keys(tags).map(key => {
      tags[key].edges.categories = [];
    });

    var P: Promise<any>[] = Object.keys(categories).map(r => update(r));

    return Promise.all(P).then(out => {
      var t2: ITags = Object.assign({}, this.tags);
      var c2: ICategories = Object.assign({}, this.categories);

      // Avoids race conditions
      Object.keys(categories).map(key => {
        try {
          c2[key].edges.tags = categories[key].edges.tags;
        } catch (e) {
          c2[key] = categories[key];
        }
      });

      Object.keys(tags).map(key => {
        try {
          t2[key].edges.categories = tags[key].edges.categories;
        } catch (e) {
          t2[key] = tags[key];
        }
      });

      this._cacheFile("categories", c2);
      this._cacheFile("tags", t2);
      return out;
    });
  }

  fetchEdgesReleaseSource(): Promise<any> {
    const update: (id: string) => Promise<any> = id => {
      return this.fred
        .getReleaseSources({ release_id: parseInt(id) })
        .then(res => {
          var arr: string[] = res.sources.map(t => {
            let idt = t.id.toString();
            try {
              sources[idt].edges.releases.push(id);
            } catch (e) {
              var to: ISource = this._appendSource(t, false);
              sources[idt] = to;
              sources[idt].edges.releases.push(id);
            }
            return idt;
          });
          releases[id].edges.sources = arr;
        });
    };

    if (!this.fetched.releases) {
      return this.fetchReleases().then(r => this.fetchEdgesReleaseSource());
    } else if (!this.fetched.sources) {
      return this.fetchSources().then(r => this.fetchEdgesReleaseSource());
    }

    var sources: ISources = Object.assign({}, this.sources);
    var releases: IReleases = Object.assign({}, this.releases);

    Object.keys(sources).map(key => {
      sources[key].edges.releases = [];
    });

    var P: Promise<any>[] = Object.keys(releases).map(r => update(r));

    return Promise.all(P).then(out => {
      var s2: ISources = Object.assign({}, this.sources);
      var r2: IReleases = Object.assign({}, this.releases);

      // Avoids race conditions
      Object.keys(sources).map(key => {
        try {
          s2[key].edges.releases = sources[key].edges.releases;
        } catch (e) {
          s2[key] = sources[key];
        }
      });

      Object.keys(releases).map(key => {
        try {
          r2[key].edges.sources = releases[key].edges.sources;
        } catch (e) {
          r2[key] = releases[key];
        }
      });

      this._cacheFile("sources", s2);
      this._cacheFile("releases", r2);
      return out;
    });
  }

  fetchEdgeCategoryRelated(): Promise<any> {
    const update: (id: string) => Promise<any> = id => {
      return this.fred
        .getCategoryRelated({ category_id: parseInt(id) })
        .then(res => {
          var arr: string[] = res.categories.map(t => t.id.toString());

          categories[id].edges.related = arr;
        });
    };

    if (!this.fetched.categories) {
      return this.fetchCategories().then(r => this.fetchEdgeCategoryRelated());
    }

    var categories: ICategories = Object.assign({}, this.categories);

    Object.keys(categories).map(key => {
      categories[key].edges.related = [];
    });

    var P: Promise<any>[] = Object.keys(categories).map(r => update(r));

    return Promise.all(P).then(out => {
      var c2: ICategories = Object.assign({}, this.categories);

      // Avoids race conditions
      Object.keys(categories).map(key => {
        c2[key].edges.related = categories[key].edges.related;
      });

      this._cacheFile("categories", c2);
      return out;
    });
  }

  /**
   * Get all related tags in a series.
   *
   * @deprecated This method takes to long to run. Instead, newer versions will make it possible to import these linkages one at a time as they are called.
   */
  fetchEdgesTagsRelated(): Promise<any> {
    console.warn(
      "DEPRECATION WARNING: Import tag <-> tag linkages piece-by-piece."
    );

    const update: (id: string, tag_name: string) => Promise<any> = (
      id,
      tag_name
    ) => {
      return this.fred
        .getRelatedTags({
          tag_name: tag_name.replace(/\s+/g, "+"),
          limit: 100,
          order_by: "popularity",
        })
        .then(res => {
          var arr: string[] = res.categories.map(t => t.name);

          tags[id].edges.related = arr;
        });
    };

    if (!this.fetched.tags) {
      return this.fetchTags().then(r => this.fetchEdgesTagsRelated());
    }

    var tags: ITags = Object.assign({}, this.tags);

    Object.keys(tags).map(key => {
      tags[key].edges.related = [];
    });

    var P: Promise<any>[] = Object.keys(tags).map(r => update(r, tags[r].name));

    return Promise.all(P).then(out => {
      var t2: ITags = Object.assign({}, this.tags);

      // Avoids race conditions
      Object.keys(tags).map(key => {
        t2[key].edges.related = tags[key].edges.related;
      });

      this._cacheFile("tags", t2);
      return out;
    });
  }

  /**
   * Fetch the series that are associated with a given tag.
   * @deprecated This method takes to long to run. Instead, newer versions will make it possible to import these linkages one at a time as they are called.
   */
  fetchEdgesTagSeries() {
    console.warn(
      "DEPRECATION WARNING: Import tag <-> series linkages piece-by-piece."
    );
    /**
     * Take the known tags and find the 300 most popular series
     *  attached to it. Get more series because ~2/3 of series are
     *  annual and expected to be thrown out.
     * @param  {[type]} ) [description]
     * @return {[type]}   [description]
     */
    const TagToSeries: () => Promise<any> = () => {
      const update: (id: string, tag_name: string) => Promise<any> = (
        id,
        tag_name
      ) => {
        return this.fred
          .getTagsSeries({
            tag_names: tag_name.replace(/\s+/g, "+"),
            limit: 100,
            order_by: "popularity",
          })
          .then(res => {
            var arr: string[] = res.seriess.map(t => {
              var idt: string = t.id.toString();

              // Ensure that
              // a.) return numeric ID of series; and
              // b.) only return ID if series has legit frequency.
              try {
                this.series[idt].edges.tags.push(id);
                return this.series[idt]._id;
              } catch (e) {
                var ns: ISeries | null = this._appendSeries(t, "tags", id);
                if (ns) {
                  return ns._id;
                } else {
                  return;
                }
              }
            });
            arr = arr.filter(el => el != null);
            this.tags[id].edges.series = arr;
          });
      };

      var P: Promise<any>[] = Object.keys(this.tags).map(r =>
        update(r, this.tags[r].name)
      );

      return Promise.all(P);
    };

    /**
     * Take each of the known series and find the 100 most popular tags
     * associated with it.
     * @param  {[type]} ) [description]
     * @return {[type]}   [description]
     */
    const SeriesToTag: () => Promise<any> = () => {
      const update: (id: string) => Promise<any> = id => {
        return this.fred
          .getSeriesTags({
            series_id: parseInt(id),
            limit: 100,
            order_by: "popularity",
          })
          .then(res => {
            var arr: string[] = res.tags.map(t => {
              var idt: string = t.name;

              try {
                this.tags[idt].edges.series.push(id);
              } catch (e) {
                this._appendTag(t);
              }
              return idt;
            });
            this.series[id].edges.tags = arr;
          });
      };

      // Split up the calls over several promises to avoid
      // overloading the counters (i.e. too many setTimeouts running).
      var sk: string[] = Object.keys(this.series);
      var P0: Promise<any>[] = sk.slice(0, sk.length / 3).map(r => update(r));

      return Promise.all(P0).then(() => {
        var P1: Promise<any>[] = sk
          .slice(sk.length / 3, (sk.length * 2) / 3)
          .map(r => update(r));
        return Promise.all(P1).then(() => {
          var P2: Promise<any>[] = sk
            .slice((sk.length * 2) / 3, sk.length + 1)
            .map(r => update(r));
          return Promise.all(P2);
        });
      });
    };

    /**
     * For each tag ensure that the list of series that are included are unique.
     * @return {[type]} [description]
     */
    const tagToSeriesUniqueEdges = () => {
      var t2: ITags = Object.assign({}, this.tags);

      var keys: string[] = Object.keys(this.tags);
      for (let ii = 0; ii < keys.length; ii++) {
        var res: string[] = [];
        var inp: string[] = this.tags[keys[ii]].edges.series;
        for (let idx = 0; idx < inp.length; idx++) {
          var v: string = inp[idx];

          if (res.indexOf(v) < 0) {
            res.push(v);
          }
        }
        this.tags[keys[ii]].edges.series = res;
      }

      return;
    };

    const SeriesToTagUniqueEdges = () => {
      var keys: string[] = Object.keys(this.series);
      for (let ii = 0; ii < keys.length; ii++) {
        var res: string[] = [];
        var inp: string[] = this.series[keys[ii]].edges.tags;
        for (let idx = 0; idx < inp.length; idx++) {
          var v: string = inp[idx];
          if (res.indexOf(v) < 0) {
            res.push(v);
          }
        }
        this.series[keys[ii]].edges.tags = res;
      }

      return;
    };

    // BEGIN MAIN FUNCTION
    if (!this.fetched.tags) {
      return this.fetchTags().then(r => this.fetchEdgesTagSeries());
    } else if (!this.fetched.series) {
      return this.fetchCategories().then(r => this.fetchEdgesTagSeries());
    }

    Object.keys(this.series).map(key => {
      this.series[key].edges.tags = [];
    });

    Object.keys(this.tags).map(key => {
      this.tags[key].edges.series = [];
    });

    return TagToSeries().then(res => {
      return SeriesToTag().then(res0 => {
        // Need to check for uniqueness because top tag of series
        // likely to also be top series of tag.
        tagToSeriesUniqueEdges();
        SeriesToTagUniqueEdges();

        this._cacheFile("series", this.series);
        this._cacheFile("tags", this.tags);
        return res;
      });
    });
  }

  _appendSeries(
    d: IStringTMap<any>,
    fromType: string | null,
    fromTypeId: number | string | null,
    inplace: boolean = true
  ): ISeries | null {
    var s: ISeries;

    if (this.series[d.id]) {
      s = this.series[d.id];
    } else if (SERIES_FREQ.short.indexOf(d.frequency_short) > -1) {
      this._seriesCount += 1;
      var _id: string = this._seriesCount.toString();
      s = {
        _id: _id,
        id: d.id,
        title: d.title,
        obsStart: new Date(d.observation_start),
        obsEnd: new Date(d.observation_end),
        freq: d.frequency,
        freqShort: d.frequency_short, // Use frequency_short
        units: d.units, // Use units_short
        unitsShort: d.units_short,
        sa: d.seasonal_adjustment_short !== "NSA", // Seasonal adjustment
        lastUpdated: new Date(d.lastUpdated),

        popularity: d.popularity,
        notes: d.notes,
        collected: false,
        lastDownloaded: null,
        upToDate: false,

        edges: {
          categories: [],
          releases: [],
          tags: [],
        },
      };
    } else {
      return null;
    }

    if (fromType && fromTypeId) {
      s.edges[fromType].push(fromTypeId.toString());
    }

    if (inplace) {
      this.series[d.id] = s;
    }
    return s;
  }

  _appendTag(d: IStringTMap<any>, inplace: boolean = true): ITag {
    var id: string = d.name;

    var t: ITag = {
      _id: id,
      name: d.name,
      groupId: d.group_id,
      notes: d.notes,
      seriesCount: d.series_count,
      popularity: d.popularity,

      edges: {
        series: [],
        releases: [],
        categories: [],
        relatedTags: [],
      },
    };

    if (inplace) {
      this.tags[id] = t;
    }
    return t;
  }

  _appendSource(d: IStringTMap<any>, inplace: boolean = true): ISource {
    var id: string = d.id.toString();

    var s: ISource = {
      _id: id,
      name: d.name,
      link: d.link,
      notes: d.notes,

      edges: {
        releases: [],
      },
    };

    if (inplace) {
      this.sources[id] = s;
    }
    return s;
  }

  /**
   * Appends releases to release catalog even if they already exist.
   * @type {[type]}
   */
  _appendRelease(d: IStringTMap<any>, inplace: boolean = true): IRelease {
    var r: IRelease = {
      _id: d.id.toString(),
      name: d.name,
      pressRelease: d.press_release,
      link: d.link,
      notes: d.notes,

      edges: {
        sources: [],
        series: [],
        tags: [],
      },
    };

    if (inplace) {
      this.releases[d.id.toString()] = r;
    }
    return r;
  }

  _appendCategory(
    d: IStringTMap<any>,
    children: IStringTMap<any>[] | null,
    inplace: boolean = true
  ): ICategory {
    var c: ICategory;
    var id: string = d.id.toString();

    if (this.categories[id]) {
      c = this.categories[id];
    } else {
      c = {
        _id: id,
        name: d.name,
        notes: d.notes,

        family: {
          parentId: d.parent_id,
          children: [],
        },

        edges: {
          related: [],
          series: [],
          tags: [],
        },
      };
    }

    if (children) {
      c.family.children = children.map(child => child.id.toString());
      children.map(child => this._appendCategory(child, null, inplace));
    }

    if (inplace) {
      this.categories[id] = c;
    }
    return c;
  }
}
