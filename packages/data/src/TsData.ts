import * as Promise from "bluebird";

import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
  ITransforms,
} from "./types";

const MILLISECONDS_IN_YEAR_MINUS_ONE_DAY = 31449600000;

type ICols = string | string[] | null;

interface ITsDataObsInput {
  series: string;
  date: string;
  value: number;
}
export type ITsDataInput = ITsDataObsInput[];

interface ITsDataObs {
  series: string;
  date: Date;
  value: number;
}
type ITsData = ITsDataObs[];
type IAgg = "ANY" | "M" | "Q" | "A" | "any" | "m" | "q" | "a";
type IAggMethod = "avg" | "sum" | "eop" | "AVG" | "SUM" | "EOP";
type IDatesAs = "string" | "date";
export type IDataShape = "long" | "wide";

interface IWideObs {
  date: Date | string;
  [keys: string]: null | number | Date | string;
}
export type IWideData = IWideObs[];
interface IWideObject {
  [dates: string]: IWideObs;
}

interface ILongObs {
  date: Date;
  value: number;
  series: string;
}
type ILongVar = ILongObs[];
export type ILongData = IStringTMap<ILongVar>;

/**
 *
 * [List of data transforms and abbreviations.](https://research.stlouisfed.org/docs/api/fred/series_observations.html#units)
 * [Growth formulas used.](https://alfred.stlouisfed.org/help#growth_formulas)
 */
export class TsData {
  /**
   * Default values returned by
   * @type {[type]}
   */
  v: ILongData | IWideData;
  _w: IWideObject;
  df: ILongData;
  dfw: ILongData;
  cols: string[];
  trans: IStringTMap<ITransforms>;
  timestamps: IStringTMap<number>;
  md: IStringTMap<{
    nObsPerYear: number;
    freq: string;
    bds: { x: Date[]; y: number[] };
  }>;

  /**
   * If true, will check to ensure that columns passed into transform methods are valid.
   * @type {boolean}
   */
  strict: boolean;

  /**
   * For wide, return the date parameter as either a date or a string.
   * @type {IDatesAs}
   */
  datesAs: IDatesAs;
  _agg: IAgg;
  _aggMethod: IAggMethod;
  _dShape: IDataShape;

  constructor(
    df: ITsDataInput,
    dataShape: IDataShape = "long",
    agg: IAgg = "any",
    aggMethod: IAggMethod = "avg",
    strict: boolean = true,
    datesAs: IDatesAs = "date"
  ) {
    this._dShape = dataShape;
    this.strict = strict;
    this._agg = agg;
    this._aggMethod = aggMethod;
    this.datesAs = datesAs;

    this._init(df);

    this._oneLog = this._oneLog.bind(this);
    this._oneLevels = this._oneLevels.bind(this);
    this._oneChange = this._oneChange.bind(this);
    this._onePercentChange = this._onePercentChange.bind(this);
    this._oneChangeFromYearAgo = this._oneChangeFromYearAgo.bind(this);
    this._onePercentChangeFromYearAgo = this._onePercentChangeFromYearAgo.bind(
      this
    );
    this._oneContinuouslyCompoundedRateOfChange = this._oneContinuouslyCompoundedRateOfChange.bind(
      this
    );
    this._oneCompoundedAnnualRateOfChange = this._oneCompoundedAnnualRateOfChange.bind(
      this
    );
    this._oneContinuouslyCompoundedAnnualRateOfChange = this._oneContinuouslyCompoundedAnnualRateOfChange.bind(
      this
    );
    this._updateAllSeries = this._updateAllSeries.bind(this);
    this._make = this._make.bind(this);
    this._makeLong = this._makeLong.bind(this);
    this._makeWide = this._makeWide.bind(this);
  }

  _init(df: ITsDataInput): void {
    var d: ILongData = <ILongData>{},
      k: string[] = [];
    for (let ii = 0; ii < df.length; ii++) {
      if (k.indexOf(df[ii].series) < 0) {
        d[df[ii].series] = [];
        k.push(df[ii].series);
      }

      d[df[ii].series].push({
        date: new Date(df[ii].date + " 10:00:00 GMT"),
        series: df[ii].series,
        value: df[ii].value,
      });
    }
    (() => {
      this.df = Object.assign({}, d);
      this.dfw = Object.assign({}, d);
      this.cols = k;
      this.trans = Object.keys(d).reduce(
        (accum, k) => Object.assign(accum, { [k]: "lin" }),
        {}
      );
      this.md = Object.keys(d).reduce((accum, k) => {
        var f = this._getNObsPerYear(k);
        accum[k] = {
          nObsPerYear: f[0],
          freq: f[1],
          bds: {
            x: [this.df[k][0].date, this.df[k].slice(-1)[0].date],
            y: [-1e99, -1e99],
          },
        };
        return accum;
      }, {});
      this._updateAllSeries();
    })();
  }

  get raw(): ITsData {
    return Object.keys(this.dfw).reduce(
      (accum, cv) => accum.concat(this.dfw[cv]),
      <ITsData>[]
    );
  }

  get values(): ILongData | IWideData {
    return this.v;
  }

  get long(): ILongData {
    return this.dfw;
  }

  get wide(): IWideData {
    if (this._dShape === "wide") {
      return this.v as IWideData;
    } else {
      var w = this._w || this._updateWide(this.cols);
      return Object.values(w);
    }
  }

  reset(): void {
    this.dfw = Object.assign({}, this.df);
    this._agg = "any";
    this._aggMethod = "AVG";
    this.md = this.cols.reduce((accum, k) => {
      var f = this._getNObsPerYear(k);
      accum[k] = {
        nObsPerYear: this.md[k].nObsPerYear,
        freq: this.md[k].freq,
        bds: {
          x: [this.df[k][0].date, this.df[k].slice(-1)[0].date],
          y: [-1e99, -1e99],
        },
      };
      return accum;
    }, {});
    this._updateAllSeries();
  }

  set agg(a: IAgg) {
    this._agg = a;
    this._updateAllSeries();
  }

  get agg(): IAgg {
    return this._agg;
  }

  set aggMethod(a: IAggMethod) {
    this._aggMethod = a;
    this._updateAllSeries();
  }

  get aggMethod(): IAggMethod {
    return this._aggMethod;
  }

  set dataShape(a: IDataShape) {
    this._dShape = a;
    this._updateAllSeries();
  }

  get dataShape(): IDataShape {
    return this._dShape;
  }

  /**
   * Alias for changeFromYearAgo
   * @param  {string|string[]} cols [description]
   * @return {ILongData}            [description]
   */
  ch1(cols: ICols = null): ILongData | IWideData {
    return this.changeFromYearAgo(cols);
  }

  /**
   * [pctDiffYearOverYear description]
   * @param  {string|string[]} cols    Columns to be made year-over-year.
   * @return {ILongData}               [description]
   */
  changeFromYearAgo(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(cols, this._oneChangeFromYearAgo, "ch1");
  }

  _oneChangeFromYearAgo(col: string): { out: ILongVar; bds: [number, number] } {
    // 31622400000
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      nObsPerYear: number = this.md[col].nObsPerYear,
      values: number[] = [];

    for (let ii = nObsPerYear; ii < d.length; ii++) {
      if (d[ii].value != null && d[ii - nObsPerYear].value != null) {
        let v = d[ii].value - d[ii - nObsPerYear].value;
        values.push(v);

        out.push({
          date: d[ii].date,
          series: d[ii].series,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  /**
   * Alias for percentChangeFromYearAgo
   * @param  {string|string[]} cols [description]
   * @return {ILongData}            [description]
   */
  pc1(cols: ICols = null): ILongData | IWideData {
    return this.percentChangeFromYearAgo(cols);
  }

  /**
   * [pctDiffYearOverYear description]
   * @param  {string|string[]} cols    Columns to be made year-over-year.
   * @return {ILongData}               [description]
   */
  percentChangeFromYearAgo(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(
      cols,
      this._onePercentChangeFromYearAgo,
      "pc1"
    );
  }

  _onePercentChangeFromYearAgo(
    col: string
  ): { out: ILongVar; bds: [number, number] } {
    // 31622400000
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      nObsPerYear: number = this.md[col].nObsPerYear,
      values: number[] = [];

    for (let ii = nObsPerYear; ii < d.length; ii++) {
      if (d[ii].value != null && d[ii - nObsPerYear].value) {
        let v =
          (100 * (d[ii].value - d[ii - nObsPerYear].value)) /
          d[ii - nObsPerYear].value;
        values.push(v);

        out.push({
          date: d[ii].date,
          series: d[ii].series,
          value: v,
        });
      }
    }
    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  log(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(cols, this._oneLog, "log");
  }

  _oneLog(col: string): { out: ILongVar; bds: [number, number] } {
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      values: number[] = [];

    for (let ii = 0; ii < d.length; ii++) {
      if (d[ii].value > 0) {
        let v = Math.log(d[ii].value);
        values.push(v);

        out.push({
          series: d[ii].series,
          date: d[ii].date,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  lin(cols: ICols = null): ILongData | IWideData {
    return this.levels(cols);
  }

  levels(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(cols, this._oneLevels, "lin");
  }

  _oneLevels(col: string): { out: ILongVar; bds: [number, number] } {
    var out = this.df[col].filter(v => v.value != null);

    var values = out.map(oo => oo.value);
    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  chg(cols: ICols = null): ILongData | IWideData {
    return this.change(cols);
  }

  change(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(cols, this._oneChange, "chg");
  }

  _oneChange(col: string): { out: ILongVar; bds: [number, number] } {
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      values: number[] = [];

    for (let ii = 1; ii < d.length; ii++) {
      if (d[ii].value != null && d[ii - 1].value != null) {
        let v = d[ii].value - d[ii - 1].value;
        values.push(v);

        out.push({
          series: d[ii].series,
          date: d[ii].date,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  pch(cols: ICols = null): ILongData | IWideData {
    return this.percentChange(cols);
  }

  percentChange(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(cols, this._onePercentChange, "pch");
  }

  _onePercentChange(col: string): { out: ILongVar; bds: [number, number] } {
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      values: number[] = [];

    for (let ii = 1; ii < d.length; ii++) {
      if (
        d[ii].value != null &&
        d[ii - 1].value != null &&
        d[ii - 1].value !== 0
      ) {
        let v = (100 * (d[ii].value - d[ii - 1].value)) / d[ii - 1].value;
        values.push(v);
        out.push({
          series: d[ii].series,
          date: d[ii].date,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  cch(cols: ICols = null): ILongData | IWideData {
    return this.continuouslyCompoundedRateOfChange(cols);
  }

  continuouslyCompoundedRateOfChange(
    cols: ICols = null
  ): ILongData | IWideData {
    return this._transformGeneral(
      cols,
      this._oneContinuouslyCompoundedRateOfChange,
      "cch"
    );
  }

  _oneContinuouslyCompoundedRateOfChange(
    col: string
  ): { out: ILongVar; bds: [number, number] } {
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      values: number[] = [];

    for (let ii = 1; ii < d.length; ii++) {
      if (d[ii].value > 0 && d[ii - 1].value > 0) {
        let v = 100 * (Math.log(d[ii].value) - Math.log(d[ii - 1].value));
        values.push(v);
        out.push({
          series: d[ii].series,
          date: d[ii].date,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  cca(cols: ICols = null): ILongData | IWideData {
    return this.continuouslyCompoundedAnnualRateOfChange(cols);
  }

  continuouslyCompoundedAnnualRateOfChange(
    cols: ICols = null
  ): ILongData | IWideData {
    return this._transformGeneral(
      cols,
      this._oneContinuouslyCompoundedAnnualRateOfChange,
      "cca"
    );
  }

  _oneContinuouslyCompoundedAnnualRateOfChange(
    col: string
  ): { out: ILongVar; bds: [number, number] } {
    var out: ILongVar = <ILongVar>[],
      times: number[] = [],
      d: ILongVar = this.df[col],
      nObsPerYear: number = this.md[col].nObsPerYear,
      values: number[] = [];

    for (let ii = 1; ii < d.length; ii++) {
      if (d[ii].value > 0 && d[ii - 1].value > 0) {
        let v =
          100 *
          nObsPerYear *
          (Math.log(d[ii].value) - Math.log(d[ii - 1].value));
        values.push(v);
        out.push({
          series: d[ii].series,
          date: d[ii].date,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  pca(cols: ICols = null): ILongData | IWideData {
    return this.compoundedAnnualRateOfChange(cols);
  }

  compoundedAnnualRateOfChange(cols: ICols = null): ILongData | IWideData {
    return this._transformGeneral(
      cols,
      this._oneCompoundedAnnualRateOfChange,
      "pca"
    );
  }

  _oneCompoundedAnnualRateOfChange(
    col: string
  ): { out: ILongVar; bds: [number, number] } {
    var out: ILongVar = <ILongVar>[],
      d: ILongVar = this.df[col],
      nObsPerYear: number = this.md[col].nObsPerYear,
      values: number[] = [];

    for (let ii = 1; ii < d.length; ii++) {
      if (d[ii].value != null && d[ii - 1].value) {
        let v =
          100 * (Math.pow(d[ii].value / d[ii - 1].value, nObsPerYear) - 1);
        values.push(v);
        out.push({
          date: d[ii].date,
          series: d[ii].series,
          value: v,
        });
      }
    }

    var bds: [number, number] = [Math.min(...values), Math.max(...values)];
    return { out, bds };
  }

  /**
   * Get the number of observations in a year.
   *
   * @todo: Rewrite this so that we can estimate number of observations per year even when have less than year of data, etc.
   * @param  {string} col [description]
   * @return {number}     [description]
   */
  _getNObsPerYear(col: string): (string | number)[] {
    var d: ITsData = this.df[col];

    // Average difference
    var diff =
      (d[d.length - 1].date.getTime() - d[0].date.getTime()) / d.length;
    if (diff >= 31535000000) {
      console.warn("Guessing this is annual but no full support guaranteed!");
      return [1, "A"];
    } else if (diff > 5227200000) {
      return [4, "Q"];
    } else if (diff > 1857600000) {
      return [12, "M"];
    } else if (diff > 950400000) {
      return [26, "BW"];
    } else if (diff > 345600000) {
      return [52, "W"];
    } else {
      var max = d
        .slice(1, 9)
        .reduce(
          (accum, row, ii) =>
            Math.max(accum, row.date.getTime() - d[ii].date.getTime()),
          -1e99
        );
      if (max > 96400000) {
        return [260, "D5"];
      } else {
        return [365, "D7"];
      }
    }
  }

  _transformGeneral(
    cols: ICols = null,
    transformer: (col: string) => { out: ILongVar; bds: [number, number] },
    trans: ITransforms
  ): ILongData | IWideData {
    cols = this._checkCols(cols);

    for (let ii = 0; ii < cols.length; ii++) {
      var tt = transformer(cols[ii]);
      this.dfw[cols[ii]] = this._aggregateGeneral(cols[ii], tt.out);
      this.md[cols[ii]].bds.y = tt.bds;
      this.trans[cols[ii]] = trans;
    }

    return this._make(cols);
  }

  _updateAllSeries() {
    var cols = this._checkCols();

    for (let ii = 0; ii < cols.length; ii++) {
      var sl = this._updateAllSeriesLookup(this.trans[cols[ii]], cols[ii]);
      this.dfw[cols[ii]] = this._aggregateGeneral(cols[ii], sl.out);
      this.md[cols[ii]].bds.y = sl.bds;
    }

    return this._make(cols);
  }

  _updateAllSeriesLookup(
    trans: ITransforms,
    col: string
  ): { out: ILongVar; bds: [number, number] } {
    switch (trans) {
      case "lin":
        return this._oneLevels(col);
      case "chg":
        return this._oneChange(col);
      case "ch1":
        return this._oneChangeFromYearAgo(col);
      case "pch":
        return this._onePercentChange(col);
      case "pc1":
        return this._onePercentChangeFromYearAgo(col);
      case "pca":
        return this._oneCompoundedAnnualRateOfChange(col);
      case "cch":
        return this._oneContinuouslyCompoundedRateOfChange(col);
      case "cca":
        return this._oneContinuouslyCompoundedAnnualRateOfChange(col);
      case "log":
        return this._oneLog(col);
      default:
        throw new Error("Invalid transform passed.");
    }
  }

  _checkCols(cols: ICols = null): string[] {
    var c: string[] =
      typeof cols === "string" ? [cols] : cols ? cols : this.cols;
    if (this.strict) {
      for (let ii = 0; ii < c.length; ii++) {
        if (this.cols.indexOf(c[ii]) < 0) {
          throw new Error(`Column name '${c[ii]}' invalid.`);
        }
      }
      return c;
    } else {
      return c;
    }
  }

  _aggregateGeneral(col: string, d: ILongVar): ILongVar {
    switch (this._agg) {
      case "any":
        return d;
      case "ANY":
        return d;
      case "m":
        return this._aggregateMonthly(col, d);
      case "M":
        return this._aggregateMonthly(col, d);
      case "q":
        return this._aggregateQuarterly(col, d);
      case "Q":
        return this._aggregateQuarterly(col, d);
      case "a":
        return this._aggregateAnnually(col, d);
      case "A":
        return this._aggregateAnnually(col, d);
      default:
        throw new Error(`Unknown aggregation ${this._agg} encountered.`);
    }
  }

  _aggregateMonthly(col: string, d: ILongVar): ILongVar {
    return this._aggregatePeriodFactory(
      col,
      d,
      ["m", "q", "a", "M", "Q", "A"],
      di => `${di.getMonth() + 1}-1-${di.getFullYear()}`
    );
  }

  _aggregateQuarterly(col: string, d: ILongVar): ILongVar {
    return this._aggregatePeriodFactory(
      col,
      d,
      ["q", "a", "Q", "A"],
      di =>
        `${(Math.floor(di.getMonth() / 3) + 1 - 1) * 3 +
          1}-1-${di.getFullYear()}`
    );
  }

  _aggregateAnnually(col: string, d: ILongVar): ILongVar {
    return this._aggregatePeriodFactory(
      col,
      d,
      ["a", "A"],
      di => `1-1-${di.getFullYear()}`
    );
  }

  _aggregatePeriodFactory(
    col: string,
    d: ILongVar,
    exempt: string[],
    dateFormatter: (d: Date) => string
  ): ILongVar {
    if (exempt.indexOf(this.md[col].freq) > -1) {
      return d;
    }

    var out = {},
      keys: string[] = [];

    for (let ii = 0; ii < d.length; ii++) {
      var dd = dateFormatter(d[ii].date);
      if (Object.keys(out).indexOf(dd) < 0) {
        out[dd] = [];
      }

      out[dd].push(d[ii].value);
    }

    return this._aggregateMethodGeneral(col, out);
  }

  _aggregateMethodGeneral(col: string, d: IStringTMap<number[]>): ILongVar {
    var func;
    switch (this._aggMethod) {
      case "avg":
        func = this._aggregateMethodAvg;
        break;
      case "sum":
        func = this._aggregateMethodSum;
        break;
      case "eop":
        func = this._aggregateMethodEOP;
        break;
      case "AVG":
        func = this._aggregateMethodAvg;
        break;
      case "SUM":
        func = this._aggregateMethodSum;
        break;
      case "EOP":
        func = this._aggregateMethodEOP;
        break;
      default:
        throw new Error(
          `Unknown aggregation method ${this._aggMethod} encountered.`
        );
    }

    return Object.keys(d).map(k => {
      return {
        date: new Date(k + " 10:00:00 GMT"),
        series: col,
        value: func(d[k]),
      };
    });
  }

  _aggregateMethodSum(d: number[]): number {
    return d.reduce((accum, di) => accum + di, 0);
  }

  _aggregateMethodAvg(d: number[]): number {
    return d.reduce((accum, di) => accum + di, 0) / d.length;
  }
  _aggregateMethodEOP(d: number[]): number {
    return d.slice(-1)[0];
  }

  _make(cols: string[]): ILongData | IWideData {
    // throw new Error("Not implemented in parent.");
    var out;
    switch (this._dShape) {
      case "long":
        this._w = this._updateWide(cols);
        out = this._makeLong();
        break;
      case "wide":
        out = this._makeWide();
        break;
      default:
        throw new Error(`Unknown data shape ${this._dShape} encountered.`);
    }

    this.v = out;
    return out;
  }

  _makeLong(): ILongData {
    return this.dfw;
  }

  _makeWide(): IWideData {
    var out = {} as IWideObject,
      keys: string[] = [],
      key: number,
      col: string;

    for (let kk = 0; kk < this.cols.length; kk++) {
      col = this.cols[kk];
      for (let ii = 0; ii < this.dfw[col].length; ii++) {
        let djj = this.dfw[col][ii].date.toString();
        if (!out[djj]) {
          out[djj] = { date: this.dfw[col][ii].date };
        }
        out[djj][col] = this.dfw[col][ii].value;
      }
    }

    var outArr: IWideData = Object.values(out);

    if (this.strict) {
      outArr.sort((a, b) => (a.date > b.date ? 1 : -1));
    }

    if (this.datesAs === "string") {
      return outArr.map(row => {
        // @ts-ignore Will always be a date
        row["date"] = row["date"].toLocaleDateString("en-US", {
          day: "2-digit",
          year: "numeric",
          month: "2-digit",
        });
        return row;
      });
    }

    this._w = out;
    return outArr;
  }

  _updateWide(cols: string[]): IWideObject {
    // @note Don't check cols because has already been done above

    if (!this._w) {
      this._makeWide();
      return this._w;
    }

    var w: IWideObject = Object.assign({}, this._w),
      col: string;

    for (let kk = 0; kk < cols.length; kk++) {
      col = cols[kk];
      for (let jj = 0; jj < this.dfw[col].length; jj++) {
        w[this.dfw[col][jj].date.toString()][col] = this.dfw[col][jj].value;
      }
    }

    return w;
  }
}
