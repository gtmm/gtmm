/**
 * Configuration files for package.
 *
 * On windows will save to somewhere like
 * C:\Users\us57144\.config\configstore
 */

import * as ConfigStore from "configstore";

import { PKG_NAME } from "./constants";

const Config = new ConfigStore(PKG_NAME, {
  lastUpdated: new Date(2018, 6, 1),
  downloadedSeries: [],
});

export default Config;
