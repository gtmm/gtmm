import * as Path from "path";
import * as FS from "fs";

import * as Promise from "bluebird";
import * as lunr from "lunr";
import * as JSONStream from "JSONStream";

import FredMetadata from "./FredMetadata";
import { Filenames } from "./utils";

import {
  ICategories,
  ISources,
  IReleases,
  ITags,
  ISeriess,
  IRelease,
  ISource,
  ISeries,
  ITag,
  ICategory,
} from "./FredMetadata.types";
import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
} from "./types";

import {
  ILunrTag,
  ILunrSource,
  ILunrSeries,
  ILunrRelease,
  ILunrCategory,
} from "./lunr.types";

export default class PackLunr {
  basePath: string;
  cachePath: string;

  constructor(basePath: string, cachePath: string) {
    this.basePath = basePath;
    this.cachePath = cachePath;
  }

  _fetchOutMemory(key: string, fields: string[]): Promise<any> {
    return new Promise((resolve, reject) => {
      var outFile: string = Filenames.lunr(key, this.basePath),
        cacheFile: string = Filenames.cache(key, this.cachePath);

      if (!FS.existsSync(cacheFile)) {
        var ei: Error = new Error(
          `The cache file: ${cacheFile} does not exist. Ensure that the FredMetadata command has been called.`
        );
        reject(ei);
      } else {
        var transformStream = JSONStream.parse("*");
        var inputStream = FS.createReadStream(cacheFile);

        lunr(function() {
          this.ref("_id");
          for (let ii = 0; ii < fields.length; ii++) {
            this.field(fields[ii]);
          }

          inputStream
            .pipe(transformStream)
            .on("data", data => {
              this.add(data);
            })
            .on("end", () => {
              var idx = this.build();
              FS.writeFile(
                outFile,
                JSON.stringify(idx.toJSON()),
                {},
                (err: Error): void => {
                  if (err) {
                    reject(err);
                    return;
                  }
                  resolve(idx);
                }
              );
            })
            .on("error", e => {
              reject(e);
            });
        });
      }
    });
  }

  _fetchInMemory(
    key: string,
    fields: string[],
    obj: IStringTMap<any> | null = null
  ): Promise<any> {
    const fetchSrc = (k: string, reject): IStringTMap<any> => {
      var cacheFile: string = Filenames.cache(key, this.cachePath);
      if (!FS.existsSync(cacheFile)) {
        var ei: Error = new Error(
          `The cache file: ${cacheFile} does not exist. Ensure that the FredMetadata command has been called.`
        );
        reject(ei);
      }

      return JSON.parse(FS.readFileSync(cacheFile).toString());
    };

    // MAIN FUNCTION
    return new Promise((resolve, reject) => {
      var idx: lunr.Index, src: IStringTMap<any>;

      src = obj ? obj : fetchSrc(key, reject);

      idx = lunr(function() {
        this.ref("_id");
        for (let ii = 0; ii < fields.length; ii++) {
          this.field(fields[ii]);
        }

        Object.keys(src).forEach(k => this.add(src[k]));
      });

      FS.writeFile(
        Filenames.lunr(key, this.basePath),
        JSON.stringify(idx),
        {},
        (err: Error): void => {
          if (err) reject(err);

          resolve(idx);
        }
      );
    });
  }

  series(obj: IStringTMap<any> | null = null): Promise<any> {
    var key = "series";
    var fields: string[] = ["title", "id"];

    if (obj) {
      return this._fetchInMemory(key, fields, obj);
    } else {
      return this._fetchOutMemory(key, fields);
    }
  }

  tags(obj: IStringTMap<any> | null = null): Promise<any> {
    var key: string = "tags";
    var fields: string[] = ["name", "groupId", "notes"];

    return this._fetchInMemory(key, fields, obj);
  }

  releases(obj: IStringTMap<any> | null = null): Promise<any> {
    var key: string = "releases";
    var fields: string[] = ["name", "link", "notes"];

    return this._fetchInMemory(key, fields, obj);
  }

  categories(obj: IStringTMap<any> | null = null): Promise<any> {
    var key: string = "categories";
    var fields: string[] = ["name", "notes"];

    return this._fetchInMemory(key, fields, obj);
  }

  sources(obj: IStringTMap<any> | null = null): Promise<any> {
    var key: string = "sources";
    var fields: string[] = ["name", "link", "notes"];

    return this._fetchInMemory(key, fields, obj);
  }

  all(fmd: FredMetadata | null = null): Promise<any> {
    if (fmd) {
      if (!Object.keys(fmd.fetched).every(f => fmd.fetched[f])) {
        throw new Error(
          "Not all elements in FredMetadata object present. Fetch them before proceeding further."
        );
      } else {
        return new Promise((resolve, reject) => {
          Promise.all([
            this.sources(fmd.sources),
            this.categories(fmd.categories),
            this.tags(fmd.tags),
            this.releases(fmd.releases),
          ])
            .then(() => {
              this.series(fmd.series)
                .then(() => {
                  this.tags(fmd.tags)
                    .then(() => resolve(true))
                    .catch(reject);
                })
                .catch(reject);
            })
            .catch(reject);
        });
      }
    } else {
      return Promise.all([
        this.sources(),
        this.categories(),
        this.tags(),
        this.releases(),
        this.series(),
      ]);
    }
  }
}
