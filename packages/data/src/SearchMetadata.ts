import * as FS from "fs";
import * as Path from "path";

import * as Promise from "bluebird";
import * as Datastore from "nedb";
import * as lunr from "lunr";

import { Filenames } from "./utils";
import * as C from "./constants";
import {
  IStringTMap,
  INumberTMap,
  IRateLimitProps,
  IEmptyObject,
  IAnyTMap,
} from "./types";
import {
  IDatabaseObject,
  ILunrIndices,
  INedbStores,
  loadDatabases,
} from "./loadDatabases";

type IReturnAs = "cursor" | "promise";

/**
 * Search the metadata for a given query.
 */
export default class SearchMetadata {
  basePath: string;
  idOnly: boolean;
  returnAs: IReturnAs;
  _db: INedbStores;
  _ln: ILunrIndices;

  constructor(
    basePath: string,
    idOnly: boolean,
    returnAs: IReturnAs,
    db: IDatabaseObject | null
  ) {
    this.basePath = basePath;
    this.idOnly = idOnly;
    this.returnAs = returnAs;

    if (db) {
      this._ln = db.lunr;
      this._db = db.nedb;
    } else {
      loadDatabases(this.basePath)
        .then(out => {
          this._ln = out.lunr;
          this._db = out.nedb;
        })
        .catch(err => {
          throw err;
        });
    }
  }

  _genericSearch(
    key: string,
    query: string,
    filter: IAnyTMap<any> | IEmptyObject | null,
    limit: number
  ): any {
    filter = filter || {};

    // Carry out the lunr search.
    var res: IStringTMap<any>[];

    if (this.returnAs === "promise") {
      return new Promise((resolve, reject) => {
        res = this._ln[key].search(query).slice(0, limit);

        if (!filter && this.idOnly) {
          resolve(
            res.map(ri => {
              return { _id: ri.ref, score: ri.score };
            })
          );
        } else {
          var matches: string[] = res.map(ri => ri.ref);
          this._db[key].find(
            { $and: [{ _id: { $in: matches } }, filter] },
            (err, docs) => {
              if (err) reject(err);
              if (this.idOnly) {
                resolve(
                  docs.map(d => {
                    return {
                      _id: d._id,
                      score: res[matches.indexOf(d._id)].score,
                    };
                  })
                );
              } else {
                resolve(
                  docs.map(d => {
                    d.score = res[matches.indexOf(d._id)].score;
                    return d;
                  })
                );
              }
            }
          );
        }
      });
    } else {
      res = this._ln[key].search(query).slice(0, limit);
      if (this.idOnly) {
        return res.map(ri => {
          return { _id: ri.ref, score: ri.score };
        });
      } else {
        var matches: string[] = res.map(ri => ri.ref);
        return this._db[key].find({
          $and: [{ _id: { $in: matches } }, filter],
        });
      }
    }
  }

  series(
    query: string,
    filter: IAnyTMap<any> | null = null,
    limit: number = Infinity
  ): any {
    return this._genericSearch("series", query, filter, limit);
  }

  tags(
    query: string,
    filter: IAnyTMap<any> | null = null,
    limit: number = Infinity
  ): any {
    return this._genericSearch("series", query, filter, limit);
  }

  releases(
    query: string,
    filter: IAnyTMap<any> | null = null,
    limit: number = Infinity
  ): any {
    return this._genericSearch("releases", query, filter, limit);
  }

  categories(
    query: string,
    filter: IAnyTMap<any> | null = null,
    limit: number = Infinity
  ): any {
    return this._genericSearch("categories", query, filter, limit);
  }

  sources(
    query: string,
    filter: IAnyTMap<any> | null = null,
    limit: number = Infinity
  ): any {
    return this._genericSearch("sources", query, filter, limit);
  }
}
