/**
 * Base webpack config used across other specific configs
 */

import path from 'path';
import webpack from 'webpack';
import { dependencies } from '../package.json';

export default {
  externals: [...Object.keys(dependencies || {})],

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          }
        }
      }
    ]
  },

  output: {
    path: path.join(__dirname, '..', 'app'),
    // https://github.com/webpack/webpack/issues/1114
    libraryTarget: 'commonjs2'
  },

  /**
   * Determine the array of extensions that should be used to resolve modules.
   */
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      'themes/default/assets/fonts': path.resolve(
        __dirname,
        '../node_modules/semantic-ui-css/themes/default/assets/fonts'
      ),
      'themes/default/assets/images': path.resolve(
        __dirname,
        '../node_modules/semantic-ui-css/themes/default/assets/images'
      ),
      '&redux': path.resolve(__dirname, '../src/redux'),
      '&utils': path.resolve(__dirname, '../src/utils'),
      '&constants': path.resolve(__dirname, '../src/constants'),
      '&components': path.resolve(__dirname, '../src/components'),
      '&src': path.resolve(__dirname, '../src')
    }
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production'
    }),

    new webpack.NamedModulesPlugin()
  ]
};
