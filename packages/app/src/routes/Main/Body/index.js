// @flow
import React from 'react';
import styled from 'styled-components';

import { Tab } from 'semantic-ui-react';
import ReactResizeDetector from 'react-resize-detector';
import SplitPane from 'react-split-pane';

const MainWrapper = styled.div.attrs({
  height: props => props.height + 'px' || '80%'
})`
  display: block;

  .Resizer.vertical {
    color: rgb(255, 255, 255);
    background-color: rgb(255, 255, 255);
    width: 10px;
    height: ${props => props.height};

    &:hover {
      cursor: ew-resize;
    }

    &:focus {
      cursor: ew-resize;
      background-color: rgb(235, 235, 235);
    }
  }
`;

const PanelCard = styled.div.attrs({
  height: props => props.height + 'px' || '80%'
})`
  box-shadow: rgb(204, 204, 204) 0px 1px 2px;
  height: ${props => props.height || '80%'};
  padding: 5px 10px 10px 10px;

  &.left {
    margin-left: 10px;
  }
  &.right {
    margin-right: 10px;
  }

  .ui.pointing.menu {
    border-radius: 0;
  }
`;

const TabPane = styled.div``;

const leftPanes = [
  {
    menuItem: 'Datasets',
    render: () => <TabPane>Tab 1 Content</TabPane>
  },
  {
    menuItem: 'Models',
    render: () => <TabPane>Tab 2 Content</TabPane>
  },
  {
    menuItem: 'Editor',
    render: () => <TabPane>Tab 3 Content</TabPane>
  },
  {
    menuItem: 'Reports',
    render: () => <TabPane>Tab 3 Content</TabPane>
  }
];

const rightPanes = [
  {
    menuItem: 'Data',
    render: () => <TabPane>Tab 1 Content</TabPane>
  },
  {
    menuItem: 'Console',
    render: () => <TabPane>Tab 2 Content</TabPane>
  },
  {
    menuItem: 'Plots',
    render: () => <TabPane>Tab 3 Content</TabPane>
  },
  {
    menuItem: 'Docs',
    render: () => <TabPane>Tab 3 Content</TabPane>
  }
];

type Props = {};
type State = {
  width: number,
  height: number
};

export default class Main extends React.Component<Props, State> {
  props: Props;

  state = {
    width: 1000,
    height: 500
  };

  render() {
    return (
      <MainWrapper height={this.state.height}>
        <ReactResizeDetector
          handleWidth
          handleHeight
          onResize={(width, height) => this.setState({ width, height })}
        >
          <SplitPane
            className="panels"
            split="vertical"
            minSize={50}
            defaultSize={this.state.width / 2 - 10}
          >
            <PanelCard className="left" height={this.state.height}>
              <Tab menu={{ pointing: true }} panes={leftPanes} />
            </PanelCard>
            <PanelCard className="right" height={this.state.height}>
              <Tab menu={{ pointing: true }} panes={rightPanes} />
            </PanelCard>
          </SplitPane>
        </ReactResizeDetector>
      </MainWrapper>
    );
  }
}
