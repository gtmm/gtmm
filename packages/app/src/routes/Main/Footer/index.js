// @flow
import React from 'react';
import styled from 'styled-components';

const Footer = styled.div`
  -webkit-box-shadow: 0px -1px 2px 0px rgb(204, 204, 204);
  -moz-box-shadow: 0px -1px 2px 0px rgb(204, 204, 204);
  box-shadow: 0px -1px 2px 0px rgb(204, 204, 204);
  padding: 0px 10px;
`;

export default Footer;
