// @flow
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Modal, Button, Icon } from 'semantic-ui-react';

import { toggle } from '&redux/actions/portalVisibility';
import { Dispatch } from '&redux/types';
import { ModalHeader } from '&components/utils';
import SettingsInner from '&components/Settings';

type Props = { toggle: ({}) => null, open: boolean };

class Settings extends React.Component<Props> {
  props: Props;

  render() {
    const { open, toggle, ...otherProps } = this.props;
    return (
      <Modal
        open={open}
        onClose={toggle}
        content={<SettingsInner />}
        closeIcon
      />
    );
  }
}

const mapStateToProps = state => {
  return { open: state.portalVisibility.settings };
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    toggle: () => dispatch(toggle('settings'))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);
