// @flow
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Modal, Button, Icon } from 'semantic-ui-react';

import { toggle } from '&redux/actions/portalVisibility';
import { Dispatch } from '&redux/types';
import { ModalHeader } from '&components/utils';

type Props = { toggle: ({}) => null, open: boolean };

class About extends React.Component<Props> {
  props: Props;

  render() {
    const { open, toggle, ...otherProps } = this.props;
    return (
      <Modal
        open={open}
        onClose={toggle}
        header={
          <ModalHeader>
            <Icon name="info" />
            About
          </ModalHeader>
        }
        content="Not yet implemented"
        closeIcon
      />
    );
  }
}

const mapStateToProps = state => {
  return { open: state.portalVisibility.about };
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    toggle: () => dispatch(toggle('about'))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(About);
