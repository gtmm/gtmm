// @flow
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import {
  Icon,
  Sidebar as Sidebar_,
  Segment,
  Menu,
  Input
} from 'semantic-ui-react';

import { toggle } from '&redux/actions/portalVisibility';
import { Dispatch } from '&redux/types';

type Props = { toggle: ({}) => null, visible: boolean };

class MainSidebar extends React.Component<Props> {
  props: Props;

  render() {
    const { visible, toggle, ...otherProps } = this.props;
    return (
      <Sidebar_
        as={Segment}
        animation="overlay"
        icon="labeled"
        inverted
        onHide={() => toggle('mainSidebar')}
        vertical
        visible={visible}
        duration={300}
      >
        <Menu vertical secondary inverted fluid>
          <Menu.Item as="h3">GTMM</Menu.Item>
          <Menu.Item as="a" onClick={() => toggle('settings')}>
            <Icon name="cog" />
            Settings
          </Menu.Item>
          <Menu.Item as="a" onClick={() => toggle('about')}>
            <Icon name="info" />
            About
          </Menu.Item>
          <Menu.Item>
            <Input
              icon={{ name: 'search', circular: false, link: true }}
              placeholder="Search the docs... (Non-functional)"
              onChange={() => alert('Not functional yet!')}
              disabled
            />
          </Menu.Item>
        </Menu>
      </Sidebar_>
    );
  }
}

const mapStateToProps = state => {
  return { visible: state.portalVisibility.mainSidebar };
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    toggle: portal => dispatch(toggle(portal))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainSidebar);
