// @flow
import React from 'react';

import Sidebar from './Sidebar';
import Settings from './Settings';
import About from './About';

export default class Portals extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Sidebar />
        <Settings />
        <About />
      </React.Fragment>
    );
  }
}
