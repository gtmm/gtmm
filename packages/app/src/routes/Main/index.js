// @flow
import React from 'react';
import styled from 'styled-components';

import Header from './Header';
import Body from './Body';
import Footer from './Footer';
import Portals from './Portals';

const Layout = styled.div`
  display: grid;
  grid-template-rows: 45px auto 20px;
  width: 100vw;
  height: 100vh;
  row-gap: 10px;
`;

type Props = {};
type State = {};

export default class MainPage extends React.Component<Props, State> {
  props: Props;

  render() {
    return (
      <React.Fragment>
        <Layout className="main-page route">
          <Header />
          <Body />
          <Footer />
        </Layout>
        <Portals />
      </React.Fragment>
    );
  }
}
