// @flow
import React from 'react';
import styled from 'styled-components';

import Sidebar from './Sidebar';
import Title from './Title';
import Workspace from './Workspace';

const HeaderWrapper = styled.div`
  padding: 3px 10px 3px 3px;

  display: grid;
  grid-template-columns: 45px 100px auto 45px;

  -webkit-box-shadow: 0px 1px 2px 0px rgb(204, 204, 204);
  -moz-box-shadow: 0px 1px 2px 0px rgb(204, 204, 204);
  box-shadow: 0px 1px 2px 0px rgb(204, 204, 204);
`;

const Header = ({}) => (
  <HeaderWrapper>
    <Sidebar />
    <Title />
    <div />
    <Workspace />
  </HeaderWrapper>
);

export default Header;
