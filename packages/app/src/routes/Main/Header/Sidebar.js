// @flow
import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { Icon } from 'semantic-ui-react';

import { Dispatch } from '&redux/types';
import { toggle } from '&redux/actions/portalVisibility';

const MainSidebarWrapper = styled.div`
  font-size: 28px;
  text-align: center;
  vertical-align: middle;
  padding-top: calc(50% - 14px);
  border-radius: 50px;

  i {
    margin: 0;
  }

  &:hover {
    cursor: pointer;
    background-color: rgb(240, 240, 240);
    box-shadow: 0px 1px 3px 0px rgb(204, 204, 204);
    transition: box-shadow 0.2s ease-in, background 0.2s ease-in;
  }
`;

type Props = { toggle: ({}) => null };

class Sidebar extends React.Component<Props> {
  props: Props;

  render() {
    return (
      <MainSidebarWrapper onClick={this.props.toggle}>
        <Icon name="bars" />
      </MainSidebarWrapper>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    toggle: () => dispatch(toggle('mainSidebar'))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
