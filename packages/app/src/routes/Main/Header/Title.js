import React from 'react';
import styled from 'styled-components';

const Title = styled.div`
  font-size: 28px;
  padding-top: 7px;
  font-weight: 900;
  text-align: middle;
  user-select: none;
`;

export default class GtmmTitle extends React.Component {
  render() {
    return <Title>GTMM</Title>;
  }
}
