import React from 'react';
import { Switch, Route } from 'react-router';

import routes from '&constants/routes';
import Main from './Main';

export default () => (
  <React.Fragment>
    <Switch>
      <Route path={routes.MAIN} component={Main} />
    </Switch>
  </React.Fragment>
);
