import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import 'semantic-ui-css/semantic.min.css';

import App from './App.js';
import { configureStore, history } from '&redux/store';

import './app.global.scss';

const store = configureStore();

render(
  <AppContainer>
    <App store={store} history={history} />
  </AppContainer>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./App.js', () => {
    // eslint-disable-next-line global-require
    const NextApp = require('./App.js').default;
    render(
      <AppContainer>
        <NextApp store={store} history={history} />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}
