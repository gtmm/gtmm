// @flow
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Icon, Segment, Input, Header } from 'semantic-ui-react';

import { toggle } from '&redux/actions/portalVisibility';
import { Dispatch } from '&redux/types';

import SettingsMenu from './Menu';
import Panel from './Panel';

const SettingsGrid = styled.div`
  padding: 15px;
  width: 100%;

  display: grid;
  grid-template-rows: 0.75fr auto;
  grid-template-columns: 210px auto;
  grid-template-areas:
    'header header'
    'menu panel';
  column-gap: 10px;

  .menu {
    grid-area: menu;
    border-radius: 0;
    margin-bottom: 0;
  }

  .panel {
    grid-area: panel;
    width: 100%;
    border-radius: 0;
  }

  .header {
    grid-area: header;
  }
`;

type Props = {};
type State = { activeItem: string };

class Settings extends React.Component<Props, State> {
  props: Props;
  state = { activeItem: 'general' };

  handleClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem, ...otherState } = this.state;

    return (
      <SettingsGrid>
        <Header as="h2" className="header">
          <Icon name="cog" />
          Settings
        </Header>
        <SettingsMenu
          handleClick={this.handleClick}
          activeItem={activeItem}
          className="menu"
        />
        <Panel activeItem={activeItem} />
      </SettingsGrid>
    );
  }
}

export default Settings;

// const mapStateToProps = state => {
//   return {};
// };
// const mapDispatchToProps = (dispatch: Dispatch) => {
//   return {};
// };

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Settings);
/*
        header={
          <ModalHeader>
            <Icon name="cog" />
            Settings
          </ModalHeader>
        }
 */
