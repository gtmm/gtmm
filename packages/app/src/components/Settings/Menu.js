import React from 'react';

import { Icon, Segment, Menu, Input, Header } from 'semantic-ui-react';

const SettingsMenu = ({ activeItem, handleClick, ...otherProps }) => (
  <Menu vertical activeIndex={activeItem} {...otherProps}>
    <Menu.Item>
      <Menu.Header>GTMM</Menu.Header>

      <Menu.Menu>
        <Menu.Item
          name="general"
          active={activeItem === 'general'}
          onClick={handleClick}
        />
        <Menu.Item
          name="appearance"
          active={activeItem === 'appearance'}
          onClick={handleClick}
        />
        <Menu.Item
          name="API Keys"
          active={activeItem === 'API Keys'}
          onClick={handleClick}
        />
      </Menu.Menu>
    </Menu.Item>

    <Menu.Item>
      <Menu.Header>Python</Menu.Header>

      <Menu.Menu>
        <Menu.Item
          name="Configuration"
          active={activeItem === 'Configuration'}
          onClick={handleClick}
        />
        <Menu.Item
          name="Installation"
          active={activeItem === 'Installation'}
          onClick={handleClick}
        />
      </Menu.Menu>
    </Menu.Item>

    <Menu.Item>
      <Menu.Header>Advanced</Menu.Header>

      <Menu.Menu>
        <Menu.Item
          name="Development"
          active={activeItem === 'Development'}
          onClick={handleClick}
        />
        <Menu.Item
          name="Danger Zone"
          active={activeItem === 'Danger Zone'}
          onClick={handleClick}
        />
      </Menu.Menu>
    </Menu.Item>
  </Menu>
);

export default SettingsMenu;
