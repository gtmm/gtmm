// @flow
import React from 'react';
import { Icon, Segment, Input, Header, Card } from 'semantic-ui-react';

import InvalidSettingsPanel from './InvalidSettingsPanel';

type Props = { activeItem: string };
type State = {};

export default class Panel extends React.Component<Props, State> {
  props: Props;
  state = {};

  handleClick = name => this.setState({ activeItem: name });

  render() {
    const { activeItem, ...otherProps } = this.props;

    switch (activeItem) {
      case 'python':
        return null;
        break;
      default:
        return <InvalidSettingsPanel {...otherProps} />;
        break;
    }
  }
}
