// @flow
import type { GetState, Dispatch } from '&redux/types';

export const TOGGLE_PORTAL = 'TOGGLE_PORTAL';

type TogglePortalAction = {
  type: 'TOGGLE_PORTAL',
  portal: string
};

export function toggle(portal: string): TogglePortalAction {
  return {
    type: TOGGLE_PORTAL,
    portal: portal
  };
}

type Action = TogglePortalAction;
