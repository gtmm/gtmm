// @flow
import update from 'immutability-helper';

import { TOGGLE_PORTAL } from '../actions/portalVisibility';
import type { Action } from '&redux/types';

const defaultState: { mainSidebar: boolean, settings: boolean } = {
  mainSidebar: false,
  settings: false,
  about: false
};

export default function portalVisibility(
  state: Object = defaultState,
  action: Action
) {
  switch (action.type) {
    case TOGGLE_PORTAL:
      return update(state, { $toggle: [action.portal] });
    default:
      return state;
  }
}
