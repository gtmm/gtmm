import * as React from "react";
import * as shortid from "shortid";
import update from "immutability-helper";

import { TsData, ITsDataInput } from "@gtmm/data/build/src/TsData";
import {
  Button,
  Header,
  Image,
  Menu,
  Ref,
  Segment,
  Sidebar,
  Icon,
} from "semantic-ui-react";

import colorWheel from "./components/colorWheel";
import {
  TsDataGrid,
  TsDataGridSeries,
  TsDataGridPlot,
} from "./components/grid";
import D3Plot from "./components/D3Plot/index";
import SeriesContainer from "./components/SeriesContainer/index";
import SeriesMenu from "./components/SeriesMenu/index";
import SeriesInfoModal from "./components/SeriesInfoModal/index";
import { ISeriesMetadata } from "../types";
import {
  IDataAxis,
  ITransforms,
  IBds,
  IOnTransformCommand,
  IOnScaleCommand,
  IOnAddSeriesCommand,
  IOnRemoveSeriesCommand,
  IOnSwitchAxisCommand,
  ILastCommand,
  IOnInitCommand,
} from "./types";

interface IObs {
  date: Date;
  series: string;
  value: number;
}

const CANONICAL_MEASURES = {
  w: (16 / 9) * 300,
  h: 300,
  m: { l: 30, r: 30, b: 30, t: 15 },
  fontSize: 10,
  heightShare: 0.85,
};

interface ITsPlotProps {
  /**
   * Data to display.
   *
   * @example
   *   [{date: "1-1-1900", series: "USMCA", value: 3},
   *    {date: "1-2-1900", series: "USMCA", value: 3.24},
   *    ...
   *    {date: "1-1-1776", series: "BEATE", value: 43424},
   *    ...
   *   ]
   * @type {[type]}
   */
  data: ITsDataInput;

  metadata: ISeriesMetadata;

  height: number;

  width: number;
}

interface ITsPlotState {
  bds: IBds;
  dims: {
    w: number;
    h: number;
    m: { l: number; r: number; t: number; b: number };
    fontSize: number;
  };
  colors: { [key: string]: string };

  /**
   * Current data that is hovered by the cursor.
   * @type {Object}
   */
  _hoverData: { [key: string]: number | Date | null };

  _downbarVisible: boolean;

  _focusedSeriesDownbar: string;

  _infoModalOpen: boolean;

  _axisScaledToFit: { r: string; l: string };

  _dataAxis: IDataAxis;

  _lastCommand: ILastCommand;
}

export default class TsPlot extends React.Component<
  ITsPlotProps,
  ITsPlotState
> {
  public segmentRef = undefined;
  public df: TsData;

  public static defaultProps = {
    height: 300,
    width: (16 / 9) * 300,
  };

  constructor(props: ITsPlotProps) {
    super(props);

    this.initializeData = this.initializeData.bind(this);
    this.getAxisBoundsY = this.getAxisBoundsY.bind(this);
    this.getAxisBoundsX = this.getAxisBoundsX.bind(this);
    this.getPlotDims = this.getPlotDims.bind(this);

    this.handleToggleDownbar = this.handleToggleDownbar.bind(this);
    this.handleSegmentRef = this.handleSegmentRef.bind(this);

    this.onTransform = this.onTransform.bind(this);
    this.onInfo = this.onInfo.bind(this);
    this.onInfoModalOpen = this.onInfoModalOpen.bind(this);
    this.onScale = this.onScale.bind(this);
    this.onAddSeries = this.onAddSeries.bind(this);
    this.onRemoveSeries = this.onRemoveSeries.bind(this);
    this.onSwitchAxis = this.onSwitchAxis.bind(this);
    this.onHoverGraph = this.onHoverGraph.bind(this);

    this.getSeriesAxis = this.getSeriesAxis.bind(this);

    this.df = new TsData(props.data, "long", "any", "avg", false, "date");
    this.state = this.initializeData(props.data);
  }

  /**
   * Given new data create the objects needed by the state to plot it.
   * @param  {[type]} data: ITsDataInput  [description]
   * @return {[type]}       [description]
   */
  private initializeData(data: ITsDataInput): ITsPlotState {
    const df = this.df;

    var dims = this.getPlotDims();

    var cw = colorWheel();
    var colors = df.cols.reduce((accum, col) => {
      accum[col] = cw.next().value;
      return accum;
    }, {});

    var _hoverData = {};
    var _downbarVisible = false;
    var _focusedSeriesDownbar = df.cols[0];
    var _infoModalOpen = false;
    var _lastCommand: IOnInitCommand = {
      timestamp: new Date(),
      name: "init" as "init",
      info: {},
    };
    var _dataAxis = { l: df.cols, r: [] as string[] };
    var _axisScaledToFit = { r: "_all", l: "_all" };

    // (y bounds are same on init)
    var ybds = this.getAxisBoundsY("_all", "l", _dataAxis);
    var bds = {
      x: this.getAxisBoundsX("_all", _dataAxis),
      y: { r: ybds, l: ybds },
    };

    return {
      _lastCommand,
      colors,
      bds,
      dims,
      _hoverData,
      _downbarVisible,
      _focusedSeriesDownbar,
      _infoModalOpen,
      _axisScaledToFit,
      _dataAxis,
    };
  }

  /**
   * Get the overall bounds on the axis as well as the ideal bounds for each column found in the dataset.
   *
   * Called at initialization and whenever data is changed.
   * @return {[type]} [description]
   */
  private getAxisBoundsY(
    type: string = "_all",
    axis: "r" | "l" = "l",
    dataAxis_: IDataAxis | null = null
  ): number[] {
    var bds = [0, 0];

    if (type === "_all") {
      var dataAxis = dataAxis_ || this.state._dataAxis;
      if (dataAxis[axis].length > 0) {
        // @ts-ignore
        bds = dataAxis[axis].reduce(
          (ac, col) => {
            var bc = this.df.md[col].bds.y;
            ac = [Math.min(ac[0], bc[0]), Math.max(ac[1], bc[1])];
            return ac;
          },
          [1e99, -1e99]
        );
      } else {
        // If nothing plotted on that side set it equal to that for the opposite axis.
        var otherAxis = dataAxis[axis === "l" ? "r" : "l"];
        if (otherAxis.length > 0) {
          bds = otherAxis.reduce(
            (ac, col) => {
              var bc = this.df.md[col].bds.y;
              ac = [Math.min(ac[0], bc[0]), Math.max(ac[1], bc[1])];
              return ac;
            },
            [1e99, -1e99]
          );
        } else {
          bds = [0, 1];
        }
      }
    } else {
      bds = this.df.md[type].bds.y;
    }

    bds = [bds[0] * 0.95, bds[1] * 1.05];

    return bds;
  }

  private getAxisBoundsX(
    type: string = "_all",
    dataAxis_: IDataAxis | null = null
  ): Date[] {
    var bds = [new Date(), new Date()];

    if (type === "_all") {
      var dataAxis = dataAxis_ || this.state._dataAxis;
      var visibleSeries = dataAxis.r.concat(dataAxis.l);
      // @ts-ignore
      bds = visibleSeries.reduce(
        (ac, col) => {
          var bc = this.df.md[col].bds.x;
          ac = [
            Math.min(ac[0], bc[0].getTime()),
            Math.max(ac[1], bc[1].getTime()),
          ];
          return ac;
        },

        [new Date("10000-01-01").getTime(), new Date("1000-01-01").getTime()]
      );

      // @ts-ignore
      bds = bds.map(bx => new Date(bx));
    } else {
      bds = this.df.md[type].bds.x;
    }
    return bds;
  }

  /**
   * Resizes width, height, margins, font size, and grid whenever there is a height/width change.
   * @return {[type]} [description]
   */
  private getPlotDims() {
    const { w, h, m, fontSize, heightShare } = CANONICAL_MEASURES;
    var dims = {
      w: -99,
      h: -99,
      m: { l: -99, r: -99, t: -99, b: -99 },
      fontSize: -99,
    };

    var w2 = this.props.width;
    var h2 = heightShare * this.props.height;
    dims.m = {
      l: m.l * (w2 / w),
      r: m.r * (w2 / w),
      b: m.b * (h2 / h),
      t: m.t * (h2 / h),
    };
    dims.w = w2 - dims.m.l - dims.m.r;
    dims.h = h2 - dims.m.t - dims.m.b;

    // Not clear what needs to be done with font size scaling yet.
    dims.fontSize = fontSize;

    return dims;
  }

  private handleToggleDownbar() {
    this.setState({ _downbarVisible: !this.state._downbarVisible });
  }
  private handleSegmentRef(c) {
    // @ts-ignore
    this.segmentRef = c;
  }

  private getSeriesAxis(s: string): "r" | "l" {
    return this.state._dataAxis.l.indexOf(s) < 0 ? "r" : "l";
  }

  private onInfo(s: string): void {
    this.setState({ _downbarVisible: true, _focusedSeriesDownbar: s });
  }

  private onInfoModalOpen(s: string): void {
    this.setState({ _infoModalOpen: true, _focusedSeriesDownbar: s });
  }

  private onTransform(s: string, t: ITransforms): void {
    this.df[t](s);
    var axis = this.getSeriesAxis(s);

    var _lastCommand: IOnTransformCommand = {
      name: "onTransform",
      info: { series: s, axis, resetBds: false },
      timestamp: new Date(),
    };

    var bds = this.state.bds;
    if (this.state._axisScaledToFit[axis] === s) {
      bds = update(bds, {
        y: {
          [axis]: {
            $set: this.getAxisBoundsY(s, axis),
          },
        },
      });
      _lastCommand.info.resetBds = true;
    }

    this.setState({
      bds,
      _lastCommand,
    });
  }

  private onScale(s: string): void {
    var axis = this.getSeriesAxis(s);

    var _lastCommand: IOnScaleCommand = {
      name: "onScale",
      info: { axis },
      timestamp: new Date(),
    };

    var _axisScaledToFit = update(this.state._axisScaledToFit, {
      [axis]: { $set: s },
    });

    var bds = update(this.state.bds, {
      y: {
        [axis]: {
          $set: this.getAxisBoundsY(s, axis),
        },
      },
    });

    this.setState({
      bds,
      _axisScaledToFit,
      _lastCommand,
    });
  }

  private onAddSeries(s: string): void {
    var _dataAxis = update(this.state._dataAxis, { l: { $push: [s] } });

    var _lastCommand: IOnAddSeriesCommand = {
      name: "onAddSeries",
      info: { axis: "l", resetBds: false, series: s },
      timestamp: new Date(),
    };

    var bds = this.state.bds;
    if (this.state._axisScaledToFit["l"] === "_all") {
      bds = update(bds, {
        y: {
          l: {
            $set: this.getAxisBoundsY("_all", "l", _dataAxis),
          },
        },
      });
      _lastCommand.info.resetBds = true;
    }

    this.setState({ _dataAxis, _lastCommand, bds });
  }

  private onRemoveSeries(s: string): void {
    var axis = this.getSeriesAxis(s);

    var _lastCommand: IOnRemoveSeriesCommand = {
      name: "onRemoveSeries",
      info: { series: s, axis, resetBds: false },
      timestamp: new Date(),
    };

    var idx = this.state._dataAxis[axis].indexOf(s);
    var _dataAxis = update(this.state._dataAxis, {
      [axis]: { $splice: [[idx, 1]] },
    });

    var _axisScaledToFit = this.state._axisScaledToFit;
    if (_axisScaledToFit[axis] === s) {
      _axisScaledToFit[axis] = "_all";
    }

    var bds = this.state.bds;
    if (_dataAxis[axis].length === 0) {
      bds = update(bds, {
        y: { [axis]: { $set: this.getAxisBoundsY("_all", axis) } },
      });
      _lastCommand.info.resetBds = true;
    }

    this.setState({ _dataAxis, _axisScaledToFit, bds, _lastCommand });
  }

  private onSwitchAxis(s: string): void {
    var { bds, _dataAxis, _axisScaledToFit, ...otherState } = this.state;

    var oldAxis = this.getSeriesAxis(s);
    var newAxis: "l" | "r" = oldAxis === "l" ? "r" : "l";
    var _lastCommand = {
      name: "onSwitchAxis" as "onSwitchAxis",
      timestamp: new Date(),
      info: { series: s, oldAxis, newAxis, resetBds: { l: false, r: false } },
    };

    var idx = _dataAxis[oldAxis].indexOf(s);
    var _dataAxis = update(_dataAxis, {
      [oldAxis]: { $splice: [[idx, 1]] },
      [newAxis]: { $push: [s] },
    });

    var bdsYold = bds.y[oldAxis];
    if (
      _dataAxis[oldAxis].length === 0 ||
      _axisScaledToFit[oldAxis] === "_all"
    ) {
      bdsYold = this.getAxisBoundsY("_all", oldAxis, _dataAxis);
      _lastCommand.info.resetBds[oldAxis] = true;
    }

    var bdsYnew = bds.y[newAxis];
    if (_axisScaledToFit[newAxis] === "_all") {
      bdsYnew = this.getAxisBoundsY("_all", newAxis, _dataAxis);
      _lastCommand.info.resetBds[newAxis] = true;
    }

    bds = update(bds, {
      y: { [oldAxis]: { $set: bdsYold }, [newAxis]: { $set: bdsYnew } },
    });

    if (_axisScaledToFit[oldAxis] === s) {
      _axisScaledToFit = update(_axisScaledToFit, {
        [oldAxis]: { $set: "_all" },
      });
    }

    this.setState({ _dataAxis, _axisScaledToFit, bds, _lastCommand });
  }

  private onHoverGraph(d: { [keys: string]: null | Date | number }): void {
    this.setState({ _hoverData: d });
  }

  public render() {
    const _data = { long: this.df.long, wide: this.df.wide };

    return (
      <React.Fragment>
        <Sidebar.Pushable as={"div"}>
          {this.segmentRef && (
            <SeriesMenu
              selectedSeries={this.state._focusedSeriesDownbar}
              visible={this.state._downbarVisible}
              metadata={this.props.metadata}
              dataAxis={this.state._dataAxis}
              axisScaledTo={this.state._axisScaledToFit}
              trans={this.df.trans}
              onAddSeries={this.onAddSeries}
              onRemove={this.onRemoveSeries}
              onTransform={this.onTransform}
              onScale={this.onScale}
              onSwitchAxis={this.onSwitchAxis}
              onClose={this.handleToggleDownbar}
              onInfoModalOpen={this.onInfoModalOpen}
              onSelectedSeriesChange={s =>
                this.setState({ _focusedSeriesDownbar: s })
              }
              // @ts-ignore
              target={this.segmentRef}
            />
          )}
          <Ref innerRef={this.handleSegmentRef}>
            <Sidebar.Pusher>
              <Segment basic>
                <Header as="h3">Application Content</Header>
                <Image src="https://react.semantic-ui.com/images/wireframe/paragraph.png" />
              </Segment>
            </Sidebar.Pusher>
          </Ref>
        </Sidebar.Pushable>
        {/*
          // @ts-ignore */}
        <TsDataGrid
          className="ts-plot"
          height={this.props.height}
          width={this.props.width}
        >
          <TsDataGridSeries>
            {" "}
            <SeriesContainer
              dataAxis={this.state._dataAxis}
              colors={this.state.colors}
              values={this.state._hoverData}
              metadata={this.props.metadata}
              onClick={this.onInfo}
            />
          </TsDataGridSeries>
          <TsDataGridPlot className="plot">
            {" "}
            <D3Plot
              data={_data}
              dataAxis={this.state._dataAxis}
              bds={this.state.bds}
              colors={this.state.colors}
              h={this.state.dims.h}
              w={this.state.dims.w}
              m={this.state.dims.m}
              fontSize={this.state.dims.fontSize}
              lastCommand={this.state._lastCommand}
              onHover={this.onHoverGraph}
            />
          </TsDataGridPlot>
        </TsDataGrid>
        <SeriesInfoModal
          isOpen={this.state._infoModalOpen}
          series={this.state._focusedSeriesDownbar}
          md={this.props.metadata[this.state._focusedSeriesDownbar]}
          onClose={() => this.setState({ _infoModalOpen: false })}
        />
      </React.Fragment>
    );
  }
}
