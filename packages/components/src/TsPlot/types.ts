export interface IDataAxis {
  l: string[];
  r: string[];
}

export type ITransforms =
  | "lin"
  | "chg"
  | "ch1"
  | "pch"
  | "pc1"
  | "cch"
  | "cca"
  | "pca"
  | "log";

export interface IBds {
  x: Date[];
  y: { l: number[]; r: number[] };
}

export interface IDataAxis {
  l: string[];
  r: string[];
}

export type IAxisTypes = "l" | "r";

export interface IOnTransformCommand {
  name: "onTransform";
  info: { series: string; axis: IAxisTypes; resetBds: boolean };
  timestamp: Date;
}

export interface IOnScaleCommand {
  name: "onScale";
  info: { axis: IAxisTypes };
  timestamp: Date;
}

export interface IOnAddSeriesCommand {
  name: "onAddSeries";
  info: { axis: "l"; resetBds: boolean; series: string };
  timestamp: Date;
}

export interface IOnRemoveSeriesCommand {
  name: "onRemoveSeries";
  info: { series: string; axis: IAxisTypes; resetBds: boolean };
  timestamp: Date;
}

export interface IOnSwitchAxisCommand {
  name: "onSwitchAxis";
  info: {
    series: string;
    oldAxis: IAxisTypes;
    newAxis: IAxisTypes;
    resetBds: { l: boolean; r: boolean };
  };
  timestamp: Date;
}

export interface IOnInitCommand {
  name: "init";
  timestamp: Date;
  info: {};
}

export type ILastCommand =
  | IOnTransformCommand
  | IOnScaleCommand
  | IOnAddSeriesCommand
  | IOnRemoveSeriesCommand
  | IOnSwitchAxisCommand
  | IOnInitCommand;
