import * as React from "react";
import styled from "styled-components";
import {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
  Card,
  Grid,
  Table,
  Placeholder,
  Divider,
  Modal,
} from "semantic-ui-react";

import { ISeriesMetadata } from "../../../types";
import { IDataAxis, ITransforms } from "../../types";
import SeriesTable from "./SeriesTable";
import SeriesInfo from "./SeriesInfo";

const SidebarWrapper = styled.div`
  background-color: white;

  .ui.divider {
    margin: 5px;
  }
`;

interface ISeriesMenuProps {
  selectedSeries: string;
  visible: boolean;
  metadata: ISeriesMetadata;
  axisScaledTo: { r: string; l: string };
  dataAxis: IDataAxis;
  trans: { [keys: string]: ITransforms };
  onTransform: (s: string, t: string) => void;
  onScale: (s: string) => void;
  onRemove: (s: string) => void;
  onSwitchAxis: (s: string) => void;
  onAddSeries: (s: string) => void;
  onClose: (s: any) => void;
  onInfoModalOpen: (s: string) => void;
  onSelectedSeriesChange: (s: string) => void;
  target?: any;
}

interface ISeriesMenuState {}

export default class SeriesMenu extends React.Component<
  ISeriesMenuProps,
  ISeriesMenuState
> {
  constructor(props: ISeriesMenuProps) {
    super(props);
  }

  render() {
    const {
      selectedSeries,
      visible,
      metadata,
      dataAxis,
      trans,
      axisScaledTo,
      onTransform,
      onScale,
      onRemove,
      onSwitchAxis,
      onAddSeries,
      onClose,
      onInfoModalOpen,
      onSelectedSeriesChange,
      target,

      ...otherProps
    } = this.props;

    return (
      <React.Fragment>
        <SidebarWrapper>
          <Sidebar
            as={Card}
            animation="overlay"
            visible={visible}
            onHide={onClose}
            target={target}
            direction="top"
            raised
            fluid
          >
            <SeriesInfo
              series={selectedSeries}
              md={metadata[selectedSeries]}
              onShowMore={onInfoModalOpen}
              onClose={onClose}
            />
            <Card.Content>
              <Segment>
                <SeriesTable
                  selectedSeries={selectedSeries}
                  trans={trans}
                  axisScaledTo={axisScaledTo}
                  dataAxis={dataAxis}
                  onTransform={onTransform}
                  onScale={onScale}
                  onRemove={onRemove}
                  onSwitchAxis={onSwitchAxis}
                  onActive={s => onSelectedSeriesChange(s)}
                  onInfo={s => onInfoModalOpen(s)}
                />
              </Segment>
            </Card.Content>
          </Sidebar>
        </SidebarWrapper>
      </React.Fragment>
    );
  }
}
