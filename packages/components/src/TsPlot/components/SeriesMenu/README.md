```js
const React = require("react");
const testMetadata = require("../../../tools/sampleData/test-metadata-1.json");

const {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
} = require("semantic-ui-react");

const randomArrayElement = arr => arr[Math.floor(Math.random() * arr.length)];

const transforms = [
  "lin",
  "chg",
  "ch1",
  "pch",
  "pc1",
  "cch",
  "cca",
  "pca",
  "log",
];
const setPlotData = s => {
  return {
    [s]: {
      trans: randomArrayElement(transforms),
      axis: randomArrayElement(["right", "left"]),
      scaledToAxis: false,
    },
  };
};

const plotMetadata = Object.keys(testMetadata).reduce(
  (ac, s) => Object.assign(ac, setPlotData(s)),
  {}
);

plotMetadata["DBKAC"]["axis"] = "right";
plotMetadata["DBKAC"]["scaledToAxis"] = true;
plotMetadata["EXDNUS"]["axis"] = "left";
plotMetadata["EXDNUS"]["scaledToAxis"] = true;
plotMetadata["EXDNUS"]["trans"] = "cca";

class Ex1 extends React.Component {
  constructor(props) {
    super(props);

    this.state = { selectedSeries: "POP", visible: true };
  }

  render() {
    return (
      <Sidebar.Pushable as={Segment} style={{ height: "500px" }}>
        <SeriesMenu
          visible={this.state.visible}
          visibleSeries={["POP", "DBKAC", "EXDNUS", "ADJRAM"]}
          selectedSeries={this.state.selectedSeries}
          metadata={testMetadata}
          plotMetadata={plotMetadata}
          onAddSeries={v => console.log("Added series: ", v)}
          onRemove={s => console.log("remove ", s)}
          onTransform={(s, t) => console.log("transform ", s, t)}
          onScale={s => console.log("scale ", s)}
          onSwitchAxis={s => console.log("switch axis ", s)}
          onClose={() => this.setState({ visible: false })}
          onInfoModalOpen={s => this.setState({ selectedSeries: s })}
          onSelectedSeriesChange={s => this.setState({ selectedSeries: s })}
        />

        <Sidebar.Pusher>
          <Segment basic>
            <Header as="h3" onClick={() => this.setState({ visible: true })}>
              Click here to make it appear again
            </Header>
            <Image src="https://react.semantic-ui.com/images/wireframe/paragraph.png" />
            <Image src="https://react.semantic-ui.com/images/wireframe/paragraph.png" />
          </Segment>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}

<Ex1 />;
```
