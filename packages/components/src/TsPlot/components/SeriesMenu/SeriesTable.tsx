import * as React from "react";
import styled from "styled-components";
import {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
  Card,
  Grid,
  Table,
  Placeholder,
  Divider,
  Modal,
} from "semantic-ui-react";

import { ISeriesMetadata } from "../../../types";

const transformOptions = [
  { key: "lin", value: "lin", text: "Level" },
  { key: "chg", value: "chg", text: "Difference" },
  { key: "ch1", value: "ch1", text: "Y/Y Difference" },
  { key: "pch", value: "pch", text: "Percent Change" },
  { key: "pc1", value: "pc1", text: "Y/Y Percent Change" },
  {
    key: "cch",
    value: "cch",
    text: (
      <span>
        Continuously Compounded
        <br /> Rate of Change
      </span>
    ),
  },
  {
    key: "cca",
    value: "cca",
    text: (
      <span>
        Continuously Compounded <br />
        Annual Rate of Change
      </span>
    ),
  },
  {
    key: "pca",
    value: "pca",
    text: (
      <span>
        Compounded Annual
        <br /> Rate of Change
      </span>
    ),
  },
  { key: "log", value: "log", text: "Log" },
];

const TransformDropdownStyled = styled.div`
  & .ui.floating.inline.dropdown {
    font-size: 10px;
    font-family: monospace;

    .text {
      font-weight: normal;
    }

    .menu.transition {
      height: 150px;
      overflow: auto;

      .item {
        font-size: 10px;
      }
    }
  }
`;

const TransformDropdown = ({ trans, onChange }) => (
  <TransformDropdownStyled>
    <Dropdown
      inline
      floating
      options={transformOptions}
      value={trans}
      onChange={(p, e) => onChange(e.value)}
    />
  </TransformDropdownStyled>
);

const TdButtonStyled = styled.div`
  background-color: white;
  border-radius: 3px;
  padding: 3px;
  text-align: center;
  font-family: monospace;

  &:hover {
    filter: brightness(0.95);
    cursor: pointer;
  }

  &.axis {
    &.right {
      color: #cc0000;
    }

    &.left {
      color: #0099cc;
    }
  }

  &.scale {
    &.set {
      i {
        color: #1b1c1e !important;
      }
    }

    i {
      color: white !important;
    }

    &:hover {
      i {
        color: rgba(0, 0, 0, 0.25) !important;
      }
    }
  }
`;
const ScaledToAxis = ({ scaledToAxis, axis, onClick }) => (
  <TdButtonStyled
    className={`scale ${scaledToAxis ? "set" : ""}`}
    onClick={onClick}
  >
    <Icon.Group size="large">
      <Icon name={axis == "r" ? "arrow right" : "arrow left"} />
      <Icon corner name="check" />
    </Icon.Group>
  </TdButtonStyled>
);

const AxisButton = ({ axis, onClick }) => (
  <TdButtonStyled className={`axis ${axis}`} onClick={onClick}>
    <Icon
      name={
        axis == "r" ? "long arrow alternate right" : "long arrow alternate left"
      }
    />
    {axis == "r" ? "Right" : "Left"}
  </TdButtonStyled>
);

const SeriesIdButtonStyled = styled.div`
  font-family: monospace;
  text-align: left;
  background-color: white;
  border-radius: 3px;
  padding: 3px;

  &:hover {
    cursor: pointer;
    filter: brightness(0.95);
  }
`;

const SeriesIdButton = ({ series, onClick, active }) => {
  if (active) {
    return <Label ribbon>{series}</Label>;
  } else {
    return (
      <SeriesIdButtonStyled onClick={onClick}>{series}</SeriesIdButtonStyled>
    );
  }
};

const TableRow = ({
  active,
  series,
  trans,
  axis,
  axisScaledTo,
  onSwitchAxis,
  onScale,
  onRemove,
  onTransform,
  onActive,
  onInfo,
  ...otherProps
}) => (
  <tr {...otherProps}>
    <th>
      <SeriesIdButton series={series} onClick={onActive} active={active} />
    </th>
    <td>
      <TransformDropdown trans={trans} onChange={onTransform} />
    </td>
    <td>
      <AxisButton axis={axis} onClick={onSwitchAxis} />
    </td>
    <td>
      <ScaledToAxis scaledToAxis={axisScaledTo} axis={axis} onClick={onScale} />
    </td>
    <td>
      <TdButtonStyled onClick={onRemove}>
        <Icon name="times circle outline" color="red" size="large" />
      </TdButtonStyled>
    </td>
    <td>
      <TdButtonStyled onClick={onInfo}>
        <Icon name="info circle" color="blue" size="large" />
      </TdButtonStyled>
    </td>
  </tr>
);

const SeriesTableStyled = styled.table`
  border-collapse: collapse;
  tr {
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }

  thead th {
    padding-left: 5px;
  }

  th div.ui.ribbon.label {
    float: left;
  }
`;

const SeriesTable = ({
  dataAxis,
  selectedSeries,
  trans,
  axisScaledTo,
  onSwitchAxis,
  onScale,
  onRemove,
  onTransform,
  onActive,
  onInfo,
}) => {
  const Rows = ["l", "r"].reduce((ac, ax) => {
    return ac.concat(
      dataAxis[ax].map(s => (
        <TableRow
          key={s}
          series={s}
          trans={trans[s]}
          axis={ax}
          axisScaledTo={axisScaledTo.r === s || axisScaledTo.l === s}
          active={selectedSeries == s}
          onTransform={t => onTransform(s, t)}
          onScale={() => onScale(s)}
          onRemove={() => onRemove(s)}
          onSwitchAxis={() => onSwitchAxis(s)}
          onActive={() => onActive(s)}
          onInfo={() => onInfo(s)}
        />
      ))
    );
  }, []);

  return (
    <SeriesTableStyled>
      <thead>
        <tr>
          <th>Series</th>
          <th>Transform</th>
          <th>Axis</th>
          <th>Rescale</th>
          <th>Remove</th>
          <th>Info</th>
        </tr>
      </thead>
      <tbody>{Rows}</tbody>
    </SeriesTableStyled>
  );
};

export default SeriesTable;
