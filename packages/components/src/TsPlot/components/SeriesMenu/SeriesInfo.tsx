import * as React from "react";
import styled from "styled-components";
import {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
  Card,
  Grid,
  Table,
  Placeholder,
  Divider,
  Modal,
} from "semantic-ui-react";

import { ISeriesMetadata } from "../../../types";

const SeriesInfoWrapper = styled.div`
  padding: 5px;
  font-family: monospace;
  font-size: 10px;
  .ui.label,
  .ui.labels .label {
    font-size: 1em;
  }
`;

const CloseButton = styled.div`
  float: right;

  &:hover {
    cursor: pointer;
    color: rgba(0, 0, 0, 0.5);
  }
`;

const SeriesInfo = ({ series, md, onShowMore, onClose }) => (
  <SeriesInfoWrapper>
    <Card.Header as="h4">
      {md.title}{" "}
      <CloseButton onClick={onClose}>
        <Icon name="close" />
      </CloseButton>
    </Card.Header>
    <Card.Meta>
      <Label.Group>
        <Label color="violet">
          {md.obsStart}-{md.obsEnd}
          <Label.Detail>Observations</Label.Detail>
        </Label>

        <Label color="blue">
          {md.freq} <Label.Detail>Frequency</Label.Detail>
        </Label>
        <Label color="red">
          {md.units} <Label.Detail>Units</Label.Detail>
        </Label>
        <Label color="green"> {md.sa ? "" : "Not "}Seasonally Adjusted</Label>
        <Label color="yellow">
          {md.lastUpdated || "N/A"} <Label.Detail>Last Updated</Label.Detail>
        </Label>
        <Label color="teal">
          {md.lastDownloaded || "N/A"}{" "}
          <Label.Detail>Last Downloaded</Label.Detail>
        </Label>
      </Label.Group>
    </Card.Meta>
  </SeriesInfoWrapper>
);

export default SeriesInfo;
