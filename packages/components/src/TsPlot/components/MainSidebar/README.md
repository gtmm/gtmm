```jsx
const React = require("react");
const {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
} = require("semantic-ui-react");

const Ex1 = () => (
  <Sidebar.Pushable as={Segment}>
    <MainSidebar
      visible={true}
      series={[
        "UNRATE",
        "DFF",
        "DFEDTARU",
        "USD3MTD156N",
        "NRATE",
        "FF",
        "FEDTARU",
        "SD3MTD156N",
      ]}
      onAddSeries={v => console.log("Added series: ", v)}
      onSave={() => console.log("Save")}
      onFrequencyChange={console.log}
      onAggMethodChange={console.log}
    />

    <Sidebar.Pusher>
      <Segment basic>
        <Header as="h3">Application Content</Header>
        <Image src="https://react.semantic-ui.com/images/wireframe/paragraph.png" />
      </Segment>
    </Sidebar.Pusher>
  </Sidebar.Pushable>
);

<Ex1 />;
```
