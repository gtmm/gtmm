import * as React from "react";

import styled from "styled-components";
import {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
} from "semantic-ui-react";

import AddSeries from "./AddSeries/index";
import * as Aggregation from "./aggregationDropdowns";

interface IMainSidebarProps {
  visible: boolean;
  series: string[];
  onAddSeries: (v: string[]) => void;
  onSave: () => void;
  onFrequencyChange: (freq: any) => void;
  onAggMethodChange: (freq: any) => void;
  onClose: () => void;
}

const MainSidebar = ({
  visible,
  series,
  onAddSeries,
  onSave,
  onFrequencyChange,
  onAggMethodChange,
  onClose,
}: IMainSidebarProps) => (
  <Sidebar
    as={Menu}
    animation="overlay"
    visible={visible}
    vertical
    width="wide"
  >
    <Menu.Item>
      Add or remove series <Icon name="filter" />
      <br />
      <br />
      <AddSeries series={series} onAdd={onAddSeries} />
    </Menu.Item>

    <Menu.Item>
      Aggregation
      <Menu.Menu>
        <Menu.Item>
          <Aggregation.Frequency onChange={onFrequencyChange} />
        </Menu.Item>
        <Menu.Item>
          <Aggregation.AggregationMethod onChange={onAggMethodChange} />
        </Menu.Item>
      </Menu.Menu>
    </Menu.Item>
    <Menu.Item as="a" onClick={onSave} disabled>
      <Icon name="save" />
      Save chart
    </Menu.Item>
    <Menu.Item as="a" onClick={onClose}>
      &nbsp;
      <Icon name="times" color="grey" />
    </Menu.Item>
  </Sidebar>
);

export default MainSidebar;
