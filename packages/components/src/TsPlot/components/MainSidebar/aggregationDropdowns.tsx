import * as React from "react";

import styled from "styled-components";
import {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
} from "semantic-ui-react";

const DropdownLabel = styled.div`
  display: inline-block;
  margin-right: 10px;
`;

const frequencyOptions = [
  { key: "any", value: "any", text: "Any" },
  { key: "m", value: "m", text: "Monthly" },
  { key: "q", value: "q", text: "Quarterly" },
  { key: "a", value: "a", text: "Annually" },
];

export const Frequency = ({ onChange }) => (
  <React.Fragment>
    <DropdownLabel>Aggregation Method</DropdownLabel>
    <Dropdown
      selection
      defaultValue="any"
      options={frequencyOptions}
      onChange={(p, e) => onChange(e.value)}
    />
  </React.Fragment>
);

const aggregationMethodOptions = [
  { key: "avg", value: "avg", text: "Average" },
  { key: "sum", value: "sum", text: "Sum" },
  { key: "eop", value: "eop", text: "End-of-period" },
];

export const AggregationMethod = ({ onChange }) => (
  <React.Fragment>
    <DropdownLabel>Aggregation Method</DropdownLabel>
    <Dropdown
      selection
      defaultValue="avg"
      options={aggregationMethodOptions}
      onChange={(p, e) => onChange(e.value)}
    />
  </React.Fragment>
);
