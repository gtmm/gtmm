import * as React from "react";
import * as shortid from "shortid";

import "semantic-ui-css/semantic.min.css";
import styled from "styled-components";

import { Dropdown, Menu, Icon, Popup } from "semantic-ui-react";

interface IAddSeriesProps {
  onAdd: (v: string[]) => void;
  series: string[];
}

interface IAddSeriesState {}

/**
 * @param  {[type]} options.series [description]
 * @param  {[type]} options.onAdd  [description]
 * @return {[type]}                [description]
 */
export default class AddSeries extends React.Component<
  IAddSeriesProps,
  IAddSeriesState
> {
  constructor(props: IAddSeriesProps) {
    super(props);
  }

  render() {
    const { onAdd, series, ...otherProps } = this.props;
    const options = series.map(s => {
      return { key: s, value: s, text: s };
    });

    return (
      <Dropdown
        placeholder={"Add or Remove Series"}
        fluid
        search
        selection
        multiple
        header={"Add or Remove Series"}
        options={options}
        // @ts-ignore
        onChange={(p, e) => onAdd(e.value)}
      />
    );
  }
}
