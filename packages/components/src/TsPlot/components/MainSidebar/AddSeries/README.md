```jsx
const React = require("react");

class AddSeriesWrap extends React.Component {
  constructor(props) {
    super(props);

    this.state = { addedSeries: [] };

    this.series = [
      "UNRATE",
      "DFF",
      "DFEDTARU",
      "USD3MTD156N",
      "NRATE",
      "FF",
      "FEDTARU",
      "SD3MTD156N",
    ];
  }

  render() {
    return (
      <div>
        <AddSeries
          series={this.series}
          onAdd={v =>
            this.setState({
              addedSeries: v,
            })
          }
          colors={this.colors}
        />
        <hr />
        <em>Series added:</em>
        <br />
        {this.state.addedSeries.join(", ")}
      </div>
    );
  }
}

<AddSeriesWrap />;
```
