export default function* colorWheel() {
    const shades = [
        "rgba(3, 169, 244, 0.7)", // rgba-blue-strong
        "rgba(244, 67, 54, 0.7)", // rgba-red-strong
        "rgba(233, 30, 99, 0.7)", // rgba-pink-strong
        "rgba(156, 39, 176, 0.7)", // rgba-purple-strong
        "rgba(63, 81, 181, 0.7)", // rgba-indigo-strong
        "rgba(0, 188, 212, 0.7)", // rgba-cyan-strong
        "rgba(0, 150, 136, 0.7)", // rgba-teal-strong
        "rgba(76, 175, 80, 0.7)", // rgba-green-strong
        "rgba(205, 220, 57, 0.7)", // rgba-lime-strong
        "rgba(255, 235, 59, 0.7)", // rgba-yellow-strong
        "rgba(255, 152, 0, 0.7)", // rgba-orange-strong
        "rgba(3, 169, 244, 0.3)", // rgba-blue-light
        "rgba(244, 67, 54, 0.3)", // rgba-red-light
        "rgba(233, 30, 99, 0.3)", // rgba-pink-light
        "rgba(156, 39, 176, 0.3)", // rgba-purple-light
        "rgba(63, 81, 181, 0.3)", // rgba-indigo-light
        "rgba(0, 188, 212, 0.3)", // rgba-cyan-light
        "rgba(0, 150, 136, 0.3)", // rgba-teal-light
        "rgba(76, 175, 80, 0.3)", // rgba-green-light
        "rgba(205, 220, 57, 0.3)", // rgba-lime-light
        "rgba(255, 235, 59, 0.3)", // rgba-yellow-light
        "rgba(255, 152, 0, 0.3)", // rgba-orange-light
        "rgba(3, 169, 244, 0.1)", // rgba-blue-slight
        "rgba(244, 67, 54, 0.1)", // rgba-red-slight
        "rgba(233, 30, 99, 0.1)", // rgba-pink-slight
        "rgba(156, 39, 176, 0.1)", // rgba-purple-slight
        "rgba(63, 81, 181, 0.1)", // rgba-indigo-slight
        "rgba(0, 188, 212, 0.1)", // rgba-cyan-slight
        "rgba(0, 150, 136, 0.1)", // rgba-teal-slight
        "rgba(76, 175, 80, 0.1)", // rgba-green-slight
        "rgba(205, 220, 57, 0.1)", // rgba-lime-slight
        "rgba(255, 235, 59, 0.1)", // rgba-yellow-slight
        "rgba(255, 152, 0, 0.1)", // rgba-orange-slight
    ];

    const shadesLength = shades.length;

    var idx = -1;

    while (true) {
        idx++;
        if (idx + 1 >= shadesLength) {
            idx = 0;
        }
        yield shades[idx];
    }
}
