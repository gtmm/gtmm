```js
const Many = () => (
  <div>
    <SeriesButton
      series={"GDPCS"}
      name={"Real Gross Domestic Product"}
      value={4.5}
      color={"rgba(3, 169, 244, 0.7)"}
      fontSize={10}
      onTransform={s => console.log("transform ", s)}
      onScale={s => console.log("scale ", s)}
      onInfo={s => console.log("info ", s)}
      onRemove={s => console.log("remove ", s)}
      onSwitchAxis={s => console.log("switch axis ", s)}
    />
    <SeriesButton
      series={"RFF"}
      name={"Effective Federal funds rate"}
      value={4.5}
      color={"rgba(244, 67, 54, 0.7)"}
      fontSize={10}
      onTransform={s => console.log("transform ", s)}
      onScale={s => console.log("scale ", s)}
      onInfo={s => console.log("info ", s)}
      onRemove={s => console.log("remove ", s)}
      onSwitchAxis={s => console.log("switch axis ", s)}
    />
    <SeriesButton
      series={"EMP"}
      name={"Employment"}
      value={4.5}
      color={"rgba(233, 30, 99, 0.7)"}
      fontSize={10}
      onTransform={s => console.log("transform ", s)}
      onScale={s => console.log("scale ", s)}
      onInfo={s => console.log("info ", s)}
      onRemove={s => console.log("remove ", s)}
      onSwitchAxis={s => console.log("switch axis ", s)}
    />
  </div>
);

<Many />;
```
