import * as React from "react";
import * as shortid from "shortid";

import "semantic-ui-css/semantic.min.css";
import styled from "styled-components";

import { Dropdown, Menu, Icon, Popup, Label } from "semantic-ui-react";

// @ts-ignore
const SeriesButtonWrapper = styled.div`
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */
  user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
  display: inline;
  padding: 2px 5px;
  margin: 2.5px;
  box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
  border-radius: 3px;
  font-family: monospace;
  color: black;
  float: left;
  font-size: 10px;
  transition: 0.3s;
  // @ts-ignore
  background-color: ${props => props.color};

  &:hover {
    cursor: pointer;
    filter: brightness(1.15);
  }
`;

interface ISeriesButtonProps {
  series: string;
  value: number | string;
  color: string;
  name?: string;
  onClick: (s: string) => void;
}

/**
 * @return {[type]} [description]
 */
const SeriesButton = ({
  series,
  name,
  value,
  color,
  onClick,
  ...otherProps
}: ISeriesButtonProps) => (
  <Popup
    trigger={
      // @ts-ignore
      <SeriesButtonWrapper onClick={onClick} color={color} {...otherProps}>
        {`${series}: ${value}`}
      </SeriesButtonWrapper>
    }
    on="hover"
    content={name || series}
    size="tiny"
  />
);

export default SeriesButton;
