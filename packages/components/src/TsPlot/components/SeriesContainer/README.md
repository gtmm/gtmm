```jsx
const ex2Md = require("../../../tools/sampleData/test-metadata-2.json");

<SeriesContainer
  series={["UNRATE", "DFF", "DFEDTARU", "USD3MTD156N"]}
  colors={{
    DFEDTARU: "rgba(233, 30, 99, 0.7)",
    DFF: "rgba(244, 67, 54, 0.7)",
    UNRATE: "rgba(3, 169, 244, 0.7)",
    USD3MTD156N: "rgba(156, 39, 176, 0.7)",
  }}
  values={{
    DFEDTARU: 4.3,
    DFF: 2.55,
    UNRATE: 4.124,
  }}
  metadata={ex2Md}
  onClick={console.log}
/>;
```

With a background to see where the container is:

```jsx
const ex2Md = require("../../../tools/sampleData/test-metadata-2.json");

<SeriesContainer
  series={["UNRATE", "DFF", "DFEDTARU", "USD3MTD156N"]}
  colors={{
    DFEDTARU: "rgba(233, 30, 99, 0.7)",
    DFF: "rgba(244, 67, 54, 0.7)",
    UNRATE: "rgba(3, 169, 244, 0.7)",
    USD3MTD156N: "rgba(156, 39, 176, 0.7)",
  }}
  values={{
    DFEDTARU: 4.3,
    DFF: 2.55,
    UNRATE: 4.124,
  }}
  metadata={ex2Md}
  onClick={console.log}
/>;
```

With more series:

```jsx
const ex2Md = require("../../../tools/sampleData/test-metadata-2.json");

<SeriesContainer
  series={[
    "UNRATE",
    "DFF",
    "DFEDTARU",
    "USD3MTD156N",
    "NRATE",
    "FF",
    "FEDTARU",
    "SD3MTD156N",
  ]}
  colors={{
    DFEDTARU: "rgba(233, 30, 99, 0.7)",
    DFF: "rgba(244, 67, 54, 0.7)",
    UNRATE: "rgba(3, 169, 244, 0.7)",
    USD3MTD156N: "rgba(156, 39, 176, 0.7)",

    FEDTARU: "rgba(63, 81, 181, 0.7)",
    FF: "rgba(0, 188, 212, 0.7)",
    NRATE: "rgba(0, 150, 136, 0.7)",
    SD3MTD156N: "rgba(76, 175, 80, 0.7)",
  }}
  values={{
    DFEDTARU: 4.3,
    DFF: 2.55,
    UNRATE: 4.124,
    FEDTARU: 1,
    FF: 3.4214214215215214214,
    NRATE: 2100000,
    SD3MTD156N: 4.5,
  }}
  metadata={ex2Md}
  style={{ backgroundColor: "rgba(0,0,0,0.05)" }}
  onClick={console.log}
/>;
```

Toggle between multiple series:

```js
const ex2Md = require("../../../tools/sampleData/test-metadata-2.json");

class ExToggle extends React.Component {
  constructor(props) {
    super(props);

    this.state = { dataset: 0 };

    this.data = [
      ["UNRATE", "DFF", "DFEDTARU", "USD3MTD156N"],
      ["UNRATE", "DFF"],
      ["DFF", "DFEDTARU"],
      ["DFF", "DFEDTARU", "USD3MTD156N"],
    ];
  }

  render() {
    return (
      <div>
        <SeriesContainer
          series={this.data[this.state.dataset]}
          colors={{
            DFEDTARU: "rgba(233, 30, 99, 0.7)",
            DFF: "rgba(244, 67, 54, 0.7)",
            UNRATE: "rgba(3, 169, 244, 0.7)",
            USD3MTD156N: "rgba(156, 39, 176, 0.7)",
          }}
          values={{
            DFEDTARU: 4.3,
            DFF: 2.55,
            UNRATE: 4.124,
          }}
          metadata={ex2Md}
          onClick={console.log}
          style={{ backgroundColor: "rgba(0,0,0,0.05)" }}
        />
        <hr />
        <button
          onClick={() =>
            this.setState({ dataset: Math.floor(Math.random() * 4) })
          }
        >
          Change Dataset
        </button>
      </div>
    );
  }
}

<ExToggle />;
```
