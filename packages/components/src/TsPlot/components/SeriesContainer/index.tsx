import * as React from "react";
import * as shortid from "shortid";
import styled from "styled-components";
import { format as d3Format } from "d3";

import SeriesButton from "../SeriesButton/index";
import { ISeriesMetadata } from "../../../types";
import { IDataAxis } from "../../types";

const numberFormat = d3Format("~s");

interface ISeriesContainerProps {
  dataAxis: IDataAxis;
  values: { [key: string]: number | Date | null };
  colors: { [key: string]: string };
  metadata: ISeriesMetadata;
  onClick: (s: string) => void;
}

interface ISeriesContainerState {}

const SeriesContainerDiv = styled.div`
  display: block;
`;
const DroppableDiv = styled.div`
  display: inline-block;
  min-width: 70px;
  min-height: 29px;
`;

export default class SeriesContainer extends React.Component<
  ISeriesContainerProps,
  ISeriesContainerState
> {
  constructor(props: ISeriesContainerProps) {
    super(props);
  }

  public render() {
    const {
      dataAxis,
      values,
      colors,
      metadata,
      onClick,
      ...otherProps
    } = this.props;

    const children = ["l", "r"].reduce(
      (ac, lr) =>
        ac.concat(
          dataAxis[lr].map(s => (
            <SeriesButton
              key={s}
              series={s}
              name={metadata[s] ? metadata[s].title : s}
              value={values[s] ? numberFormat(values[s] || -9) : "N/A"}
              color={colors[s]}
              onClick={() => onClick(s)}
            />
          ))
        ),
      []
    );

    return (
      <SeriesContainerDiv className="series-container" {...otherProps}>
        {children}
      </SeriesContainerDiv>
    );
  }
}
