```js
const { TsData, ITsDataInput } = require("@gtmm/data/build/src/TsData.js");
const data = require("../../../tools/sampleData/test-data-2.json");
const md = require("../../../tools/sampleData/test-metadata-2.json");

var df = new TsData(data, "long", "any", "avg", false);

var tic = new Date();
var wide = df.wide;
var toc = new Date();
console.log("load time: ", toc - tic);

<div>{["load time: ", toc - tic, "  ", wide.length]}</div>;
```
