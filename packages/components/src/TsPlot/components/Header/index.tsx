import * as React from "react";

interface IHeaderProps {}
interface IHeaderState {}

/**
 * @example ./Header.example.md
 */
export default class Header extends React.Component<
  IHeaderProps,
  IHeaderState
> {
  constructor(props: IHeaderProps) {
    super(props);
  }

  render() {
    return <div />;
  }
}
