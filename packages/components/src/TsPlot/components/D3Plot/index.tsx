import * as React from "react";
import * as shortid from "shortid";
import * as d3 from "d3";
import styled from "styled-components";

import {
  IDataAxis,
  IBds,
  IOnTransformCommand,
  IOnScaleCommand,
  IOnAddSeriesCommand,
  IOnRemoveSeriesCommand,
  IOnSwitchAxisCommand,
  ILastCommand,
  IOnInitCommand,
} from "../../types";

import D3PlotContainer from "./D3PlotContainer";

const nowTimestamp = new Date();

interface ID3PlotProps {
  /**
   * Dataframe
   * @type {[type]}
   */
  data: { long: any; wide: any };

  /**
   * Height of svg.
   * @type {[type]}
   */
  h: number;

  /**
   * Width of SVG
   * @type {[type]}
   */
  w: number;

  /**
   * Margins
   * @type {Object}
   */
  m: { l: number; r: number; t: number; b: number };

  /**
   * Font size of the labels.
   * @type {[type]}
   */
  fontSize: number;

  /**
   * Colors of each series/line. Usually hex.
   * @type {Object}
   */
  colors: { [key: string]: string };

  /**
   * Names of columns that should be visible
   * @type {[type]}
   */
  dataAxis: IDataAxis;

  /**
   * Bounds on the x and y axes
   * @type {Object}
   */
  bds: IBds;

  /**
   * tells when the SVG should be updated. (See shouldComponentUpdate.)
   * @type {[type]}
   */
  lastCommand: ILastCommand;

  /**
   * What should happen to the data that is associated with the date being hovered?
   * @param  {[type]} d: {[keys:string]: number|Date} [description]
   * @return {[type]}    [description]
   */
  onHover: (d: { [keys: string]: number | Date }) => void;
}

interface ID3PlotState {
  svg: any;
  sc: { x: any; y: any[] };
  scPerm: { x: any; y: any };
  axis: { ax: { x: any; y: any[] }; gl: { x: any; y: any } };
}

export default class D3Plot extends React.Component<
  ID3PlotProps,
  ID3PlotState
> {
  /**
   * ID used to hook into d3.
   * @type {String}
   */
  public readonly id = "id-" + shortid.generate();

  constructor(props: ID3PlotProps) {
    super(props);

    this.plotSeries = this.plotSeries.bind(this);
    this.plotAxisLabels = this.plotAxisLabels.bind(this);
    this.plotTooltips = this.plotTooltips.bind(this);
    this.initPanAndZoom = this.initPanAndZoom.bind(this);
    this.setAxesDomain = this.setAxesDomain.bind(this);
  }

  componentDidMount() {
    this.setState(this.createSvgObjects(), () => {
      this.plotSeries();
      this.plotAxisLabels();
      this.plotTooltips();
      this.initPanAndZoom();
    });
  }

  componentWillUnmount() {
    d3.selectAll(`div.d3-plot svg#${this.id}`).remove();
  }

  /**
   * For now, do not update any components
   */
  shouldComponentUpdate(nextProps, nextState) {
    this.updateSvg(nextProps, this.props);
    return false;
  }

  private updateSvg(nextProps: ID3PlotProps, props: ID3PlotProps): void {
    if (
      !props.lastCommand ||
      nextProps.lastCommand.timestamp <= props.lastCommand.timestamp
    ) {
      return;
    }
    const { svg, axis, ...otherState } = this.state;

    const sc = this.setAxesDomain(nextProps.bds);
    const lineMapper = this.getLineMapper(sc);

    const onResetBds = (resetBds, ax): void => {
      if (resetBds) {
        d3.selectAll(`g.series-group g.series.${ax} path`).attr(
          "d",
          lineMapper[ax]
        );

        if (ax === "l") {
          svg.selectAll("g.axis g.axis-labels g.y.l").call(axis.ax.y[0]);
          svg.selectAll("g.axis g.grid g.y").call(axis.gl.y[0]);
        } else {
          svg.selectAll("g.axis g.axis-labels g.y.r").call(axis.ax.y[1]);
        }
      }

      return;
    };

    const onTransform = _lastCommand => {
      var { info, name, timestamp } = _lastCommand as IOnTransformCommand;
      svg
        .select(`g.series-group g.series.${info.series} path`)
        .datum(nextProps.data.long[info.series]);
      onResetBds(info.resetBds, info.axis);
    };

    const onScale = _lastCommand => {
      var { info, name, timestamp } = _lastCommand as IOnScaleCommand;
      onResetBds(true, info.axis);
    };

    const onAddSeries = _lastCommand => {
      var { info, name, timestamp } = _lastCommand as IOnAddSeriesCommand;
      svg
        .select(`g.series-group`)
        .append("g")
        .attr("class", `series ${info.axis} ${info.series}`)
        .append("path")
        .style("stroke", nextProps.colors[info.series])
        .datum(nextProps.data.long[info.series])
        .attr("d", lineMapper[info.axis]);
      onResetBds(info.resetBds, info.axis);
    };

    const onRemoveSeries = _lastCommand => {
      var { info, name, timestamp } = _lastCommand as IOnRemoveSeriesCommand;
      svg.select(`g.series-group g.${info.axis}.${info.series}`).remove();
      onResetBds(info.resetBds, info.axis);
    };

    const onSwitchAxis = (_lastCommand: ILastCommand): void => {
      var { info, name, timestamp } = _lastCommand as IOnSwitchAxisCommand;

      svg
        .select(`g.series-group g.${info.series}`)
        .classed(info.oldAxis, false)
        .classed(info.newAxis, true);
      onResetBds(info.resetBds[info.oldAxis], info.oldAxis);
      onResetBds(info.resetBds[info.newAxis], info.newAxis);
    };

    switch (nextProps.lastCommand.name) {
      case "onTransform":
        onTransform(nextProps.lastCommand);
        break;
      case "onScale":
        onScale(nextProps.lastCommand);
        break;
      case "onAddSeries":
        onAddSeries(nextProps.lastCommand);
        break;
      case "onRemoveSeries":
        onRemoveSeries(nextProps.lastCommand);
        break;
      case "onSwitchAxis":
        onSwitchAxis(nextProps.lastCommand);
        break;
      default:
        break;
    }
  }

  /**
   * Creates all SVG/plot objects that are held in state
   * @return {[type]} [description]
   */
  private createSvgObjects() {
    const { w, h, m, fontSize, ...otherProps } = this.props;

    var svgOuter = d3
      .select(`div.d3-plot#${this.id}`)
      .append("svg")
      .attr("height", h + m.t + m.b)
      .attr("width", w + m.l + m.r);

    // Actual operation space.
    var svg = svgOuter
      .append("g")
      .attr("class", "d3-plot-main")
      .attr("transform", `translate(${m.l},${m.t})`);

    // Set the clip path
    // https://stackoverflow.com/questions/24490261/d3-js-show-only-part-of-data-on-xaxis
    // https://bl.ocks.org/mbostock/34f08d5e11952a80609169b7917d4172
    svg
      .append("defs")
      .append("clipPath")
      .attr("id", `clip-${this.id}`)
      .append("rect")
      .attr("width", w)
      .attr("height", h);

    // set the axes
    var sc = {
      x: d3
        .scaleTime()
        .range([0, w])
        .domain(this.props.bds.x),
      y: [
        d3
          .scaleLinear()
          .range([h, 0])
          .domain(this.props.bds.y.l),
        d3
          .scaleLinear()
          .range([h, 0])
          .domain(this.props.bds.y.r),
      ],
    };

    var scPerm = {
      x: d3
        .scaleTime()
        .range([0, w])
        .domain(this.props.bds.x),
      y: [
        d3
          .scaleLinear()
          .range([h, 0])
          .domain(this.props.bds.y.l),
        d3
          .scaleLinear()
          .range([h, 0])
          .domain(this.props.bds.y.r),
      ],
    };

    //https://github.com/d3/d3-format#locale_format
    var axis = {
      ax: {
        x: d3.axisBottom(sc.x).ticks(5),
        y: [
          d3
            .axisLeft(sc.y[0])
            .ticks(5, "~s")
            .tickSize(0),
          d3
            .axisRight(sc.y[1])
            .ticks(5, "~s")
            .tickSize(0),
        ],
      },
      gl: {
        x: d3
          .axisBottom(sc.x)
          .ticks(5, "")
          .tickSize(-h),
        y: d3
          .axisLeft(sc.y[0])
          .ticks(5, "")
          .tickSize(-w),
      },
    };

    return { svg, scPerm, sc, axis };
  }

  private initPanAndZoom() {
    const { w, h, m, ...otherProps } = this.props;
    const { svg, sc, scPerm, axis } = this.state;

    const zoomed = () => {
      var t = d3.event.transform;
      sc.x.domain(t.rescaleX(scPerm.x).domain());

      const lineMapper = this.getLineMapper(sc);

      d3.selectAll("g.series-group g.series.l path").attr("d", lineMapper.l);
      d3.selectAll("g.series-group g.series.r path").attr("d", lineMapper.r);

      d3.selectAll("g.tooltips line.trip-line")
        // @ts-ignore
        .attr("x1", d_ => sc.x(d_.date))
        // @ts-ignore
        .attr("x2", d_ => sc.x(d_.date))
        .style("stroke-width", t.k * 1);

      svg.selectAll("g.axis g.axis-labels g.x").call(axis.ax.x);
      svg.selectAll("g.axis g.grid g.x").call(axis.gl.x);
    };

    var zoom = d3
      .zoom()
      .scaleExtent([1 - 1e-10, 10]) // May dial back infinity...
      .translateExtent([[0, 0], [w, h]])
      .extent([[0, 0], [w, h]])
      .on("zoom", zoomed);

    svg.call(zoom);
  }

  private setAxesDomain(bds: IBds | null = null): any {
    var bds_ = bds || this.props.bds;
    this.state.sc.x.domain(bds_.x);
    this.state.sc.y[0].domain(bds_.y.l);
    this.state.sc.y[1].domain(bds_.y.r);

    this.state.scPerm.x.domain(bds_.x);
    this.state.scPerm.y[0].domain(bds_.y.l);
    this.state.scPerm.y[1].domain(bds_.y.r);

    return this.state.sc;
  }

  /**
   * Plots the series on the graph.
   * @param  {[type]} series:string|null [description]
   * @return {[type]}                    [description]
   */
  private plotSeries(series: string | null = null): void {
    const { data, dataAxis, colors, ...otherProps } = this.props;
    const { svg, sc, scPerm, axis } = this.state;

    svg.selectAll("g.series-group").remove();
    var g = svg.append("g").attr("class", "series-group");

    const plotOneSeries = (s: string, lr: "l" | "r"): void => {
      let path = g
        .append("g")
        .attr("class", `series ${s} ${lr}`)
        .append("path")
        .style("stroke", colors[s])
        .datum(data.long[s]);
    };

    if (series) {
      plotOneSeries(series, dataAxis["l"].indexOf(series) < 0 ? "r" : "l");
    } else {
      for (let lr of ["l", "r"] as ("l" | "r")[]) {
        for (let ii = 0; ii < dataAxis[lr].length; ii++) {
          plotOneSeries(dataAxis[lr][ii], lr);
        }
      }
    }

    const lineMapper = this.getLineMapper(sc);

    d3.selectAll("g.series-group g.series.l path").attr("d", lineMapper.l);
    d3.selectAll("g.series-group g.series.r path").attr("d", lineMapper.r);
  }

  private plotAxisLabels(): void {
    const { svg, scPerm, sc, axis } = this.state;
    const { h, w, ...otherProps } = this.props;

    svg.select("g.axis").remove();
    var axis_ = svg.append("g").attr("class", "axis");

    // Axis labels
    var axisLabels = axis_.append("g").attr("class", "axis-labels");
    axisLabels
      .append("g")
      .attr("class", "x")
      .attr("transform", `translate(0,${h})`)
      .call(axis.ax.x);
    axisLabels
      .append("g")
      .attr("class", "y l")
      .attr("transform", `translate(0,0)`)
      //https://github.com/d3/d3-format#locale_format
      .call(axis.ax.y[0]);
    axisLabels
      .append("g")
      .attr("class", "y r")
      .attr("transform", `translate(${w},0)`)
      .call(axis.ax.y[1]);

    // Grid lines
    var grid = axis_.append("g").attr("class", "grid");

    grid
      .append("g")
      .attr("class", "x")
      .attr("transform", `translate(0,${h})`)
      .call(axis.gl.x);
    grid
      .append("g")
      .attr("class", "y")
      .call(axis.gl.y);

    axis_.selectAll(".y .domain").remove();

    return;
  }

  private plotTooltips() {
    const {
      bds,
      dataAxis,
      data,
      h,
      w,
      fontSize,
      m,
      ...otherProps
    } = this.props;
    const { svg, sc, scPerm, axis, ...otherState } = this.state;

    // MAIN

    // @todo make this more flexible
    const v = data.wide;

    var seriesData = svg.select("g.tooltips").remove();
    var tooltips = svg.append("g").attr("class", "tooltips");

    var displayLine = tooltips
      .append("line")
      // @ts-ignore
      .attr("x1", sc.x(bds.x[1]))
      // @ts-ignore
      .attr("x2", sc.x(bds.x[1]))
      .attr("y1", 0)
      .attr("y2", h)
      .style("stroke-width", 1)
      .style("stroke", "black")
      .style("stroke-opacity", 0)
      .style("stroke-dasharray", "3, 3");

    var dateLabel = tooltips
      .append("text")
      .attr("x", sc.x(bds.x[1]))
      .attr("y", -2)
      .attr("text-anchor", "middle")
      .text("none")
      .style("font-family", "monospace")
      .style("font-size", fontSize)
      .style("opacity", 0);

    var tripLine = tooltips.append("g").attr("class", "trip-line");
    var tripLines = tripLine
      .selectAll("line.trip-line")
      .data(v)
      .enter()
      .append("line")
      .attr("class", "trip-line")
      // @ts-ignore
      .attr("x1", d_ => sc.x(d_.date))
      // @ts-ignore
      .attr("x2", d_ => sc.x(d_.date))
      .attr("y1", 0)
      .attr("y2", h)
      .style("stroke-width", 1)
      .on("mouseover", (d, idx, obj) => {
        this.props.onHover(d);
        displayLine
          .style("stroke-opacity", 0.75)
          .attr("x1", sc.x(d.date))
          .attr("x2", sc.x(d.date));
        dateLabel
          .attr("x", sc.x(d.date))
          .style("opacity", 1)
          .text(d.date.toLocaleDateString());
        d3.selectAll("div.series-data span.series-button")
          // @ts-ignore
          .text(sb_ => `${sb_}: ${d[sb_] || "N/A"}`);
      });
  }

  private getLineMapper(sc_) {
    return {
      l: d3
        .line()
        // @ts-ignore
        .x(d_ => sc_.x(d_.date))
        // @ts-ignore
        .y(d_ => sc_.y[0](d_.value)),
      r: d3
        .line()
        // @ts-ignore
        .x(d_ => sc_.x(d_.date))
        // @ts-ignore
        .y(d_ => sc_.y[1](d_.value)),
    };
  }

  public render() {
    console.warn("d3-plot rerendering");
    return <D3PlotContainer className="d3-plot" id={this.id} />;
  }
}

/*
: {
    onHover: (d: { [keys: string]: number | Date }) => void;
    fontSize: number;
    timestamp: Date;
  } = {
    onHover: (d: { [keys: string]: number | Date }) => null,
    fontSize: 10,
    timestamp: nowTimestamp,
  };

 */
// For zooming/panning
// svg
//   .append("rect")
//   .attr("width", w)
//   .attr("height", h)
//   .style("fill", "none")
//   .style("pointer-events", "all")
//   .call(
//     d3
//       .zoom()
//       .scaleExtent([0.5, 20])
//       .extent([[0, 0], [w, h]])
//       .on("zoom", this.onZoomAndPan)
//   );
