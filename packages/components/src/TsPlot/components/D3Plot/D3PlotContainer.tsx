import styled from "styled-components";

interface ID3PlotContainer {
  id: string;
}

const D3PlotContainer = styled.div`
  font-family: monospace;
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */
  user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */

  svg g.d3-plot-main {
    .series {
      clip-path: url(${(props: ID3PlotContainer): string =>
        `#clip-${props.id}`});

      path {
        stroke-width: 2;
        fill: none;
      }
    }

    line.trip-line {
      stroke-opacity: 0;
      stroke: red;
    }

    g.grid {
      opacity: 0.1;
      stroke-width: 0.5;
      font-size: 0;
    }
  }
`;

export default D3PlotContainer;
