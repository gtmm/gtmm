```js
const React = require("react");
const ex1 = require("../../../tools/sampleData/test-data-1.json");
const ex1Md = require("../../../tools/sampleData/test-metadata-1.json");
const { TsData } = require("@gtmm/data/build/src/TsData");
const colorWheel = require("../colorWheel").default;

const CANONICAL_MEASURES = {
  w: (16 / 9) * 300,
  h: 300,
  m: { l: 30, r: 30, b: 30, t: 15 },
  fontSize: 10,
  heightShare: 0.85,
};

const getAxisBoundsY = (type, axis, df_, dataAxis_) => {
  var df = df_;
  var bds = [0, 0];

  if (type === "_all") {
    var dataAxis = dataAxis_;
    if (dataAxis[axis].length > 0) {
      // @ts-ignore
      bds = dataAxis[axis].reduce(
        (ac, col) => {
          var bc = df.md[col].bds.y;
          ac = [Math.min(ac[0], bc[0]), Math.max(ac[1], bc[1])];
          return ac;
        },
        [1e99, -1e99]
      );
    } else {
      // If nothing plotted on that side set it equal to that for the opposite axis.
      var otherAxis = dataAxis[axis === "l" ? "r" : "l"];
      if (otherAxis.length > 0) {
        bds = otherAxis.reduce(
          (ac, col) => {
            var bc = df.md[col].bds.y;
            ac = [Math.min(ac[0], bc[0]), Math.max(ac[1], bc[1])];
            return ac;
          },
          [1e99, -1e99]
        );
      } else {
        bds = [0, 1];
      }
    }
  } else {
    bds = df.md[type].bds.y;
  }

  bds = [bds[0] * 0.95, bds[1] * 1.05];

  return bds;
};

const getAxisBoundsX = (type, df_, dataAxis_) => {
  var df = df_;

  var bds = [new Date(), new Date()];

  if (type === "_all") {
    var dataAxis = dataAxis_;
    var visibleSeries = dataAxis.r.concat(dataAxis.l);
    // @ts-ignore
    bds = visibleSeries.reduce(
      (ac, col) => {
        var bc = df.md[col].bds.x;
        ac = [
          Math.min(ac[0], bc[0].getTime()),
          Math.max(ac[1], bc[1].getTime()),
        ];
        return ac;
      },

      [new Date("10000-01-01").getTime(), new Date("1000-01-01").getTime()]
    );

    // @ts-ignore
    bds = bds.map(bx => new Date(bx));
  } else {
    bds = df.md[type].bds.x;
  }
  return bds;
};

const getPlotDims = (width, height) => {
  const { w, h, m, fontSize, heightShare } = CANONICAL_MEASURES;
  var dims = {
    w: -99,
    h: -99,
    m: { l: -99, r: -99, t: -99, b: -99 },
    fontSize: -99,
  };

  var w2 = width;
  var h2 = heightShare * height;
  dims.m = {
    l: m.l * (w2 / w),
    r: m.r * (w2 / w),
    b: m.b * (h2 / h),
    t: m.t * (h2 / h),
  };
  dims.w = w2 - dims.m.l - dims.m.r;
  dims.h = h2 - dims.m.t - dims.m.b;

  // Not clear what needs to be done with font size scaling yet.
  dims.fontSize = fontSize;

  return dims;
};

var h = 400,
  w = (4 / 3) * 400;
var df = new TsData(ex1, "long", "any", "avg", false);
var cw = colorWheel();
var colors = df.cols.reduce((accum, col) => {
  accum[col] = cw.next().value;
  return accum;
}, {});
var dataAxis = {
  l: df.cols.filter(s => !["POP", "EXDNUS"].includes(s)),
  r: ["POP", "EXDNUS"],
};
var bds = {
    x: getAxisBoundsX("_all", df, dataAxis),
    y: {
      l: getAxisBoundsY("_all", "l", df, dataAxis),
      r: getAxisBoundsY("_all", "r", df, dataAxis),
    },
  },
  dims = getPlotDims(w, h);

var data = { long: df.v, wide: df.wide };
var timestamp = new Date();

class Ex1 extends React.Component {
  constructor(props) {
    super(props);

    this.state = { hover: { hover: "To See values" } };
  }

  render() {
    return (
      <React.Fragment>
        <D3Plot
          data={data}
          dataAxis={dataAxis}
          bds={bds}
          colors={colors}
          h={dims.h}
          w={dims.w}
          m={dims.m}
          fontSize={dims.fontSize}
          onHover={s => this.setState({ hover: s })}
          timestamp={timestamp}
        />
        <hr />
        <table>
          <tbody>
            {Object.keys(this.state.hover).map(s => (
              <tr key={s}>
                <td>
                  <b>{s}</b>
                </td>
                <td>{`${this.state.hover[s]}`}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

<Ex1 />;
```

A sub-component of `TsPlot` that renders the plot using D3.

## Reference

- [A good pan-and-zoom example](https://bl.ocks.org/mbostock/34f08d5e11952a80609169b7917d4172)
- [Graph with gridlines](https://bl.ocks.org/d3noob/c506ac45617cf9ed39337f99f8511218)
- [`shouldComponentUpdate`](https://reactjs.org/docs/react-component.html#shouldcomponentupdate)
- ["Margin convention"](https://bl.ocks.org/mbostock/3019563)
- [React + D3.js: Balancing Performance & Developer Experience](https://medium.com/@tibotiber/react-d3-js-balancing-performance-developer-experience-4da35f912484)
  - Favors [`Olical/react-faux-dom`](https://github.com/Olical/react-faux-dom) approach.
  - Problem is that: "This library is intended to be used to build a React DOM tree from some mutable intermediate value which is then thrown away inside a `render` function. This means things that require mutation of the DOM, such as D3's animations, zooming, dragging and brushing will not work."
  - Problems with isolating the component:
    - "you get slower rendering performance by doing heavy DOM manipulation that React’s reconciliation algorithm could have shaved milliseconds off."
    - "leaves you with a little island of mutable DOM, festering away inside your tree. It just doesn’t feel quite right to me."
