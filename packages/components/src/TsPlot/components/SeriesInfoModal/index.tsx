import * as React from "react";
import styled from "styled-components";
import {
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
  Card,
  Grid,
  Table,
  Placeholder,
  Divider,
  Modal,
} from "semantic-ui-react";

const SeriesInfoModal = ({ isOpen, series, md, onClose }) => (
  <Modal open={isOpen} onClose={onClose} closeIcon>
    <Modal.Header>{series}</Modal.Header>
    <Modal.Content>
      <Modal.Header as="h3">{md.title}</Modal.Header>
      <Label.Group>
        <Label color="violet">
          {md.obsStart}-{md.obsEnd}
          <Label.Detail>Observations</Label.Detail>
        </Label>

        <Label color="blue">
          {md.freq} <Label.Detail>Frequency</Label.Detail>
        </Label>
        <Label color="red">
          {md.units} <Label.Detail>Units</Label.Detail>
        </Label>
        <Label color="green"> {md.sa ? "" : "Not "}Seasonally Adjusted</Label>
        <Label color="yellow">
          {md.lastUpdated || "N/A"} <Label.Detail>Last Updated</Label.Detail>
        </Label>
        <Label color="teal">
          {md.lastDownloaded || "N/A"}{" "}
          <Label.Detail>Last Downloaded</Label.Detail>
        </Label>
      </Label.Group>
      <Modal.Description>{md.notes}</Modal.Description>
    </Modal.Content>
  </Modal>
);

export default SeriesInfoModal;
