import styled from "styled-components";

/** @ignore */
export const TsDataGrid = styled.div`
  margin: 0;
  display: grid;
  // @ts-ignore
  grid-template-rows: 48px auto;
  grid-template-areas:
    "series"
    "plot";
  // @ts-ignore
  height: ${props => `${props.height + 10}px`};
  // @ts-ignore
  width: ${props => `${props.width}px`};
`;

export const TsDataGridSeries = styled.div`
  // background-color: #ffc409;
  // border: 1px solid rgba(0, 0, 0, 0.2);
  grid-area: series;
  margin: 0;
  overflow-x: auto;
`;

export const TsDataGridPlot = styled.div`
  // background-color: #262d97;
  grid-area: plot;
  margin: 0;
`;
