```js
// const ex1 = require("../tools/sampleData/test-data-1.json");
// const ex1Md = require("../tools/sampleData/test-metadata-1.json");

// <TsPlot data={ex1} metadata={ex1Md} height={300} width={(16 / 9) * 300} />;
null;
```

```js
const ex2 = require("../tools/sampleData/test-data-1.json");
const ex2Md = require("../tools/sampleData/test-metadata-1.json");

// console.table(ex2Md);
// console.log(
//   ex2.reduce((ac, s) => {
//     if (!ac.includes(s.series)) {
//       ac.push(s.series);
//     }
//     return ac;
//   }, [])
// );

<TsPlot data={ex2} metadata={ex2Md} height={300} width={(16 / 9) * 300} />;
```

// var ex2 = ex1.filter(d => !["GDPC1", "CIVPART"].includes(d.series));

[Bloomberg](https://www.bloomberg.com/quote/INDU:IND) has really nice charts:

<img src="https://i.imgur.com/xPg0gt6.png" width="600"/>
