export type IStringOrNumber = number | string;

export interface IStringTMap<T> {
  [key: string]: T;
}

export interface INumberTMap<T> {
  [key: number]: T;
}

export interface IAnyTMap<T> {
  key: T;
}

export interface IEmptyObject {}

export interface IRateLimitProps {
  wait: number;
  blockSize: number;
  errTol: number;
}

export type IRejectFunc = () => any;

export interface ISeriesMetadata {
  id: string;
  title: string;
  obsStart: string | Date;
  obsEnd: string | Date;
  freq: string;
  freqShort: string;
  units: string;
  sa: boolean;
  lastUpdated: string | Date;
  notes: string;
  lastDownloaded: string | Date;
  upToDate: boolean;
}
