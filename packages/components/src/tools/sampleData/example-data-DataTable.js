// prettier-ignore
export const ex1 = [
 {
   "series": "GDPC1",
   "date": "01/01/2018",
   "value": 18323.96300
 },
 {
   "series": "GDPC1",
   "date": "04/01/2018",
   "value": 18511.57600
 },
 {
   "series": "GDPC1",
   "date": "07/01/2018",
   "value": 18664.97300
 },
 {
   "series": "UNRATE",
   "date": "01/01/2018",
   "value": 4.10000
 },
 {
   "series": "UNRATE",
   "date": "02/01/2018",
   "value": 4.10000
 },
 {
   "series": "UNRATE",
   "date": "03/01/2018",
   "value": 4.00000
 },
 {
   "series": "UNRATE",
   "date": "04/01/2018",
   "value": 3.90000
 },
 {
   "series": "UNRATE",
   "date": "05/01/2018",
   "value": 3.80000
 },
 {
   "series": "UNRATE",
   "date": "06/01/2018",
   "value": 4.00000
 },
 {
   "series": "UNRATE",
   "date": "07/01/2018",
   "value": 3.90000
 },
 {
   "series": "UNRATE",
   "date": "08/01/2018",
   "value": 3.80000
 },
 {
   "series": "UNRATE",
   "date": "09/01/2018",
   "value": 3.70000
 },
 {
   "series": "UNRATE",
   "date": "10/01/2018",
   "value": 3.80000
 },
 {
   "series": "UNRATE",
   "date": "11/01/2018",
   "value": 3.70000
 },
 {
   "series": "UNRATE",
   "date": "12/01/2018",
   "value": 3.90000
 },
 {
   "series": "DFF",
   "date": "01/01/2018",
   "value": 1.33000
 },
 {
   "series": "DFF",
   "date": "01/02/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/03/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/04/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/05/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/06/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/07/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/08/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/09/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/10/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/11/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/12/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/13/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/14/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/15/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/16/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/17/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/18/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/19/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/20/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/21/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/22/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/23/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/24/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/25/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/26/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/27/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/28/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/29/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/30/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "01/31/2018",
   "value": 1.34000
 },
 {
   "series": "DFF",
   "date": "02/01/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/02/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/03/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/04/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/05/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/06/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/07/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/08/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/09/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/10/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/11/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/12/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/13/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/14/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/15/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/16/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/17/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/18/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/19/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/20/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/21/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/22/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/23/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/24/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/25/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/26/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/27/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "02/28/2018",
   "value": 1.35000
 },
 {
   "series": "DFF",
   "date": "03/01/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/02/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/03/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/04/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/05/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/06/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/07/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/08/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/09/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/10/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/11/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/12/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/13/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/14/2018",
   "value": 1.42000
 },
 {
   "series": "DFF",
   "date": "03/15/2018",
   "value": 1.43000
 },
 {
   "series": "DFF",
   "date": "03/16/2018",
   "value": 1.43000
 },
 {
   "series": "DFF",
   "date": "03/17/2018",
   "value": 1.43000
 },
 {
   "series": "DFF",
   "date": "03/18/2018",
   "value": 1.43000
 },
 {
   "series": "DFF",
   "date": "03/19/2018",
   "value": 1.43000
 },
 {
   "series": "DFF",
   "date": "03/20/2018",
   "value": 1.44000
 },
 {
   "series": "DFF",
   "date": "03/21/2018",
   "value": 1.44000
 },
 {
   "series": "DFF",
   "date": "03/22/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/23/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/24/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/25/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/26/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/27/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/28/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/29/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "03/30/2018",
   "value": 1.67000
 },
 {
   "series": "DFF",
   "date": "03/31/2018",
   "value": 1.67000
 },
 {
   "series": "DFF",
   "date": "04/01/2018",
   "value": 1.67000
 },
 {
   "series": "DFF",
   "date": "04/02/2018",
   "value": 1.68000
 },
 {
   "series": "DFF",
   "date": "04/03/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/04/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/05/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/06/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/07/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/08/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/09/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/10/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/11/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/12/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/13/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/14/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/15/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/16/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/17/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/18/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/19/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "04/20/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/21/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/22/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/23/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/24/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/25/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/26/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/27/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/28/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/29/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "04/30/2018",
   "value": 1.69000
 },
 {
   "series": "DFF",
   "date": "05/01/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/02/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/03/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/04/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/05/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/06/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/07/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/08/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/09/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/10/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/11/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/12/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/13/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/14/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/15/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/16/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/17/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/18/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/19/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/20/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/21/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/22/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/23/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/24/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/25/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/26/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/27/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/28/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/29/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/30/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "05/31/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/01/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/02/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/03/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/04/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/05/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/06/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/07/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/08/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/09/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/10/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/11/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/12/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/13/2018",
   "value": 1.70000
 },
 {
   "series": "DFF",
   "date": "06/14/2018",
   "value": 1.90000
 },
 {
   "series": "DFF",
   "date": "06/15/2018",
   "value": 1.90000
 },
 {
   "series": "DFF",
   "date": "06/16/2018",
   "value": 1.90000
 },
 {
   "series": "DFF",
   "date": "06/17/2018",
   "value": 1.90000
 },
 {
   "series": "DFF",
   "date": "06/18/2018",
   "value": 1.90000
 },
 {
   "series": "DFF",
   "date": "06/19/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "06/20/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/21/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/22/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/23/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/24/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/25/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/26/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "06/27/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "06/28/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "06/29/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "06/30/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/01/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/02/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/03/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/04/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/05/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/06/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/07/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/08/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/09/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/10/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/11/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/12/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/13/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/14/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/15/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/16/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/17/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/18/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/19/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/20/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/21/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/22/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/23/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/24/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/25/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/26/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/27/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/28/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/29/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/30/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "07/31/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/01/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/02/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/03/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/04/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/05/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/06/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/07/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/08/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/09/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/10/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/11/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/12/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/13/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/14/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/15/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "08/16/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/17/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/18/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/19/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/20/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/21/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/22/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/23/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/24/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/25/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/26/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/27/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/28/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/29/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/30/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "08/31/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "09/01/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "09/02/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "09/03/2018",
   "value": 1.91000
 },
 {
   "series": "DFF",
   "date": "09/04/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/05/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/06/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/07/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/08/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/09/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/10/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/11/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/12/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/13/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/14/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/15/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/16/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/17/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/18/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/19/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/20/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/21/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/22/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/23/2018",
   "value": 1.92000
 },
 {
   "series": "DFF",
   "date": "09/24/2018",
   "value": 1.93000
 },
 {
   "series": "DFF",
   "date": "09/25/2018",
   "value": 1.93000
 },
 {
   "series": "DFF",
   "date": "09/26/2018",
   "value": 1.93000
 },
 {
   "series": "DFF",
   "date": "09/27/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "09/28/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "09/29/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "09/30/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/01/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/02/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/03/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/04/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/05/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/06/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/07/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/08/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/09/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/10/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/11/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/12/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/13/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/14/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/15/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/16/2018",
   "value": 2.18000
 },
 {
   "series": "DFF",
   "date": "10/17/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "10/18/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "10/19/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "10/20/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "10/21/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "10/22/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "10/23/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/24/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/25/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/26/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/27/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/28/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/29/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/30/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "10/31/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/01/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/02/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/03/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/04/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/05/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/06/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/07/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/08/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/09/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/10/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/11/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/12/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "11/13/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/14/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/15/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/16/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/17/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/18/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/19/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/20/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/21/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/22/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/23/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/24/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/25/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/26/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/27/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/28/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/29/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "11/30/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/01/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/02/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/03/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/04/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/05/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/06/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/07/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/08/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/09/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/10/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/11/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/12/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/13/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/14/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/15/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/16/2018",
   "value": 2.19000
 },
 {
   "series": "DFF",
   "date": "12/17/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/18/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/19/2018",
   "value": 2.20000
 },
 {
   "series": "DFF",
   "date": "12/20/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/21/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/22/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/23/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/24/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/25/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/26/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/27/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/28/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/29/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/30/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "12/31/2018",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/01/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/02/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/03/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/04/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/05/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/06/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/07/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/08/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/09/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/10/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/11/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/12/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/13/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/14/2019",
   "value": 2.40000
 },
 {
   "series": "DFF",
   "date": "01/15/2019",
   "value": 2.40000
 },
 {
   "series": "DFEDTARU",
   "date": "01/01/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/02/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/03/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/04/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/05/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/06/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/07/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/08/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/09/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/10/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/11/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/12/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/13/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/14/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/15/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/16/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/17/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/18/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/19/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/20/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/21/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/22/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/23/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/24/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/25/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/26/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/27/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/28/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/29/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/30/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/31/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/01/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/02/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/03/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/04/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/05/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/06/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/07/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/08/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/09/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/10/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/11/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/12/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/13/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/14/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/15/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/16/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/17/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/18/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/19/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/20/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/21/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/22/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/23/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/24/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/25/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/26/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/27/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "02/28/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/01/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/02/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/03/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/04/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/05/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/06/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/07/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/08/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/09/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/10/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/11/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/12/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/13/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/14/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/15/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/16/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/17/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/18/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/19/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/20/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/21/2018",
   "value": 1.50000
 },
 {
   "series": "DFEDTARU",
   "date": "03/22/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/23/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/24/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/25/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/26/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/27/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/28/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/29/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/30/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "03/31/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/01/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/02/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/03/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/04/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/05/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/06/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/07/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/08/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/09/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/10/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/11/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/12/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/13/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/14/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/15/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/16/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/17/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/18/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/19/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/20/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/21/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/22/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/23/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/24/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/25/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/26/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/27/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/28/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/29/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "04/30/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/01/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/02/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/03/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/04/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/05/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/06/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/07/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/08/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/09/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/10/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/11/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/12/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/13/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/14/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/15/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/16/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/17/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/18/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/19/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/20/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/21/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/22/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/23/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/24/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/25/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/26/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/27/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/28/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/29/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/30/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "05/31/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/01/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/02/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/03/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/04/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/05/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/06/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/07/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/08/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/09/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/10/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/11/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/12/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/13/2018",
   "value": 1.75000
 },
 {
   "series": "DFEDTARU",
   "date": "06/14/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/15/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/16/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/17/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/18/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/19/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/20/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/21/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/22/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/23/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/24/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/25/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/26/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/27/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/28/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/29/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "06/30/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/01/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/02/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/03/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/04/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/05/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/06/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/07/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/08/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/09/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/10/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/11/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/12/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/13/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/14/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/15/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/16/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/17/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/18/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/19/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/20/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/21/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/22/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/23/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/24/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/25/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/26/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/27/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/28/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/29/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/30/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "07/31/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/01/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/02/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/03/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/04/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/05/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/06/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/07/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/08/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/09/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/10/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/11/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/12/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/13/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/14/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/15/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/16/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/17/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/18/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/19/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/20/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/21/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/22/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/23/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/24/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/25/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/26/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/27/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/28/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/29/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/30/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "08/31/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/01/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/02/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/03/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/04/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/05/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/06/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/07/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/08/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/09/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/10/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/11/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/12/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/13/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/14/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/15/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/16/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/17/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/18/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/19/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/20/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/21/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/22/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/23/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/24/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/25/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/26/2018",
   "value": 2.00000
 },
 {
   "series": "DFEDTARU",
   "date": "09/27/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "09/28/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "09/29/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "09/30/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/01/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/02/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/03/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/04/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/05/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/06/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/07/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/08/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/09/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/10/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/11/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/12/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/13/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/14/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/15/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/16/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/17/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/18/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/19/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/20/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/21/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/22/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/23/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/24/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/25/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/26/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/27/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/28/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/29/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/30/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "10/31/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/01/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/02/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/03/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/04/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/05/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/06/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/07/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/08/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/09/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/10/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/11/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/12/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/13/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/14/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/15/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/16/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/17/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/18/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/19/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/20/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/21/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/22/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/23/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/24/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/25/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/26/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/27/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/28/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/29/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "11/30/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/01/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/02/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/03/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/04/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/05/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/06/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/07/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/08/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/09/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/10/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/11/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/12/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/13/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/14/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/15/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/16/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/17/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/18/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/19/2018",
   "value": 2.25000
 },
 {
   "series": "DFEDTARU",
   "date": "12/20/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/21/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/22/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/23/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/24/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/25/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/26/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/27/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/28/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/29/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/30/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "12/31/2018",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/01/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/02/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/03/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/04/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/05/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/06/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/07/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/08/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/09/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/10/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/11/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/12/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/13/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/14/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/15/2019",
   "value": 2.50000
 },
 {
   "series": "DFEDTARU",
   "date": "01/16/2019",
   "value": 2.50000
 },
 {
   "series": "USD3MTD156N",
   "date": "01/02/2018",
   "value": 1.69693
 },
 {
   "series": "USD3MTD156N",
   "date": "01/03/2018",
   "value": 1.69593
 },
 {
   "series": "USD3MTD156N",
   "date": "01/04/2018",
   "value": 1.70381
 },
 {
   "series": "USD3MTD156N",
   "date": "01/05/2018",
   "value": 1.70393
 },
 {
   "series": "USD3MTD156N",
   "date": "01/08/2018",
   "value": 1.70802
 },
 {
   "series": "USD3MTD156N",
   "date": "01/09/2018",
   "value": 1.70457
 },
 {
   "series": "USD3MTD156N",
   "date": "01/10/2018",
   "value": 1.70911
 },
 {
   "series": "USD3MTD156N",
   "date": "01/11/2018",
   "value": 1.72019
 },
 {
   "series": "USD3MTD156N",
   "date": "01/12/2018",
   "value": 1.72152
 },
 {
   "series": "USD3MTD156N",
   "date": "01/15/2018",
   "value": 1.73133
 },
 {
   "series": "USD3MTD156N",
   "date": "01/16/2018",
   "value": 1.73408
 },
 {
   "series": "USD3MTD156N",
   "date": "01/17/2018",
   "value": 1.73918
 },
 {
   "series": "USD3MTD156N",
   "date": "01/18/2018",
   "value": 1.74470
 },
 {
   "series": "USD3MTD156N",
   "date": "01/19/2018",
   "value": 1.74447
 },
 {
   "series": "USD3MTD156N",
   "date": "01/22/2018",
   "value": 1.74130
 },
 {
   "series": "USD3MTD156N",
   "date": "01/23/2018",
   "value": 1.74520
 },
 {
   "series": "USD3MTD156N",
   "date": "01/24/2018",
   "value": 1.75246
 },
 {
   "series": "USD3MTD156N",
   "date": "01/25/2018",
   "value": 1.76031
 },
 {
   "series": "USD3MTD156N",
   "date": "01/26/2018",
   "value": 1.76690
 },
 {
   "series": "USD3MTD156N",
   "date": "01/29/2018",
   "value": 1.77225
 },
 {
   "series": "USD3MTD156N",
   "date": "01/30/2018",
   "value": 1.77340
 },
 {
   "series": "USD3MTD156N",
   "date": "01/31/2018",
   "value": 1.77777
 },
 {
   "series": "USD3MTD156N",
   "date": "02/01/2018",
   "value": 1.78698
 },
 {
   "series": "USD3MTD156N",
   "date": "02/02/2018",
   "value": 1.78902
 },
 {
   "series": "USD3MTD156N",
   "date": "02/05/2018",
   "value": 1.79345
 },
 {
   "series": "USD3MTD156N",
   "date": "02/06/2018",
   "value": 1.79070
 },
 {
   "series": "USD3MTD156N",
   "date": "02/07/2018",
   "value": 1.79989
 },
 {
   "series": "USD3MTD156N",
   "date": "02/08/2018",
   "value": 1.81050
 },
 {
   "series": "USD3MTD156N",
   "date": "02/09/2018",
   "value": 1.82000
 },
 {
   "series": "USD3MTD156N",
   "date": "02/12/2018",
   "value": 1.83338
 },
 {
   "series": "USD3MTD156N",
   "date": "02/13/2018",
   "value": 1.83875
 },
 {
   "series": "USD3MTD156N",
   "date": "02/14/2018",
   "value": 1.85000
 },
 {
   "series": "USD3MTD156N",
   "date": "02/15/2018",
   "value": 1.87250
 },
 {
   "series": "USD3MTD156N",
   "date": "02/16/2018",
   "value": 1.88494
 },
 {
   "series": "USD3MTD156N",
   "date": "02/19/2018",
   "value": 1.89213
 },
 {
   "series": "USD3MTD156N",
   "date": "02/20/2018",
   "value": 1.90394
 },
 {
   "series": "USD3MTD156N",
   "date": "02/21/2018",
   "value": 1.91975
 },
 {
   "series": "USD3MTD156N",
   "date": "02/22/2018",
   "value": 1.94363
 },
 {
   "series": "USD3MTD156N",
   "date": "02/23/2018",
   "value": 1.95625
 },
 {
   "series": "USD3MTD156N",
   "date": "02/26/2018",
   "value": 1.98419
 },
 {
   "series": "USD3MTD156N",
   "date": "02/27/2018",
   "value": 2.00625
 },
 {
   "series": "USD3MTD156N",
   "date": "02/28/2018",
   "value": 2.01719
 },
 {
   "series": "USD3MTD156N",
   "date": "03/01/2018",
   "value": 2.02457
 },
 {
   "series": "USD3MTD156N",
   "date": "03/02/2018",
   "value": 2.02519
 },
 {
   "series": "USD3MTD156N",
   "date": "03/05/2018",
   "value": 2.03490
 },
 {
   "series": "USD3MTD156N",
   "date": "03/06/2018",
   "value": 2.04728
 },
 {
   "series": "USD3MTD156N",
   "date": "03/07/2018",
   "value": 2.05725
 },
 {
   "series": "USD3MTD156N",
   "date": "03/08/2018",
   "value": 2.07140
 },
 {
   "series": "USD3MTD156N",
   "date": "03/09/2018",
   "value": 2.08875
 },
 {
   "series": "USD3MTD156N",
   "date": "03/12/2018",
   "value": 2.10688
 },
 {
   "series": "USD3MTD156N",
   "date": "03/13/2018",
   "value": 2.12450
 },
 {
   "series": "USD3MTD156N",
   "date": "03/14/2018",
   "value": 2.14500
 },
 {
   "series": "USD3MTD156N",
   "date": "03/15/2018",
   "value": 2.17750
 },
 {
   "series": "USD3MTD156N",
   "date": "03/16/2018",
   "value": 2.20175
 },
 {
   "series": "USD3MTD156N",
   "date": "03/19/2018",
   "value": 2.22249
 },
 {
   "series": "USD3MTD156N",
   "date": "03/20/2018",
   "value": 2.24814
 },
 {
   "series": "USD3MTD156N",
   "date": "03/21/2018",
   "value": 2.27108
 },
 {
   "series": "USD3MTD156N",
   "date": "03/22/2018",
   "value": 2.28557
 },
 {
   "series": "USD3MTD156N",
   "date": "03/23/2018",
   "value": 2.29155
 },
 {
   "series": "USD3MTD156N",
   "date": "03/26/2018",
   "value": 2.29496
 },
 {
   "series": "USD3MTD156N",
   "date": "03/27/2018",
   "value": 2.30200
 },
 {
   "series": "USD3MTD156N",
   "date": "03/28/2018",
   "value": 2.30800
 },
 {
   "series": "USD3MTD156N",
   "date": "03/29/2018",
   "value": 2.31175
 },
 {
   "series": "USD3MTD156N",
   "date": "03/30/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "04/02/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "04/03/2018",
   "value": 2.32084
 },
 {
   "series": "USD3MTD156N",
   "date": "04/04/2018",
   "value": 2.32461
 },
 {
   "series": "USD3MTD156N",
   "date": "04/05/2018",
   "value": 2.33063
 },
 {
   "series": "USD3MTD156N",
   "date": "04/06/2018",
   "value": 2.33746
 },
 {
   "series": "USD3MTD156N",
   "date": "04/09/2018",
   "value": 2.33730
 },
 {
   "series": "USD3MTD156N",
   "date": "04/10/2018",
   "value": 2.33903
 },
 {
   "series": "USD3MTD156N",
   "date": "04/11/2018",
   "value": 2.34163
 },
 {
   "series": "USD3MTD156N",
   "date": "04/12/2018",
   "value": 2.34769
 },
 {
   "series": "USD3MTD156N",
   "date": "04/13/2018",
   "value": 2.35281
 },
 {
   "series": "USD3MTD156N",
   "date": "04/16/2018",
   "value": 2.35509
 },
 {
   "series": "USD3MTD156N",
   "date": "04/17/2018",
   "value": 2.35539
 },
 {
   "series": "USD3MTD156N",
   "date": "04/18/2018",
   "value": 2.35866
 },
 {
   "series": "USD3MTD156N",
   "date": "04/19/2018",
   "value": 2.36156
 },
 {
   "series": "USD3MTD156N",
   "date": "04/20/2018",
   "value": 2.35923
 },
 {
   "series": "USD3MTD156N",
   "date": "04/23/2018",
   "value": 2.35954
 },
 {
   "series": "USD3MTD156N",
   "date": "04/24/2018",
   "value": 2.36167
 },
 {
   "series": "USD3MTD156N",
   "date": "04/25/2018",
   "value": 2.36561
 },
 {
   "series": "USD3MTD156N",
   "date": "04/26/2018",
   "value": 2.35878
 },
 {
   "series": "USD3MTD156N",
   "date": "04/27/2018",
   "value": 2.35805
 },
 {
   "series": "USD3MTD156N",
   "date": "04/30/2018",
   "value": 2.36294
 },
 {
   "series": "USD3MTD156N",
   "date": "05/01/2018",
   "value": 2.35375
 },
 {
   "series": "USD3MTD156N",
   "date": "05/02/2018",
   "value": 2.36294
 },
 {
   "series": "USD3MTD156N",
   "date": "05/03/2018",
   "value": 2.36313
 },
 {
   "series": "USD3MTD156N",
   "date": "05/04/2018",
   "value": 2.36906
 },
 {
   "series": "USD3MTD156N",
   "date": "05/07/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "05/08/2018",
   "value": 2.35250
 },
 {
   "series": "USD3MTD156N",
   "date": "05/09/2018",
   "value": 2.35575
 },
 {
   "series": "USD3MTD156N",
   "date": "05/10/2018",
   "value": 2.35500
 },
 {
   "series": "USD3MTD156N",
   "date": "05/11/2018",
   "value": 2.34250
 },
 {
   "series": "USD3MTD156N",
   "date": "05/14/2018",
   "value": 2.33000
 },
 {
   "series": "USD3MTD156N",
   "date": "05/15/2018",
   "value": 2.32063
 },
 {
   "series": "USD3MTD156N",
   "date": "05/16/2018",
   "value": 2.32563
 },
 {
   "series": "USD3MTD156N",
   "date": "05/17/2018",
   "value": 2.33125
 },
 {
   "series": "USD3MTD156N",
   "date": "05/18/2018",
   "value": 2.32938
 },
 {
   "series": "USD3MTD156N",
   "date": "05/21/2018",
   "value": 2.33000
 },
 {
   "series": "USD3MTD156N",
   "date": "05/22/2018",
   "value": 2.33000
 },
 {
   "series": "USD3MTD156N",
   "date": "05/23/2018",
   "value": 2.33000
 },
 {
   "series": "USD3MTD156N",
   "date": "05/24/2018",
   "value": 2.31938
 },
 {
   "series": "USD3MTD156N",
   "date": "05/25/2018",
   "value": 2.31813
 },
 {
   "series": "USD3MTD156N",
   "date": "05/28/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "05/29/2018",
   "value": 2.30719
 },
 {
   "series": "USD3MTD156N",
   "date": "05/30/2018",
   "value": 2.30031
 },
 {
   "series": "USD3MTD156N",
   "date": "05/31/2018",
   "value": 2.32125
 },
 {
   "series": "USD3MTD156N",
   "date": "06/01/2018",
   "value": 2.31781
 },
 {
   "series": "USD3MTD156N",
   "date": "06/04/2018",
   "value": 2.31381
 },
 {
   "series": "USD3MTD156N",
   "date": "06/05/2018",
   "value": 2.31919
 },
 {
   "series": "USD3MTD156N",
   "date": "06/06/2018",
   "value": 2.32088
 },
 {
   "series": "USD3MTD156N",
   "date": "06/07/2018",
   "value": 2.32713
 },
 {
   "series": "USD3MTD156N",
   "date": "06/08/2018",
   "value": 2.32631
 },
 {
   "series": "USD3MTD156N",
   "date": "06/11/2018",
   "value": 2.33263
 },
 {
   "series": "USD3MTD156N",
   "date": "06/12/2018",
   "value": 2.33563
 },
 {
   "series": "USD3MTD156N",
   "date": "06/13/2018",
   "value": 2.34063
 },
 {
   "series": "USD3MTD156N",
   "date": "06/14/2018",
   "value": 2.33469
 },
 {
   "series": "USD3MTD156N",
   "date": "06/15/2018",
   "value": 2.32594
 },
 {
   "series": "USD3MTD156N",
   "date": "06/18/2018",
   "value": 2.32469
 },
 {
   "series": "USD3MTD156N",
   "date": "06/19/2018",
   "value": 2.33025
 },
 {
   "series": "USD3MTD156N",
   "date": "06/20/2018",
   "value": 2.33188
 },
 {
   "series": "USD3MTD156N",
   "date": "06/21/2018",
   "value": 2.33506
 },
 {
   "series": "USD3MTD156N",
   "date": "06/22/2018",
   "value": 2.33888
 },
 {
   "series": "USD3MTD156N",
   "date": "06/25/2018",
   "value": 2.33700
 },
 {
   "series": "USD3MTD156N",
   "date": "06/26/2018",
   "value": 2.33563
 },
 {
   "series": "USD3MTD156N",
   "date": "06/27/2018",
   "value": 2.33438
 },
 {
   "series": "USD3MTD156N",
   "date": "06/28/2018",
   "value": 2.33738
 },
 {
   "series": "USD3MTD156N",
   "date": "06/29/2018",
   "value": 2.33575
 },
 {
   "series": "USD3MTD156N",
   "date": "07/02/2018",
   "value": 2.34250
 },
 {
   "series": "USD3MTD156N",
   "date": "07/03/2018",
   "value": 2.33725
 },
 {
   "series": "USD3MTD156N",
   "date": "07/04/2018",
   "value": 2.33731
 },
 {
   "series": "USD3MTD156N",
   "date": "07/05/2018",
   "value": 2.33863
 },
 {
   "series": "USD3MTD156N",
   "date": "07/06/2018",
   "value": 2.33144
 },
 {
   "series": "USD3MTD156N",
   "date": "07/09/2018",
   "value": 2.33313
 },
 {
   "series": "USD3MTD156N",
   "date": "07/10/2018",
   "value": 2.33744
 },
 {
   "series": "USD3MTD156N",
   "date": "07/11/2018",
   "value": 2.33700
 },
 {
   "series": "USD3MTD156N",
   "date": "07/12/2018",
   "value": 2.33919
 },
 {
   "series": "USD3MTD156N",
   "date": "07/13/2018",
   "value": 2.33600
 },
 {
   "series": "USD3MTD156N",
   "date": "07/16/2018",
   "value": 2.33263
 },
 {
   "series": "USD3MTD156N",
   "date": "07/17/2018",
   "value": 2.34194
 },
 {
   "series": "USD3MTD156N",
   "date": "07/18/2018",
   "value": 2.34750
 },
 {
   "series": "USD3MTD156N",
   "date": "07/19/2018",
   "value": 2.34706
 },
 {
   "series": "USD3MTD156N",
   "date": "07/20/2018",
   "value": 2.34156
 },
 {
   "series": "USD3MTD156N",
   "date": "07/23/2018",
   "value": 2.33531
 },
 {
   "series": "USD3MTD156N",
   "date": "07/24/2018",
   "value": 2.33488
 },
 {
   "series": "USD3MTD156N",
   "date": "07/25/2018",
   "value": 2.33688
 },
 {
   "series": "USD3MTD156N",
   "date": "07/26/2018",
   "value": 2.33888
 },
 {
   "series": "USD3MTD156N",
   "date": "07/27/2018",
   "value": 2.34238
 },
 {
   "series": "USD3MTD156N",
   "date": "07/30/2018",
   "value": 2.34313
 },
 {
   "series": "USD3MTD156N",
   "date": "07/31/2018",
   "value": 2.34856
 },
 {
   "series": "USD3MTD156N",
   "date": "08/01/2018",
   "value": 2.34825
 },
 {
   "series": "USD3MTD156N",
   "date": "08/02/2018",
   "value": 2.34050
 },
 {
   "series": "USD3MTD156N",
   "date": "08/03/2018",
   "value": 2.34300
 },
 {
   "series": "USD3MTD156N",
   "date": "08/06/2018",
   "value": 2.34325
 },
 {
   "series": "USD3MTD156N",
   "date": "08/07/2018",
   "value": 2.34144
 },
 {
   "series": "USD3MTD156N",
   "date": "08/08/2018",
   "value": 2.34050
 },
 {
   "series": "USD3MTD156N",
   "date": "08/09/2018",
   "value": 2.33800
 },
 {
   "series": "USD3MTD156N",
   "date": "08/10/2018",
   "value": 2.31925
 },
 {
   "series": "USD3MTD156N",
   "date": "08/13/2018",
   "value": 2.31375
 },
 {
   "series": "USD3MTD156N",
   "date": "08/14/2018",
   "value": 2.31519
 },
 {
   "series": "USD3MTD156N",
   "date": "08/15/2018",
   "value": 2.31175
 },
 {
   "series": "USD3MTD156N",
   "date": "08/16/2018",
   "value": 2.32225
 },
 {
   "series": "USD3MTD156N",
   "date": "08/17/2018",
   "value": 2.31188
 },
 {
   "series": "USD3MTD156N",
   "date": "08/20/2018",
   "value": 2.30963
 },
 {
   "series": "USD3MTD156N",
   "date": "08/21/2018",
   "value": 2.31025
 },
 {
   "series": "USD3MTD156N",
   "date": "08/22/2018",
   "value": 2.31175
 },
 {
   "series": "USD3MTD156N",
   "date": "08/23/2018",
   "value": 2.31138
 },
 {
   "series": "USD3MTD156N",
   "date": "08/24/2018",
   "value": 2.31725
 },
 {
   "series": "USD3MTD156N",
   "date": "08/27/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "08/28/2018",
   "value": 2.31475
 },
 {
   "series": "USD3MTD156N",
   "date": "08/29/2018",
   "value": 2.31263
 },
 {
   "series": "USD3MTD156N",
   "date": "08/30/2018",
   "value": 2.32125
 },
 {
   "series": "USD3MTD156N",
   "date": "08/31/2018",
   "value": 2.32075
 },
 {
   "series": "USD3MTD156N",
   "date": "09/03/2018",
   "value": 2.31563
 },
 {
   "series": "USD3MTD156N",
   "date": "09/04/2018",
   "value": 2.32275
 },
 {
   "series": "USD3MTD156N",
   "date": "09/05/2018",
   "value": 2.31681
 },
 {
   "series": "USD3MTD156N",
   "date": "09/06/2018",
   "value": 2.32706
 },
 {
   "series": "USD3MTD156N",
   "date": "09/07/2018",
   "value": 2.33125
 },
 {
   "series": "USD3MTD156N",
   "date": "09/10/2018",
   "value": 2.33425
 },
 {
   "series": "USD3MTD156N",
   "date": "09/11/2018",
   "value": 2.33425
 },
 {
   "series": "USD3MTD156N",
   "date": "09/12/2018",
   "value": 2.33150
 },
 {
   "series": "USD3MTD156N",
   "date": "09/13/2018",
   "value": 2.33413
 },
 {
   "series": "USD3MTD156N",
   "date": "09/14/2018",
   "value": 2.33713
 },
 {
   "series": "USD3MTD156N",
   "date": "09/17/2018",
   "value": 2.33875
 },
 {
   "series": "USD3MTD156N",
   "date": "09/18/2018",
   "value": 2.33750
 },
 {
   "series": "USD3MTD156N",
   "date": "09/19/2018",
   "value": 2.35338
 },
 {
   "series": "USD3MTD156N",
   "date": "09/20/2018",
   "value": 2.36638
 },
 {
   "series": "USD3MTD156N",
   "date": "09/21/2018",
   "value": 2.37263
 },
 {
   "series": "USD3MTD156N",
   "date": "09/24/2018",
   "value": 2.37363
 },
 {
   "series": "USD3MTD156N",
   "date": "09/25/2018",
   "value": 2.38100
 },
 {
   "series": "USD3MTD156N",
   "date": "09/26/2018",
   "value": 2.38613
 },
 {
   "series": "USD3MTD156N",
   "date": "09/27/2018",
   "value": 2.39600
 },
 {
   "series": "USD3MTD156N",
   "date": "09/28/2018",
   "value": 2.39838
 },
 {
   "series": "USD3MTD156N",
   "date": "10/01/2018",
   "value": 2.39813
 },
 {
   "series": "USD3MTD156N",
   "date": "10/02/2018",
   "value": 2.40750
 },
 {
   "series": "USD3MTD156N",
   "date": "10/03/2018",
   "value": 2.40825
 },
 {
   "series": "USD3MTD156N",
   "date": "10/04/2018",
   "value": 2.40963
 },
 {
   "series": "USD3MTD156N",
   "date": "10/05/2018",
   "value": 2.40806
 },
 {
   "series": "USD3MTD156N",
   "date": "10/08/2018",
   "value": 2.41425
 },
 {
   "series": "USD3MTD156N",
   "date": "10/09/2018",
   "value": 2.42044
 },
 {
   "series": "USD3MTD156N",
   "date": "10/10/2018",
   "value": 2.42519
 },
 {
   "series": "USD3MTD156N",
   "date": "10/11/2018",
   "value": 2.43631
 },
 {
   "series": "USD3MTD156N",
   "date": "10/12/2018",
   "value": 2.43644
 },
 {
   "series": "USD3MTD156N",
   "date": "10/15/2018",
   "value": 2.44881
 },
 {
   "series": "USD3MTD156N",
   "date": "10/16/2018",
   "value": 2.44456
 },
 {
   "series": "USD3MTD156N",
   "date": "10/17/2018",
   "value": 2.44963
 },
 {
   "series": "USD3MTD156N",
   "date": "10/18/2018",
   "value": 2.46900
 },
 {
   "series": "USD3MTD156N",
   "date": "10/19/2018",
   "value": 2.47719
 },
 {
   "series": "USD3MTD156N",
   "date": "10/22/2018",
   "value": 2.48738
 },
 {
   "series": "USD3MTD156N",
   "date": "10/23/2018",
   "value": 2.48988
 },
 {
   "series": "USD3MTD156N",
   "date": "10/24/2018",
   "value": 2.50800
 },
 {
   "series": "USD3MTD156N",
   "date": "10/25/2018",
   "value": 2.50925
 },
 {
   "series": "USD3MTD156N",
   "date": "10/26/2018",
   "value": 2.52038
 },
 {
   "series": "USD3MTD156N",
   "date": "10/29/2018",
   "value": 2.52663
 },
 {
   "series": "USD3MTD156N",
   "date": "10/30/2018",
   "value": 2.54100
 },
 {
   "series": "USD3MTD156N",
   "date": "10/31/2018",
   "value": 2.55850
 },
 {
   "series": "USD3MTD156N",
   "date": "11/01/2018",
   "value": 2.58150
 },
 {
   "series": "USD3MTD156N",
   "date": "11/02/2018",
   "value": 2.59238
 },
 {
   "series": "USD3MTD156N",
   "date": "11/05/2018",
   "value": 2.58925
 },
 {
   "series": "USD3MTD156N",
   "date": "11/06/2018",
   "value": 2.59125
 },
 {
   "series": "USD3MTD156N",
   "date": "11/07/2018",
   "value": 2.60113
 },
 {
   "series": "USD3MTD156N",
   "date": "11/08/2018",
   "value": 2.61463
 },
 {
   "series": "USD3MTD156N",
   "date": "11/09/2018",
   "value": 2.61813
 },
 {
   "series": "USD3MTD156N",
   "date": "11/12/2018",
   "value": 2.61413
 },
 {
   "series": "USD3MTD156N",
   "date": "11/13/2018",
   "value": 2.61613
 },
 {
   "series": "USD3MTD156N",
   "date": "11/14/2018",
   "value": 2.62900
 },
 {
   "series": "USD3MTD156N",
   "date": "11/15/2018",
   "value": 2.64000
 },
 {
   "series": "USD3MTD156N",
   "date": "11/16/2018",
   "value": 2.64450
 },
 {
   "series": "USD3MTD156N",
   "date": "11/19/2018",
   "value": 2.64581
 },
 {
   "series": "USD3MTD156N",
   "date": "11/20/2018",
   "value": 2.65313
 },
 {
   "series": "USD3MTD156N",
   "date": "11/21/2018",
   "value": 2.67694
 },
 {
   "series": "USD3MTD156N",
   "date": "11/22/2018",
   "value": 2.68925
 },
 {
   "series": "USD3MTD156N",
   "date": "11/23/2018",
   "value": 2.69119
 },
 {
   "series": "USD3MTD156N",
   "date": "11/26/2018",
   "value": 2.70681
 },
 {
   "series": "USD3MTD156N",
   "date": "11/27/2018",
   "value": 2.70600
 },
 {
   "series": "USD3MTD156N",
   "date": "11/28/2018",
   "value": 2.70663
 },
 {
   "series": "USD3MTD156N",
   "date": "11/29/2018",
   "value": 2.73813
 },
 {
   "series": "USD3MTD156N",
   "date": "11/30/2018",
   "value": 2.73613
 },
 {
   "series": "USD3MTD156N",
   "date": "12/03/2018",
   "value": 2.75125
 },
 {
   "series": "USD3MTD156N",
   "date": "12/04/2018",
   "value": 2.73888
 },
 {
   "series": "USD3MTD156N",
   "date": "12/05/2018",
   "value": 2.76575
 },
 {
   "series": "USD3MTD156N",
   "date": "12/06/2018",
   "value": 2.76713
 },
 {
   "series": "USD3MTD156N",
   "date": "12/07/2018",
   "value": 2.77106
 },
 {
   "series": "USD3MTD156N",
   "date": "12/10/2018",
   "value": 2.77594
 },
 {
   "series": "USD3MTD156N",
   "date": "12/11/2018",
   "value": 2.77900
 },
 {
   "series": "USD3MTD156N",
   "date": "12/12/2018",
   "value": 2.77750
 },
 {
   "series": "USD3MTD156N",
   "date": "12/13/2018",
   "value": 2.78819
 },
 {
   "series": "USD3MTD156N",
   "date": "12/14/2018",
   "value": 2.80069
 },
 {
   "series": "USD3MTD156N",
   "date": "12/17/2018",
   "value": 2.80363
 },
 {
   "series": "USD3MTD156N",
   "date": "12/18/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "12/19/2018",
   "value": 2.78963
 },
 {
   "series": "USD3MTD156N",
   "date": "12/20/2018",
   "value": 2.82375
 },
 {
   "series": "USD3MTD156N",
   "date": "12/21/2018",
   "value": 2.82163
 },
 {
   "series": "USD3MTD156N",
   "date": "12/24/2018",
   "value": 2.81344
 },
 {
   "series": "USD3MTD156N",
   "date": "12/25/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "12/26/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "12/27/2018",
   "value": 2.80300
 },
 {
   "series": "USD3MTD156N",
   "date": "12/28/2018",
   "value": 2.79700
 },
 {
   "series": "USD3MTD156N",
   "date": "12/31/2018",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "01/01/2019",
   "value": null
 },
 {
   "series": "USD3MTD156N",
   "date": "01/02/2019",
   "value": 2.79388
 },
 {
   "series": "USD3MTD156N",
   "date": "01/03/2019",
   "value": 2.79500
 },
 {
   "series": "USD3MTD156N",
   "date": "01/04/2019",
   "value": 2.80388
 },
 {
   "series": "USD3MTD156N",
   "date": "01/07/2019",
   "value": 2.79681
 },
 {
   "series": "USD3MTD156N",
   "date": "01/08/2019",
   "value": 2.78250
 },
 {
   "series": "USD3MTD156N",
   "date": "01/09/2019",
   "value": 2.79888
 },
 {
   "series": "USD3MTD156N",
   "date": "01/10/2019",
   "value": 2.79694
 },
 {
   "series": "CIVPART",
   "date": "01/01/2018",
   "value": 62.70000
 },
 {
   "series": "CIVPART",
   "date": "02/01/2018",
   "value": 63.00000
 },
 {
   "series": "CIVPART",
   "date": "03/01/2018",
   "value": 62.90000
 },
 {
   "series": "CIVPART",
   "date": "04/01/2018",
   "value": 62.80000
 },
 {
   "series": "CIVPART",
   "date": "05/01/2018",
   "value": 62.80000
 },
 {
   "series": "CIVPART",
   "date": "06/01/2018",
   "value": 62.90000
 },
 {
   "series": "CIVPART",
   "date": "07/01/2018",
   "value": 62.90000
 },
 {
   "series": "CIVPART",
   "date": "08/01/2018",
   "value": 62.70000
 },
 {
   "series": "CIVPART",
   "date": "09/01/2018",
   "value": 62.70000
 },
 {
   "series": "CIVPART",
   "date": "10/01/2018",
   "value": 62.90000
 },
 {
   "series": "CIVPART",
   "date": "11/01/2018",
   "value": 62.90000
 },
 {
   "series": "CIVPART",
   "date": "12/01/2018",
   "value": 63.10000
 }
];
