import * as React from "react";
import styled from "styled-components";

import * as Handsontable from "handsontable";
import { HotTable } from "@handsontable/react";

import "handsontable/dist/handsontable.full.css";

import { TsData, ITsDataInput } from "@gtmm/data/build/src/TsData";

import { StyledWrapper } from "../styled";
import { IStringTMap } from "../types";

interface IOnGraph {
  (cols: string[]): void;
}

interface IDataTableProps {
  /**
   * Data to display.
   *
   * @example
   *   [{date: "1-1-1900", series: "USMCA", value: 3},
   *    {date: "1-2-1900", series: "USMCA", value: 3.24},
   *    ...
   *    {date: "1-1-1776", series: "BEATE", value: 43424},
   *    ...
   *   ]
   * @type {[type]}
   */
  data: ITsDataInput;

  /**
   * What happens when user wants to ADD columns to a graph.
   * @type {[type]}
   * @default (cols: string[]): void => console.log("GRAPH ADD", cols)
   */
  onGraphAdd: IOnGraph;

  /**
   * What happens when user wants to REMOVE columns to a graph.
   * @type {[type]}
   * @default (cols: string[]): void => console.log("GRAPH REMOVE", cols)
   */
  onGraphRemove: IOnGraph;

  /**
   * What happens when user wants to TOGGLE columns to a graph.
   * @type {[type]}
   * @default (cols: string[]): void => console.log("GRAPH TOGGLE", cols)
   */
  onGraphToggle: IOnGraph;
}

interface IDataTableState {
  /**
   * Dataframe for manipulations, etc.
   * @type {[type]}
   */
  df: TsData;
  format: {
    date: string;
    numeric: { pattern: string; culture: string };
  };
  contextMenu: {
    callback: (key: string, options: object) => void;
    items: object;
  };
}

const TableStyle = styled.div`
  font-size: 11px;
`;

export default class DataTable extends React.Component<
  IDataTableProps,
  IDataTableState
> {
  public static defaultProps = {
    onGraphAdd: (cols: string[]): void => console.log("GRAPH ADD", cols),
    onGraphRemove: (cols: string[]): void => console.log("GRAPH REMOVE", cols),
    onGraphToggle: (cols: string[]): void => console.log("GRAPH TOGGLE", cols),
  };

  constructor(props: IDataTableProps) {
    super(props);

    this.renderDateColumn = this.renderDateColumn.bind(this);
    this.renderDataColumn = this.renderDataColumn.bind(this);
    this.onGraph = this.onGraph.bind(this);
    this.onAggregate = this.onAggregate.bind(this);
    this.onAggregateMethod = this.onAggregateMethod.bind(this);
    this.onTransform = this.onTransform.bind(this);
    this.constructContextMenu = this.constructContextMenu.bind(this);

    this.state = {
      // Be careful setting strict = false
      df: new TsData(props.data, "wide", "any", "avg", false, "string"),
      format: {
        date: "DD/MM/YYYY",
        numeric: { pattern: "0", culture: "en-US" },
      },
      contextMenu: this.constructContextMenu(),
    };
  }

  private constructContextMenu() {
    const transforms = [
      ["lin", "Level"],
      ["chg", "Difference"],
      ["ch1", "Y/Y Difference"],
      ["pch", "Percent Change"],
      ["pc1", "Y/Y Percent Change"],
      ["cch", "Continuously Compounded Rate of Change"],
      ["cca", "Continuously Compounded Annual Rate of Change"],
      ["pca", "Compounded Annual Rate of Change"],
      ["log", "Log"],
    ].map(t => {
      return { key: `trans:${t[0]}`, name: t[1], callback: this.onTransform };
    });

    const aggMethods = [
      ["avg", "Average"],
      ["sum", "Sum"],
      ["eop", "End-of-period"],
    ].map(t => {
      return {
        key: `am:${t[0]}`,
        name: t[1],
        callback: this.onAggregateMethod,
      };
    });

    const aggregates = [
      ["m", "Month"],
      ["q", "Quarter"],
      ["a", "Annual"],
      ["any", "Reset"],
    ].map(t => {
      return {
        key: `agg:${t[0]}`,
        name: t[1],
        callback: this.onAggregate,
      };
    });

    const graph = ["Add", "Remove", "Toggle"].map(t => {
      return {
        key: `graph:${t.toLowerCase()}`,
        name: t,
        callback: this.onGraph,
      };
    });

    return {
      callback: (key, options) => {},
      items: {
        trans: {
          key: "trans",
          name: "Transform",
          submenu: {
            items: transforms,
          },
        },
        agg: {
          key: "agg",
          name: "Frequency",
          submenu: {
            items: aggregates,
          },
        },
        am: {
          key: "am",
          name: "Aggregation Method",
          submenu: {
            items: aggMethods,
          },
        },
        graph: {
          key: "graph",
          name: "Graph",
          submenu: {
            items: graph,
          },
        },
      },
    };
  }

  private onGraph(key: string, options: object): void {
    var start = Math.max(1, options[0].start.col),
      end = options[0].end.col;

    // Don't return the date column
    if (end === 0) return;

    var cols =
      start === end
        ? [this.state.df.cols[start - 1]]
        : this.state.df.cols.slice(start - 1, end);

    switch (key) {
      case "graph:add":
        this.props.onGraphAdd(cols);
        break;
      case "graph:remove":
        this.props.onGraphRemove(cols);
        break;
      case "graph:toggle":
        this.props.onGraphToggle(cols);
        break;
    }
  }

  private onAggregate(key: string, options: object): void {
    // @ts-ignore Know that this is one of correct aggregations
    this.state.df.agg = key.split(":")[1];
    this.forceUpdate();
  }

  private onAggregateMethod(key: string, options: object): void {
    // @ts-ignore Know that this is one of correct aggregations
    this.state.df.aggMethod = key.split(":")[1];
    this.forceUpdate();
  }

  private onTransform(key: string, options: object): void {
    var start = Math.max(1, options[0].start.col),
      end = options[0].end.col,
      method = key.split(":")[1];

    // Don't return the date column
    if (end === 0) return;

    var cols =
      start === end
        ? [this.state.df.cols[start - 1]]
        : this.state.df.cols.slice(start - 1, end);

    this.state.df[method](cols);
    this.forceUpdate();
  }

  private renderDateColumn(
    instance,
    td,
    row,
    col,
    prop,
    value,
    cellProperties
  ): void {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    // td.style.fontStyle = "italic";
    td.style.backgroundColor = "#f0f0f0";
  }

  private renderDataColumn(
    instance,
    td,
    row,
    col,
    prop,
    value,
    cellProperties
  ): void {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
  }

  public render() {
    const data = this.state.df.v,
      columns = [
        {
          data: "date",
          type: "date",
          editor: false,
          renderer: this.renderDateColumn,
        },
      ].concat(
        this.state.df.cols.map(c => {
          return {
            data: c,
            type: "numeric",
            editor: false,
            renderer: this.renderDataColumn,
          };
        })
      ),
      colHeaders = ["Date"].concat(this.state.df.cols);

    const settings = {
      // @ts-ignore: Cannot see that object[] is superset of IWideData
      data: data,
      columns: columns,
      colHeaders: colHeaders,
      rowHeaders: false,
      width: 500,
      height: 300,
      stretchH: "all",
      fixedColumnsLeft: 1,
      correctFormat: true,
      dateFormat: this.state.format.date,
      numericFormat: this.state.format.numeric,
      manualColumnMove: true,
      manualColumnResize: true,
      contextMenu: this.state.contextMenu,
    };

    return (
      <TableStyle>
        <HotTable settings={settings} />
      </TableStyle>
    );
  }
}
