```js
const { ex1 } = require("../tools/sampleData/example-data-DataTable.js");

<DataTable data={ex1} />;
```

### Goals/features for `DataTable`:

- [x] Easy transformations.
- [ ] Integration of forecast and regular data. ([relevant docs](https://handsontable.com/docs/6.2.2/demo-highlighting-selection.html))
- [ ] Lots of information on the data series
- [x] Ability to interact with charts, etc.
- [ ] Easy resize: Min dimensions + "ideal" dimensions.
- [ ] Rounding
- [ ] Summary stats?

### Other

- [Handsontable Settings/Configuration](https://github.com/handsontable/handsontable/blob/65dc03147e411df27e6c2699b93fdc7ade9bad67/src/defaultSettings.js)
