const shortid = require("shortid");

const generateWiderData = (m_, T_, funkProb) => {
  var data = {},
    now = new Date(),
    di,
    date,
    IDs = Array.apply(null, Array(m_)).map(ii => shortid.generate());

  for (let tt = 0; tt < T_; tt++) {
    // Interesting sorting.
    date =
      Math.random() > funkProb
        ? new Date(now * 1 + tt * 1e5 + 1e5)
        : new Date(now * 1 + tt * 1e5 - 1.23e5);
    di = { date: date };

    for (let ii = 0; ii < m_; ii++) {
      di[IDs[ii]] = Math.random() > 0.5 ? Math.random() : null;
    }
    data[date.toString()] = di;
  }

  return data;
};

const deconstructWithDates = (d, nRuns) => {
  var out,
    tic,
    toc,
    avg = 0;
  for (let rr = 0; rr < nRuns; rr++) {
    tic = new Date();
    out = Object.values(d).sort((a, b) => (a.date > b.date ? 1 : -1));
    toc = new Date();
    avg += toc - tic;
  }
  console.log(`With Dates:  ${avg / nRuns}`);
};

const deconstructWithStrings = (d, nRuns_) => {
  var out,
    tic,
    toc,
    avg = 0;
  for (let rr = 0; rr < nRuns_; rr++) {
    tic = new Date();
    out = Object.values(d).sort(
      (a, b) => (a.date.toString() > b.date.toString() ? 1 : -1)
    );
    toc = new Date();
    avg += toc - tic;
  }

  console.log(`With Strings: ${avg / nRuns_}`);
};

var T = 1000,
  m = 5,
  nRuns = 5;

console.log(
  `Time comparisons out of ${nRuns} runs for data with ${m} variables.`
);
for (let fp of [0.25, 0.1]) {
  console.log(`With disorganization probability ${fp}`);
  for (let T of [1000, 10000]) {
    var dataT = generateWiderData(m, T, fp);
    console.log(`With ${T} observations.`);
    deconstructWithDates(dataT, nRuns);
    deconstructWithStrings(dataT, nRuns);
  }
}
