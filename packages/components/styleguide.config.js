/**
 * https://github.com/bebraw/styleguidist-ts-sc
 * https://github.com/styleguidist/react-styleguidist/issues/750
 */

const path = require("path");
const reactDocgen = require("react-docgen");
const reactDocgenTS = require("react-docgen-typescript");
const tsDocgen = reactDocgenTS.withDefaultConfig({
    componentNameResolver: (exp, source) =>
        exp.getName() === "StyledComponentClass" &&
        reactDocgenTS.getDefaultExportForFile(source),
});

module.exports = {
    propsParser(filePath, source, resolver, handlers) {
        const isTS = path.extname(filePath).startsWith(".ts");

        return isTS
            ? tsDocgen.parse(filePath, resolver, handlers)
            : reactDocgen.parse(source, resolver, handlers);
    },
    styleguideComponents: {
        // Wrapper: path.join(__dirname, "src/ThemeWrapper"),
    },
    // Is the resolver really needed?
    resolver: reactDocgen.resolver.findAllComponentDefinitions,
    // components: "src/**/*.{ts,tsx}", // deprecated by sections
    webpackConfig: require("react-scripts/config/webpack.config")(
        "development"
    ),
    require: [path.join(__dirname, "src/styles.css")],
    ignore: ["src/styled.tsx"],
    skipComponentsWithoutExample: false,
    title: "@gtmm/components",
    pagePerSection: true,
    sections: [
        {
            name: "Components",
            content: "docs/Components.md",
            sections: [
                {
                    name: "DataTable",
                    content: "docs/DataTable.md",
                    components: () => ["./src/DataTable/index.tsx"],
                    exampleMode: "hide",
                    usageMode: "collapse",
                },
                {
                    name: "TsPlots",
                    content: "docs/TsPlots.md",
                    sections: [
                        {
                            name: "Demo",
                            components: () => ["./src/TsPlot/index.tsx"],
                            exampleMode: "hide",
                            usageMode: "collapse",
                        },
                        {
                            name: "Children",
                            components: () => [
                                "./src/TsPlot/components/**/index.tsx",
                            ],
                            exampleMode: "hide",
                            usageMode: "collapse",
                        },
                    ],
                },
            ],
            sectionDepth: 3,
        },
        {
            name: "Links",
            //content: "docs/Documentation.md",
            sections: [
                {
                    name: "GTMM Main Site",
                    href: "https://gtmm.gitlab.io",
                    external: true,
                },
                {
                    name: "GitLab",
                    href:
                        "https://gitlab.com/gtmm/gtmm/tree/master/packages/components",
                    external: true,
                },
            ],
            sectionDepth: 0,
        },
    ],
};

/*
    sections: [
        {
            name: "Components",
            sectionDepth: 3,
            content: "./docs/components.md",
            components: () => ["./src/DataTable/index.tsx"],
        },
    ],
 */

// [
//         {
//             name: "Components",
//             sectionDepth: 3,
//             content: "./docs/components.md",
//             sections: [
//                 {
//                     name: "DataTable",
//                     content: "./docs/components.md",
//                     components: () => ["./src/DataTable/index.tsx"],
//                     usageMode: "expand",
//                 },
//                 {
//                     name: "TsPlot",
//                     content: "./docs/components.md",
//                     sections: [
//                         {
//                             name: "Full Component",
//                             content: "./docs/components.md",
//                             components: () => ["./src/TsPlot/index.tsx"],
//                             usageMode: "expand",
//                         },
//                         {
//                             name: "Constituents",
//                             content: "./docs/components.md",
//                             components: () => [
//                                 "./src/TsPlot/components/**/*.{tsx,ts}",
//                             ],
//                             usageMode: "expand",
//                         },
//                     ],
//                 },
//             ],
//         },
//     ],
