var Fred, http, root;

const FS = require("fs");

/**
 * @class Fred
 * @constructor
 * @param {String} apiKey FRED API key. To obtain, register at https://research.stlouisfed.org/useraccount/register/
 */
Fred = function(apiKey, rateLimitProps) {
    "use strict";
    this.apiKey = apiKey;

    this._requests = 0;
    this._requestErrors = 0;
    this._rl = Object.assign(
        { wait: 10 * 1000, blockSize: 1000, errTol: 200 },
        rateLimitProps || {}
    );

    http = require("https");
    Promise = require("bluebird");

    root = "https://api.stlouisfed.org/fred/";
};

Fred.prototype._getWait = function() {
    this._requests += 1;
    return Math.floor(this._requests / this._rl.blockSize) * this._rl.wait;
};

Fred.prototype._handleGetOutput = function(self, isXml, body, requestUrl) {
    var completeResponse;

    self._requests -= 1;
    self._requestErrors = 0;

    if (isXml) {
        completeResponse = body;
        if (completeResponse.match(/error code/)) {
            var status = completeResponse.match(/error code="\d{3}"/);
            status = status[0].match(/\d{3}/)[0];

            var message = completeResponse.match(/message="[A-z0-9 \.]+"/);
            message = message[0].match(/"[A-z0-9 \.]+/)[0].substring(1);

            let err = new Error(`${message} (Status: ${status})`);

            return { err };
        } else {
            return { completeResponse };
        }
    } else {
        // Sometimes xml format will be returned if there is a "server down" error or the like.
        try {
            completeResponse = JSON.parse(body);
        } catch (e) {
            let err = new Error(`Expected JSON.
Query: ${requestUrl}
Received: 
${body.slice(0, 500)}
                                `);

            return { err };
        }

        if (completeResponse.error_code) {
            let err = new Error(
                `${completeResponse.error_message} (Status: ${
                    completeResponse.error_code
                })`
            );

            return { err };
        } else {
            return { completeResponse };
        }
    }
};

Fred.prototype._handleResponseError = function(
    self,
    err,
    reject,
    retryCallback
) {
    if (self._requestErrors > self._rl.errTol) {
        var errLong = new Error(
            `FRED API has throttled your last ${
                self._rl.errTol
            } requests as a rate limiting measure.`,
            "\n",
            "-".repeat(80),
            "\nConsider decreasing the size or number of requests that you make. You could also tinker with the request parameters.\n",
            "-".repeat(80),
            "\nError message from https:\n",
            err.message
        );
        reject(errLong);
        return null;
    }

    self._requestErrors += 1;

    var wait = self._getWait() + Math.random() * 20;
    return setTimeout(retryCallback, self._rl.wait, wait);
};

Fred.prototype.get = function(url, params) {
    "use strict";

    var requestUrl,
        queryString,
        innerGet,
        innerGetDelay,
        isXml = false,
        _handleGetOutput = this._handleGetOutput,
        _handleResponseError = this._handleResponseError,
        self = this;
    queryString = "?";

    params = params || {};

    if (!params.file_type) {
        params.file_type = "json";
    } else if (params.file_type === "xml") {
        isXml = true;
    }

    for (var key in params) {
        queryString += key + "=" + params[key] + "&";
    }

    requestUrl = root + url + queryString + "api_key=" + this.apiKey;

    innerGet = (resolve, reject) => {
        var req = http.get(requestUrl, response => {
            var body = "";

            response
                .on("data", chunk => {
                    body += chunk;
                })
                .on("end", () => {
                    var out = _handleGetOutput(self, isXml, body, requestUrl);

                    if (out.err) {
                        return _handleResponseError(self, out.err, reject, () =>
                            innerGet(resolve, reject)
                        );
                    } else {
                        resolve(out.completeResponse);
                    }

                    return null;
                });
        });
        req.on("error", err => {
            return _handleResponseError(self, err, reject, () =>
                innerGet(resolve, reject)
            );
        });
    };

    innerGetDelay = (resolve, reject) => {
        var wait = this._getWait();
        setTimeout(() => innerGet(resolve, reject), wait);
    };

    return new Promise((resolve, reject) => innerGetDelay(resolve, reject));
};

Fred.prototype.getAll = function(url, params, arrayName) {
    "use strict";

    var queryString,
        initOffset,
        getPage,
        getPageDelay,
        setRequestUrl,
        isXml = false,
        _handleGetOutput = this._handleGetOutput,
        _handleResponseError = this._handleResponseError,
        self = this;

    params = params || {};

    if (!params.file_type) {
        params.file_type = "json";
    } else if (params.file_type === "xml") {
        isXml = true;
    }

    initOffset = params.offset || 0;

    queryString = "?";
    for (var key in params) {
        if (key !== "offset") {
            queryString += key + "=" + params[key] + "&";
        }
    }

    queryString = root + url + queryString + "api_key=" + this.apiKey + "&";

    setRequestUrl = offset => queryString + "offset=" + offset;

    getPage = (resolve, reject, offset, output, requestUrl) => {
        var req = http.get(requestUrl, response => {
            var body = "";

            response
                .on("data", chunk => {
                    body += chunk;
                })
                .on("end", function() {
                    var completeResponse;

                    var out = _handleGetOutput(self, isXml, body, requestUrl);

                    if (out.err) {
                        return _handleResponseError(self, out.err, reject, () =>
                            getPage(resolve, reject, offset, output, requestUrl)
                        );
                    } else {
                        completeResponse = out.completeResponse;
                    }

                    // New Logic of getAll
                    if (
                        params.limit &&
                        completeResponse.count + completeResponse.offset >=
                            params.limit
                    ) {
                        // Set a limit and the limit was hit.
                        if (output) {
                            // Append to output if already recursive.
                            output[arrayName] = output[arrayName].concat(
                                completeResponse[arrayName]
                            );
                            resolve(output);
                        } else {
                            // Just return what we had
                            resolve(completeResponse);
                        }
                    } else if (
                        completeResponse[arrayName].length + params.offset <
                        completeResponse.count
                    ) {
                        // Haven't collected everything yet.

                        // Append what has been collected
                        if (output) {
                            output[arrayName].concat(
                                completeResponse[arrayName]
                            );
                        } else {
                            output = completeResponse;
                        }

                        // Recursion
                        return getPageDelay(
                            resolve,
                            reject,
                            offset + completeResponse[arrayName].length,
                            output
                        );
                    } else {
                        // Have collected everything
                        if (output) {
                            output[arrayName].concat(
                                completeResponse[arrayName]
                            );
                            resolve(output);
                        } else {
                            resolve(completeResponse);
                        }
                    }

                    return null;
                });
        });

        req.on("error", err => {
            return _handleResponseError(self, err, reject, () =>
                getPage(resolve, reject, offset, output, requestUrl)
            );
        });
    };

    getPageDelay = (resolve, reject, offset, output) => {
        var requestUrl = setRequestUrl(offset);
        var wait = this._getWait();

        setTimeout(
            () => getPage(resolve, reject, offset, output, requestUrl),
            wait
        );
    };

    return new Promise((resolve, reject) =>
        getPageDelay(resolve, reject, initOffset, null)
    );
};

Fred.prototype.getCategory = function(params) {
    return this.get("category", params);
};

Fred.prototype.getCategoryChildren = function(params) {
    return this.get("category/children", params);
};

Fred.prototype.getCategoryRelated = function(params) {
    return this.get("category/related", params);
};

Fred.prototype.getCategorySeries = function(params) {
    return this.getAll("category/series", params, "seriess");
};

Fred.prototype.getCategoryTags = function(params) {
    return this.getAll("category/tags", params, "tags");
};

Fred.prototype.getCategoryRelatedTags = function(params) {
    return this.get("category/related_tags", params, "tags");
};

Fred.prototype.getReleases = function(params) {
    return this.getAll("releases", params, "releases");
};

Fred.prototype.getReleasesDates = function(params) {
    return this.getAll("releases/dates", params, "release_dates");
};

Fred.prototype.getRelease = function(params) {
    return this.get("release", params);
};

Fred.prototype.getReleaseDates = function(params) {
    return this.getAll("release/dates", params, "release_dates");
};

Fred.prototype.getReleaseSeries = function(params) {
    return this.getAll("release/series", params, "seriess");
};

Fred.prototype.getReleaseSources = function(params) {
    return this.get("release/sources", params);
};

Fred.prototype.getReleaseTags = function(params) {
    return this.getAll("release/tags", params, "tags");
};

Fred.prototype.getReleaseRelatedTags = function(params) {
    return this.getAll("release/related_tags", params, "tags");
};

Fred.prototype.getSeries = function(params) {
    return this.get("series", params);
};

Fred.prototype.getSeriesCategories = function(params) {
    return this.get("series/categories", params);
};

Fred.prototype.getSeriesObservations = function(params) {
    return this.getAll("series/observations", params, "observations");
};

Fred.prototype.getSeriesRelease = function(params) {
    return this.get("series/release", params);
};

Fred.prototype.getSeriesSearch = function(params) {
    return this.getAll("series/search", params, "seriess");
};

Fred.prototype.getSeriesSearchTags = function(params) {
    return this.getAll("series/search/tags", params, "tags");
};

Fred.prototype.getSeriesSearchRelatedTags = function(params) {
    return this.getAll("series/search/related_tags", params, "tags");
};

Fred.prototype.getSeriesTags = function(params) {
    return this.getAll("series/tags", params, "tags");
};

Fred.prototype.getSeriesUpdates = function(params) {
    return this.getAll("series/updates", params, "seriess");
};

Fred.prototype.getSeriesVintageDates = function(params) {
    return this.getAll("series/vintagedates", params, "vintage_dates");
};

Fred.prototype.getSources = function(params) {
    return this.getAll("sources", params, "sources");
};

Fred.prototype.getSource = function(params) {
    return this.get("source", params);
};

Fred.prototype.getSourceReleases = function(params) {
    return this.getAll("source/releases", params, "releases");
};

Fred.prototype.getTags = function(params) {
    return this.getAll("tags", params, "tags");
};

Fred.prototype.getRelatedTags = function(params) {
    return this.getAll("related_tags", params, "tags");
};

Fred.prototype.getTagsSeries = function(params) {
    return this.getAll("tags/series", params, "seriess");
};

module.exports = Fred;
