const Promise = require("bluebird");

var Fred = require("../lib/index");
var testFred;
var testApiKey;

jest.setTimeout(30000);

describe("Fred", () => {
    beforeAll(() => {
        testApiKey = process.env.FRED_KEY;
        testFred = new Fred(testApiKey);
    });

    describe("init", () => {
        it("should create a new instance of Fred", () => {
            expect(testFred.apiKey).toEqual(testApiKey);
        });
    });

    describe("get", () => {
        it("should return a success object on succesful request", () => {
            expect.assertions(1);

            return testFred
                .get("releases", {})
                .then(res => {
                    expect(res.releases.length).toBeGreaterThan(0);
                })
                .catch(err => {
                    expect(
                        `${err.message} file: ${err.fileName} line: ${
                            err.lineNumber
                        }`
                    ).toEqual(null);
                });
        });

        it("should return a succes if no params given", () => {
            expect.assertions(1);

            return testFred
                .get("releases", {})
                .then(res => {
                    expect(res.releases.length).toBeGreaterThan(0);
                })
                .catch(err => {
                    expect(
                        `${err.message} file: ${err.fileName} line: ${
                            err.lineNumber
                        }`
                    ).toEqual(null);
                });
        });

        it("should successfully handle xml requests", () => {
            expect.assertions(1);

            return testFred
                .get("releases", { file_type: "xml" })
                .then(res => {
                    expect(res.match(/releases/).length).toBeGreaterThan(0);
                })
                .catch(err => {
                    expect(
                        `${err.message} file: ${err.fileName} line: ${
                            err.lineNumber
                        }`
                    ).toEqual(null);
                });
        });

        it("should return an error if an xml request errors", () => {
            expect.assertions(1);

            return testFred
                .get("nonexistent", { file_type: "xml" })
                .then(res => {
                    expect(res).toBe(null);
                })
                .catch(err => {
                    expect(err.message).toBe(
                        "Internal Server Error (Status: 500)"
                    );
                });
        });
    });

    describe("Categories", () => {
        describe("getCategory", () => {
            it("should return a category", () => {
                expect.assertions(1);

                return testFred
                    .getCategory({ category_id: 125 })
                    .then(res => {
                        expect(res.categories.length).toEqual(1);
                    })
                    .catch(err => {
                        expect(err).toEqual(null);
                    });
            });

            it("should error if not given an integer for category_id", () => {
                expect.assertions(1);

                return testFred
                    .getCategory({ category_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable category_id is not 0 or a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        describe("getCategoryChildren", () => {
            it("should return a category's children", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryChildren({ category_id: 13 })
                    .then(res => {
                        expect(res.categories.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        expect(err).toEqual(null);
                    });
            });

            it("should error if not given an integer for category_id", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryChildren({ category_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable category_id is not 0 or a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        describe("getCategoryRelated", () => {
            it("should return a category's related categories", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryRelated({ category_id: 32073 })
                    .then(res => {
                        expect(res.categories.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        expect(err).toEqual(null);
                    });
            });

            it("should error if not given an integer for category_id", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryRelated({ category_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable category_id is not 0 or a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        describe("getCategorySeries", () => {
            it("should return the series in a category", () => {
                expect.assertions(1);

                return testFred
                    .getCategorySeries({ category_id: 125 })
                    .then(res => {
                        expect(res.seriess.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            it("should error if not given an integer for category_id", () => {
                expect.assertions(1);

                return testFred
                    .getCategorySeries({ category_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable category_id is not 0 or a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        describe("getCategoryTags", () => {
            it("should return the tags for a category", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryTags({ category_id: 125 })
                    .then(res => {
                        expect(res.tags.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            it("should error if not given an integer for category_id", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryTags({ category_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable category_id is not 0 or a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        describe("getCategoryRelatedTags", () => {
            it("should return the related tags for a category", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryRelatedTags({
                        category_id: 125,
                        tag_names: "services;quarterly",
                    })
                    .then(res => {
                        expect(res.tags.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            it("should error if not given an integer for category_id", () => {
                expect.assertions(1);

                return testFred
                    .getCategoryRelatedTags({ category_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  The variable tag_names must be set. (Status: 400)"
                        );
                    });
            });
        });
    });

    describe("Releases", () => {
        describe("getReleases", () => {
            it("should return all releases", () => {
                expect.assertions(1);

                return testFred
                    .getReleases({})
                    .then(res => {
                        expect(res.releases.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            it("should error if given an invalid parameter", () => {
                expect.assertions(1);

                return testFred
                    .getReleases({ limit: -1000 })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable limit is not between 1 and 1000. (Status: 400)"
                        );
                    });
            });
        });

        describe("getReleasesDates", () => {
            it("should return all release dates", () => {
                expect.assertions(1);

                return testFred
                    .getReleasesDates({})
                    .then(res => {
                        expect(res.release_dates.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            it("should error if given an invalid parameter", () => {
                expect.assertions(1);

                return testFred
                    .getReleasesDates({ limit: -1000 })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable limit is not between 1 and 1000. (Status: 400)"
                        );
                    });
            });
        });

        describe("getRelease", () => {
            it("should return a release", () => {
                expect.assertions(1);
                return testFred
                    .getRelease({ release_id: 53 })
                    .then(res => {
                        expect(res.releases.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            expect.assertions(1);

            it("should error if given an invalid parameter", () => {
                expect.assertions(1);
                return testFred
                    .getRelease({ release_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable release_id is not a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        describe("getReleaseDates", () => {
            it("should return release dates for a release", () => {
                expect.assertions(1);
                return testFred
                    .getReleaseDates({ release_id: 82 })
                    .then(res => {
                        expect(res.release_dates.length).toBeGreaterThan(0);
                    })
                    .catch(err => {
                        console.error(err);
                        expect(err).toEqual(null);
                    });
            });

            it("should error if given an invalid parameter", () => {
                expect.assertions(1);
                return testFred
                    .getReleaseDates({ release_id: "cat" })
                    .then(res => {
                        expect(res).toEqual(null);
                    })
                    .catch(err => {
                        expect(err.message).toEqual(
                            "Bad Request.  Variable release_id is not a positive integer. (Status: 400)"
                        );
                    });
            });
        });

        // describe("getReleaseSeries", () =>  {
        //     it("should return the release series for a release", () =>  {
        //         testFred.getReleaseSeries({ release_id: 51 }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.seriess.length).toBeGreaterThan(0);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getReleaseSeries({ release_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getReleaseSources", () =>  {
        //     it("should return the sources for a release", () =>  {
        //         testFred.getReleaseSources({ release_id: 51 }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.sources.length).toBeGreaterThan(0);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getReleaseSources({ release_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getReleaseTags", () =>  {
        //     it("should return the sources for a release", () =>  {
        //         testFred.getReleaseTags({ release_id: 86 }, function(err, res) {
        //             expect(err).toEqual(null);
        //             expect(res.tags.length).toBeGreaterThan(0);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getReleaseTags({ release_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getReleaseRelatedTags", () =>  {
        //     it("should return the sources for a release", () =>  {
        //         testFred.getReleaseRelatedTags(
        //             { release_id: 86, tag_names: "sa;foreign" },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.tags.length).toBeGreaterThan(0);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getReleaseRelatedTags({ release_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeries", () =>  {
        //     it("should return a series", () =>  {
        //         testFred.getSeries({ series_id: "GNPCA" }, function(err, res) {
        //             expect(err).toEqual(null);
        //             expect(res.seriess.length).toEqual(1);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeries({ series_id: "cat" }, function(err, res) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesCategories", () =>  {
        //     it("should return categories for a series", () =>  {
        //         testFred.getSeriesCategories({ series_id: "EXJPUS" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.categories.length).toBeGreaterThan(0);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesCategories({ series_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesObservations", () =>  {
        //     it("should return observations for a series", () =>  {
        //         testFred.getSeriesObservations(
        //             { series_id: "EXJPUS" },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.observations.length).toBeGreaterThan(0);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesObservations({ series_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesRelease", () =>  {
        //     it("should return a release for a series", () =>  {
        //         testFred.getSeriesRelease({ series_id: "IRA" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.releases.length).toEqual(1);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesRelease({ series_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesSearch", () =>  {
        //     it("should return series that relate to a search query", () =>  {
        //         testFred.getSeriesSearch(
        //             { search_text: "monetary,service,index" },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.seriess.length).toEqual(32);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesSearch({ series_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesSearchTags", () =>  {
        //     it("should return tags that relate to a search query", () =>  {
        //         testFred.getSeriesSearchTags(
        //             { series_search_text: "monetary,service,index" },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.tags.length).toEqual(46);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesSearchTags({ series_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesSearchRelatedTags", () =>  {
        //     it("should return related tags that relate to a search query", () =>  {
        //         testFred.getSeriesSearchRelatedTags(
        //             {
        //                 series_search_text: "mortgage+rate",
        //                 tag_names: "30-year;frb",
        //             },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.tags.length).toEqual(12);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesSearchRelatedTags(
        //             { series_id: "cat" },
        //             function(err, res) {
        //                 expect(err.status).toEqual(400);
        //                 expect(err.message).toMatch(/Bad Request/);

        //                 done();
        //             }
        //         );
        //     });
        // });

        // describe("getSeriesTags", () =>  {
        //     it("should return tags for a series", () =>  {
        //         testFred.getSeriesTags({ series_id: "STLFSI" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.tags.length).toEqual(9);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesTags({ series_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesUpdates", () =>  {
        //     it("should return series in order of recency of updates", () =>  {
        //         testFred.getSeriesUpdates({}, function(err, res) {
        //             expect(err).toEqual(null);
        //             expect(res.seriess.length).toBeGreaterThan(1);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesUpdates({ limit: -1000 }, function(err, res) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSeriesVintageDates", () =>  {
        //     it("should return the dates when changes were made to a series", () =>  {
        //         testFred.getSeriesVintageDates({ series_id: "GNPCA" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.vintage_dates.length).toBeGreaterThan(1);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSeriesVintageDates({ limit: -1000 }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSources", () =>  {
        //     it("should return all sources of economic data", () =>  {
        //         testFred.getSources({ limit: 20 }, function(err, res) {
        //             expect(err).toEqual(null);
        //             expect(res.sources.length).toEqual(20);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSources({ limit: -1000 }, function(err, res) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSource", () =>  {
        //     it("should return a source of economic data", () =>  {
        //         testFred.getSource({ source_id: 1 }, function(err, res) {
        //             expect(err).toEqual(null);
        //             expect(res.sources.length).toEqual(1);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSource({ source_id: "cat" }, function(err, res) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getSourceReleases", () =>  {
        //     it("should return the releases for a source", () =>  {
        //         testFred.getSourceReleases({ source_id: 1 }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err).toEqual(null);
        //             expect(res.releases.length).toBeGreaterThan(10);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getSourceReleases({ source_id: "cat" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getTags", () =>  {
        //     it("should return FRED tags", () =>  {
        //         testFred.getTags({ limit: 20 }, function(err, res) {
        //             expect(err).toEqual(null);
        //             expect(res.tags.length).toEqual(20);

        //             done();
        //         });
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getTags({ limit: -20 }, function(err, res) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getRelatedTags", () =>  {
        //     it("should return related tags for a tag", () =>  {
        //         testFred.getRelatedTags(
        //             { tag_names: "monetary+aggregates" },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.tags.length).toBeGreaterThan(1);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getRelatedTags({ tag_names: "fwfrf" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });

        // describe("getTagsSeries", () =>  {
        //     it("should return the series matching tag names", () =>  {
        //         testFred.getTagsSeries(
        //             { tag_names: "slovenia;food;oecd" },
        //             function(err, res) {
        //                 expect(err).toEqual(null);
        //                 expect(res.seriess.length).toBeGreaterThan(0);

        //                 done();
        //             }
        //         );
        //     });

        //     it("should error if given an invalid parameter", () =>  {
        //         testFred.getTagsSeries({ tag_names: "fwfrf" }, function(
        //             err,
        //             res
        //         ) {
        //             expect(err.status).toEqual(400);
        //             expect(err.message).toMatch(/Bad Request/);

        //             done();
        //         });
        //     });
        // });
    });
});
